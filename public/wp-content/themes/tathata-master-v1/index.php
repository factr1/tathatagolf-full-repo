<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 */

get_header();
setPageBanner( 'Tathata Posts' );
?>

    <div id="blackbar"></div>

<?php setBreadcrumbs( 'Blog' ); ?>

    <section class="index-page pageInnerContentWrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9">
                    <?php
                    if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
                        get_template_part( 'parts/featured-content' );
                    }
                    ?>

                    <div class="index-loop">
                        <?php
                        if ( have_posts() ) : while ( have_posts() ) : the_post();
                            get_template_part( 'parts/content', get_post_format() );
                        endwhile;
                        else :
                            get_template_part( 'parts/content', 'none' );
                        endif;
                        ?>
                    </div>

                    <?php twentyfourteen_paging_nav(); ?>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <?php
                        get_sidebar();
                        get_sidebar( 'content' );
                    ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
