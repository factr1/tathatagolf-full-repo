<?php
get_header();
get_template_part( 'parts/page-title' );
// TODO: check the markup, seems off where the closing endif is
?>

<?php if ( have_posts() ) : ?>

    <div class="container">
        <div class="row">
            <div class="clearfix"></div>

            <div class="breadcrumbs-wrapper">
                <ol class="breadcrumb pull-left">
                    <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                    <li><a href="<?php echo esc_url( home_url( '/60-day-program/' ) ); ?>">60 Day Program</a></li>

                    <?php if ( ! empty( $post->post_parent ) ) {
                        $parentTitle = get_the_title( $post->post_parent ); ?>
                        <li><a href="<?php echo get_permalink( $post->post_parent ); ?>" title="<?php echo $parentTitle; ?>"><?php echo $parentTitle; ?></a></li>
                    <?php } ?>

                    <li><?php the_title(); ?></li>
                </ol>

                <?php echo do_shortcode( '[followUs]' ); ?>
            </div>
        </div>
    </div>

    <section class="contentWrapper">
        <article class="pageInnerContentWrap ourFacilitiesPage">
            <div class="container">
                <div class="row onlineTopwrap broken-apart-page-row">
                    <div class="row-inner">
                        <article class="col-md-12 col-sm-12 col-xs-12">
                            <div id="form-main">
                                <div id="form-div">
                                    <?php
                                    wp_nav_menu(array(
                                        'menu' => 'video menu',
                                        'container_id' => 'owncssmenu'
                                    ));
                                    ?>
                                </div>
                            </div>

                            <div>
                                <h1 class="broken-apart-title"> <?php echo get_the_title(); ?></h1>
                            </div>

                            <div class="broken-videos-wrap">
                                <?php
                                while ( have_posts() ) : the_post();
                                    get_template_part( 'parts/content', get_post_format() );
                                endwhile;
                                twentyfourteen_paging_nav();
                                else :
                                    get_template_part( 'parts/content', 'none' );
                                endif;
                                ?>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </article>
    </section>

<?php
get_footer();
