<?php
// Identify the user
$groups_user = new Groups_User( get_current_user_id() );
// Get the group objects they belong to
$user_groups = $groups_user->groups;
// Get the IDs of the groups they directly belong to
$user_group_ids = $groups_user->group_ids;

if ( in_array( 'sensei', get_body_class() ) ) :
    // Kick them out if they belong to the expired trial group
    if ( in_array( 9, $user_group_ids) ) :
        header( 'Location: http://www.tathatagolf.com/7-day-trial-expired/' );
        exit;
    endif;
endif;
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <link rel="profile" href="//gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <meta name="theme-color" content="#000">
    <meta name="msapplication-TileColor" content="#000">
    <meta name="msapplication-TileImage" content="/favicons/ms-icon-144x144.png">

    <!-- Favicon -->
    <link rel="manifest" href="/favicons/manifest.json">
    <link rel="apple-touch-icon" href="apple-touch-icon-iphone.png" />

    <!-- Apple Icon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo esc_url(home_url('/wp-content/uploads/2017/02/57x57_tathata_ios.png')); ?>" />
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url(home_url('/wp-content/uploads/2017/02/72x72_tathata_ios.png')); ?>" />
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url(home_url('/wp-content/uploads/2017/02/114x114_tathata_ios.png')); ?>" />
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo esc_url(home_url('/wp-content/uploads/2017/02/114x114_tathata_ios.png')); ?>" />

    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php // Facebook Pixel ?>
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) {
                return;
            }
            n = f.fbq = function() {
                n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (! f._fbq) {
                f._fbq = n;
            }
            n.push = n;
            n.loaded = ! 0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = ! 0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '851523454968993');
        fbq('track', "PageView");
    </script>
    <noscript>
        <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=851523454968993&ev=PageView&noscript=1" />
    </noscript>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> role="document">
    <?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) {
        gtm4wp_the_gtm_tag();
    } ?>

    <!-- Header -->
    <header id="primary-header" role="banner">
        <section class="header-top">
            <div class="container">
                <div class="row">

                    <!-- Logo -->
                    <div class="col-xs-12 col-md-4">
                        <h1 class="header-logo text-hide">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>">Tathata Golf</a>
                        </h1>
                    </div>

                    <!-- Login/Logout -->
                    <div class="col-xs-12 col-md-8">
                        <div class="user-status">
                            <?php if ( is_user_logged_in() ) :
                                $current_user = wp_get_current_user();
                                echo '<a href="' . esc_url( home_url() ) . '/dashboard/" class="user-welcome"><span>Welcome, </span>' . $current_user->user_login . '</a>';
                                ?>
                                <a href="<?php echo wp_logout_url(); ?>" class="user-logout">Logout</a>
                            <?php else : ?>
                                <a href="<?php echo esc_url( home_url( '/login/' ) ); ?>">Login</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="header-bottom">

            <!-- Primary Navigation -->
            <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
                <div class="container">
                    <span class="menu-text">Menu</span>

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-nav" aria-expanded="false">
                        <span class="sr-only">View Menu</span>

                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    </button>

                    <!-- Mobile Menu -->
                    <div id="mobile-nav" class="collapse navbar-collapse">
                        <?php
                        $user_id = get_current_user_id();
                        list($user_id, $tathusermenu, $oavisibility, $learnervisibility, $cmsvisibility, $advisibility, $gradvisibility, $user_group, $group_array) = tath_view_role_params( $user_id );
                        wp_nav_menu( array('menu' => $tathusermenu) );
                        ?>
                    </div>

                    <!-- Desktop Menu -->
                    <div id="desktop-nav" class="collapse navbar-collapse">
                        <?php
                        $user_id = get_current_user_id();
                        list($user_id, $tathusermenu, $oavisibility, $learnervisibility, $cmsvisibility, $advisibility, $gradvisibility, $user_group, $group_array) = tath_view_role_params( $user_id );
                        wp_nav_menu( array('menu' => $tathusermenu) );
                        ?>
                    </div>
                </div>
            </nav>
        </section>
    </header>

    <!-- Main Content -->
    <main id="primary-site-content" class="site-container" role="main">
