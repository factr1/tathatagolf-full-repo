<?php
/**
 * The template for displaying the footer
 */
?>

</main><!-- /.contentWrapper -->

<!-- Footer -->
<footer id="primary-footer" class="footer" role="contentinfo">
    <?php get_template_part( 'parts/affiliations' ); ?>

    <section class="tg-footer-top">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-md-4">
                    <h3 class="footer-title">Quick links</h3>
                    <?php wp_nav_menu( array( 'menu' => 'footer-menu' ) ); ?>
                </div>

                <div class="col-xs-12 col-md-4 -bordered">
                    <h3 class="footer-title">Get Involved with Tathata</h3>
                    <p>Learn more about our affiliate program, corporate rewards, and bringing Tathata to your facility.</p>
                    <a class="tg-btn" href="<?php echo esc_url( home_url( '/welcome-affiliates' ) ); ?>">Find out more</a>
                </div>

                <div class="col-xs-12 col-md-4">
                    <h3 class="footer-title">Grow the Game</h3>

                    <p>Learn how you can help Tathata "Grow The Game Through Better Golf"</p>
                    <a class="tg-btn -donate" href="<?php echo esc_url( home_url( '/tathata-golf-foundation/' ) ); ?>">Donate Now</a>

                    <div class="tg-social-shares">
                        <h4 class="footer-title">Where else to find us:</h4>

                        <a href="<?php echo esc_url( 'https://www.facebook.com/pages/Tathata-Golf/526700354108550' ); ?>" title="Facebook" target="_blank" rel="noopener noreferrer">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="<?php echo esc_url( 'https://twitter.com/TathataGolf' ); ?>" title="Twitter" target="_blank" rel="noopener noreferrer">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a href="<?php echo esc_url( 'https://www.linkedin.com/company/tathata-golf' ); ?>" title="LinkedIN" target="_blank" rel="noopener noreferrer">
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                        </a>
                        <a href="<?php echo esc_url( 'https://i.instagram.com/tathatagolf' ); ?>" title="Instagram" target="_blank" rel="noopener noreferrer">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a href="<?php echo esc_url( 'https://www.youtube.com/channel/UCG1AlyrH_eqgQ7cekyYc3Ig' ); ?>" title="YouTube" target="_blank" rel="noopener noreferrer">
                            <i class="fa fa-youtube" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- Copyright and Legal info -->
    <section class="tg-footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-9">
                    <div class="footer-logo"></div>
                    <p class="copyright">&copy; <?php echo date( "Y" ) ?> Tathata, LLC. &copy; 2012-15 Hepler Golf, LLC. All Rights Reserved.</p>
                    <span class="legal-info">
                        <a href="<?php echo esc_url( home_url( '/privacy-policy/' ) ); ?>">Privacy Policy</a> /
                        <a href="<?php echo esc_url( home_url( '/terms/' ) ); ?>">Terms of Use</a> /
                        <a href="<?php echo esc_url( home_url( '/returns-and-refunds/' ) ); ?>">Return Policy</a>
                    </span>
                    <p class="image-permission">No imagery or logos contained within this site may be used without the express permission of Hepler Golf, LLC.</p>
                </div>

                <div class="col-xs-12 col-md-3">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/cash-icons.png" alt="">
                </div>
            </div>
        </div>
    </section>
</footer>

<?php // SharpSpring Tracking ?>
<script type="text/javascript">
    var _ss = _ss || [];
    _ss.push(['_setDomain', 'https://koi-3QDIROJGW8.marketingautomation.services/net']);
    _ss.push(['_setAccount', 'KOI-3QHHUSNN0O']);
    _ss.push(['_trackPageView']);
    (function() {
        var ss = document.createElement('script');
        ss.type = 'text/javascript';
        ss.async = true;

        ss.src = (
                'https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QDIROJGW8.marketingautomation.services/client/ss.js?ver=1.1.1';
        var scr = document.getElementsByTagName('script')[0];
        scr.parentNode.insertBefore(ss, scr);
    })();
</script>

<?php // Google Code for Remarketing Tag ?>
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 926426886;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/926426886/?guid=ON&amp;script=0" />
    </div>
</noscript>

<?php // Twitter universal website tag code ?>
<script>
    ! function(e, t, n, s, u, a) {
        e.twq || (s = e.twq = function() {
            s.exe ? s.exe.apply(s, arguments) : s.queue.push(arguments);
        }, s.version = '1.1', s.queue = [], u = t.createElement(n), u.async = ! 0, u.src = '//static.ads-twitter.com/uwt.js', a = t.getElementsByTagName(n)[0], a.parentNode.insertBefore(u, a))
    }(window, document, 'script');
    // Insert Twitter Pixel ID and Standard Event data below
    twq('init', 'nvnqy');
    twq('track', 'PageView');
</script>

<?php wp_footer(); ?>
</body>
</html>
