<?php
/**
 * The Template for displaying all single posts
 */

get_header();
get_template_part( 'parts/page-title' );
?>

    <div id="blackbar"></div>

<?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section class="single-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9">
                    <?php
                    if (have_posts()) : while ( have_posts() ) : the_post();

                        get_template_part( 'parts/content', get_post_format() );
                        the_tags( '<footer class="entry-meta tags"><h2 class="tagsDetail">TAGS:<span class="tag-links">', '', '</span></h2></footer>' );
                        twentyfourteen_post_nav();

                        if ( comments_open() || get_comments_number() ) {
                            comments_template();
                        }

                    endwhile; endif;
                    ?>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <?php
                        get_sidebar();
                        get_sidebar('content');
                    ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
