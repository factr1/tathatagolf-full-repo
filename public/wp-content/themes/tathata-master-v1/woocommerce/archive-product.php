<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

get_header();
?>

    <section id="page-hero" class="hero -shop">
        <div class="container">
            <div class="flex">
                <div class="hero-content">
                    <h1 class="page-title"><span>Gear</span> Up</h1>
                    <p>Explore Tathata's collection of gear and accessories.</p>
                </div>
            </div>
        </div>
    </section>

    <div class="captionWrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7">
                    <div class="cap_left">
                        <p><i>"Tathata has given me a <span class="red">new way to look at how I play and teach the game</span> and has excited me to do more than I've ever done. <span class="white">My game has changed. I have more power, more presence and more enthusiasm...</span> I get up in the morning and I want to go do it. That hasn't happened for more than a decade."</i></p>
                        <p align="right">
                            <small><span class="white">- Karen Davies</span>, 2010 LPGA T&amp;CP National Champion</small>
                        </p>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-5">
                    <?php get_template_part('inc/60day_redbox'); ?>
                </div>
            </div>
        </div>
    </div>

    <?php setBreadcrumbs( 'Shop' ); ?>

    <div class="container">
        <?php if ( is_user_logged_in() ) { ?>
            <div class="row">
                <div class="memberPromo">
                    <div class="col-sm-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/memberdvds.png" class="img-responsive" alt="DVD Bundle">
                    </div>

                    <div class="col-sm-7 promocontent">
                        <h3>30-Disc DVD Set Available</h3>
                        <p>As a purchaser of 60-Day Training Program, you are eligible to purchase the 30-Disc hardback of DVDs. These DVDs are great to have for offline training. Please note, the DVDs contain only the 60 lessons of the program, no chapter support or daily extras.</p>
                    </div>

                    <div class="col-sm-3 memberbuy">
                        <p class="price">$89.95</p>
                        <a href="<?php echo esc_url( home_url( '/?add_to_cart=42489' ) ); ?>" class="buynowbutton">Add to Cart</a>
                    </div>

                    <br class="clear">
                </div>
            </div>
        <?php } ?>

        <div class="row">
            <div class="col-xs-12">
                <section class="pageInnerContentWrap profilePage">
                    <div id="storegrid">
                        <?php
                        /**
                         * woocommerce_before_shop_loop hook
                         * @hooked woocommerce_result_count - 20
                         * @hooked woocommerce_catalog_ordering - 30
                         */
                        do_action( 'woocommerce_before_shop_loop' );
                        ?>

                        <div class="row">
                            <div class="col-xs-12 col-sm-9" style="margin-top: 20px;">
                                <?php if (have_posts()) : ?>
                                    <?php woocommerce_product_loop_start(); ?>

                                    <?php woocommerce_product_subcategories(); ?>

                                    <?php while (have_posts()) : the_post(); ?>
                                        <?php wc_get_template_part('content', 'product'); ?>
                                    <?php endwhile; // end of the loop. ?>

                                    <?php woocommerce_product_loop_end(); ?>

                                    <?php
                                    /**
                                     * woocommerce_after_shop_loop hook
                                     * @hooked woocommerce_pagination - 10
                                     */
                                    do_action('woocommerce_after_shop_loop');
                                    ?>

                                <?php elseif (!woocommerce_product_subcategories(array(
                                        'before' => woocommerce_product_loop_start(false),
                                        'after'  => woocommerce_product_loop_end(false)
                                    ))
                                ) : ?>

                                    <?php wc_get_template('loop/no-products-found.php'); ?>
                                <?php endif; ?>
                            </div><!-- END gear-part-left -->

                            <div class="col-xs-12 col-sm-3">
                                <?php
                                  /**
                                   * woocommerce_sidebar hook
                                   * @hooked woocommerce_get_sidebar - 10
                                   */
                                  do_action('woocommerce_sidebar');
                                ?>

                                <?php
                                  /**
                                   * woocommerce_after_main_content hook
                                   * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                                   */
                                  do_action('woocommerce_after_main_content');
                                ?>
                            </div><!-- End rightPanel t_right div -->
                        </div><!-- End row div -->
                    </div><!-- End #storegrid -->
                </section><!-- END pageInnerContentWrap -->
            </div>
        </div>
    </div>

<?php
get_footer();
