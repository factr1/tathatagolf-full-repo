<?php
/**
 * Email Footer
 * @author      WooThemes
 * @package     WooCommerce/Templates/Emails
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Load colours
$base = get_option( 'woocommerce_email_base_color' );

$base_lighter_40 = wc_hex_lighter( $base, 40 );

// For gmail compatibility, including CSS styles in head/body are stripped out therefore styles need to be inline. These variables contain rules which are added to the template inline.
$template_footer = "
    border-top:0;
  ";

$credit = "
    border:0;
    color: $base_lighter_40;
    font-family: Arial;
    font-size:12px;
    line-height:125%;
    text-align:center;
    background-color: black;
    -webkit-border-radius: 0 !important;
    padding: 35px 15px !important;
  ";

$h3 = "
    color: #fff;
    font-family: 'open sans', helvetica, verdana;
  ";

$h3_a = "
    color: #fff;
    font-weight: 700;
    font-family: 'open sans', helvetica, verdana;
    text-decoration: none;
    font-size: 24px;
  ";

$second_footer_main_div = "
    padding: 5px 15px;
    background-color: #b40001;
    background-color: #900;
    color: white;
    overflow: hidden;
  ";

$width_wrapper = "
    max-width: 600px;
    margin: 0 auto;
    overflow: hidden;
  ";

$wrapper_div = "
    width: 50%;
    float: left;
    box-sizing: border-box;
    padding-right: 15px;
    font-family: 'open sans', helvetivca, verdana;
    font-size: 12px;
  ";

$logo_div = "
    margin: 0 auto;
    width: 300px;
    text-align: center;
  ";

$ul = "
    list-style-type: none;
    padding: 0;
    font-family: 'open sans', helvetica, verdana;
    font-size: 14px;
  ";

$li = "
    text-align: left;
    font-weight: 600;
    letter-spacing: normal;
  ";

$li_a = "
    text-decoration: none;
    text-align: left;
    font-weight: 600;
    letter-spacing: normal;
    color: #fff;
  ";

$subscribe = "
    float: right;
    color: #fff;
    margin-top: 10px;
    text-decoration: none;
    font-weight: 600;
    font-family: 'open sans', helvetica, verdana;
  ";
?>

                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table><!-- End Content -->
                                    </td>
                                </tr>
                            </table><!-- End Body -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <!-- Footer -->
                            <table border="0" cellpadding="10" cellspacing="0" width="100%" id="template_footer" style="<?php echo $template_footer; ?>">
                                <tr id="email_footer_row">
                                    <td style="padding:0;">
                                        <div style="<?php echo $second_footer_main_div; ?>">
                                            <div style="<?php echo $width_wrapper; ?>">
                                                <div style="<?php echo $wrapper_div; ?>">
                                                    <ul style="<?php echo $ul; ?>">
                                                        <li style="<?php echo $li; ?>">Tathata Golf</li>
                                                        <li style="<?php echo $li; ?>">7505 E McCormick Pkwy</li>
                                                        <li style="<?php echo $li; ?>">Scottsdale, AZ 85258</li>
                                                    </ul>
                                                </div>

                                                <div style="<?php echo $wrapper_div; ?>">
                                                    <ul style="<?php echo $ul; ?>">
                                                        <li style="<?php echo $li; ?>"><a style="<?php echo $li_a; ?>" href="<?php echo esc_url( 'https://www.facebook.com/TathataGolf' ); ?>">Facebook</a></li>
                                                        <li style="<?php echo $li; ?>"><a style="<?php echo $li_a; ?>" href="<?php echo esc_url( 'https://twitter.com/TathataGolf' ); ?>">Twitter</a></li>
                                                        <li style="<?php echo $li; ?>"><a style="<?php echo $li_a; ?>" href="<?php echo esc_url( 'https://i.instagram.com/tathatagolf' ); ?>">Instagram</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                            <tr>
                                                <td colspan="2" valign="middle" id="credit" style="<?php echo $credit; ?>">
                                                    <div class="width-wrapper" style="<?php echo $width_wrapper; ?>">
                                                        <!-- <?php echo wpautop( wp_kses_post( wptexturize( apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) ) ) ) ); ?>  -->

                                                        <div style="<?php echo $logo_div; ?>">
                                                            <a href="<?php echo esc_url( 'http://www.tathatagolf.com/' ); ?>">
                                                                <img src="<?php echo esc_url( 'http://www.tathatagolf.com/wp-content/uploads/sites/5/2015/05/logo_footer.png' ); ?>" alt="tathataGolfLogo" title="Tathata Golf Logo">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table><!-- End Footer -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

</body>
</html>
