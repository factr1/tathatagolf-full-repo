<?php
/**
 * The Template for displaying all single products.
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

get_header();
?>

<section id="page-hero" class="hero -shop">
    <div class="container">
        <div class="flex">
            <div class="hero-content">
                <h1 class="page-title"><span>Gear</span> Up</h1>
                <p>Explore Tathata's collection of gear and accessories.</p>
            </div>
        </div>
    </div>
</section>

<div>
    <article class="captionWrap single-product-wrap"></article>

    <section id="breadcrumbs">
        <div class="hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <div class="flex -flex-start">
                            <ul class="breadcrumb">
                                <?php do_action( 'woocommerce_before_main_content' ); ?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="flex -flex-end">
                            <?php echo do_shortcode( '[followUs]' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="visible-xs">
            <div class="container">
                <div class="flex">
                    <div>
                        <ul class="breadcrumb">
                            <?php do_action( 'woocommerce_before_main_content' ); ?>
                        </ul>
                    </div>

                    <div>
                        <?php echo do_shortcode( '[followUs]' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php while ( have_posts() ) : the_post(); ?>

        <?php wc_get_template_part( 'content', 'single-product' ); ?>

    <?php endwhile; // end of the loop. ?>

    <?php
    /**
     * woocommerce_after_main_content hook
     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
     */
    do_action( 'woocommerce_after_main_content' );

    /**
     * woocommerce_sidebar hook
     * @hooked woocommerce_get_sidebar - 10
     */
    //do_action( 'woocommerce_sidebar' );
    ?>
</div>

<?php get_footer( 'shop' ); ?>

<script>
    'use strict';

    jQuery(function($) {
        $('.woocommerce-review-link').click(function() {
            $('#collapsereviews').removeClass('collapse');
            $('#collapsereviews').css('height', 'auto');
            $('#collapsereviews').slideDown(500);
            $('a[href="#collapsereviews"]').removeClass('collapsed');
            event.preventDefault();
        });

        $('a[href="#collapsereviews"]').click(function() {
            $(this).addClass('collapsed');
        });
    });
</script>
