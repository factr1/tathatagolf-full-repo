<?php
/**
 * Template Name: Template with no sidebar
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */

get_header();
?>

    <div>
        <?php get_template_part( 'parts/page-title' ); ?>

        <div id="blackbar"></div>

        <?php if ( ( is_bbpress() ) && ( ! bbp_is_single_user() ) ) { ?>
            <div class="headerwidgets">
                <div class="forehead-wrapper">
                    <?php dynamic_sidebar( 'sidebar-forehead' ); ?>
                </div>

                <div class="clear"></div>
            </div>
        <?php } ?>

        <section class="pageInnerContentWrap">
            <?php get_template_part( 'parts/breadcrumbs' ); ?>

            <div class="container">
                <div class="row">

                    <article class="col-xs-12">
                        <?php
                        if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
                            // Include the featured content template.
                            get_template_part( 'parts/featured-content' );
                        }
                        ?>
                        <div id="primary" class="content-area">
                            <div id="content" class="site-content" role="main">
                                <?php
                                // Start the Loop.
                                while ( have_posts() ) : the_post();

                                    // Include the page content template.
                                    get_template_part( 'parts/content', 'page' );

                                    // If comments are open or we have at least one comment, load up the comment template.
                                    if ( comments_open() || get_comments_number() ) {
                                        comments_template();
                                    }
                                endwhile;
                                ?>
                            </div><!-- #content -->
                        </div><!-- #primary -->
                    </article>
                </div>
            </div><!-- #main-content -->
        </section>
    </div>

<?php if ( is_page( 171 ) ) : ?>
    <script>fbq('track', 'AddToCart');</script>
<?php elseif ( is_page( 172 ) ) : ?>
    <script>fbq('track', 'Purchase', {value: '1.00', currency: 'USD'});</script>
<?php endif; ?>

<?php
get_footer();
