<?php
/**
 * The Template for displaying all single posts
 */

get_header();
get_template_part( 'parts/page-title' );
?>

    <section class="pageInnerContentWrap">
        <div class="container">
            <div class="row">

                <ol class="breadcrumb pull-left">
                    <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                    <li><a href="<?php echo esc_url( home_url( '/blog' ) ); ?>">Blog</a></li>
                </ol>

                <ul class="shareUs pull-right">
                    <li>SHARE US</li>
                    <?php
                    if ( function_exists( 'ssb_share_icons' ) ) {
                        echo ssb_share_icons();
                    } else {
                        echo "s";
                    }
                    ?>
                </ul>

                <div class="clearfix"></div>

                <article class="col-xs-12 col-sm-8">
                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">
                            <?php
                            while ( have_posts() ) : the_post();
                                /*
                                * Include the post format-specific template for the content. If you want to
                                * use this in a child theme, then include a file called called content-___.php
                                * (where ___ is the post format) and that will be used instead.
                                */
                                get_template_part( 'parts/content', get_post_format() );

                                // Previous/next post navigation.
                                twentyfourteen_post_nav();

                                // If comments are open or we have at least one comment, load up the comment template.
                                if ( comments_open() || get_comments_number() ) {
                                    comments_template();
                                }
                            endwhile;
                            ?>
                        </div>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-4 rightPanel">
                    <?php get_sidebar(); ?>
                </article>

            </div>
        </div>
    </section>

<?php
get_footer();
