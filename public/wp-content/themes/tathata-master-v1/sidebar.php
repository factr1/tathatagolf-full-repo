<?php
/**
 * The Sidebar containing the main widget area
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */
?>

<aside id="secondary">
    <?php
    $description = get_bloginfo( 'description', 'display' );
    if ( ! empty ( $description ) ) : ?>
        <h2 class="site-description"><?php echo esc_html( $description ); ?></h2>
    <?php endif; ?>

    <?php if ( has_nav_menu( 'secondary' ) ) : ?>
        <nav role="navigation" class="navigation site-navigation secondary-navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'secondary' ) ); ?>
        </nav>
    <?php endif; ?>

    <?php // Determine what kind of page we're on, and serve up the appropriate sidebar
    if ( bbp_is_single_user_edit() || bbp_is_single_user() ) {
        $istathprofile = 'true';
    }

    if ( is_bbpress() && ! $istathprofile ) { ?>
        <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
            <?php dynamic_sidebar( 'sidebar-forum' ); ?>
        </div>
    <?php } else if ( is_bbpress() && $istathprofile ) { ?>

        <!-- no sidebar -->

    <?php } else { ?>
        <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
            <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                <?php
                    dynamic_sidebar( 'sidebar-1' );
                    dynamic_sidebar( 'sidebar-2' );
                ?>
            </div><!-- #primary-sidebar -->
        <?php endif; ?>
    <?php } ?>
</aside><!-- #secondary -->
