<?php
/**
 * The template for displaying bbpress
 */

get_header();
?>

<?php get_template_part( 'parts/page-title' ); ?>

    <section class="pageInnerContentWrap">

        <section id="breadcrumbs">
            <div class="hidden-xs">
                <div class="container">
                    <div class="row">

                        <div class="col-xs-12 col-sm-8">
                            <div class="flex -flex-start">
                                <ul class="breadcrumb">
                                    <?php bbp_breadcrumb(); ?>
                                </ul>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="flex -flex-end">
                                <?php echo do_shortcode( '[followUs]' ); ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="visible-xs">
                <div class="container">
                    <div class="flex">
                        <div>
                            <ul class="breadcrumb">
                                <?php bbp_breadcrumb(); ?>
                            </ul>
                        </div>

                        <div>
                            <?php echo do_shortcode( '[followUs]' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="container bbpress-container">
            <div class="row bbpress-forum-container">
                <article class="col-sm-9 left-forum">
                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">
                            <?php
                            // Start the Loop.
                            while ( have_posts() ) : the_post();

                                // Include the page content template.
                                get_template_part( 'parts/content', 'page' );

                                // If comments are open or we have at least one comment, load up the comment template.
                                if ( comments_open() || get_comments_number() ) {
                                    comments_template();
                                }
                            endwhile;
                            ?>
                        </div><!-- #content -->
                    </div><!-- #primary -->
                </article>

                <article class="col-sm-3 rightPanel right-forum">
                    <?php
                        get_sidebar();
                        get_sidebar( 'content' );
                    ?>
                </article>
            </div>
        </div><!-- #main-content -->
    </section>

<?php
get_footer();
