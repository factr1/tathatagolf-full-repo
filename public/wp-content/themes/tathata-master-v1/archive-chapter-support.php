<?php
// Redirect the user to the login page if they are not logged in
if ( ! is_user_logged_in() ) {
    wp_redirect( home_url( '/login/' ) );
    exit;
}

get_header();
setPageBanner( 'Chapter Support Archive' );
?>

    <div id="blackbar"></div>

    <section id="tg-video-archive" class="tg-video-archive">
        <div class="container">

            <div class="video-archive-title">
                <h1>Chapter Support Materials</h1>
            </div>

            <div class="video-archive-description" style="text-align: center">
                <p>Here you can review Chapter Support materials for the 60-Day Program</p>
            </div>

            <div class="tg-video-archive-videos">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="archive-video-container">
                            <?php
                            if ( have_posts() ) : while ( have_posts() ) : the_post();
                                get_template_part( 'parts/content', 'video-pdf' );
                                endwhile;
                            else :
                                get_template_part( 'parts/content', 'none' );
                            endif;
                            ?>
                        </div>

                        <?php twentyfourteen_paging_nav(); ?>
                    </div>
                </div>
            </div>

        </div>
    </section>

<?php
get_footer();
