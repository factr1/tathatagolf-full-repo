<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link       http://codex.wordpress.org/Template_Hierarchy
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */

get_header();
setPageBanner( 'Tathata Archives' );
?>

    <div id="blackbar"></div>

    <section id="archives" class="archives">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="archive-loop">
                        <?php
                        if (have_posts()) : while (have_posts()) : the_post();
                            get_template_part('parts/content', get_post_format());
                            endwhile;
                        else :
                            get_template_part('parts/content', 'none');
                        endif;
                        ?>
                    </div>

                    <?php twentyfourteen_paging_nav(); ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
