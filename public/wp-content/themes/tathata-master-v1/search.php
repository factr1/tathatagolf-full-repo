<?php
/**
 * The template for displaying Search Results pages
 */

get_header();
?>

<?php get_template_part( 'parts/page-title' ); ?>

    <section class="pageInnerContentWrap" style="padding-top: 40px;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
                        get_template_part( 'parts/featured-content' );
                    }
                    ?>

                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">
                            <?php if ( have_posts() ) : ?>
                                <header class="page-header" style="background: none;">
                                    <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyfourteen' ), get_search_query() ); ?></h1>
                                </header><!-- .page-header -->

                                <?php
                                while ( have_posts() ) : the_post();
                                    get_template_part( 'parts/content', get_post_format() );
                                endwhile;
                                twentyfourteen_paging_nav();
                            else : ?>
                                <div id="nothing-found" class="page-content" style="padding: 0;">
                                    <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyfourteen' ); ?></p>

                                    <?php get_search_form(); ?>
                                </div>
                            <?php endif; ?>
                        </div><!-- #content -->
                    </div><!-- #primary -->
                </div>
            </div>
        </div><!-- #main-content -->
    </section>

<?php
get_footer();
