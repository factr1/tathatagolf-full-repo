<div id="remodal-30-day-promo" class="remodal" data-remodal-id="30-day-promo" data-remodal-options="hashTracking: false">
    <style scoped>
        img {
            margin: 0 auto;
            float: none !important;
        }

        h1 {
            font-size: 30px;
        }

        .price {
            font-size: 24px;
            padding: 10px 0;
        }

        .remodal-confirm,
        .remodal-cancel {
            min-width: 49%;
        }

        .remodal-confirm {
            background: rgb(57, 151, 52);
        }

        .remodal-confirm:hover {
            color: rgb(255, 255, 255);
        }

        .remodal-cancel {
            background: transparent;
            color: rgb(102, 0, 0);
        }

        .remodal-cancel:hover {
            background: transparent;
        }

        @media screen and (min-width: 992px) {
            .remodal-cancel {
                min-width: 20%;
            }
        }

        .promocontent--btns {
            text-align: left;
        }

        @media screen and (min-width: 768px) {
            h1, p {
                text-align: left;
            }

            h1 {
                margin: 0 0 20px;
            }
        }
    </style>

    <button data-remodal-action="close" class="remodal-close"></button>

    <div class="row">
        <div class="col-sm-4">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/memberdvds.png" class="img-responsive" alt="Tathata Golf 30-Day DVD Bundle">
        </div>

        <div class="col-sm-8 promocontent">
            <h1>30-Disc DVD Set Available!</h1>
            <p>As a purchaser of 60-Day Training Program, you are eligible to purchase the 30-Disc hardback of DVDs. These DVDs are great to have for offline training. Please note, the DVDs contain only the 60 lessons of the program, no chapter support or daily extras.</p>
            <p class="price">$89.95</p>

            <div class="promocontent--btns">
                <a href="<?php echo esc_url( home_url( '/?add_to_cart=42489' ) ); ?>" class="remodal-confirm">Add to Cart</a>
                <button data-remodal-action="cancel" class="remodal-cancel">No Thanks</button>
            </div>
        </div>
    </div>
</div>

<script defer>
    jQuery(document).ready(function($) {
        var inst = $('[data-remodal-id=30-day-promo]').remodal();
        inst.open();

        $(document).on('closed', '#remodal-30-day-promo', function() {
            inst.destroy();
        });
    });
</script>
