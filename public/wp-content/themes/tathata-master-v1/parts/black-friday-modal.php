<div id="black-friday-modal" data-remodal-id="black-friday-modal" data-remodal-options="hashTracking: false">
    <style scoped>
        #black-friday-modal {
            max-width: 1000px;
        }
    </style>
    <button data-remodal-action="close" class="remodal-close"></button>
    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/promos/black-friday.jpg" alt="Get a special promo on the Tathata Golf 60-Day Program.">
</div>

<script>
    jQuery(function($) {
        var startDate, endDate, now, inst, blackFridayLink, blackFridayAddOn, affiliateSale;

        startDate = new Date(2016, 10, 25);
        endDate = new Date(2016, 10, 28);
        now = Date.now();
        inst = $('[data-remodal-id=black-friday-modal]').remodal();
        blackFridayLink = 'http://www.tathatagolf.com/?add_to_cart=8251';
        blackFridayAddOn = 'http://www.tathatagolf.com/?add_to_cart=8579';

        affilateSale = function() {
            $('.woobottom').find('.price').css('display', 'none');
            $('.woobottom').find('> a').attr('href', blackFridayLink);
            $('.dvd-addon').find('> a').attr('href', blackFridayAddOn);
        };

        if (now > startDate && now < endDate) {
            affilateSale();
            inst.open();
        }
    });
</script>
