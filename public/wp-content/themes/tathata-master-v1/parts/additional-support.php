<?php
/**
 * Template part that displays the additional benefits and support provided by Tathata Golf
 */

$ids = [ 161346 ]; // Environment IDs
?>

<?php if ( get_field( '60-day-support_content', setEnvironmentID( $ids ) ) ) : ?>

    <section id="additional-support">
        <div class="container">
            <div class="as-intro">
                <?php the_field( '60-day-support_content', setEnvironmentID( $ids ) ); ?>
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <img src="<?php the_field( '60-day-support_image', setEnvironmentID( $ids ) ); ?>" alt="Become a member of the world's best in home golf training program">
                </div>

                <div class="col-xs-12 col-md-5 col-md-offset-1">
                    <?php if ( have_rows( '60-day-support_featured_cards', setEnvironmentID( $ids ) ) ) : ?>
                        <div class="card-slider">
                            <?php while ( have_rows( '60-day-support_featured_cards', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                                <div class="card">
                                    <div class="card-image">
                                        <img class="img-responsive -full-width" src="<?php the_sub_field( '60-day-support_fc_image', setEnvironmentID( $ids ) ); ?>">
                                    </div>

                                    <div class="card-title">
                                        <h4><?php the_sub_field( '60-day-support_fc_title', setEnvironmentID( $ids ) ); ?></h4>
                                    </div>

                                    <div class="card-content">
                                        <?php the_sub_field( '60-day-support_fc_content', setEnvironmentID( $ids ) ); ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>
