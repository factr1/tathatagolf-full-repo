<div id="cyber-monday-modal" data-remodal-id="cyber-monday-modal" data-remodal-options="hashTracking: false">
    <style scoped>
        #cyber-monday-modal {
            max-width: 1000px;
        }
    </style>
    <button data-remodal-action="close" class="remodal-close"></button>
    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/promos/cyber-monday.jpg" alt="Get a special promo on the Tathata Golf 60-Day Program.">
</div>

<script>
    jQuery(function($) {
        var startDateMonday, endDateMonday, nowMonday, instMonday;

        startDateMonday = new Date(2016, 10, 28);
        endDateMonday = new Date(2016, 10, 29);
        nowMonday = Date.now();
        instMonday = $('[data-remodal-id=cyber-monday-modal]').remodal();

        if (nowMonday > startDateMonday && nowMonday < endDateMonday) {
            instMonday.open();
        }
    });
</script>
