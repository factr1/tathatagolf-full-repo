<div class="booking-wrapper">
    <h2><?php the_sub_field( 'schedule_title' ); ?></h2>

    <nav class="booking">
        <div class="booking-hero" style="background: url(<?php the_sub_field( 'schedule_image' ); ?>) center center/cover no-repeat"></div>

        <p class="booking-content"><?php the_sub_field( 'schedule_content' ); ?></p>

        <ul class="booking-list">
            <?php if ( have_rows( 'schedule' ) ): while ( have_rows( 'schedule' ) ) : the_row(); ?>
                <?php $status = get_sub_field( 'sold_out' ); ?>
                <li class="booking-item">
                    <?php if ( in_array( 'soldout', $status ) ) : ?>
                        <?php the_sub_field( 'date' ); ?>
                        <span>Sold Out!</span>
                    <?php else : ?>
                        <?php the_sub_field( 'date' ); ?>
                        <a href="<?php the_sub_field( 'buy_now_url' ); ?>" class="booking-link">Book Now</a>
                    <?php endif; ?>
                </li>
            <?php endwhile; endif; ?>
        </ul>
    </nav>
</div>
