<?php
/**
 * Template part showing a Fathers Day promotion
 */

$expiresDate = new DateTime( '2016-06-20' );
$now         = new DateTime( 'now' );
$timeLeft    = $now->diff( $expiresDate );
?>


<?php if ( $timeLeft->format( '%R%a' ) >= 0 ) : ?>
    <section id="fathers-day-promo" class="modal-promo">
        <div class="remodal fathers-day-modal" data-remodal-id="fathers-day-modal" data-remodal-options="hashTracking: false">
            <button data-remodal-action="close" class="remodal-close"></button>

            <div class="modal-promo-box">
                <div class="promo-image">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/60day-icon.png" class="img-responsive" alt="The Tathata Golf 60-Day Training Program">
                </div>

                <div class="promo-content">
                    <h3 class="title">Father's Day Is June 19<sup>th</sup></h3>

                    <div class="promo-coupon">
                        <p>Get a FREE 60-Day Training Program with all purchases of the program now through June 19<sup>th</sup></p>
                    </div>

                    <p>Streaming Version (Retail: $179.95) only. Every purchaser of the 60-Day Training Program will get an email with a promo code for an additional free 60-Day Training Program. Great for your dad, grandpa, or anyone you choose!</p>

                    <a href="<?php echo esc_url( home_url( '/?add_to_cart=8251' ) ); ?>" class="tg-btn">Buy Now</a>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>


<?php if ( $modalTime ) : ?>
    <script>
        jQuery(document).ready(function($) {
            setTimeout(function() {
                var fathersPromo = jQuery('[data-remodal-id=fathers-day-modal]').remodal();
                var fathersPromoState = fathersPromo.getState();
                fathersPromo.open();

                $(document).on('closed', '.fathers-day-modal', function() {
                    fathersPromo.destroy();
                });
            }, 8000);
        });
    </script>
<?php endif; ?>
