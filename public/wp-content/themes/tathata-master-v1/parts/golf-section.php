<?php
$section_title = get_sub_field( 'title' ) ? get_sub_field( 'title' ) : get_sub_field( 'navigation_title' );

$section_background = '';

if ( 'color' == get_sub_field('background') ) :
    $section_background = sprintf('style="background-color: %s"', get_sub_field('background_color'));
elseif ( 'image' == get_sub_field('background') ) :
    $section_background = sprintf('style="background-image: url(%s)"', get_sub_field('background_image'));
endif;
?>

<section id="section-<?php the_sub_field( 'id' ); ?>" class="page-section" <?php echo $section_background; ?>>
    <div class="container">
        <div class="row" style="margin-bottom: 20px">
            <div class="col-xs-12">
                <h1 class="title"><?php echo $section_title; ?></h1>

                <?php if (get_sub_field( 'content' )) : ?>
                    <?php the_sub_field( 'content' ); ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?php if ( 'left' == get_sub_field('show_hide_schedule') ) : ?>
                    <?php get_template_part('parts/booking'); ?>
                <?php else : ?>
                    <?php the_sub_field( 'content_left' ); ?>
                <?php endif; ?>
            </div>

            <div class="col-sm-6">
                <?php if ( 'right' == get_sub_field('show_hide_schedule') ) : ?>
                    <?php get_template_part('parts/booking'); ?>
                <?php else : ?>
                    <?php the_sub_field( 'content_right' ); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
