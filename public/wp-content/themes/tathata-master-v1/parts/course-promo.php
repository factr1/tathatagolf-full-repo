<div id="remodal-course-promo" class="remodal" data-remodal-id="course-promo" data-remodal-options="hashTracking: false">
    <style scoped>
        img {
            margin: 0 auto;
            float: none !important;
        }

        h1 {
            font-size: 30px;
        }

        .price {
            font-size: 24px;
            padding: 10px 0;
        }

        .remodal-confirm,
        .remodal-cancel {
            width: 100%;
        }

        .remodal-confirm {
            margin-bottom: 5px;
            background: rgb(57, 151, 52);
        }

        .remodal-confirm:hover {
            color: rgb(255, 255, 255);
        }

        .remodal-cancel {
            background: transparent;
            color: rgb(102, 0, 0);
        }

        .remodal-cancel:hover {
            background: transparent;
        }

        .promo-image {
            width: 100%;
        }

        .promo-content {
            margin-top: 20px;
            padding: 0;
        }

        .promo-content .page-title {
            font-size: 28px;
        }

        .promo-content p {
            margin-bottom: 20px;
        }

        .promocontent--btns {
            text-align: left;
        }

        @media screen and (min-width: 768px) {
            h1, p {
                text-align: left;
            }

            h1 {
                margin: 0 0 20px;
            }

            .remodal-confirm {
                width: 49%;
            }
        }
    </style>

    <button data-remodal-action="close" class="remodal-close"></button>

    <div class="row">
        <div class="promo-image">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/modal-followups.jpg" alt="">
        </div>

        <div class="promo-content">
            <h1 class="page-title">Want Live 1-on-1 Training?</h1>
            <p>Check out our Additional Training Opportunities available both at the Tathata Training Center as well as Online. Personalized Chapter Follow-Ups, Grad Schools, and more!</p>

            <div class="promocontent--btns">
                <a href="<?php echo esc_url( home_url( '/tathata-online' ) ); ?>" class="remodal-confirm">I Want Additional Training Online</a>
                <a href="<?php echo esc_url( home_url( '/tathata-training-center' ) ); ?>" class="remodal-confirm">I Want Additional Training in Person</a>
                <button data-remodal-action="cancel" class="remodal-cancel">Maybe Next Time...</button>
            </div>
        </div>
    </div>
</div>

<script defer>
    'use strict';
    jQuery(document).ready(function($) {
        var inst = $('[data-remodal-id=course-promo]').remodal();

        inst.open();

        $(document).on('closed', '#remodal-course-promo', function() {
            inst.destroy();
        });
    });
</script>
