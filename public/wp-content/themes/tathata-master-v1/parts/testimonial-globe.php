<?php
/**
 * Testimonial part that shows a globe .gif on the left, and testimonials on the right with hours trained
 */

$args = array(
    'post_type'      => 'testimonial',
    'orderby'        => 'rand',
    'order'          => 'ASC',
    'tax_query'      => array(
        array(
            'taxonomy' => 'testimonial_category',
            'field'    => 'slug',
            'terms'    => 'ticker'
        )
    ),
    'posts_per_page' => 50
);

$args2 = array(
    'post_type'      => 'testimonial',
    'orderby'        => 'rand',
    'order'          => 'DESC',
    'offset'         => 15,
    'tax_query'      => array(
        array(
            'taxonomy' => 'testimonial_category',
            'field'    => 'slug',
            'terms'    => 'ticker'
        )
    ),
    'posts_per_page' => 50
);

$ticker1 = new WP_Query( $args );
$ticker2 = new WP_Query( $args2 );
?>

<section id="testimonials-globe" class="-dark">

    <!-- MS Testimonials -->
    <?php if ( $ticker1->have_posts() ): ?>
        <div id="ms-testimonials">
            <div class="container">
                <div class="ms-hours">
                    <h3>
                        <span id="hoursTrained">24,514</span> Hours of Learning &amp; Training Completed by Tathata Students Since September 1, 2015
                    </h3>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="content">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/MapGif.gif" class="img-responsive testimonial-globe" alt="Animated map of Tathata Movement Specialist locations">
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="content flex-column">
                            <div id="globe-ticker" class="testimonial-ticker">
                                <?php
                                while ( $ticker1->have_posts() ) : $ticker1->the_post();
                                    $gtContent = get_field( 'tt_testimonial' );

                                    // Social Source Settings
                                    $source = get_field( 'social_source' );

                                    switch ( $source ) :
                                        case 'facebook':
                                            $social_icon  = 'fa-facebook';
                                            $social_color = '#3b5998';
                                            break;
                                        case 'twitter':
                                            $social_icon  = 'fa-twitter';
                                            $social_color = '#55acee';
                                            break;
                                        case 'web':
                                            $social_icon  = 'fa-globe';
                                            $social_color = '#fd9306';
                                            break;
                                        default:
                                            $social_icon  = 'fa-envelope';
                                            $social_color = '#CF0A2C';
                                    endswitch;
                                    ?>
                                    <blockquote style="border-left: 5px solid <?php echo $social_color; ?>">
                                        <?php echo $gtContent; ?>

                                        <footer>
                                            <cite title="A testimonial from <?php the_title(); ?>" style="color: <?php echo $social_color; ?>">
                                                <?php the_title(); ?> |
                                                <i class="fa <?php echo $social_icon; ?>" style="color: <?php echo $social_color; ?>; font-size: 14px"></i>
                                            </cite>
                                        </footer>
                                    </blockquote>
                                <?php endwhile; wp_reset_postdata(); ?>
                            </div>

                            <div id="globe-ticker2" class="testimonial-ticker">
                                <?php
                                while ( $ticker2->have_posts() ) : $ticker2->the_post();
                                    $gtContent = get_field( 'tt_testimonial' );

                                    // Social Source Settings
                                    $source = get_field( 'social_source' );

                                    switch ( $source ) :
                                        case 'facebook':
                                            $social_icon  = 'fa-facebook';
                                            $social_color = '#3b5998';
                                            break;
                                        case 'twitter':
                                            $social_icon  = 'fa-twitter';
                                            $social_color = '#55acee';
                                            break;
                                        case 'web':
                                            $social_icon  = 'fa-globe';
                                            $social_color = '#fd9306';
                                            break;
                                        default:
                                            $social_icon  = 'fa-envelope';
                                            $social_color = '#CF0A2C';
                                    endswitch;
                                    ?>
                                    <blockquote style="border-left: 5px solid <?php echo $social_color; ?>">
                                        <?php echo $gtContent; ?>

                                        <footer>
                                            <cite title="A testimonial from <?php the_title(); ?>" style="color: <?php echo $social_color; ?>">
                                                <?php the_title(); ?> |
                                                <i class="fa <?php echo $social_icon; ?>" style="color: <?php echo $social_color; ?>; font-size: 14px"></i>
                                            </cite>
                                        </footer>
                                    </blockquote>
                                <?php endwhile; wp_reset_postdata(); ?>
                            </div>

                            <a href="<?php echo esc_url( home_url( '/tathata-golf-testimonials' ) ); ?>" class="btn-new">Submit Yours / See All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php endif; ?>

</section>

