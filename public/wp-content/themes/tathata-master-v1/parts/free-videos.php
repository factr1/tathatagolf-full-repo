<?php
// Show the free videos
?>

<section class="free-videos">
    <div class="container">
        <div class="_content">
            <h1><?php the_field( 'main_title' ); ?></h1>
            <h2><?php the_field( 'sub_title' ); ?></h2>
        </div>

        <?php if ( have_rows( 'videos' ) ) : while ( have_rows( 'videos' ) ) : the_row(); ?>
            <div class="row ">
                <div class="col-xs-12 col-md-4">
                    <h3><?php the_sub_field( 'title' ); ?></h3>
                    <p><?php the_sub_field( 'content' ); ?></p>
                </div>

                <div class="col-xs-12 col-md-8">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/<?php the_sub_field( 'video_id' ); ?>"></iframe>
                    </div>
                </div>
            </div>

            <hr>
        <?php endwhile; endif; ?>
    </div>
</section>
