<?php
/**
 * Template part for custom page titles
 *
 * This template is similar to banner.php but it does not attempt to pull data from an ACF field.
 * Use this instead of the getPageHero() function on archive and category pages.
 */

$pageTitle = get_the_title();
$customPageTitle;

if ( $customPageTitle ) {
    $pageTitle = $customPageTitle;
}
?>

<section id="title-section">
    <div class="container">
        <h1><?php echo $pageTitle; ?></h1>
    </div>
</section>
