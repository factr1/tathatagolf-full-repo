<?php
/**
 * The template for displaying all pages
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 */

get_header();
?>

<?php get_template_part( 'parts/page-title' ); ?>

<?php if ( ( is_bbpress() ) && ( ! bbp_is_single_user() ) ) { ?>
    <div class="headerwidgets">
        <div class="forehead-wrapper">
            <?php dynamic_sidebar( 'sidebar-forehead' ); ?>
        </div>

        <div class="clear"></div>
    </div>

    <div class="container">
        <div class="row">
            <ol class="breadcrumb pull-left">
                <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                <li>Forum</li>
            </ol>

            <?php echo do_shortcode( '[followUs]' ); ?>
        </div>
    </div>
<?php } ?>

<?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section class="pageInnerContentWrap">
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-8 col-md-8">
                    <?php
                    if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
                        // Include the featured content template.
                        get_template_part( 'parts/featured-content' );
                    }
                    ?>
                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">
                            <?php
                            while ( have_posts() ) : the_post();
                                // Include the page content template.
                                get_template_part( 'parts/content', 'page' );

                                // If comments are open or we have at least one comment, load up the comment template.
                                if ( comments_open() || get_comments_number() ) {
                                    comments_template();
                                }
                            endwhile;
                            ?>
                        </div><!-- #content -->
                    </div><!-- #primary -->
                </article>

                <article class="col-xs-12 col-sm-4 col-md-4 rightPanel">
                    <?php
                        get_sidebar();
                        get_sidebar( 'content' );
                    ?>
                </article>
            </div>
        </div><!-- #main-content -->
    </section>

<?php
get_footer();
