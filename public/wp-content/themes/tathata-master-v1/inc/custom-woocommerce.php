<?php
// Add a referrer field to the checkout page
function customReferrer( $checkout ) {
	printf( '<div id="wc-referrer"><h3>%s</h3>', __( 'Were You Referred?' ) );

	woocommerce_form_field( 'wc-custom-referrer', array(
		'type'        => 'text',
		'class'       => array( 'wc-referrer form-row-wide' ),
		'label'       => __( 'Were you referred to Tathata Golf buy someone?' ),
		'placeholder' => __( 'Who referred you to Tathata?' ),
	), $checkout->get_value( 'wc-custom-referrer' ) );

	printf( '</div>' );
}

function customReferrerLocation( $checkout ) {
	printf( '<div id="wc-referrer"><h3 style="display: none">%s</h3>', __( 'Referrer Location' ) );

	woocommerce_form_field( 'wc-custom-referrer-location', array(
		'type'        => 'text',
		'class'       => array( 'wc-referrer form-row-wide' ),
		'placeholder' => __( 'Where are they located (city, state)?' ),
	), $checkout->get_value( 'wc-custom-referrer-location' ) );

	printf( '</div>' );
}

add_action( 'woocommerce_after_order_notes', 'customReferrer' );
add_action( 'woocommerce_after_order_notes', 'customReferrerLocation' );

// Process the values
function customReferrerValidation() {
	if ( $_POST['wc-custom-referrer-location'] && ! $_POST['wc-custom-referrer'] ) {
		wc_add_notice( __( 'You cannot provide a referrer location without the referrer!' ), 'error' );
	}
}

add_action( 'woocommerce_checkout_process', 'customReferrerValidation' );

// Actually store the value with the order
function storeCustomReferrer( $order_id ) {
	if ( ! empty( $_POST['wc-custom-referrer'] ) ) {
		update_post_meta( $order_id, 'Were You Referred?', sanitize_text_field( $_POST['wc-custom-referrer'] ) );
	}

	if ( ! empty( $_POST['wc-custom-referrer-location'] ) ) {
		update_post_meta( $order_id, 'Referrer Location', sanitize_text_field( $_POST['wc-custom-referrer-location'] ) );
	}
}

add_action( 'woocommerce_checkout_update_order_meta', 'storeCustomReferrer' );

// Display field value on the order edit page
function displayCustomReferrer( $order ) {
	printf( '<p><strong>%s:</strong> %s</p>', __( 'Referred By' ), get_post_meta( $order->id, 'Were You Referred?', true ) );
	printf( '<p><strong>%s:</strong> %s</p>', __( 'Referrer Location' ), get_post_meta( $order->id, 'Referrer Location', true ) );
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'displayCustomReferrer', 10, 1 );

// Add some fine print to international orders
function internationalComment() {
    printf('<p class="fine-print">%s</p>', 'Taxes will be displayed on the checkout page. International shipments may be subject to import duties and taxes, which are levied once the package reaches your country. These charges are the buyer\'s responsibility.');
}
add_action('woocommerce_after_cart_totals', 'internationalComment', 10,1);
