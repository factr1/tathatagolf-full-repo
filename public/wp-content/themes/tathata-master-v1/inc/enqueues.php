<?php
/**
 * Enqueue scripts and styles for the front end.
 * @since Twenty Fourteen 1.0
 */
function twentyfourteen_scripts() {
	// CSS
	// --------------------------------------------------------------
	wp_enqueue_style( 'tathata-open-sans', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic|Open+Sans+Condensed:300,300italic,700&subset=latin,latin-ext', array(), '1.0.0' );

	// Load the Internet Explorer specific stylesheet
	wp_enqueue_style( 'twentyfourteen-ie', get_template_directory_uri() . '/ie.css', array( 'theme' ), '20131205' );
	wp_style_add_data( 'twentyfourteen-ie', 'conditional', 'lt IE 9' );

	wp_enqueue_style( 'theme', get_template_directory_uri() . '/assets/css/tathata.css', '', '1.0.0' );

	// Load Gravity Forms styles and scripts on these pages
	if ( is_page( array( 'tathata-golf-testimonials' ) ) ) {
		gravity_form_enqueue_scripts( 'Testimonials', true );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// JS
	// --------------------------------------------------------------
	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentyfourteen-keyboard-image-navigation', get_template_directory_uri() . '/assets/js/base/keyboard-image-navigation.min.js', array( 'jquery' ), '20130402' );
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		wp_enqueue_script( 'jquery-masonry' );
	}

	if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
		wp_enqueue_script( 'twentyfourteen-slider', get_template_directory_uri() . '/assets/js/base/slider.min.js', array( 'jquery' ), '20131205', true );
		wp_localize_script( 'twentyfourteen-slider', 'featuredSliderDefaults', array(
			'prevText' => __( 'Previous', 'twentyfourteen' ),
			'nextText' => __( 'Next', 'twentyfourteen' )
		) );
	}

	// Font Awesome
	wp_enqueue_script( 'font-awesome', 'https://use.fontawesome.com/71eb3125bb.js', true );

	wp_enqueue_script( 'tathata', get_template_directory_uri() . '/assets/js/tathata.js', array( 'jquery' ), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'twentyfourteen_scripts' );

// Add fonts for print
function load_fonts() {
	wp_enqueue_style( 'tathata-open-sans' );
}

add_action( 'wp_print_styles', 'load_fonts' );
