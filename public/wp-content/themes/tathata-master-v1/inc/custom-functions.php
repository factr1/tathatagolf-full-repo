<?php
/**
 * A function to simply determine the current working environment.
 * You should add your development URL(s)
 *
 * @return string The current working environment
 */
function getEnvironment() {
	$environments = [
		'development' => [
			'http://tathata:8888',
			'http://localhost:3000'
		],
		'staging'     => [
			'http://staging.tathatagolf.org',
			'https://staging.tathatagolf.org'
		],
		'production'  => [
			'http://tathatagolf.com',
			'https://tathatagolf.com',
			'http://www.tathatagolf.com',
			'https://www.tathatagolf.com'
		]
	];

	$currentURL = get_home_url();
	$currentEnv = ''; // Not being set to development by default because don't want to return an environment if the project is mis-configured

	if ( in_array( $currentURL, $environments['development'] ) ) {
		$currentEnv = 'development';
	} else if ( in_array( $currentURL, $environments['staging'] ) ) {
		$currentEnv = 'staging';
	} else if ( in_array( $currentURL, $environments['production'] ) ) {
		$currentEnv = 'production';
	}

	return $currentEnv;
}

/**
 * A function to grab the selected Page Hero Type ACF option
 */
function getPageHero() {
	switch ( get_field( 'hero_type' ) ) :
		case 'none':
			return;
		case 'hero':
			return get_template_part( 'parts/hero' );
		case 'slider':
			return get_template_part( 'parts/slider' );
		case 'banner':
			return get_template_part( 'parts/banner' );
	endswitch;
}

/**
 * A function that grabs the featured image for a post/page
 *
 * @param integer $id (Optional) The post ID of the post to grab the featured image for. If not supplied, the current
 *                    post will be used.
 *
 * @return string The URL of the featured image for the post/page
 */
function getFeaturedImage( $id = null ) {
	if ( has_post_thumbnail() ) {
		$featuredImageID  = get_post_thumbnail_id( $id );
		$featuredImageURL = wp_get_attachment_image_src( $featuredImageID, true );

		return $featuredImageURL[0];
	} else {
		return;
	}
}

/**
 * Take control of the WordPress search functionality, and customize it.
 *
 * @param $query
 */
function custom_wp_search( $query ) {
	if ( ! is_admin() && $query->is_main_query() && $query->is_search() ) {

		$tax_query = array(
			array(
				'taxonomy' => 'product_cat',
				'field'    => 'slug',
				'terms'    => array( '1-day-jumpstart', 'half-day-live-exp', 'golf-school', 'grad-school', 'dvds' ),
				'operator' => 'NOT IN'
			)
		);

		// Define what is allowed to be searched
		$query->set( 'post_type', array( 'post', 'page', 'forum', 'topic', 'testimonial', 'product' ) );
		// Don't show these taxonomies
		$query->set( 'tax_query', $tax_query );
		// Don't show these specific posts or pages
		$query->set( 'post__not_in', array( 8235, 7518, 8838 ) );
	}
}

add_action( 'pre_get_posts', 'custom_wp_search' );

/**
 * A function to create a banner/page-title for a category or archive page (or another one off situation)
 *
 * @param string $title The content that should be used as the banner/page title
 */
function setPageBanner( $title ) {
	$customPageTitle = $title;
	include( locate_template( 'parts/page-title.php' ) );
}

/**
 * A function meant to grab an ACF image that is intended to be used as a background image
 *
 * @param string $field     The name of the ACF field which to check against if an image has been uploaded or not
 * @param float  $gradient1 The initial opacity for the start of the linear gradient. 1 is totally opaque, 0 is
 *                          transparent
 * @param float  $gradient2 The ending opacity for the end of the linear gradient. 1 is totally opaque, 0 is
 *                          transparent
 * @param string $size      The value for the background size CSS property
 * @param bool   $repeat    True or False, if the background should repeat or not
 */
function setACFBackground( $field, $gradient1 = .5, $gradient2 = .7, $position = '0% 0%', $size = 'cover', $repeat = false ) {
	$gradientStart = $gradient1;
	$gradientEnd   = $gradient2;
	$imgPosition   = $position;
	$bgSize        = '/' . $size;
	$repeatable    = 'no-repeat';

	if ( $position == null ) {
		$imgPosition = '';
	}

	if ( $size == null ) {
		$bgSize = '';
	}

	if ( $repeat ) {
		$repeatable = '';
	}

	if ( $field && get_field( $field ) ) {
		echo 'style="background: linear-gradient(rgba(0, 0, 0, ' . $gradientStart . '), rgba(0, 0, 0, ' . $gradientEnd . ')), url(\'' . get_field( $field ) . '\') ' . $imgPosition . $bgSize . ' ' . $repeatable . '"';
	} else if ( $field && get_sub_field( $field ) ) {
		echo 'style="background: linear-gradient(rgba(0, 0, 0, ' . $gradientStart . '), rgba(0, 0, 0, ' . $gradientEnd . ')), url(\'' . get_sub_field( $field ) . '\') ' . $imgPosition . $bgSize . ' ' . $repeatable . '"';
	} else {
		return;
	}
}

/**
 * A function to grab the internal/external link from ACF.
 * This function requires in the ACF fields both an internal and external link option is set.
 *
 * @param string $field1   The first field name to check for
 * @param string $field2   The second field name to check for
 * @param string $field3   The third field name to check for. This likely will only appear if the link is to an on
 *                         page ID
 */
function setACFLink( $field1, $field2, $field3 = null ) {
	if ( $field1 && get_field( $field1 ) ) {
		echo esc_url( get_field( $field1 ) );
	} else if ( $field2 && get_field( $field2 ) ) {
		echo esc_url( get_field( $field2 ) );
	} else if ( $field1 && get_sub_field( $field1 ) ) {
		echo esc_url( get_sub_field( $field1 ) );
	} else if ( $field2 && get_sub_field( $field2 ) ) {
		echo esc_url( get_sub_field( $field2 ) );
	} else if ( $field3 && get_field( $field3 ) ) {
		echo '#' . get_field( $field3 );
	} else if ( $field3 && get_sub_field( $field3 ) ) {
		echo '#' . get_sub_field( $field3 );
	} else {
		return;
	}
}

/**
 * A function to set a post/page ID based upon the current working environment.
 * This is only to be used in situations where a page/post/form/etc. has been created across the 3 environments at
 * separate times, and as a result has 3 different assigned IDs. This function aims to ensure that the correct
 * content is pulled in any environment.
 *
 * Most commonly will be used as a parameter for functions that accept a post/page ID
 *
 * @param  array $idList An array of the item ID's in the development, staging, and production environments
 *
 * @return integer The item ID for the current working environment
 */
function setEnvironmentID( $idArray ) {
	$currentID = $idArray[0];

	if ( getEnvironment() == 'staging' ) {
		if ( isset( $idArray[1] ) && $idArray[1] != $idArray[0] ) {
			$currentID = $idArray[1];
		}
	}

	if ( getEnvironment() == 'production' ) {
		if ( isset( $idArray[1], $idArray[2] ) ) {
			$currentID = $idArray[2];
		} else if ( isset( $idArray[1] ) && $idArray[1] != $idArray[0] ) {
			$currentID = $idArray[1];
		}
	}

	return $currentID;
}

/**
 * Create breadcrumbs for a category or archive page
 *
 * @param string $title A string of what the title of the last breadcrumb should be
 */
function setBreadcrumbs( $title ) {
	$title = $title;
	include( locate_template( 'parts/breadcrumbs.php' ) );
}

/**
 * Simple wrapper for print_r()
 *
 * @param $code The variable to be checked
 */
function debug( $code ) {
	printf( '<pre>%s</pre>', print_r( $code, true ) );
}

/**
 * A function to pass a category into the redesigned Testimonial Slider
 *
 * This exists because the current slider post type is paired to a custom plugin, and to avoid having to make edits
 * to that functionality and potentially lose testimonials, it is   better to just be able to change the query on the
 * fly
 *
 * @param string $category The category that should be used as the or the slider
 */
function testimonialSlider( $category = 'slider' ) {
	$sliderCategory = $category;
	include( locate_template( 'parts/testimonial-slider.php' ) );
}

/**
 * Custom sort the bbPress topics by modified_date ( aka "Freshness"), in descending order
 */
function custom_bbpress_topic_sorting() {
	$args['orderby'] = 'meta_value';
	$args['order']   = 'DESC';

	return $args;
}

add_filter( 'bbp_before_has_topics_parse_args', 'custom_bbpress_topic_sorting' );
