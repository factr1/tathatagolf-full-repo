<?php
/**
 * Custom wrapper shortcode
 *
 * Creates a wrapping element, useful when using default page layouts but still need to add custom elements from
 * within the editor
 *
 * Default Usage: [wrapper]...[/wrapper] => <div class="content-wrapper">...</div>
 * Custom Usage: [wrapper container="section" id="test" class="my-class"]...[/wrapper] => <section id="test"
 * class="my-class">...</section>
 */

// TODO: add support for nested shortcodes
// TODO: add support for custom attributes
function createWrapper( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'container' => 'div',
		'id'        => '',
		'class'     => 'content-wrapper',
	), $atts ) );

	if ( ! $id ) {
		return '<' . $container . ' class="' . $class . '">' . $content . '</' . $container . '>';
	}

	return '<' . $container . ' id="' . $id . '" class="' . $class . '">' . $content . '</' . $container . '>';
}

add_shortcode( 'wrapper', 'createWrapper' );

// Buy Now Button
function programBuyNow( $atts ) {
	extract( shortcode_atts( array(
		'content' => 'Buy now for $179.95!',
	), $atts ) );

	return '<a href="' . esc_url( home_url( "/?add_to_cart=8251" ) ) . '" class="buynow-button">' . $content . '</a>';
}

add_shortcode( 'buynow', 'programBuyNow' );

// Gravity Form Modal
function gravityFormModal( $atts ) {
	extract( shortcode_atts( array(
		'name'    => '',
		'id'      => '',
		'content' => 'Find Out More!',
	), $atts ) );

	function buildModal( $name, $id ) {
		echo '<div class="remodal" data-remodal-id="' . $name . '" data-remodal-options="hashTracking: false">';
		echo '<button data-remodal-action="close" class="remodal-close"></button>';
		gravity_form( $id, false, false, false, '', false );
		echo '</div>';
	}

	buildModal( $name, $id );

	return '<a class="gravity-form-modal" data-remodal-target="' . $name . '">' . $content . '</a>';
}

add_shortcode( 'gravity-modal', 'gravityFormModal' );
