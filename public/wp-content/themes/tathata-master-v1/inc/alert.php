<style>
    .advertisement {
        background: rgb(204, 0, 0);
        text-align: center;
        width: 100%;
        padding: 10px 4px;
        margin: 0;
        color: rgb(255, 255, 255);
        display: none;
        position: absolute;
    }

    .advertisement h3, .advertisement p, .advertisement a {
        color: rgb(255, 255, 255);
    }

    .advertisement a {
        text-decoration: underline;
    }
</style>

<div class="advertisement">
    <div class="row">
        <div class="small-12 columns">
            <h3>Tathata Golf Gift Cards now available.
                <a href="<?php echo esc_url( home_url( '/product-category/gift-cards/' ) ); ?>">Click here to shop now</a>
            </h3>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(document).ready(function() {

            setTimeout(function() {
                $('.advertisement').slideDown(700).css("position", "relative")
            }, 1000);
            setTimeout(function() {
                $('.advertisement').slideUp(700).css("position", "relative")
            }, 20000);
        });
    });
</script>
