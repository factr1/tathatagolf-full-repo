<?php
/**
 * The template for displaying 404 pages (Not Found)
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */

get_header();
?>

    <div id="primary" class="content-area" style="height:80vh; background:url(<?php echo get_template_directory_uri(); ?>/assets/img/Bg-Paper.jpg); background-size:cover;">
        <div id="content" class="site-content container" role="main">
            <div class="row">
                <div style="margin:150px 0; text-align:center;">
                    <h1>UH-OH!</h1>
                    <h3 style="color:#c00;">It looks like there was either an error with the page, or what you are looking for moved.</h3>
                    <p>You can try the search tool or main navigation in the header to track down what you wanted.</p>
                </div>
            </div>
        </div><!-- #content -->
    </div><!-- #primary -->

<?php
get_footer();
