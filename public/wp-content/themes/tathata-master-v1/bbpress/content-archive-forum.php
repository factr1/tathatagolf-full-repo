<?php
/**
 * Archive Forum Content Part
 * @package    bbPress
 * @subpackage Theme
 */
?>

<div id="bbpress-forums">
    <?php bbp_forum_subscription_link(); ?>

    <div class="new-forum-topic-button">
        <a href="<?php echo esc_url( home_url( '/new-forum-topic' ) ); ?>">Open up a new forum Thread</a>
    </div>

    <?php
    do_action( 'bbp_template_before_forums_index' );

    if ( bbp_has_forums() ) :
        bbp_get_template_part( 'loop', 'forums' );
    else :
        bbp_get_template_part( 'feedback', 'no-forums' );
    endif;

    do_action( 'bbp_template_after_forums_index' );
    ?>
</div>
