<?php
/**
 * Single View Content Part
 * @package    bbPress
 * @subpackage Theme
 */
?>

<div id="bbpress-forums">
    <?php
    bbp_breadcrumb();
    bbp_set_query_name( bbp_get_view_rewrite_id() );

    if ( bbp_view_query() ) :
        bbp_get_template_part( 'pagination', 'topics' );
        bbp_get_template_part( 'loop', 'topics' );
        bbp_get_template_part( 'pagination', 'topics' );
    else :
        bbp_get_template_part( 'feedback', 'no-topics' );
    endif;

    bbp_reset_query_name();
    ?>
</div>
