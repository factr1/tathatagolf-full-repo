<?php
/**
 * Single User Content Part
 */
?>

<div id="bbpress-forums profile-public">
    <?php do_action( 'bbp_template_notices' ); ?>

    <div id="bbp-user-wrapper">
        <?php bbp_get_template_part( 'user', 'details' ); ?>
        <div id="bbp-user-body">
            <ul class="profile_menu"><!-- quicklist start -->
                <li><a href="#">Select your profile items</a>
                    <ul>
                        <li class="profile-items-selection <?php if ( bbp_is_single_user_profile() ) : ?>current<?php endif; ?>" role="presentation">
							<span class="vcard bbp-user-profile-link">
								<a role="menuitem" tabindex="-1" href=" <?php global $current_user;
                                get_currentuserinfo();
                                echo esc_url( home_url() ) . "/forums/members/" . $current_user->user_login . "/"; ?> "> Profile </a>
							</span>
                        </li>
                        <li class="profile-items-selection <?php if ( bbp_is_single_user_topics() ) : ?>current<?php endif; ?>">
							<span class='bbp-user-topics-created-link'>
								<a role="menuitem" tabindex="-1" href=" <?php global $current_user;
                                get_currentuserinfo();
                                echo esc_url( home_url() ) . "/forums/members/" . $current_user->user_login . "/topics/"; ?> ">
                                    Topics Started
                                </a>
							</span>
                        </li>
                        <li class="profile-items-selection <?php if ( bbp_is_single_user_replies() ) : ?>current<?php endif; ?>">
							<span class='bbp-user-replies-created-link'>
                                <a role="menuitem" tabindex="-1" href=" <?php global $current_user;
                                get_currentuserinfo();
                                echo esc_url( home_url() ) . "/forums/members/" . $current_user->user_login . "/replies/"; ?> ">
                                    Recent Replies
                                </a>
							</span>
                        </li>
                        <?php if ( bbp_is_user_home() || current_user_can( 'edit_users' ) ) : ?>
                            <li class="profile-items-selection <?php if ( bbp_is_single_user_edit() ) : ?>current<?php endif; ?>">
                                <span class="bbp-user-edit-link">
                                    <a role="menuitem" tabindex="-1" href=" <?php global $current_user;
                                    get_currentuserinfo();
                                    echo esc_url( home_url() ) . "/forums/members/" . $current_user->user_login . "/edit/"; ?> ">
                                        Update Account Info
                                    </a>
                                </span>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>
            </ul><!-- quicklist end -->

            <?php
            if ( bbp_is_favorites() ) {
                bbp_get_template_part( 'user', 'favorites' );
            }

            if ( bbp_is_subscriptions() ) {
                bbp_get_template_part( 'user', 'subscriptions' );
            }

            if ( bbp_is_single_user_topics() ) {
                bbp_get_template_part( 'user', 'topics-created' );
            }

            if ( bbp_is_single_user_replies() ) {
                bbp_get_template_part( 'user', 'replies-created' );
            }

            if ( bbp_is_single_user_edit() ) {
                bbp_get_template_part( 'form', 'user-edit' );
            }

            if ( bbp_is_single_user_profile() ) {
                bbp_get_template_part( 'user', 'profile' );
            }
            ?>
        </div>
    </div>
</div>
