<?php
// Redirect the user to the login page if they are not logged in
if ( ! is_user_logged_in() ) {
    wp_redirect( home_url( '/login/' ) );
    exit;
}

$term    = $wp_query->queried_object;
$taxname = $term->name;

get_header();
setPageBanner( '60-Day Program Video Library' );
?>

    <div id="blackbar"></div>

<?php // Custom Breadcrumbs ?>
    <section id="breadcrumbs">
        <div class="hidden-xs">
            <div class="container">
                <div class="row">

                    <div class="col-xs-12 col-sm-8">
                        <div class="flex -flex-start">
                            <ul class="breadcrumb">
                                <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                                <li><a href="<?php echo esc_url( home_url( '/grad-school/60-day-program-video-library/' ) ); ?>">60-Day Program Video Library</a></li>

                                <?php if ( ! empty( $post->post_parent ) ) {
                                    $parentTitle = get_the_title( $post->post_parent ); ?>
                                    <li><a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo $parentTitle; ?></a></li>
                                <?php } ?>

                                <li><?php echo $taxname; ?></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="flex -flex-end">
                            <?php echo do_shortcode( '[followUs]' ); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="visible-xs">
            <div class="container">
                <div class="flex">
                    <div>
                        <ul class="breadcrumb">
                            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                            <li><a href="<?php echo esc_url( home_url( '/grad-school/60-day-program-video-library/' ) ); ?>">60-Day Program Video Library</a></li>

                            <?php if ( ! empty( $post->post_parent ) ) {
                                $parentTitle = get_the_title( $post->post_parent ); ?>
                                <li><a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo $parentTitle; ?></a></li>
                            <?php } ?>

                            <li><?php echo $taxname; ?></li>
                        </ul>
                    </div>

                    <div>
                        <?php echo do_shortcode( '[followUs]' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="tg-video-archive" class="tg-video-archive">
        <div class="container">

            <div class="video-menu">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <?php wp_nav_menu( array( 'menu' => 'video menu', 'container_id' => 'owncssmenu' ) ); ?>
                    </div>
                </div>
            </div>

            <div class="video-archive-title">
                <h1><?php echo $taxname; ?></h1>
            </div>

            <div class="video-archive-description" style="text-align: center">
                <p><?php echo $term->description; ?></p>
            </div>

            <div class="tg-video-archive-videos">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="archive-video-container">
                            <?php
                                if ( have_posts() ) : while ( have_posts() ) : the_post();
                                    get_template_part( 'parts/content', 'oa-chapter' );
                                endwhile;
                                else :
                                    get_template_part( 'parts/content', 'none' );
                                endif;
                            ?>
                        </div>

                        <?php twentyfourteen_paging_nav(); ?>
                    </div>
                </div>
            </div>

        </div>
    </section>

<?php
get_footer();
