<?php
/**
 * The Template for displaying all single posts
 */

$term    = $wp_query->queried_object;
$taxname = $term->post_title;
$terms   = get_the_terms( get_the_ID(), 'oa-chapter' );

get_header();
setPageBanner( '60-Day Program Video Library' );
?>

    <div id="blackbar"></div>

    <section id="single-oa-video" class="single-oa-video" style="padding-top: 40px">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <h1><?php the_title(); ?></h1>

                        <div class="embed-responsive embed-responsive-16by9">
                            <?php the_content(); ?>
                        </div>

                        <a class="tg-btn" href="javascript:void(0);" onclick="history.go(-1);" style="max-width: 100px; margin: 20px auto 0;">Go Back</a>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
