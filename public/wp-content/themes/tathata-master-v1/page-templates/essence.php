<?php
/**
 * Template Name: The Essence
 *
 * New redesigned template for the Essence page.
 */

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <section id="essence">

        <!-- CTA -->
        <section class="cta -grow-the-game flex-column" style="background: url('<?php the_field( 'e_call_to_action_background' ); ?>') top center/cover no-repeat">
            <div class="container">
                <?php the_field( 'e_call_to_action' ); ?>
            </div>
        </section>

        <!-- Main Content -->
        <section class="content">
            <div class="container">

                <div class="content-row">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/tathata-core-foundation.png">

                            <h3>Tathata's <span>Core</span> Foundation</h3>

                            <?php the_field( 'e_core_foundation' ); ?>
                        </div>

                        <div class="col-md-6">
                            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/tathata-curriculum.png">

                            <h3>Tathata <span>Curriculum</span></h3>

                            <?php the_field( 'e_tathata_curriculum' ); ?>

                            <a href="<?php echo esc_url( home_url( '/60-day-program' ) ); ?>">Start Your Training <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

                <div class="content-row">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="flex">
                                <p class="pullquote"><strong>Tathata</strong>, in its truest sense means <span>"suchness," a sense of complete understanding and all knowing</span>. <strong>Tathata Golf Training</strong> has been built around this principle.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content-row">
                    <h3>The 3 <span>Core</span> areas within our training</h3>

                    <div class="row">

                        <?php if ( have_rows( 'e_mind_body_swing' ) ) : while ( have_rows( 'e_mind_body_swing' ) ) : the_row(); ?>
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-image">
                                        <img class="img-responsive -full-width" src="<?php the_sub_field( 'e_mbs_image' ); ?>">
                                    </div>

                                    <div class="card-content">
                                        <h4 class="-uppercase"><span><?php the_sub_field( 'e_mbs_title' ); ?></span></h4>

                                        <?php the_sub_field( 'e_mbs_content' ); ?>
                                    </div>
                                </div>
                            </div>

                        <?php endwhile; endif; ?>
                    </div>
                </div>

                <div class="content-row">
                    <div class="row">
                        <div class="col-md-6">
                            <h3><span>Story Behind</span> Tathata Golf</h3>

                            <img class="img-responsive -full-width" src="<?php echo get_template_directory_uri(); ?>/assets/img/essenceStory.png">

                            <p class="-padding-top">A story spoken by Bryan Hepler, a man driven by a passion for golf. The story of how his journey seeking the ultimate golf instruction brought him to creating and sharing what is now called Tathata Golf.</p>

                            <a href="<?php echo esc_url( home_url( '/essence-story' ) ); ?>">Continue Reading <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></a>
                        </div>

                        <div class="col-md-6">
                            <h3><span>Thanks</span> for the Support</h3>

                            <img class="img-responsive -full-width" src="<?php echo get_template_directory_uri(); ?>/assets/img/essenceStory2.png">

                            <p class="-padding-top">"I am forever grateful that so many of you have come together to share your love, support, and wisdom with me in this lifetime." - <strong>Bryan Helper</strong></p>

                            <a href="<?php echo esc_url( home_url( '/thank-you' ) ); ?>">Continue Reading <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <?php if ( have_rows( 'e_featured_cards' ) ) : ?>
            <section id="essence-featured-cards" class="callouts" style="background: linear-gradient(rgba(0, 0, 0, .0), rgba(0, 0, 0, .2)), url('<?php the_field( 'e_fc_background' ); ?>') center center/cover no-repeat;">
                <div class="container">
                    <div class="row">

                        <?php if ( have_rows( 'e_featured_cards' ) ) : while ( have_rows( 'e_featured_cards' ) ) : the_row(); ?>
                            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                <div class="card -dark">
                                    <div class="card-title">
                                        <h4><?php the_sub_field( 'e_fc_title' ); ?></h4>
                                    </div>

                                    <div class="card-image">
                                        <img class="img-responsive -full-width" src="<?php the_sub_field( 'e_fc_image' ); ?>">
                                    </div>

                                    <div class="card-content">
                                        <?php the_sub_field( 'e_fc_content' ); ?>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; endif; ?>

                    </div>
                </div>
            </section>
        <?php endif; ?>

        <img class="beacon" border="0" src="http://r.turn.com/r/beacon?b2=P_AYcJBQXbQH0yAC_9IQpZhwxyZGxpt9A9pzJmqIATDwmwgCAYlZ5lTJGOrh3DGCbjopRYcdYaZeLRI9P-WP4Q&cid=">
    </section>

<script>
    jQuery(document).ready(function($) {
        $('.card').matchHeight();
    });
</script>

<?php
get_footer();
