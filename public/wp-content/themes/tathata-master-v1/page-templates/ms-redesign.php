<?php
/**
 * Template Name: Movement Specialist (Redesign)
 * This is the new template file for http://www.tathatagolf.com/movement-specialists/
 */

get_header();
getPagehero();
?>

    <div id="blackbar"></div>

    <div class="ms-container">

        <!-- Call to Action -->
        <section class="ms-cta">
            <h1 class="ms-title"><?php the_field( 'ms_cta_title' ); ?></h1>

            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <?php if ( get_field( 'ms_find_internal_link' ) ) : ?>
                            <a class="tg-btn" href="<?php the_field( 'ms_find_internal_link' ); ?>"><i class="fa fa-search"></i> <?php the_field( 'ms_find_button' ); ?></a>
                        <?php else : ?>
                            <a class="tg-btn" href="<?php the_field( 'ms_find_external_link' ); ?>"><i class="fa fa-search"></i> <?php the_field( 'ms_find_button' ); ?></a>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <?php if ( get_field( 'ms_become_internal_link' ) ) : ?>
                            <a class="tg-btn" href="<?php the_field( 'ms_become_internal_link' ); ?>"><i class="fa fa-graduation-cap"></i> <?php the_field( 'ms_become_button' ); ?></a>
                        <?php else : ?>
                            <a class="tg-btn" href="<?php the_field( 'ms_become_external_link' ); ?>"><i class="fa fa-graduation-cap"></i> <?php the_field( 'ms_become_button' ); ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>

        <!-- Introducing the Movement Specialist -->
        <section class="ms-intro">
            <span>Introducing the</span>

            <?php if ( get_field( 'ms_logo' ) ) : ?>
                <div class="ms-intro--logo" style="background: url('<?php the_field( 'ms_logo' ); ?>') center center/contain no-repeat"></div>
            <?php else : ?>
                <div class="ms-intro--logo"></div>
            <?php endif; ?>

            <h2><?php the_field( 'ms_logo_text' ); ?></h2>
        </section>

        <!-- Movement Specialist Details -->
        <section class="ms-intro-details">
            <h1 class="ms-title"><?php the_field( 'ms_details_title' ); ?></h1>

      <?php if (get_field('ms_details_image')) : ?>
        <img class="img-responsive" src="<?php the_field('ms_details_image'); ?>" alt="Receive one-to-one training from a Certified Tathata Golf Movement Specialist">
      <?php else : ?>
        <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/whatAreCMS650.jpg" alt="Receive one-to-one training from a Certified Tathata Golf Movement Specialist">
      <?php endif; ?>

            <article class="ms-content">
                <?php the_field( 'ms_details_content' ); ?>
            </article>
        </section>

        <!-- Training Opportunities -->
        <?php if ( have_rows( 'ms_opportunities' ) ): ?>
            <section id="new-ms-training" class="ms-training">
                <h1 class="ms-title"><?php the_field( 'ms_training_title' ); ?></h1>

        <ul class="ms-icon--grid">
          <?php while (have_rows('ms_opportunities')) : the_row(); ?>
            <li>
              <a class="<?php the_sub_field('ms_op_id'); ?>">
                <div class="ms-icon-container">
                  <img class="img-responsive" src="<?php the_sub_field('ms_op_icon'); ?>" alt="">
                </div>
                <span><?php the_sub_field('ms_op_title'); ?></span> </a>
            </li>
          <?php endwhile; ?>
        </ul>

                <!-- For the targets of each li -->
                <?php while ( have_rows( 'ms_opportunities' ) ) : the_row(); ?>
                    <div id="<?php the_sub_field( 'ms_op_id' ); ?>" class="ms-description--container">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="ms-description--image" style="background: url('<?php the_sub_field( 'ms_op_desc_image' ); ?>') center center/cover no-repeat"></div>
                                </div>

                                <div class="col-xs-6">
                                    <div class="ms-description--detail">
                                        <div>
                                            <h1 class="ms-title"><?php the_sub_field( 'ms_op_desc_title' ); ?></h1>
                                            <?php the_sub_field( 'ms_op_description' ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </section>
        <?php endif; ?>

        <!-- Comparison Chart -->
        <?php if ( get_field( 'ms_compare_section_title' ) ) : ?>
            <section id="msComparison">
                <h1 class="ms-title"><?php the_field( 'ms_compare_section_title' ); ?></h1>

                <div class="msComparison--container">
                    <?php if ( have_rows( 'ms_comparison_item' ) ) : while ( have_rows( 'ms_comparison_item' ) ) : the_row(); ?>
                        <div class="msComparison-item">
                            <h3><?php the_sub_field( 'ms_compare_title' ); ?></h3>

                            <nav>
                                <ol>
                                    <?php if ( have_rows( 'ms_compare_list' ) ) : while ( have_rows( 'ms_compare_list' ) ) : the_row(); ?>
                                        <li><?php the_sub_field( 'ms_compare_listitem' ); ?></li>
                                    <?php endwhile; endif; ?>
                                </ol>
                            </nav>
                        </div>
                    <?php endwhile; endif; ?>
                </div>
            </section>
        <?php endif; ?>

        <!-- Information Break -->
        <?php if ( get_field( 'ms_break_background' ) ) : ?>
        <section class="ms-info" style="background: linear-gradient(rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0.50)), url('<?php the_field( 'ms_break_background' ); ?>') center center/cover no-repeat">
        <?php else : ?>
        <section class="ms-info" style="background: linear-gradient(rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0.50)), url('<?php echo get_template_directory_uri(); ?>/assets/img/correctMovement.jpg') center center/cover no-repeat">
        <?php endif; ?>
            <div>
                <div class="ms-info-content">
                    <h1 class="ms-title"><?php the_field( 'ms_break_title' ); ?></h1>
                </div>

                <div class="ms-info-content">
                    <?php the_field( 'ms_break_content' ); ?>
                </div>
            </div>
        </section>

        <!-- Call to Action -->
        <section class="ms-cta">
            <h1 class="ms-title"><?php the_field( 'ms_cta_title' ); ?></h1>

            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <?php if ( get_field( 'ms_find_internal_link' ) ) : ?>
                            <a class="tg-btn" href="<?php the_field( 'ms_find_internal_link' ); ?>"><i class="fa fa-search"></i> <?php the_field( 'ms_find_button' ); ?></a>
                        <?php else : ?>
                            <a class="tg-btn" href="<?php the_field( 'ms_find_external_link' ); ?>"><i class="fa fa-search"></i> <?php the_field( 'ms_find_button' ); ?></a>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <?php if ( get_field( 'ms_become_internal_link' ) ) : ?>
                            <a class="tg-btn" href="<?php the_field( 'ms_become_internal_link' ); ?>"><i class="fa fa-graduation-cap"></i> <?php the_field( 'ms_become_button' ); ?></a>
                        <?php else : ?>
                            <a class="tg-btn" href="<?php the_field( 'ms_become_external_link' ); ?>"><i class="fa fa-graduation-cap"></i> <?php the_field( 'ms_become_button' ); ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>

        <!-- Sales -->
        <section class="ms-sales">
            <h1 class="ms-title"><?php the_field( 'ms_sales_title' ); ?></h1>
            <hr>
            <h2><?php the_field( 'ms_sales_sub_title' ); ?></h2>
        </section>

        <!-- Sales Content -->
        <section class="ms-sales-content">
            <?php if ( have_rows( 'ms_sales_barriers' ) ) : ?>
                <div class="container">
                    <?php while ( have_rows( 'ms_sales_barriers' ) ) : the_row(); ?>
                        <h2><?php the_sub_field( 'ms_barrier_title' ); ?></h2>

                        <div class="row">
                            <?php if ( get_sub_field( 'ms_barrier_solution2' ) ) : ?>
                                <div class="col-xs-12 col-md-6">
                                    <?php the_sub_field( 'ms_barrier_solution' ); ?>
                                </div>

                                <div class="col-xs-12 col-md-6">
                                    <?php the_sub_field( 'ms_barrier_solution2' ); ?>
                                </div>
                            <?php else : ?>
                                <div class="col-xs-12">
                                    <?php the_sub_field( 'ms_barrier_solution' ); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
        </section>

        <!-- Tathata Promise -->
        <section class="ms-promise">
            <h1 class="ms-title"><?php the_field( 'ms_promise_title' ); ?></h1>

            <div class="ms-content">
                <?php the_field( 'ms_promise_content' ); ?>
            </div>
        </section>

        <!-- Call to Action -->
        <section class="ms-cta">
            <h1 class="ms-title"><?php the_field( 'ms_cta_title' ); ?></h1>

            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <?php if ( get_field( 'ms_find_internal_link' ) ) : ?>
                            <a class="tg-btn" href="<?php the_field( 'ms_find_internal_link' ); ?>"><i class="fa fa-search"></i> <?php the_field( 'ms_find_button' ); ?></a>
                        <?php else : ?>
                            <a class="tg-btn" href="<?php the_field( 'ms_find_external_link' ); ?>"><i class="fa fa-search"></i> <?php the_field( 'ms_find_button' ); ?></a>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <?php if ( get_field( 'ms_become_internal_link' ) ) : ?>
                            <a class="tg-btn" href="<?php the_field( 'ms_become_internal_link' ); ?>"><i class="fa fa-graduation-cap"></i> <?php the_field( 'ms_become_button' ); ?></a>
                        <?php else : ?>
                            <a class="tg-btn" href="<?php the_field( 'ms_become_external_link' ); ?>"><i class="fa fa-graduation-cap"></i> <?php the_field( 'ms_become_button' ); ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <img class="beacon" border="0" src="http://r.turn.com/r/beacon?b2=P_AYcJBQXbQH0yAC_9IQpZhwxyZGxpt9A9pzJmqIATDwmwgCAYlZ5lTJGOrh3DGCbjopRYcdYaZeLRI9P-WP4Q&cid=">

<?php
get_footer();
