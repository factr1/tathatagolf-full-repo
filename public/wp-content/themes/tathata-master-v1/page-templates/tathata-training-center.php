<?php
// Template Name: Tathata Training Center

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <section class="tathata-training-center">
        <!-- Focus Areas -->
        <div class="focus-areas focus-areas_3">
            <?php if ( have_rows( 'th_featured_sections' ) ) : ?>
                <?php while ( have_rows( 'th_featured_sections' ) ) : the_row(); ?>
                    <a href="#<?php the_sub_field( 'th-fs_link' ); ?>" class="focus-area" title="<?php the_sub_field( 'th-fs_title' ); ?>" style="background-image: url(<?php the_sub_field( 'th-fs_background' ); ?>)">
                        <h2 class="focus-area_title"><?php the_sub_field( 'th-fs_title' ); ?></h2>
                        <p class="focus-area_content"><?php the_sub_field( 'th-fs_content' ); ?></p>
                    </a>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>

        <!-- Chapter Follow Ups -->
        <section id="th-follow-ups" class="th-follow-ups section fp-auto-height" <?php setACFBackground( 'th-section_background_1', 0, .3, null, null, true ); ?>>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="title"><?php the_field( 'title_1' ); ?></h1>
                        <?php the_field( 'th-section_intro_1' ); ?>
                    </div>
                </div>

                <div class="row -padded-v">
                    <div class="col-sm-6">
                        <?php the_field( 'th_content_area_1' ); ?>
                    </div>

                    <div class="col-sm-6">
                        <?php the_field( 'th_content_area_2' ); ?>
                    </div>
                </div>
            </div>
        </section>

        <!-- Divider -->
        <section id="info-break" class="info-break">
            <div class="container">
                <div class="content">
                    <q>The Tathata Outdoor Facility is like Disneyland for golf training.</q>
                    <p>- Brandel Chamblee</p>
                </div>
            </div>
        </section>

        <!-- 1-on-1 Training -->
        <section id="th-1-on-1" class="th-follow-ups section fp-auto-height" <?php setACFBackground( 'one_th-section_background_1', 0, .3, null, null, true ); ?>>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="title"><?php the_field( 'one_title_1' ); ?></h1>
                        <?php the_field( 'one_th-section_intro_1' ); ?>
                    </div>
                </div>

                <div class="row -padded-v">
                    <div class="col-sm-6">
                        <?php the_field( 'one_th_content_area_1' ); ?>
                    </div>

                    <div class="col-sm-6">
                        <?php the_field( 'one_th_content_area_2' ); ?>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <script>
        'use strict';
        jQuery(function() {
            var offset = - 30;
            var time = 700;

            jQuery('main').find('a').click(function() {
                jQuery('html, body').animate({
                    scrollTop: jQuery(jQuery(this).attr('href')).offset().top + offset
                }, time);

                return false;
            });
        });
    </script>

<?php
get_footer();
