<?php
/**
 * Template Name: Homepage - Video
 *
 * Redesigned homepage featuring a hero video
 */

get_header();
?>

    <section id="homepage-content">

        <!-- Hero -->
        <section class="home-hero">
            <!-- For mobile -->
            <div class="video-swap" style="background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.6)), url(<?php the_field( 'home-video_poster' ); ?>)">
                <div class="container">
                    <h1><?php the_field( 'home-overlay_title' ); ?></h1>
                    <a href="<?php setACFLink( 'home-overlay_internal_link', 'home-overlay_external_link' ); ?>" class="tg-btn"><?php the_field( 'overlay_link_text' ); ?></a>
                </div>
            </div>

            <!-- Hero Video -->
            <div class="video embed-responsive embed-responsive-16by9">
                <video autoplay loop preload="auto">
                    <source src="<?php the_field( 'home_video' ); ?>" type="video/mp4">
                    <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/homepage-video.webm" type="video/webm">
                    <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/homepage-video.ogv" type="video/ogg">
                </video>
            </div>

            <!-- Video Overlay -->
            <div class="video-overlay">
                <div class="container">
                    <h1><?php the_field( 'home-overlay_title' ); ?></h1>
                    <a href="<?php setACFLink( 'home-overlay_internal_link', 'home-overlay_external_link' ); ?>" class="tg-btn"><?php the_field( 'overlay_link_text' ); ?></a>
                </div>
            </div>
        </section>

        <!-- 60-Day Program CTA -->
        <section class="cta -light">
            <div class="container">
                <div class="col-xs-12 col-sm-6">
                    <div class="flex-column">
                        <img src="<?php the_field( 'home-cta_image' ); ?>" class="img-responsive" alt="The Tathata Golf 60-Day Program">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <div class="flex-column">
                        <h1 class="cta-title"><?php the_field( 'home-cta_content' ); ?></h1>
                        <a href="<?php setACFLink( 'home-cta_internal_link', 'home-cta_external_link' ); ?>" class="tg-btn">Learn More</a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Testimonial Ticker -->
        <?php get_template_part( 'parts/testimonial-globe' ); ?>

        <!-- Main Content -->
        <section class="content-container" <?php setACFBackground( 'home-content_background_image', .1, .4, 'top center' ); ?>>
            <div class="container">
                <div class="content">
                    <?php the_field( 'home-content' ); ?>
                </div>
            </div>
        </section>

        <!-- Solution Content -->
        <section class="solution-container">
            <div class="solution-content">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/TG_revolution_content.png" alt="" class="img-responsive">
            </div>

            <div class="solution-sliders">
                <?php if ( have_rows( 'ms_sliders' ) ) : ?>
                    <div id="solution-slider">
                        <?php while ( have_rows( 'ms_sliders' ) ) : the_row(); ?>
                            <?php $sbg = get_sub_field( 'ms_slider_background_color' ); ?>
                            <div class="solution-slide" <?php if ( $sbg ) {
                                echo 'style="background: ' . $sbg . '"';
                            } ?>>
                                <h3 class="title"><?php the_sub_field( 'ms_slider_slide_title' ); ?></h3>
                                <p class="content"><?php the_sub_field( 'ms_slider_slide_content' ); ?></p>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </div>
        </section>

    </section>

<?php
get_footer();
