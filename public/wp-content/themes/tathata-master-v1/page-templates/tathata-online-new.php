<?php
// Template Name: Tathata Online New

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <section id="tathata-online" class="tathata-online">

        <!-- Focus Areas -->
        <div class="focus-areas focus-areas_3">
            <?php if ( have_rows( 'featured_items' ) ) : ?>
                <?php while ( have_rows( 'featured_items' ) ) : the_row(); ?>
                    <a href="#<?php the_sub_field( 'link' ); ?>" class="focus-area" title="<?php the_sub_field( 'title' ); ?>" style="background-image: url(<?php the_sub_field( 'background' ); ?>)">
                        <h2 class="focus-area_title"><?php the_sub_field( 'title' ); ?></h2>
                        <p class="focus-area_content"><?php the_sub_field( 'content' ); ?></p>
                    </a>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>

        <!-- Live Video Follow-Ups -->
        <section id="live-videos" class="th-follow-ups -padded-v" <?php setACFBackground( 'background_image_2', 0, .3, null, null, true ); ?>>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="title"><?php the_field( 'title_1' ); ?></h1>
                        <?php the_field( 'intro_content_1' ); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?php the_field( 'content_area_1' ); ?>
                    </div>

                    <div class="col-sm-6">
                        <?php the_field( 'content_area_2' ); ?>
                    </div>
                </div>
            </div>
        </section>

        <!-- 1-on-1 Founder Session -->
        <section id="th-1-on-1" class="th-follow-ups section fp-auto-height" <?php setACFBackground( 'one_th-section_background_1', 0, .3, null, null, true ); ?>>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="title"><?php the_field( 'one_title_1' ); ?></h1>
                        <?php the_field( 'one_th-section_intro_1' ); ?>
                    </div>
                </div>

                <div class="row -padded-v">
                    <div class="col-sm-6">
                        <?php the_field( 'one_th_content_area_1' ); ?>
                    </div>

                    <div class="col-sm-6">
                        <?php the_field( 'one_th_content_area_2' ); ?>
                    </div>
                </div>
            </div>
        </section>

        <!-- Online Swing Submittal -->
        <section id="full-swing" class="th-follow-ups -padded-v" <?php setACFBackground( 'background_image_3', 0, .3, null, null, true ); ?>>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="title"><?php the_field( 'title_2' ); ?></h1>
                        <?php the_field( 'intro_content_2' ); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?php the_field( 'content_area_3' ); ?>
                    </div>

                    <div class="col-sm-6">
                        <?php the_field( 'content_area_4' ); ?>
                        <a href="<?php echo esc_url( home_url( '/product/online-swing-submittal/' ) ); ?>" class="tg-btn">Submit Your Swing</a>
                    </div>
                </div>
            </div>
        </section>

    </section>

    <script>
        'use strict';
        jQuery(function() {
            var offset = -30;
            var time = 700;

            jQuery('main').find('a').click(function() {
                jQuery('html, body').animate({
                    scrollTop: jQuery(jQuery(this).attr('href')).offset().top + offset
                }, time);

                return false;
            });
        });
    </script>

<?php
get_footer();
