<?php
// Template Name: 60 Days Broken Apart

// Redirect the user to the login page if they are not logged in
if ( ! is_user_logged_in() ) {
    wp_redirect( home_url( '/login/' ) );
    exit;
}

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

<?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section id="broken-apart" class="contentWrapper">
        <div class="container">
            <section class="row">
                <div class="col-xs-12">
                    <h1>Welcome To The 60-Day Training Program <span>Broken Apart</span></h1>
                    <h2><span>Congratulations</span> on graduating from the 60-Day Training Program</h2>

                    <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'full' );
                    }
                    ?>
                </div>
            </section>

            <section class="row">
                <div class="col-xs-12 col-md-6">
                    <?php
                    while ( have_posts() ) : the_post();
                        the_content();
                    endwhile;
                    ?>
                </div>

                <div class="col-xs-12 col-md-6">
                    <h3>View the chapter calendar breakdown</h3>

                    <a href="<?php echo esc_url( home_url( '/wp-content/uploads/2015/05/60DayCalendar.pdf' ) ); ?>">
                        <img class="img-responsive" src="<?php echo esc_url( home_url( '/wp-content/uploads/2015/05/calendar.jpg' ) ); ?>">
                    </a>
                </div>
            </section>

            <section class="row">
                <div class="col-xs-12">
                    <h2>Select your training video by category:</h2>

                    <?php
                    wp_nav_menu( array(
                        'menu'         => 'video menu',
                        'container_id' => 'brokenoutlist'
                    ) );
                    ?>
                </div>
            </section>
        </div>
    </section>

<?php
get_footer();
