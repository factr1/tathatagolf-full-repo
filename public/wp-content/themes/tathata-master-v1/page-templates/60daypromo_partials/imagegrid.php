<section class="featured_headline">
    <style>
        .featured_headline {
            position: relative;
            display: block;
            width: 100%;
            margin: 0 0 30px;
            background: rgb(102, 0, 0);
            color: rgb(255, 255, 255);
        }

        /* TODO: is this actually used? */
        .headline_bgimage {
            width: 100%;
            height: 100%;
            display: block;
            background: url('http://www.tathatagolf.com/wp-content/uploads/2015/10/bkg_slider-04.jpg') center center no-repeat;
            background-size: cover;
            position: absolute;
            z-index: -1;
        }

        .featured_headline .row {
            padding: 20px 25px;
            text-align: center;
        }

        .featured_headline h1 {
            color: rgb(255, 255, 255);
            font-size: 32px !important;
            text-align: center;
        }
    </style>

    <div class="container">
        <div class="row">
            <h1>Revolutionizing the way golf Is Taught and Played</h1>
        </div>
    </div>
</section>

<section class="image-grid">
    <div class="container">
        <div class="row">
            <p class="visible-xs" style="text-align:center; font-weight: bold">(Tap On Each Photo To See More)</p>

            <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">
                <?php if ( have_rows( '60day_image_grid', setEnvironmentID( $ids ) ) ): while ( have_rows( '60day_image_grid', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                    <li>
                        <div class="image-grid-container" style="background: url('<?php the_sub_field( '60day_ig_image' ); ?>') top center no-repeat;">
                            <div class="box-overlay">
                                <p class="text-center"><?php the_sub_field( '60day_ig_content' ); ?></p>
                            </div>

                            <p class="text-center grid-title"><?php the_sub_field( '60day_ig_title' ); ?></p>
                        </div>
                    </li>
                <?php endwhile; endif; ?>
            </ul>
        </div>
    </div>
</section>
