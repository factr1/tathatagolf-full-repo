<section class="testimonials toast">
    <div class="container">
        <!-- Pro Testimonials -->
        <div class="row">
            <div class="col-md-12 col-md-centered text-center">
                <h1>What Professionals Are Saying</h1>
            </div>
        </div>

        <div class="row single">
            <div class="col-xs-4 col-md-2">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/DebVangellow.jpg">
            </div>

            <div class="col-xs-8 col-md-10">
                <blockquote>"A teacher/student win-win for sure!"</blockquote>
                <cite>Deb Vangellow, LPGA President, 2012 National Teacher of the Year</cite>
            </div>
        </div>

        <div class="row single">
            <div class="col-xs-4 col-md-2 col-md-push-10">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/SueWieger.jpg">
            </div>

            <div class="col-xs-8 col-md-10 col-md-pull-2">
                <blockquote>"I've incorporated it into newer students, brand new players as well as tour players. They are absorbing it, they are loving it, they're getting better and the rapid pace they are getting better is incredible."</blockquote>
                <cite>Sue Wieger, LPGA, M. Ed., Peak Performance Coach and Golf Professional</cite>
            </div>
        </div>

        <div class="row single">
            <div class="col-xs-4 col-md-2">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/Ed-Gowan.jpg">
            </div>

            <div class="col-xs-8 col-md-10">
                <blockquote>"Tathata is a new and revolutionary concept for learning that helps the average golfers as well as the very accomplished one understand how to swing the club effectively."</blockquote>
                <cite>Ed Gowan, Executive Director of the Arizona Golf Association</cite>
            </div>
        </div>

        <div class="row single">
            <div class="col-xs-4 col-md-2 col-md-push-10">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/t-cir-JasonLoth.png">
            </div>

            <div class="col-xs-8 col-md-10 col-md-pull-2">
                <blockquote>"Tathata Golf doesn't just allow someone to play better golf, it allows them to stay injury free. It will also allow them to become healthier with everything built into the program."</blockquote>
                <cite>Dr. Jason Loth, Certified Chiropractic Sports Physician, Certified Strength and Conditioning Specialist</cite>
            </div>
        </div>

        <div class="row single">
            <div class="col-xs-4 col-md-2">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/Todd-Demsey.jpg">
            </div>

            <div class="col-xs-8 col-md-10">
                <blockquote>"With Bryan and Tathata, it just becomes a part of you, golf becomes fun again. You do the training and you're ready to play."</blockquote>
                <cite>Todd Demsey, Tour Professional, 4-Time All-American</cite>
            </div>
        </div>

        <div class="row single">
            <div class="col-xs-4 col-md-2 col-md-push-10">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/KarenDavies2.jpg">
            </div>

            <div class="col-xs-8 col-md-10 col-md-pull-2">
                <blockquote>"Tathata has given me a new way to look at how I play and teach the game and excited me to do more than I have ever done. My game has changed. I have more power, more presence and more enthusiasm. I get up in the morning and I want to go do it. That hasn't happened for more than a decade."</blockquote>
                <cite>Karen Davies, LPGA, 2010 LPGA T&amp;GC National Champion</cite>
            </div>
        </div>

        <!-- Student Testimonials -->
        <div class="row">
            <div class="col-md-12 col-md-centered text-center">
                <h1>What Students Are Saying</h1>
            </div>
        </div>

        <div class="row single">
            <div class="col-xs-4 col-md-2">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/PaxtonJevnick.png">
            </div>

            <div class="col-xs-8 col-md-10">
                <blockquote>"It's not a bunch of swing tips, its not a bunch of band-aids, its built from the ground up, the right way, and it's going to last for me, its not going to disappear tomorrow, it's in me."</blockquote>
                <cite>Paxton Jevnick, HDCP 12, Tathata Student</cite>
            </div>
        </div>

        <div class="row single">
            <div class="col-xs-4 col-md-2 col-md-push-10">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/DianeMott.png">
            </div>

            <div class="col-xs-8 col-md-10 col-md-pull-2">
                <blockquote>"Tathata Golf has definitely caused me to enjoy golf more and look forward to playing, look forward to shots that otherwise I might have feared."</blockquote>
                <cite>Diane Mott, HDCP 12, Tathata Student</cite>
            </div>
        </div>

        <div class="row single">
            <div class="col-xs-4 col-md-2">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/Rob-Mangini.jpg">
            </div>

            <div class="col-xs-8 col-md-10">
                <blockquote>"I've been fortunate to have worked with some of the worlds top-rated instructors. Tathata Golf is definitely on an entirely different learning level."</blockquote>
                <cite>Rob Mangini, HDCP +1, Tathata Student</cite>
            </div>
        </div>

        <div class="row single">
            <div class="col-xs-4 col-md-2 col-md-push-10">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/Alan-Jackson.jpg">
            </div>

            <div class="col-xs-8 col-md-10 col-md-pull-2">
                <blockquote>"I've never tried anything that compares to Tathata training. I make better contact, I'm hitting it longer, I just have more enthusiasm for the game."</blockquote>
                <cite>Alan Jackson, HDCP 15, Tathata Student</cite>
            </div>
        </div>
    </div>
</section>
