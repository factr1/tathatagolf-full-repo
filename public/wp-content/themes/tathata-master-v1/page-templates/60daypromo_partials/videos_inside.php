<div class="container twovideos">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h1 class="title" style="text-align: center; text-transform: uppercase; font-size: 1.85em; margin-bottom: .25em">A Message From <span class="red">Gary McCord</span> &amp; <span class="red">Brandel Chamblee</span></h1>

            <div class="flex-video vimeo widescreen">
                <iframe src="https://player.vimeo.com/video/134999203?color=ffffff&title=0&byline=0&portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>

