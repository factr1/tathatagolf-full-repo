<div class="header60day desktop">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gtimages/TrainingBanner2.jpg">

    <?php if (!is_page('ajga')) : ?>
        <span class="official-ajga">The Preferred Golf Training Program of the <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/partners/ajga.svg"></span>
    <?php endif; ?>
</div>

<div class="header60day mobile">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gtimages/TrainingMobile.jpg">

    <?php if (!is_page('ajga')) : ?>
        <span class="official-ajga">The Preferred Golf Training Program of the <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/partners/ajga.svg"></span>
    <?php endif; ?>
</div>

<div id="blackbar"></div>

<?php get_template_part( 'parts/corporate-partner' ); ?>

<section class="as-seen-on" style="border: none">
    <div class="container">
        <div class="row">
            <p>As seen on <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gtimages/nbc-golf.png" width="100" height="25"> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ESPN.png" width="100" height="25"></p>
        </div>
    </div>
</section>
