<div class="video-selection">
    <?php // Vimeo API bridge ?>
    <script src="https://f.vimeocdn.com/js/froogaloop2.min.js" defer></script>

    <div class="container">
        <div class="row">
            <?php if ( have_rows( '60day_intro_videos', setEnvironmentID( $ids ) ) ): while ( have_rows( '60day_intro_videos', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                <div class="col-md-4">
                    <div class="-screenshot hidden-xs hidden-sm" style="background: url('<?php the_sub_field( '60day_intro_video_screenshot' ); ?>') center center/cover no-repeat">
                        <a href="video" data-remodal-target="<?php the_sub_field( '60day_intro_video_title' ); ?>" class="ms-play"><i class="fa fa-play"></i></a>
                    </div>

                    <div class="flex-video vimeo widescreen visible-xs visible-sm">
                        <iframe src="https://player.vimeo.com/video/<?php the_sub_field( '60day_intro_video_video' ); ?>?color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>

                    <p class="video-title"><?php the_sub_field( '60day_intro_video_title' ); ?></p>

                    <p>Total Time (<?php the_sub_field( '60day_intro_video_duration' ); ?>)</p>

                    <div class="remodal hidden-xs hidden-sm" data-remodal-id="<?php the_sub_field( '60day_intro_video_title' ); ?>" data-remodal-options="hashTracking: false">
                        <style scoped>
                            .remodal-close {
                                right: 0;
                                left: initial;
                            }
                        </style>

                        <button data-remodal-action="close" class="remodal-close"></button>

                        <div class="flex-video vimeo widescreen video-top-video">
                            <iframe id="<?php the_sub_field( '60day_intro_video_video' ); ?>" src="https://player.vimeo.com/video/<?php the_sub_field( '60day_intro_video_video' ); ?>?api=1&amp;player_id=<?php the_sub_field( '60day_intro_video_video' ); ?>&amp;color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>
                    </div>

                    <?php // Reset the Vimeo video on modal close ?>
                    <script>
                        jQuery(function($) {
                            var iframe = $('#<?php the_sub_field( '60day_intro_video_video' ); ?>')[0];
                            var player = $f(iframe);

                            $(document).on('closed', '.remodal', function() {
                                player.api('unload');
                            });
                        });
                    </script>
                </div>
            <?php endwhile; endif; ?>
        </div>
    </div>

    <!-- Featured Videos Intro -->
    <section class="promo_intro" style="padding: 10px 0">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <p style="font-weight: 700; padding: 5px 0; font-size: 15px">For more free videos, previews, &amp; routines enter your email:</p>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <form class="hide-for-touch">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="email" name="initial_email" id="initial_email" value="" placeholder="Email Address" width="100%;">
                            </div>

                            <div class="col-md-6">
                                <input type="submit" value="Access My Free Videos" data-remodal-target="emailform" href="#" onclick="getEmail()">
                            </div>
                        </div>
                    </form>

                    <form class="show-for-touch">
                        <input type="submit" value="Enter Email and Access Free Videos" data-remodal-target="emailform" href="#">
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
