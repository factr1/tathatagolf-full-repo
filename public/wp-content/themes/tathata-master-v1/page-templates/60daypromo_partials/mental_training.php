<section class="mental-training">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1>Tathata Mental Training</h1>

                <h3><?php the_field( '60day_mental_description', setEnvironmentID( $ids ) ); ?></h3>

                <ul>
                    <?php if ( have_rows( '60day_mental_features', setEnvironmentID( $ids ) ) ): while ( have_rows( '60day_mental_features', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                        <li><?php the_sub_field( '60day_mental_feature' ); ?></li>
                    <?php endwhile; endif; ?>
                </ul>
            </div>
        </div>
    </div>
</section>
