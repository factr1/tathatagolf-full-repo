<section class="inside-the-program">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-centered text-center">
                <h1>What's Inside the Program?</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <p><strong>Chapter Breakdown:</strong></p>

                <ul>
                    <li><i class="fa fa-check-circle"></i> Chapter 1 | Days 1-10 | Body, Stretching &amp; Mind 1</li>
                    <li><i class="fa fa-check-circle"></i> Chapter 2 | Days 11-20 | Hands, Arms &amp; Mind 2</li>
                    <li><i class="fa fa-check-circle"></i> Chapter 3 | Days 21-30 | Pressure, Impact &amp; Mind 3</li>
                    <li><i class="fa fa-check-circle"></i> Chapter 4 | Days 31-40 | Speed, Strength &amp; Mind 4</li>
                    <li><i class="fa fa-check-circle"></i> Chapter 5 | Days 41-50 | Short Game, Putting &amp; Mind 5</li>
                    <li><i class="fa fa-check-circle"></i> Chapter 6 | Days 51-60 | Shape, Trajectory &amp; Mind 6</li>
                </ul>
            </div>

            <div class="col-md-6">
                <p><strong>Course Takeaways:</strong></p>

                <ul>
                    <li><i class="fa fa-plus-circle"></i> 140 training movements</li>
                    <li><i class="fa fa-plus-circle"></i> 14 different movement routines</li>
                    <li><i class="fa fa-plus-circle"></i> 3 greatest golfer and athletes movement videos</li>
                    <li><i class="fa fa-plus-circle"></i> 195 test questions and video answers</li>
                    <li><i class="fa fa-plus-circle"></i> 45 deeper discussions (mental and movement)</li>
                    <li><i class="fa fa-plus-circle"></i> 13 mental training lay down exercises</li>
                    <li><i class="fa fa-plus-circle"></i> 12 sitting/standing mental training exercises</li>
                </ul>
            </div>
        </div>
    </div>
</section>
