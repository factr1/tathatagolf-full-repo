<article class="darkbar fun-easy-convienent">
    <div class="container">
        <div class="row">
            <div class="small-centered col-md-6 col-md-uncentered ">
                <h1>Fun, Easy and Convenient</h1>
                <h3><?php the_field( '60day_fec_description', setEnvironmentID( $ids ) ); ?></h3>

                <ul>
                    <?php if ( have_rows( '60day_fec_features', setEnvironmentID( $ids ) ) ): while ( have_rows( '60day_fec_features', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                        <li><?php the_sub_field( '60day_fec_feature' ); ?></li>
                    <?php endwhile; endif; ?>
                </ul>

                <a href="<?php echo esc_url( home_url( '/additional-benefits/' ) ); ?>" class="greyLinkBtn additional_benefitsBTN">See additional benefits</a>
            </div>
        </div>
    </div>
</article>

<section class="TathataMovementTraining">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Tathata Movement Training</h2>

                <h4 class="text-center"><?php the_field( '60day_mt_description', setEnvironmentID( $ids ) ); ?></h4>

                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gtimages/60d-con-mSet-full.png">
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <ul>
                    <?php if ( have_rows( '60day_mt_features', setEnvironmentID( $ids ) ) ): while ( have_rows( '60day_mt_features', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                        <li><?php the_sub_field( '60day_mt_feature' ); ?></li>
                    <?php endwhile; endif; ?>
                </ul>
            </div>

            <div class="col-md-6">
                <ul>
                    <?php if ( have_rows( '60day_mt_features2', setEnvironmentID( $ids ) ) ): while ( have_rows( '60day_mt_features2', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                        <li><?php the_sub_field( '60day_mt_feature2' ); ?></li>
                    <?php endwhile; endif; ?>
                </ul>
            </div>
        </div>

        <hr>

        <h2 style="text-align: center; font-size: 24px; line-height: 1.5; font-weight: 300">Based on a combination of movements from the <span class="red">greatest golfers and athletes</span> of all-time, and <span class="red">timeless martial-art</span> movement and striking principles.</h2>
    </div>
</section>
