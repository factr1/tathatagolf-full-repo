<article class="CompleteTraining">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="red" style="padding-top: 40px;">
                    A Complete Learning and<br>
                    Training Curriculum
                </h1>

                <p>The world's most thorough and complete mind, body and swing training curriculum ever created for golfers of all ages, body types and ability levels. </p>

                <p>Tathata students quickly realize they are capable of much greater learning and retention while being guided through a strategically built and structured path of learning than what is currently seen in golf instruction. For the first time ever, students are discovering not only how they can improve in their games, but also come to understand all the things they already do well in their swing and dozens of complimentary movements to add to their natural efficiencies.</p>

                <p>With the most transformational and empowering learning and training experience ever offered in the game of golf, you are just 2 months away from playing golf at a level far beyond anything you have ever thought possible and never looking back. </p>
            </div>

            <div class="col-md-6">
                <img src="<?php echo esc_url( home_url( '/wp-content/uploads/2015/04/Tathata-Curriculum.png' ) ); ?>" style="padding:30px; width:70%; margin:0 auto;">

                <h1 class="red" style="padding:0 20px 10px;">Improve Every Aspect Of Your Game</h1>

                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <li>Full Swing</li>
                            <li>Pitching</li>
                            <li>Chipping</li>
                            <li>Flop/lob shots</li>
                        </ul>
                    </div>

                    <div class="col-md-6">
                        <ul>
                            <li>Bunker shots</li>
                            <li>Putting</li>
                            <li>All shapes</li>
                            <li>All trajectories</li>
                        </ul>
                    </div>

                    <div class="clearfix"></div>

                    <a href="<?php echo esc_url( home_url( '/additional-benefits' ) ); ?>" class="greyLinkBtn additional_benefitsBTN" style="min-width:35%; text-align: center; margin:10px auto; padding: 10px 20px">See additional benefits</i></a>
                </div>
            </div>
        </div>
    </div>
</article>
