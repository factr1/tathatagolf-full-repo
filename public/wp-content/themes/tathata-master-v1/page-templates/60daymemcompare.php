<?php
/**
 * Template Name: 60 Day Mem Compare
 */

get_header();
getPageHero();
?>

<div id="blackbar"></div>

<article class="captionWrap">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <?php
                if ( get_field( "training_black_box_content" ) ):
                    the_field( "training_black_box_content" );
                endif;
                ?>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="redBoxContent">
                    <?php
                    $args = array(
                        'post_type'      => 'product',
                        'posts_per_page' => 1,
                        'product_cat'    => 'streaming-and-memberships',
                        'orderby'        => 'rand'
                    );
                    $loop = new WP_Query( $args );

                    while ( $loop->have_posts() ) : $loop->the_post();
                        global $product;
                        ?>
                        <div class="woocommerce">
                            <p class="wootitle"><?php the_title(); ?></p>

                            <p class="woobottom">
                                <a href="<?php echo do_shortcode( '[add_to_cart_url id="4388"]' ); ?>" title="<?php echo esc_attr( $loop->post->post_title ? $loop->post->post_title : $loop->post->ID ); ?>" class="greyLinkBtn">Start Training Today</a>
                                <span class="price"> <?php echo $product->get_price_html(); ?></span>
                            </p>

                            <p style="clear:both">
                                <a href="<?php echo esc_url( home_url( '/?add_to_cart=8579' ) ); ?>" style="color:#fff; text-decoration:underline;">Add on the 30-Disc DVD set for offline viewing</a>
                            </p>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div>
</article>

<?php get_template_part( 'parts/breadcrumbs' ); ?>

<section class="contentWrapper storypage60day">
    <section class="pageInnerContentWrap pageInner60Wrap">
        <div class="container">
            <div class="row onlineTopwrap rightWrap">
                <div class="row-inner">
                    <?php
                    while ( have_posts() ) : the_post();
                        the_content();
                    endwhile
                    ?>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>

    <?php testimonialSlider( '60-day-training-program' ); ?>

    <div class="clearfix"></div>

<?php
get_footer();
