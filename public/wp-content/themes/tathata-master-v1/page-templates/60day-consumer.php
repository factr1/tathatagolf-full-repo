<?php
/**
 * Template Name: 60 Day Program Consumer Page
 *
 * This is the template that displays home page with multiple sections.
 */

get_header();
getPageHero();
?>

    <div class="golfSchoolConsumer">

        <div id="blackbar"></div>

        <article class="captionWrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7">
                        <?php
                        if ( get_field( "black_box_content" ) ):
                            the_field( "black_box_content" );
                        endif;
                        ?>
                    </div>

                    <div class="col-xs-12 col-sm-5 col-md-5">
                        <!-- Include the redbox content ( rating systems, etc..) -->
                        <div class="redBoxContent">
                            <?php
                            $args = array(
                                'post_type'      => 'product',
                                'posts_per_page' => 1,
                                'product_cat'    => 'streaming-and-memberships',
                                'orderby'        => 'rand'
                            );
                            $loop = new WP_Query( $args );

                            while ( $loop->have_posts() ) : $loop->the_post();
                                global $product;
                                ?>
                                <div class="woocommerce">
                                    <p class="wootitle"><?php the_title(); ?></p>
                                    <p class="woobottom">
                                        <a href="<?php echo do_shortcode( '[add_to_cart_url id="4388"]' ); ?>" title="<?php echo esc_attr( $loop->post->post_title ? $loop->post->post_title : $loop->post->ID ); ?>" class="greyLinkBtn"> Start Training Today</a>
                                        <span class="price"> <?php echo $product->get_price_html(); ?></span>
                                    </p>

                                    <p style="clear:both">
                                        <a href="<?php echo esc_url( home_url( '/?add_to_cart=8579' ) ); ?>" style="color:#fff; text-decoration:underline;">Add on the 30-Disc DVD set for offline viewing</a>
                                    </p>
                                </div>
                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>

        <?php get_template_part( 'parts/breadcrumbs' ); ?>

        <section class="pageInnerContentWrap">
            <div class="container">
                <div class="row onlineTopwrap">
                    <article class="col-md-6 col-sm-6 col-xs-12 Days60Consumer onlinetopPart">
                        <?php
                        while ( have_posts() ) : the_post();
                            the_content();
                        endwhile;
                        ?>
                    </article>

                    <article class="col-xs-12 col-sm-6 col-md-6 golfdemowrap onlinetopPart">
                        <?php
                        if ( get_field( "right_side_content" ) ):
                            the_field( "right_side_content" );
                        endif;
                        ?>
                    </article>

                    <article class="col-xs-12 col-sm-6 col-md-6 pull-right testmonialRightText onlinetopPart">
                        <?php
                        if ( get_field( "right_side_quote" ) ):
                            the_field( "right_side_quote" );
                        endif;
                        ?>
                    </article>
                </div>
            </div>
        </section>

        <section class="OnlineacademyConsumerWrap">
            <div class="container">
                <?php
                if ( get_field( "consumer_label" ) ):
                    the_field( "consumer_label" );
                endif;
                ?>
            </div>
        </section>

        <?php get_template_part( 'parts/money-back' ); ?>

        <section class="pageInnerContentWrap courseChapter golf-60-dayTrain">
            <div class="container">
                <div class="row">
                    <article class="col-xs-12 col-sm-6 col-md-6 onlinetopPart">
                        <?php
                        if ( get_field( "complete_left_content" ) ):
                            the_field( "complete_left_content" );
                        endif;
                        ?>
                    </article>

                    <article class="col-xs-12 col-sm-6 col-md-6 right onlinetopPart">
                        <?php
                        if ( get_field( "complete_right_content" ) ):
                            the_field( "complete_right_content" );
                        endif;
                        ?>
                    </article>
                </div>

                <div class="row">
                    <?php
                    if ( get_field( "training_wrap_image" ) ):
                        the_field( "training_wrap_image" );
                    endif;
                    ?>
                </div>

                <div style="margin-top:55px;"></div>

                <div class="row bodySwing-3col">
                    <?php
                    if ( get_field( "block_main_title" ) ):
                        the_field( "block_main_title" );
                    endif;
                    ?>

                    <div class="col-toggle">
                        <article class="col-xs-12 col-sm-4 col-md-4 toggle-col">
                            <?php
                            if ( get_field( "block_content_1" ) ):
                                the_field( "block_content_1" );
                            endif;
                            ?>
                        </article>

                        <article class="col-xs-12 col-sm-4 col-md-4 toggle-col">
                            <?php
                            if ( get_field( "block_content_2" ) ):
                                the_field( "block_content_2" );
                            endif;
                            ?>
                        </article>

                        <article class="col-xs-12 col-sm-4 col-md-4 toggle-col">
                            <?php
                            if ( get_field( "block_content_3" ) ):
                                the_field( "block_content_3" );
                            endif;
                            ?>
                        </article>
                    </div>
                </div>

                <div class="row bodySwing-3col">
                    <?php
                    if ( get_field( "block_2__main_title" ) ):
                        the_field( "block_2__main_title" );
                    endif;
                    ?>

                    <div class="col-toggle">
                        <article class="col-xs-12 col-sm-4 col-md-4 toggle-col">
                            <?php
                            if ( get_field( "block_2_content_1" ) ):
                                the_field( "block_2_content_1" );
                            endif;
                            ?>
                        </article>

                        <article class="col-xs-12 col-sm-4 col-md-4 toggle-col">
                            <?php
                            if ( get_field( "block_2_content_2" ) ):
                                the_field( "block_2_content_2" );
                            endif;
                            ?>
                        </article>

                        <article class="col-xs-12 col-sm-4 col-md-4 toggle-col">
                            <?php
                            if ( get_field( "block_2_content_3" ) ):
                                the_field( "block_2_content_3" );
                            endif;
                            ?>
                        </article>
                    </div>
                </div>

                <div class="row">
                    <article class="col-xs-12 col-sm-6 col-md-6">
                        <?php
                        if ( get_field( "upper_left_footer" ) ):
                            the_field( "upper_left_footer" );
                        endif;
                        ?>
                    </article>

                    <article class="col-xs-12 col-sm-6 col-md-6 right">
                        <?php
                        if ( get_field( "upper_right_footer" ) ):
                            the_field( "upper_right_footer" );
                        endif;
                        ?>
                    </article>
                </div>
            </div>
        </section>

        <div class="clearfix"></div>

        <?php testimonialSlider( '60-day-training-program' ); ?>
        <?php get_template_part('parts/script', 'ajga'); ?>

    </div>

<?php
get_footer();
