<?php
/* Template Name: 7 Day Trial Expired */

get_header();
?>

    <section class="expired-hero">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Welcome to the <strong>Tathata Golf</strong> family!</h1>
                    <h3>This is <span class="red-light">your</span> place for <span class="red-light">creating new and incredible</span> outcomes on and off the golf course.</h3>
                    <p>Your authentic greatness is within you right now. From this moment forward, if you choose, you will begin to simply separate from it less and less.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="container seven-day-content">
        <div class="row">
            <div class="col-xs-12">
                <p class="expired-text">Your trial has expired.</p>

                <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
                    <article>
                        <?php the_content(); ?>
                    </article>
                <?php endwhile; endif; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <p>#BecomeYourGreatness</p>
            </div>

            <div class="col-md-6">
                <div class="redBoxContent">
                    <?php
                    $args = array(
                        'post_type'      => 'product',
                        'posts_per_page' => 1,
                        'product_cat'    => 'streaming-and-memberships',
                        'orderby'        => 'rand'
                    );
                    $loop = new WP_Query( $args );

                    while ( $loop->have_posts() ) : $loop->the_post();
                        global $product;
                        ?>
                        <div class="woocommerce">
                            <p class="wootitle"><?php the_title(); ?></p>

                            <p><?php the_content(); ?></p>

                            <p class="woobottom">
                                <a href="<?php echo do_shortcode( '[add_to_cart_url id="4388"]' ); ?>" title="<?php echo esc_attr( $loop->post->post_title ? $loop->post->post_title : $loop->post->ID ); ?>" class="greyLinkBtn">Start Training Today</a>

                                <div class="star-rating" title="Rated <?php echo( $product->get_average_rating() ? $product->get_average_rating() : '0' ); ?> out of 5">
                                    <span style="width: <?php echo $product->get_average_rating() * 20; ?>%;">
                                        <strong itemprop="ratingValue" class="rating"><?php echo( $product->get_average_rating() ? $product->get_average_rating() : '0' ); ?></strong> out of 5
                                    </span>
                                </div>

                                <div class="count">(<?php echo $product->get_rating_count(); ?>)</div>
                                <span class="price"><?php echo $product->get_price_html(); ?></span>
                            </p>
                        </div>
                    <?php endwhile; wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
