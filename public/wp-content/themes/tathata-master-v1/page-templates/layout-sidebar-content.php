<?php
/**
 * Template Name: Layout - Sidebar/Content
 *
 * A max width blank page layout.
 * Max width is determined by the Bootstrap grid container class.
 * This layout features the default sidebar on the left, and the content on the right
 */

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section id="main-content" class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <?php get_sidebar(); ?>
                </div>

                <div class="col-xs-12 col-md-8">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <?php the_content(); ?>
                        </article><!-- #post-## -->
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
