<?php
/**
 * Template Name: Testing New Features
 */

get_header();
getPageHero();
?>

    <section id="test-body">
        <section class="test-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                            <?php the_posts_pagination(); ?>
                        <?php else: ?>
                            <p>No posts yet.</p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </section>

<?php
get_footer();
