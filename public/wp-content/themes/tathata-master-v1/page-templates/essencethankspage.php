<?php
/**
 * Template Name: Essence Thanks Page
 */

get_header();
getPagehero();
?>

    <section class="contentWrapper">
        <article class="essence-thankyou-page pageTitleWrap essence-section" style="height: auto">
            <style scoped>
                .breadcrumb li,
                .breadcrumb li a {
                    color: rgb(255, 255, 255);
                }
            </style>

            <?php get_template_part( 'parts/breadcrumbs' ); ?>

            <div class="container">
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; ?>
            </div>
        </article>
    </section>

<?php
get_footer();
