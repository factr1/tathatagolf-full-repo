<?php
/**
 * Template Name: Layout - Container
 *
 * A max width blank page layout.
 * Max width is determined by the Bootstrap grid container class.
 */

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section id="main-content" class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <?php the_content(); ?>
                        </article><!-- #post-## -->
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
