<?php
/**
 * Template Name: Final Congrats Page
 */

// Redirect the user to the login page if they are not logged in
if ( ! is_user_logged_in() ) {
    wp_redirect( home_url( '/login/' ) );
    exit;
}

global $post, $woothemes_sensei, $current_user;

// Get User Meta
get_currentuserinfo();

// get status of day 60 quiz
$day_60_id = 2624;
$status    = WooThemes_Sensei_Utils::sensei_user_quiz_status_message( $day_60_id, $current_user->ID );

// if day 60 quiz pass or 100%
if ( $status['status'] == "passed" ) {

}

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <?php get_template_part( 'parts/breadcrumbs' ); ?>

    <div class="container final-congrats-wrap">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="congrats-title">Congratulations!</h1>
                <p class="congrats-message"> You have completed the Tathata Golf 60-Day Training Program</p>
            </div>
        </div>
    </div>

    <div class="final-congrats-graphics-wrap">
        <div class="final-congrats-graphics">
            <img src="<?php echo esc_url( home_url( '/wp-content/uploads/sites/5/2015/06/final-congrats-global-graphic.png' ) ); ?>">
        </div>
    </div>

    <section class="contentWrapper">
        <div class="container final-congrats-wrap">
            <?php while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; ?>
            </div>
    </section>

    <div class="clearfix"></div>

<?php
get_footer();
