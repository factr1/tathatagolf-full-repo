<?php
/**
 * Template Name: Golf Schools Consumer
 *
 * This is the template that displays home page with multiple sections.
 */

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <div class="golfSchoolConsumer">
        <article class="captionWrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6" align="center" style="margin-top:40px;">
                        <?php
                        if ( get_field( "black_box_content" ) ):
                            the_field( "black_box_content" );
                        endif;
                        ?>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="redBoxContent">
                            <div class="woocommerce">
                                <p class="wootitle">New 2 1/2 Day Live Grad School Experiences</p>

                                <p>Enjoy a one of a kind Tathata Live Training Experience upon graduation from the 60-Day Program </p>

                                <p class="woobottom">
                                    <a href="<?php echo esc_url( home_url( '/tathata-headquarters' ) ); ?>" title="Get Started with the 60-Day Program" class="greyLinkBtn">See Details &amp; Dates</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>

        <?php setBreadcrumbs( 'Golf Schools' ); ?>

        <section class="pageInnerContentWrap">
            <div class="container">
                <div class="row golfconsumerrow">
                    <article class="col-xs-12 col-md-6">
                        <?php
                        // Start the Loop.
                        while ( have_posts() ) : the_post();

                            // Include the page content template.
                            get_template_part( 'parts/content', 'page' );

                        endwhile;
                        ?>
                    </article>

                    <article class="col-xs-12 col-md-6">
                        <?php
                        if ( get_field( "tathata_curriculum" ) ):
                            the_field( "tathata_curriculum" );
                        endif;
                        ?>
                    </article>
                </div>
            </div>
        </section>

        <section class="GolfschoolConsumerWrap pageInnerContentWrap">
            <div class="GolfschoolConsumerWrapPara">
                <?php
                if ( get_field( "consumer_label" ) ):
                    the_field( "consumer_label" );
                endif;
                ?>
            </div>
        </section>

        <section class="golfSchoolConcept pageInnerContentWrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6 golfSchoolwhat">
                        <?php
                        if ( get_field( "content_left" ) ):
                            the_field( "content_left" );
                        endif;
                        ?>
                    </div>

                    <div class="col-xs-12 col-md-6 golfSchoolwhat">
                        <?php
                        if ( get_field( "content_right" ) ):
                            the_field( "content_right" );
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="GolfprofileThumbnailWrap">
            <div class="container golfSchoolBox">
                <div class="row">
                    <article class="col-xs-12 col-sm-4">
                        <div>
                            <h2 class="inOutTraining">Home <span class="red">Campus</span></h2>

                            <?php
                            if ( get_field( "intro_section1" ) ):
                                the_field( "intro_section1" );
                            endif;
                            ?>
                        </div>
                    </article>

                    <article class="col-xs-12 col-sm-4">
                        <div>
                            <h2 class="complete">Education</h2>

                            <?php
                            if ( get_field( "intro_section2" ) ):
                                the_field( "intro_section2" );
                            endif;
                            ?>
                        </div>
                    </article>

                    <article class="col-xs-12 col-sm-4">
                        <div>
                            <h2 class="mentalExercises">Advanced <span class="red">Curriculum</span></h2>

                            <?php
                            if ( get_field( "intro_section3" ) ):
                                the_field( "intro_section3" );
                            endif;
                            ?>
                        </div>
                    </article>
                </div>
            </div>
        </section>

        <?php testimonialSlider( 'golf-schools' ); ?>
    </div>

<?php
get_footer();
