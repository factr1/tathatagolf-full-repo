<?php
// Template Name: Affiliate Welcome

get_header();
?>

    <div id="blackbar"></div>

    <?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section class="affiliate-welcome">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <article>
                        <h1 class="page-title"><?php the_field( 'title' ); ?></h1>

                        <?php the_field( 'content' ); ?>

                        <div class="embed-responsive embed-responsive-16by9">
                            <?php the_field('video'); ?>
                        </div>
                    </article>
                </div>
            </div>
        </div>

        <div class="darkbar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Complete Your Affiliate Set Up in 3 Steps!</h2>
                        <p>Step 1: <a href="<?php echo esc_url( home_url( '/wp-content/uploads/2016/08/AffiliatePacket-TathataGolf.pdf' ) ); ?>" download>Download the affiliate packet</a></p>
                        <p>Step 2: <a href="<?php the_field( 'contract' ); ?>">Review contract terms</a></p>
                        <p>Step 3: Fill out the form below</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php gravity_form( get_field('form_id'), false, false, false, '', false ); ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
