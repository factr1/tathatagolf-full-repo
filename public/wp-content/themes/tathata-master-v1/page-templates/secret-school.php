<?php
// Template Name: Secret School

$section_title = get_field( 'title' ) ? get_field( 'title' ) : get_field( 'navigation_title' );

$section_background = '';

if ( 'color' == get_field('background') ) :
    $section_background = sprintf('style="background-color: %s"', get_field('background_color'));
elseif ( 'image' == get_field('background') ) :
    $section_background = sprintf('style="background-image: url(%s)"', get_field('background_image'));
endif;

get_header();
getPageHero(); ?>

<div id="blackbar"></div>

<section id="page-golf-schools" class="golf-schools reset-bootstrap">
    <?php if (get_field( 'video' )) : ?>
        <section class="video-message">
            <div class="container">
                <h1 class="title"><?php the_field( 'video_title' ); ?></h1>

                <div class="embed-responsive embed-responsive-16by9">
                    <?php the_field( 'video' ); ?>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <section class="page-section" <?php echo $section_background; ?>>
        <div class="container">
            <div class="row" style="margin-bottom: 20px">
                <div class="col-xs-12">
                    <h1 class="title"><?php echo $section_title; ?></h1>

                    <?php if (get_field( 'content' )) : ?>
                        <?php the_field( 'content' ); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <?php if ( 'left' == get_field('show_hide_schedule') ) : ?>
                        <?php get_template_part('parts/booking', 'single'); ?>
                    <?php else : ?>
                        <?php the_field( 'content_left' ); ?>
                    <?php endif; ?>
                </div>

                <div class="col-sm-6">
                    <?php if ( 'right' == get_field('show_hide_schedule') ) : ?>
                        <?php get_template_part('parts/booking', 'single'); ?>
                    <?php else : ?>
                        <?php the_field( 'content_right' ); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</section>

<?php
get_footer();
