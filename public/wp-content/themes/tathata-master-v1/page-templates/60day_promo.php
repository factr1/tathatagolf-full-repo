<?php
/**
 * Template Name: 60 Day promo staged
 */

// Grab the page IDs where the videos live
$ids = [ 1917 ];

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <div id="gtwrapper">
        <!-- As Seen On -->
        <section class="as-seen-on" style="border: none">
            <div class="container">
                <div class="row">
                    <p>As seen on <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gtimages/nbc-golf.png" width="100" height="25"> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ESPN.png" width="100" height="25"></p>
                </div>
            </div>
        </section>

        <!-- Ticker -->
        <?php get_template_part( 'parts/testimonial-ticker' ); ?>
        <?php include_once '60daypromo_partials/videos_top.php'; ?>

        <!-- Call to Action -->
        <section class="product-buy">
            <div class="container">
                <div class="row">
                    <h1 class="col-md-12 text-center">The Revolutionary 60-Day<br>In-Home Golf Training Program</h1>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60Day.png" alt="Online instant Access on all devices">
                    </div>

                    <div class="col-md-4">
                        <div class="redBoxContent">
                            <div class="woocommerce">
                                <p class="wootitle">The 60-Day Training Program</p>

                                <p>Immediate and lifetime streaming access to the 60-Day Program.</p>

                                <p class="woobottom">
                                    <a href="<?php echo esc_url( home_url( '/?add_to_cart=8251' ) ); ?>" title="The 60-Day Training Program" class="greyLinkBtn">Buy Now <i class="fa fa-angle-right"></i></a>
                                    <span class="price">
                                        <span class="amount">$179.95 - Start Training Today!</span>
                                    </span>
                                </p>
                            </div>
                        </div>

                        <p class="dvd-addon">
                            <a href="<?php echo esc_url( home_url( '/?add_to_cart=8579' ) ); ?>">
                                Add on the 30-Disc<br>
                                DVD set for offline viewing
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <?php
        include_once '60daypromo_partials/imagegrid.php';
        include_once '60daypromo_partials/inside_program.php';
        include_once '60daypromo_partials/videos_inside.php';
        ?>

        <section class="instant-access">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60dayAccess.jpg" alt="Get online access to mobile devices for the Tathata 60-Day program" width="1000" height="600">
                    </div>

                    <div class="col-md-4">
                        <h2>Train Anywhere</h2>
                        <p>Enjoy our streaming product on any device wherever you may be.</p>

                        <div class="redBoxContent">
                            <div class="woocommerce">
                                <p class="wootitle">The 60-Day Training Program</p>

                                <p>Immediate and lifetime streaming access to the 60-Day Program.</p>

                                <p class="woobottom">
                                    <a href="<?php echo esc_url( home_url( '/?add_to_cart=8251' ) ); ?>" title="The 60-Day Training Program" class="greyLinkBtn">Buy Now <i class="fa fa-angle-right"></i></a>
                                    <span class="price">
                                        <span class="amount">$179.95 - Start Training Today!</span>
                                    </span>
                                </p>
                            </div>
                        </div>

                        <p class="dvd-addon">
                            <a href="<?php echo esc_url( home_url( '/?add_to_cart=8579' ) ); ?>">
                                Add on the 30-Disc<br>
                                DVD set for offline viewing
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <?php get_template_part( 'parts/money-back' ); ?>

        <!-- Email CTA -->
        <section class="email-signup">
            <div class="email-signup_bgimage1"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Free Video Giveaway</h3>
                        <p>Enter your e-mail for immediate access and learn how Tathata Golf will help your game.</p>
                    </div>

                    <div class="col-md-6">
                        <form class="hide-for-touch">
                            <input type="email" name="initial_email" id="initial_email" value="" placeholder="Email Address" width="100%;">
                            <br>
                            <input type="submit" value="Access My Free Videos" data-remodal-target="emailform" href="#" onclick="getEmail()">
                        </form>

                        <form class="show-for-touch">
                            <input type="submit" value="Enter Email and Access Free Videos" data-remodal-target="emailform" href="#">
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <!-- Sliders -->
        <?php if ( have_rows( 'ms_sliders', setEnvironmentID( $ids ) ) ): ?>
            <section id="ms-linked-sliders">
                <div class="holder"><h1>The Change Golf Has Been Waiting For</h1></div>

                <div id="msInfoSliders" class="ms-blocks--row" style="background: #f7f7f7">
                    <?php while ( have_rows( 'ms_sliders', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                        <div class="ms-blocks--column ms-half">
                            <div class="ms-blocks--column">
                                <h3 class="golf-industry"><?php the_sub_field( 'ms_sub_slider_title', setEnvironmentID( $ids ) ); ?></h3>

                                <?php if ( have_rows( 'ms_sub_slider', setEnvironmentID( $ids ) ) ): ?>
                                    <div class="msLinkedSlider">
                                        <?php while ( have_rows( 'ms_sub_slider', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                                            <div class="msLinkedSlider--slide">
                                                <div class="giSlide-container">
                                                    <p class="ms-number--title"><?php the_sub_field( 'ms_slider_slide_title', setEnvironmentID( $ids ) ); ?></p>

                                                    <div class="giSlide-content">
                                                        <span class="ms-number"><?php the_sub_field( 'ms_slider_slide_id', setEnvironmentID( $ids ) ); ?></span>
                                                        <p><?php the_sub_field( 'ms_slider_slide_content', setEnvironmentID( $ids ) ); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>

                                <p class="swipe-text">Swipe left or right for more </p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </section>
        <?php endif; ?>

        <?php
        get_template_part( 'parts/additional-support' );
        include_once '60daypromo_partials/final_features.php';
        ?>

        <section class="cta email-signup">
            <div class="email-signup_bgimage2"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h2 style="color:#fff; font-size:32px;">Experience golf's <span>most transformational and empowering</span> training opportunity ever introduced to the game.</h2>
                    </div>

                    <div class="col-md-4">
                        <div class="redBoxContent">
                            <div class="woocommerce">
                                <p class="wootitle">The 60-Day Training Program</p>

                                <p>Immediate and lifetime streaming access to the 60-Day Program.</p>

                                <p class="woobottom">
                                    <a href="<?php echo esc_url( home_url( '/?add_to_cart=8251' ) ); ?>" title="The 60-Day Training Program" class="greyLinkBtn">Buy Now <i class="fa fa-angle-right"></i></a>
                                    <span class="price">
                                        <span class="amount">$179.95 - Start Training Today!</span>
                                    </span>
                                </p>
                            </div>
                        </div>

                        <p class="dvd-addon">
                            <a href="<?php echo esc_url( home_url( '/?add_to_cart=8579' ) ); ?>">
                                Add on the 30-Disc<br>
                                DVD set for offline viewing
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <?php
        include_once '60daypromo_partials/mental_training.php';
        include_once '60daypromo_partials/testimonials.php';
        include_once '60daypromo_partials/complete_training.php';
        ?>

    </div><!-- END gtwrapper -->

    <div class="remodal" data-remodal-id="emailform" data-remodal-options="hashTracking: false">
        <style scoped>
            .remodal-close {
                right: 0;
                left: initial;
            }
        </style>

        <button data-remodal-action="close" class="remodal-close"></button>

        <h1>Free Video Giveaway</h1>
        <p>Enter your e-mail for immediate access and <br>learn how Tathata Golf will help your game.</p>

        <?php gravity_form( 4, false, false, false, '', false ); ?>

        <script>
            var userEmail;
            var getEmail = function() {
                userEmail = document.getElementById('initial_email').value;
                jQuery('#input_4_2').val(userEmail);
            }
        </script>
    </div>

<?php get_template_part('parts/script', 'ajga'); ?>

<?php // SharpSpring Form tracking ?>
<script type="text/javascript">
    var __ss_noform = __ss_noform || [];
    __ss_noform.push([
        'baseURI', 'https://app-3QDIROJGW8.marketingautomation.services/webforms/receivePostback/MzYwtzC2sDS3AAA/'
    ]);
    __ss_noform.push(['endpoint', '460fedef-9c50-40b9-8f8a-77ec0cd0b573']);
</script>
<script type="text/javascript" src="https://koi-3QDIROJGW8.marketingautomation.services/client/noform.js?ver=1.24"></script>

<?php // Beacon ?>
<img class="beacon" border="0" src="http://r.turn.com/r/beacon?b2=P_AYcJBQXbQH0yAC_9IQpZhwxyZGxpt9A9pzJmqIATDwmwgCAYlZ5lTJGOrh3DGCbjopRYcdYaZeLRI9P-WP4Q&cid=">

<?php // Facebook Pixel ?>
<script>fbq('track', 'ViewContent');</script>

<?php
get_footer();
