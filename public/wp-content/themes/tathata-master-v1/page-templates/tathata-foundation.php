<?php
// Template Name: Tathata Golf Foundation

$pageID = 1497; // Page ID of the Essence page
get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <section id="tathata-foundation" class="tathata-foundation">
        <section class="cta -grow-the-game flex-column" style="background: url('<?php the_field( 'e_call_to_action_background', $pageID ); ?>') top center/cover no-repeat">
            <div class="container">
                <?php the_field('e_call_to_action', $pageID); ?>
            </div>
        </section>

        <div class="content -padded-v">
            <div class="container">
                <?php
                if ( have_posts() ) : while ( have_posts() ) : the_post();
                    the_content();
                endwhile; endif;
                ?>
            </div>
        </div>
    </section>

<?php
get_footer();
