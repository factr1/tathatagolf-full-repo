<?php
/**
 * Template Name: The Essense Template
 * This is the template that displays home page with multiple sections.
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $post, $chapter_name_glob;

$args     = array(
    'post_type'  => 'page',
    'meta_query' => array(
        array(
            'key' => 'essense_top_left'
        )
    )
);
$meta_det = new WP_Query( $args );

get_header();
getPageHero();
?>

<?php // NOTE: this template is no longer used, delete it ?>
    <article class="captionWrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <?php
                    if ( get_field( "essense_top_left" ) ) {
                        the_field( "essense_top_left" );
                    }
                    ?>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-6">
                    <?php
                    if ( get_field( "essense_top_right" ) ) {
                        the_field( "essense_top_right" );
                    }
                    ?>
                </div>
            </div>
        </div>
    </article>

    <?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section>
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12">
                    <div class="welcomeToHeadingEssense">
                        <h1>The <span class="red">Complete</span> Path of Learning</h1>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <section class="pageInnerContentWrap courseChapter EssenceWrap">
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-md-6 left">
                    <?php
                    if ( get_field( "essense_mid_left" ) ) {
                        the_field( "essense_mid_left" );
                    }
                    ?>
                </article>

                <article class="col-xs-12 col-md-6 right">
                    <?php
                    if ( get_field( "essense_mid_right" ) ) {
                        the_field( "essense_mid_right" );
                    }
                    ?>
                </article>
            </div>
        </div>
    </section>

    <section class="essence-wrp">
        <?php
        while ( have_posts() ) : the_post();
            the_content();
        endwhile;
        ?>
    </section>

    <section class="pageInnerContentWrap courseChapter EssenceWrap">
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-6 col-md-6 left">
                    <?php
                    if ( get_field( "core_foundation" ) ) {
                        the_field( "core_foundation" );
                    }
                    ?>
                </article>

                <article class="col-xs-12 col-sm-6 col-md-6 right es-right">
                    <?php
                    if ( get_field( "curriculum_content" ) ) {
                        the_field( "curriculum_content" );
                    }
                    ?>
                </article>
            </div>
        </div>
    </section>

    <!-----3 CORE------>
    <section class="profileThumbnailWrapUpper profileThumbnailWrap essenseNoneThumbnail grad1">
        <div class="container essenceBoxWrap">
            <h4 class="essence-margin">The 3 <span class="red">Core</span> areas within our training </h4>

            <div class="row">
                <article class="col-sm-12 col-md-4 boxEssence">
                    <div>
                        <?php
                        if ( get_field( "mind" ) ) {
                            the_field( "mind" );
                        }
                        ?>
                    </div>
                </article>

                <article class="col-sm-12 col-md-4 boxEssence">
                    <div>
                        <?php
                        if ( get_field( "body" ) ) {
                            the_field( "body" );
                        }
                        ?>
                    </div>
                </article>

                <article class="col-sm-12 col-md-4 boxEssence">
                    <div>
                        <?php
                        if ( get_field( "swing" ) ) {
                            the_field( "swing" );
                        }
                        ?>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <!---STORY BEHIND TATHATA------>

    <section class="pageInnerContentWrap courseChapter EssenceWrap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 story-inside-essence">
                    <?php
                    if ( get_field( "story_behind" ) ) {
                        the_field( "story_behind" );
                    }
                    ?>
                </div>

                <div class="col-xs-12 col-sm-6 story-inside-essence">
                    <?php
                    if ( get_field( "thanks_for_the_support" ) ) {
                        the_field( "thanks_for_the_support" );
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="profileThumbnailWrap essenseThumbnail">
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-4 col-md-4">
                    <div>
                        <h2 class="">Our <span>Unique</span> Training Facilities</h2>

                        <?php
                        if ( get_field( "training_facilities" ) ) {
                            the_field( "training_facilities" );
                        }
                        ?>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-4 col-md-4">
                    <div>
                        <h2 class="">Tathata <span>Outdoor</span> Facility</h2>

                        <?php
                        if ( get_field( "outdoor_training_center" ) ) {
                            the_field( "outdoor_training_center" );
                        }
                        ?>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-4 col-md-4">
                    <div>
                        <h2 class="">Indoor <span>Training</span> Center</h2>

                        <?php
                        if ( get_field( "indoor_training_center" ) ) {
                            the_field( "indoor_training_center" );
                        }
                        ?>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>

    <img class="beacon" border="0" src="http://r.turn.com/r/beacon?b2=P_AYcJBQXbQH0yAC_9IQpZhwxyZGxpt9A9pzJmqIATDwmwgCAYlZ5lTJGOrh3DGCbjopRYcdYaZeLRI9P-WP4Q&cid=">

<?php
get_footer();
