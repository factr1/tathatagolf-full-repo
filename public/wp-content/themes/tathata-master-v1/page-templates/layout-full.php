<?php
/**
 * Template Name: Layout - Full Width
 *
 * A full width blank page layout
 */

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section id="main-content" class="main-content">
        <?php while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php the_content(); ?>
            </article><!-- #post-## -->
        <?php endwhile; ?>
    </section>

<?php
get_footer();
