<?php
/**
 *  Template Name: Affiliate Landing
 */

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <section id="affiliate-landing">
        <div class="container">
            <nav class="card-nav">
                <?php if ( have_rows( 'al_action_items' ) ) : ?>
                    <ul class="card-menu">
                        <?php while ( have_rows( 'al_action_items' ) ) : the_row(); ?>
                            <li>
                                <a href="<?php setACFLink( 'al-ai_internal_link', 'al-ai_external_link' ); ?>" class="card-nav-link">
                                    <div class="card-nav-item">
                                        <div class="title">
                                            <h3><?php the_sub_field( 'al-ai_title' ); ?></h3>
                                        </div>

                                        <div class="icon">
                                            <img src="<?php the_sub_field( 'al-ai_image' ); ?>" class="img-responsive" alt="">
                                        </div>

                                        <?php the_sub_field( 'al-ai_content' ); ?>
                                    </div>
                                </a>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </nav>
        </div>
    </section>

    <?php get_template_part('parts/script', 'ajga'); ?>

<?php
get_footer();
