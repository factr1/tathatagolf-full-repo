<?php
/**
 *  Template Name: Additional Training
 */

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <section id="additional-training">

        <div class="at-intro">
            <div class="container">
                <?php the_field( 'at_intro' ); ?>
            </div>
        </div>

        <div class="at-callouts">
            <div class="container">
                <div class="row">

                    <?php if ( have_rows( 'at_callouts' ) ) : while ( have_rows( 'at_callouts' ) ) : the_row(); ?>

                        <div class="col-xs-12 col-md-4">
                            <a href="<?php setACFLink( 'at-callout_internal_link', 'at-callout_external_link' ); ?>" class="card-link">
                                <div class="card -as-nav-link" <?php setACFBackground( 'at-callout_background_image' ); ?>>
                                    <div class="card-title">
                                        <h2><?php the_sub_field( 'at-callout_title' ); ?></h2>
                                    </div>

                                    <?php if ( get_sub_field( 'at-callout_icon' ) ) : ?>
                                        <div class="card-image">
                                            <img class="img-responsive" src="<?php the_sub_field( 'at-callout_icon' ); ?>" alt="">
                                        </div>
                                    <?php endif; ?>

                                    <?php if ( get_sub_field( 'at-callout_content' ) ) : ?>
                                        <div class="card-content">
                                            <?php the_sub_field( 'at-callout_content' ); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </a>
                        </div>

                    <?php endwhile; endif; ?>

                </div>
            </div>
        </div>

    </section>

<?php
get_footer();
