<?php
/**
 * Template Name: Essence Story Page
 */

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section class="tathata-story">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
