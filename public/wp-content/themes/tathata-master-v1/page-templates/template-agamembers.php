<?php
/* Template Name: Welcome aga members */

get_header();
?>

    <style>
        .golfSchoolConsumer {
            background: url("<?php the_field('top_banner_img'); ?>") bottom center no-repeat;
            height: 390px;
        }

        .tathataTraining {
            background: url("<?php the_field('center_banner_img'); ?>") bottom center no-repeat;
        }

        .ms-bot-left {
            color: rgb(0, 0, 0);
        }

        .ms-bot-right {
            color: rgb(0, 0, 0);
        }

        .purple-button-area {
            text-align: center;
        }

        #main-content > section {
            float: left;
            width: 100%;
        }

        .testimonial-slider {
            height: 300px;
        }

        .cap_left {
            padding: 25px 0;
        }

        .redBoxContent {
            margin: 25px 0;
        }

        .msprofileThumbnailWrap article > div {
            float: left;
            min-height: 515px;
            width: 100%;
        }

        .answer-area {
            background: rgb(255, 255, 255) none repeat scroll 0 0;
            color: rgb(0, 0, 0);
            float: left;
            font-size: 14px;
            margin-top: -10px;
            padding: 0px;
            position: absolute;
            width: 100%;
        }

        .answer-area-1 {
            none repeat scroll 0 0;
            color: rgb(0, 0, 0);
            float: left;
            font-size: 14px;
            width: 100%;
        }
    </style>

    <div id="main-content" class="main-content">
        <section class="golfSchoolConsumer">
            <div class="text-container-top-kon">
                <div class="banner-text-one-kon">
                    <?php the_field( 'top_banner_text' ); ?>
                </div>
            </div>
        </section>

        <div id="blackbar"></div>

        <article class="captionWrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7">
                        <div class="cap_left">
                            <p><i>"Tathata has given me a <span class="red">new way to look at how I play and teach the game</span> and has excited me to do more than I've ever done. <span class="white">My game has changed. I have more power, more presence and more enthusiasm...</span> I get up in the morning and I want to go do it. That hasn't happened for more than a decade."</i></p>

                            <p align="right">
                                <small><span class="white">- Karen Davies</span>, 2010 LPGA T&amp;CP National Champion</small>
                            </p>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-5 col-md-5">
                        <div class="redBoxContent">
                            <div class="woocommerce">
                                <p class="wootitle">The 60-Day Training Program</p>

                                <p>Let's get started on your path of learning...</p>

                                <p>
                                    <script type="text/javascript">
                                        var addthis_for_wordpress = 'wpp-4.0.7';
                                    </script>
                                    <script async="async" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54f23b0841238168" type="text/javascript"></script>
                                </p>

                                <p class="woobottom">
                                    <a class="greyLinkBtn" title="The 60-Day Training Program" href="<?php echo esc_url( home_url( '/welcome-to-aga-members/?add-to-cart=4388' ) ); ?>">Start Training Today</a>
                                    <span class="price"><span class="amount">$179.95</span></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>

        <section class="pageInnerContentWrap">
            <div class="container">
                <div class="row">
                    <article class="col-xs-12 col-sm-6 col-md-6">
                        <div class="ms-bot-left">
                            <h2 class="content-title-aga"><?php the_field( 'content_title' ); ?></h2>
                            <p><?php the_field( 'content_left_text' ); ?></p>
                        </div>
                    </article>

                    <article class="col-xs-12 col-sm-6 col-md-6">
                        <div class="ms-bot-right">
                            <img src="<?php the_field( 'content_right_image' ); ?>" />
                            <span style="margin-top:25px; float:left; width:100%;"><?php the_field( 'content_right_text' ); ?></span>
                        </div>
                    </article>
                </div>
            </div>
        </section>

        <div class="clearfix"></div>

        <section class="tathataTraining pageInnerContentWrap">
            <div class="container">
                <div class="GolfschoolConsumerWrapPara-1" style="font-style:italic;">
                    <?php
                    if ( get_field( 'first_text_testimonial' ) ):
                        the_field( 'first_text_testimonial' );
                    endif;
                    ?>
                </div>

                <div class="GolfschoolConsumerWrapPara-second-text">
                    <?php the_field( 'second_text_testimonial' ); ?>
                </div>
            </div>
        </section>

        <section class="tathataTraining-2 pageInnerContentWrap">
            <div class="container">
                <div class="row">
                    <article class="col-xs-12 col-sm-6 col-md-6">
                        <div class="ms-bot-left">
                            <h2 class="content-title-aga"><?php the_field( 'content_second_title' ); ?></h2>

                            <?php
                            if ( get_field( "intro_section_3" ) ):
                                the_field( "intro_section_3" );
                            endif;
                            ?>

                            <p style="margin-top: 20px"><?php the_field( "chapters_title" ); ?></p>

                            <?php if ( get_field( 'chapters' ) ) {
                                $liczba = 1;
                                while ( has_sub_field( 'chapters' ) ) { ?>
                                    <div class="single-chapter">Chapter <span>0<?php echo $liczba; ?></span> -
                                        <strong><?php the_sub_field( 'chapter' ); ?></strong> Training
                                    </div>
                                    <?php $liczba ++;
                                }
                            } ?>
                        </div>
                    </article>

                    <article class="col-xs-12 col-sm-6 col-md-6">
                        <div class="ms-bot-right-2">
                            <?php
                            if ( get_field( "intro_section_4" ) ):
                                the_field( "intro_section_4" );
                            endif;
                            ?>
                        </div>

                        <p style="font-size:18px; margin-bottom:30px;"><?php the_field( "introduced_title" ); ?></p>

                        <div class="cover-points-ul-1">
                            <?php if ( get_field( 'introduced' ) ) {
                                echo '<ul>';
                                while ( has_sub_field( 'introduced' ) ) { ?>
                                    <li><?php the_sub_field( 'point' ); ?></li>
                                <?php }
                                echo '</ul>';
                            } ?>
                        </div>
                    </article>
                </div>
            </div>
        </section>

        <div class="clearfix"></div>

        <section class="msprofileThumbnailWrap movement-specialists-bottom">
            <div class="container">
                <div class="row">
                    <article class="col-xs-12 col-sm-6 col-md-4">
                        <div>
                            <h2 class="quickTips2"><?php the_field( 'first_box_title' ); ?></h2>

                            <img class="image-boxer" src="<?php the_field( 'first_box_image' ); ?>" alt="">

                            <div class="textContent2">
                                <?php the_field( 'first_box_text' ); ?><br>

                                <img src="<?php the_field( 'first_box_second_image' ); ?>" />

                                <p class="purple-button-area">
                                    <a class="greyLinkBtn-1" title="The 60-Day Training Program" href="<?php echo esc_url( home_url( '/welcome-to-aga-members/?add-to-cart=4388' ) ); ?>">START TRAINING TODAY</a>
                                </p>
                            </div>
                        </div>
                    </article>

                    <article class="col-xs-12 col-sm-6 col-md-4">
                        <div>
                            <h2 class="quickTips2"><?php the_field( 'second_box_title' ); ?></h2>

                            <img class="image-boxer" src="<?php the_field( 'second_box_image' ); ?>" alt="">

                            <div class="textContent2">
                                <?php the_field( 'second_box_text' ); ?>

                                <form class="sender-second" method="post" action="<?php echo get_permalink( 7619 ); ?>">
                                    <?php $postid = get_the_ID(); ?>
                                    <p class="purple-button-area" style="margin-top:37px">
                                        <input style="border:none;" type="submit" class="greyLinkBtn-1" value="Let's get started">
                                    </p>
                                </form>
                            </div>
                        </div>
                    </article>

                    <article class="col-xs-12 col-sm-12 col-md-4">
                        <div>
                            <h2 class="quickTips2"><?php the_field( 'third_box_title' ); ?></h2>

                            <img src="<?php the_field( 'third_box_image' ); ?>" alt="">

                            <div class="textContent2 newsletter-signup-box">
                                <?php the_field( 'third_box_text' ); ?>
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>">www.tathatagolf.com</a>.
                                <?php echo do_shortcode( '[gravityform id="5" title="false" description="false"]' ); ?>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>

        <?php testimonialSlider( 'golf-schools' ); ?>

        <div class="clearfix"></div>

    </div><!-- #main-content -->

<?php
get_footer();
