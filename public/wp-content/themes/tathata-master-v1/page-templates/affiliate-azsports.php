<?php
/**
 * Template Name: Affiliate - AZ Sports
 *
 * This page exists due to some unique partnerships. Rather than write a ton of conditionals on the original
 * affiliate page, just forked it to be specific to this partner.
 */

// Grab the page IDs where the videos live
$ids = [ 1917 ];

// Default partner buy now URL
$buynow = esc_url( home_url( '/coupon/azsports59/' ) );
$pricing = '$149.95';
$dvdbuy = esc_url( home_url( '/coupon/partner_buy_dvd1/' ) );

get_header();
getPageHero();
?>

    <div id="gtwrapper">

        <?php get_template_part( 'parts/testimonial-ticker' ); ?>
        <?php include_once '60daypromo_partials/videos_top.php'; ?>

        <?php if ( get_field( 'affiliate_intro' ) ) : ?>
            <div class="affiliate-intro">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <p><?php the_field( 'affiliate_intro' ); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <!-- Call to Action -->
        <section class="product-buy">
            <div class="container">
                <div class="row">
                    <h1 class="col-md-12 text-center">The Revolutionary 60-Day<br>In-Home Golf Training Program</h1>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <img src="http://gettathata.com/wp-content/themes/get-tathata/images/Streaming-ChapterOverview.png" alt="Online instant Access on all devices">
                    </div>

                    <div class="col-md-4">
                        <div class="redBoxContent">
                            <div class="woocommerce">
                                <p class="wootitle">The 60-Day Training Program</p>
                                <p>Let's get started on your path of learning...</p>

                                <p class="woobottom">
                                    <a href="<?php echo $buynow ?>" title="The 60-Day Training Program" class="greyLinkBtn">Buy Now <i class="fa fa-angle-right"></i></a>
                                    <span class="price">
                                        <span class="amount">
                                            <s>$179.95</s> <span class="red">Special offer: <?php echo $pricing; ?></span>
                                        </span>
                                    </span>
                                </p>
                            </div>
                        </div>

                        <p class="dvd-addon">
                            <a href="<?php echo $dvdbuy ?>">
                                Add on the 30-Disc<br>
                                DVD set for offline viewing
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <?php
        include_once '60daypromo_partials/imagegrid.php';
        include_once '60daypromo_partials/inside_program.php';
        include_once '60daypromo_partials/videos_inside.php';
        ?>

        <section class="instant-access" style="margin-bottom:30px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60dayAccess.jpg" alt="Online_instantAccess_devices" width="1000" height="600">
                    </div>

                    <div class="col-md-4 ">
                        <h2>Train Anywhere</h2>
                        <p>Enjoy our streaming product on any device wherever you may be.</p>

                        <div class="redBoxContent">
                            <div class="woocommerce">
                                <p class="wootitle">The 60-Day Training Program</p>
                                <p>Let's get started on your path of learning...</p>

                                <p class="woobottom">
                                    <a href="<?php echo $buynow ?>" title="The 60-Day Training Program" class="greyLinkBtn">Buy Now <i class="fa fa-angle-right"></i></a>
                                    <span class="price">
                                        <span class="amount">
                                            <s>$179.95</s> <span class="red">Special offer: <?php echo $pricing; ?></span>
                                        </span>
                                    </span>
                                </p>
                            </div>
                        </div>

                        <p class="dvd-addon">
                            <a href="<?php echo $dvdbuy ?>">
                                Add on the 30-Disc<br>
                                DVD set for offline viewing
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <?php get_template_part( 'parts/money-back' ); ?>

        <!-- Email Giveaway -->
        <section class="email-signup">
            <div class="email-signup_bgimage1"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 ">
                        <h3>Free Video Giveaway</h3>
                        <p>Enter your e-mail for immediate access and learn how Tathata Golf will help your game.</p>
                    </div>

                    <div class="col-md-6 ">
                        <form class="hide-for-touch">
                            <input type="email" name="initial_email" id="initial_email" value="" placeholder="Email Address" width="100%;">
                            <br>
                            <input type="submit" value="Access My Free Videos" data-remodal-target="emailform" href="#" onclick="getEmail()">
                        </form>

                        <form class="show-for-touch">
                            <input type="submit" value="Enter Email and Access Free Videos" data-remodal-target="emailform" href="#">
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <!-- Sliders -->
        <?php if ( have_rows( 'ms_sliders', setEnvironmentID( $ids ) ) ): ?>
            <section id="ms-linked-sliders">
                <div class="holder"><h1>The Change Golf Has Been Waiting For</h1></div>

                <div id="msInfoSliders" class="ms-blocks--row" style="background: #f7f7f7">
                    <?php while ( have_rows( 'ms_sliders', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                        <div class="ms-blocks--column ms-half">
                            <div class="ms-blocks--column">
                                <h3 class="golf-industry"><?php the_sub_field( 'ms_sub_slider_title', setEnvironmentID( $ids ) ); ?></h3>

                                <?php if ( have_rows( 'ms_sub_slider', setEnvironmentID( $ids ) ) ): ?>
                                    <div class="msLinkedSlider">
                                        <?php while ( have_rows( 'ms_sub_slider', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                                            <div class="msLinkedSlider--slide">
                                                <div class="giSlide-container">
                                                    <p class="ms-number--title"><?php the_sub_field( 'ms_slider_slide_title', setEnvironmentID( $ids ) ); ?></p>

                                                    <div class="giSlide-content">
                                                        <span class="ms-number"><?php the_sub_field( 'ms_slider_slide_id', setEnvironmentID( $ids ) ); ?></span>
                                                        <p><?php the_sub_field( 'ms_slider_slide_content', setEnvironmentID( $ids ) ); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>

                                <p class="swipe-text">Swipe left or right for more </p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </section>
        <?php endif; ?>


        <?php
        get_template_part( 'parts/additional-support' );
        include_once '60daypromo_partials/final_features.php';
        ?>

        <section class="cta email-signup">
            <div class="email-signup_bgimage2"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h2 style="color:#fff; font-size:32px;">
                            Experience golf's
                            <span>most transformational and empowering</span> training opportunity ever introduced to the game.
                        </h2>
                    </div>

                    <div class="col-md-4">
                        <div class="redBoxContent">
                            <div class="woocommerce">
                                <p class="wootitle">The 60-Day Training Program</p>
                                <p>Let's get started on your path of learning...</p>

                                <p class="woobottom">
                                    <a href="<?php echo $buynow ?>" title="The 60-Day Training Program" class="greyLinkBtn">Buy Now <i class="fa fa-angle-right"></i></a>
                                    <span class="price">
                                        <span class="amount">
                                            <s>$179.95</s> <span class="red">Special offer: <?php echo $pricing; ?></span>
                                        </span>
                                    </span>
                                </p>
                            </div>
                        </div>

                        <p class="dvd-addon">
                            <a href="<?php echo $dvdbuy ?>">
                                Add on the 30-Disc<br>
                                DVD set for offline viewing
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <?php
        include_once '60daypromo_partials/mental_training.php';
        include_once '60daypromo_partials/testimonials.php';
        include_once '60daypromo_partials/complete_training.php';
        ?>

    </div><!-- END gtwrapper -->

    <!-- Special Offer Modal -->
    <div id="remodal-specialoffer" class="remodal" data-remodal-id="specialoffer" data-remodal-options="hashTracking: false">
        <button data-remodal-action="close" class="remodal-close"></button>

        <h1>This Special Offer Pricing</h1>

        <h5>only works on this page, thanks to <strong><?php the_field( 'affiliate_name' ); ?></strong></h5>

        <div style="margin:20px auto">
            <img src="<?php the_field( 'affiliate_logo' ); ?>" class="modal-af-logo">
        </div>

        <p style="font-size:16px;">
            You are more than welcome to browse around TathataGolf.com<br>
            and learn more about us. However you will need to revisit this page,<br>
            <span class="affiliateurl"><?php echo get_permalink( $post->ID ); ?></span> to purchase the 60-Day Training Program for this special affiliate discounted rate.
        </p>

        <div class="modal-2col" style="margin-top:40px;">
            <h3 style="text-align: center;">Check Out:</h3>
            <a href="<?php echo esc_url( home_url( '/the-essence/' ) ); ?>" style="font-size:18px; padding:8px;">The Essence of Tathata</a><br>
            <a href="<?php echo esc_url( home_url( '/blog/' ) ); ?>" style="font-size:18px; padding:8px;">Tathata Blogs</a>
        </div>

        <div class="modal-2col">
            <a href="<?php echo $buynow ?>" title="The 60-Day Training Program" class="button button-green">Buy Now at <?php echo $pricing ?> <i class="fa fa-angle-right"></i></a>
        </div>

        <div>
            <img class="modal-af-logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/tglogo.jpg">
        </div>
    </div>

    <!-- Email Sign-Up Modal -->
    <div class="remodal" data-remodal-id="emailform" data-remodal-options="hashTracking: false">
        <style scoped>
            .remodal-close {
                right: 0;
                left: initial;
            }
        </style>

        <button data-remodal-action="close" class="remodal-close"></button>

        <h1>Free Video Giveaway</h1>

        <p>Enter your e-mail for immediate access and <br>learn how Tathata Golf will help your game.</p>

        <?php gravity_form( 4, false, false, false, '', false ); ?>

        <script>
            // populate email sign up modal with on page field
            var userEmail;
            var getEmail = function() {
                userEmail = document.getElementById('initial_email').value;
                jQuery('#input_4_2').val(userEmail);
            }
        </script>
    </div>

    <?php get_template_part('parts/script', 'ajga'); ?>

<script>
    jQuery(document).ready(function($) {

        // Modal hovering functions
        var hovering = false;
        var $window = $(window).width();
        var inst = $('[data-remodal-id=specialoffer]').remodal();

        if ($window <= 1024) {
            setTimeout(function() {
                inst.open();
            }, 25000);
        } else {
            var i = 0;
            $("#gtwrapper").mouseleave(function() {
                if (! hovering && i < 1) {
                    inst.open();
                    i ++;
                }
            });
        }

        $(document).on('closed', '#remodal-specialoffer', function() {
            inst.destroy();
        });
    });
</script>

<?php if ( get_field( 'affiliate_id' ) ) : ?>
    <script>
        jQuery(document).ready(function($) {

            var appendID = function(ID) {
                var querystring = '&ref=' + ID;

                $('a').each(function() {
                    var href = $(this).attr('href');
                    href += (href.match(/\?/) ? '&' : '?') + querystring;
                    $(this).attr('href', href);
                });
            }

            var currentAffiliate = '<?php the_field( 'affiliate_id' ); ?>';

            appendID(currentAffiliate);
        });
    </script>
<?php endif; ?>

<img class="beacon" border="0" src="http://r.turn.com/r/beacon?b2=P_AYcJBQXbQH0yAC_9IQpZhwxyZGxpt9A9pzJmqIATDwmwgCAYlZ5lTJGOrh3DGCbjopRYcdYaZeLRI9P-WP4Q&cid=">

<?php
get_footer();
