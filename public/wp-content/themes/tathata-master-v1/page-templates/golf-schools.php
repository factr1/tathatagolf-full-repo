<?php
// Template Name: Golf Schools

get_header();
getPageHero(); ?>

<div id="blackbar"></div>

<section id="page-golf-schools" class="golf-schools reset-bootstrap">
    <?php if ( have_rows( 'page_sections' ) ) : ?>

        <!-- Section Navigation -->
        <div class="focus-areas focus-areas_5">
            <?php while ( have_rows( 'page_sections' ) ) : the_row(); ?>

                <a href="#section-<?php the_sub_field( 'id' ); ?>" class="focus-area" title="<?php the_sub_field( 'navigation_title' ); ?>" style="background-image: url(<?php the_sub_field( 'navigation_image' ); ?>)">
                    <h2 class="focus-area_title"><?php the_sub_field( 'navigation_title' ); ?></h2>

                    <div class="focus-area_content">
                        <?php the_sub_field( 'navigation_content' ); ?>
                    </div>
                </a>

            <?php endwhile; ?>
        </div>

        <!-- Sections -->
        <?php while ( have_rows( 'page_sections' ) ) : the_row(); ?>
            <?php get_template_part('parts/golf', 'section'); ?>
        <?php endwhile; ?>

    <?php endif; ?>
</section>

<script>
    'use strict';

    jQuery(function() {
        var offset = -10;
        var time = 700;

        jQuery('main').find('a').click(function() {
            jQuery('html, body').animate({
                scrollTop: jQuery(jQuery(this).attr('href')).offset().top + offset
            }, time);

            return false;
        });
    });
</script>

<?php
get_footer();
