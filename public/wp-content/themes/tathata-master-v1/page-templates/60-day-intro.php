<?php
// Template Name: 60-Day Program Introduction

// Grab the page IDs where the videos live
$ids = [ 1917 ];
$background = get_field( 'background_image') ? sprintf('style="background: url(%s) center center/cover no-repeat"', get_field( 'background_image')) : '';

get_header();
getPageHero(); ?>

<div id="blackbar"></div>

<div class="reset-bootstrap">
    <!-- As Seen On -->
    <div id="gtwrapper">
        <section class="as-seen-on" style="border: none">
            <div class="container">
                <div class="row">
                    <p>As seen on <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gtimages/nbc-golf.png" width="100" height="25"> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ESPN.png" width="100" height="25"></p>
                </div>
            </div>
        </section>
    </div>

    <!-- Call to Action -->
    <section class="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="call-to-action_title"><?php the_field( 'cta_title' ); ?></h1>
                    <img class="img-responsive" src="<?php the_field( 'cta_image' ); ?>" alt="The Tathata 60-Day program provides online instant access.">
                </div>

                <div class="col-md-6 pt-20">
                    <?php the_field( 'cta_content' ); ?>
                    <?php if ('none' !== get_field( 'link3' )) : ?>
                        <a class="btn tg-primary" href="<?php setACFLink('internal3', 'external3'); ?>"><?php the_field( 'link_text3' ); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Introduction -->
    <section class="introduction" <?php echo $background; ?>>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="introduction_title"><?php the_field( 'title' ); ?></h1>

                    <p class="introduction_content"><?php the_field( 'content' ); ?></p>

                    <?php if ('none' !== get_field( 'link' )) : ?>
                        <a class="btn btn-square" href="<?php setACFLink('internal', 'external'); ?>"><?php the_field( 'link_text' ); ?></a>
                    <?php endif; ?>
                </div>

                <div class="col-md-6">
                    <h1 class="introduction_title"><?php the_field( 'title2' ); ?></h1>

                    <p class="introduction_content"><?php the_field( 'content2' ); ?></p>

                    <?php if ('none' !== get_field( 'link2' )) : ?>
                        <a class="btn btn-square" href="<?php setACFLink('internal2', 'external2'); ?>"><?php the_field( 'link_text2' ); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <!-- 3 Videos -->
    <div id="gtwrapper">
        <?php include_once '60daypromo_partials/videos_top.php'; ?>
    </div>

    <div class="remodal" data-remodal-id="emailform" data-remodal-options="hashTracking: false">
        <style scoped>
            .remodal-close {
                right: 0;
                left: initial;
            }
        </style>

        <button data-remodal-action="close" class="remodal-close"></button>

        <h1>Free Video Giveaway</h1>
        <p>Enter your e-mail for immediate access and <br>learn how Tathata Golf will help your game.</p>

        <?php gravity_form( 4, false, false, false, '', false ); ?>

        <script>
            var userEmail;
            var getEmail = function() {
                userEmail = document.getElementById('initial_email').value;
                jQuery('#input_4_2').val(userEmail);
            }
        </script>
    </div>
</div>

<?php get_template_part('parts/script', 'ajga'); ?>

<?php get_footer();
