<?php
/**
 * Template Name: Testimonials
 *
 * This page will be used to display a list of and picked testimonials for Tathata Golf
 */

// Grab the testimonials
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$args = array(
    'post_type'      => 'testimonial',
    'tax_query'      => array(
        array(
            'taxonomy' => 'testimonial_category',
            'field'    => 'slug',
            'terms'    => 'student-testimonials'
        )
    ),
    'posts_per_page' => 50,
    'paged'          => $paged
);

$testimonials = new WP_Query( $args );
$featured = get_field( 'testimonial' );

get_header();
?>

    <section class="testimonial-banner">
        <h1>Tathata Golf Testimonials</h1>

        <?php if ( have_rows( 'tgt_logos' ) ) : ?>
            <div class="tgt-logos">
                <?php while ( have_rows( 'tgt_logos' ) ) : the_row(); ?>
                    <img src="<?php the_sub_field( 'tgt_logo' ); ?>">
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </section>

    <div id="blackbar"></div>

    <section class="testimonial-area">
        <section class="testimonial-content container">
            <div class="row">
                <?php if (get_field( 'testimonial' )) : ?>
                    <div class="col-xs-12 col-md-8">
                        <blockquote class="featured-quote"><!-- Featured Testimonial -->
                            <p><?php echo $featured->post_content; ?></p>

                            <footer>
                                <cite title="A happy Tathata customer"><?php echo $featured->post_title; ?></cite>
                            </footer>
                        </blockquote>
                    </div>
                <?php endif; ?>

                <div class="col-xs-12 col-md-4">
                    <div class="submit-testimonial"><!-- Submit Button -->
                        <a class="new-btn large" data-remodal-target="modal">Submit a testimonial</a>

                        <div class="remodal" data-remodal-id="modal" data-remodal-options="hashTracking: false">
                            <button data-remodal-action="close" class="remodal-close"></button>

                            <?php gravity_form( 'Testimonials', false, true, false, '', true ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="testimonial-list container"><!-- All Testimonials -->
            <div class="row">
                <div class="col-xs-12">
                    <?php if ( $testimonials->have_posts() ) : while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
                        <?php
                        $ttContent = get_field( 'tt_testimonial' );

                        // Social Source Settings
                        $source = get_field( 'social_source' );

                        if ( $source == 'facebook' ):
                            $social_icon  = 'fa-facebook';
                            $social_color = '#3b5998';
                        elseif ( $source == 'twitter' ):
                            $social_icon  = 'fa-twitter';
                            $social_color = '#55acee';
                        elseif ( $source == 'web' ):
                            $social_icon  = 'fa-globe';
                            $social_color = '#fd9306';
                        else:
                            $social_icon  = 'fa-envelope';
                            $social_color = '#CF0A2C';
                        endif;
                        ?>
                        <div class="testimonial-item">
                            <blockquote style="border: 5px solid <?php echo $social_color; ?>">
                                <?php the_content(); ?>

                                <footer>
                                    <cite title="<?php the_title(); ?>" style="color: <?php echo $social_color; ?>">
                                        <?php the_title(); ?> |
                                        <i class="fa <?php echo $social_icon; ?>" style="color: <?php echo $social_color; ?>; font-size: 14px"></i>
                                    </cite>
                                </footer>
                            </blockquote>
                        </div>
                    <?php endwhile; ?>

                        <style>
                            /* Hack due to the added functionality of displaying social sources. Will do for now though */
                            .testimonial-item:nth-child(odd) blockquote {
                                border-right: none !important;
                                background: white;
                            }

                            .testimonial-item:nth-child(even) blockquote {
                                text-align: right;
                                border-left: none !important;
                                background: white;
                            }
                        </style>

                        <?php
                        if ( function_exists( custom_pagination ) ) {
                            custom_pagination( $testimonials->max_num_pages, "", $paged );
                        }
                        ?>

                    <?php else : ?>
                        <h1>Sorry...</h1>
                        <p>There aren't any testimonials yet! <a data-remodal-target="modal">Be the first!</a></p>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </section>

<?php
get_footer();
