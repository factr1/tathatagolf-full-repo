<?php
/**
 * Template Name: Profile Template
 */

// Redirect the user to the login page if they are not logged in
if ( ! is_user_logged_in() ) {
    wp_redirect( home_url( '/login/' ) );
    exit;
}

get_header();
?>

    <section class="user-profile">
        <?php
        while ( have_posts() ) : the_post();
            the_content();
        endwhile;
        ?>
    </section>

<?php
get_footer();
