<?php
/* Template Name: Giveaway Sign Up */

get_header();
?>

    <style>
        #testimonials {
            display: none !important;
        }
    </style>

    <?php get_template_part( 'parts/page-title' ); ?>

    <section class="pageInnerContentWrap">
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-6 col-md-6 sign-right">
                    <div class="welcomeToHeading">
                        <h3 class="title"><?php the_field( 'title' ); ?></h3>
                        <p><?php the_field( 'description' ); ?></p>
                    </div>

                    <?php echo do_shortcode( '[gravityform id="4" title="false" description="false"]' ); ?>

                    <div class="clearfix"></div>
                </article>

                <article class="col-xs-12 col-sm-6 col-md-6 sign-left">
                    <img src="<?php the_field( 'first_image' ); ?>" />
                    <img src="<?php the_field( 'second_image' ); ?>" style="margin-top:40px;" />
                </article>

                <div class="clearfix"></div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>

<?php
get_footer();
