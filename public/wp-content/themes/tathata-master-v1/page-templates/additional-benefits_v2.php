<?php
/**
 * Template Name: Additional Benefits - staged
 */

get_header();
getPageHero();
?>

    <div id="blackbar"></div>

    <?php get_template_part( 'parts/breadcrumbs' ); ?>

    <div id="gtwrapper">
        <section class="pageInnerContentWrap">
            <div class="container">
                <div class="row onlineTopwrap">
                    <div class="row-inner">
                        <article class="col-md-6 col-sm-6 col-xs-12 Days60Consumer">
                            <?php
                            while ( have_posts() ) : the_post();
                                the_content();
                            endwhile;
                            ?>
                        </article>

                        <article class="col-xs-12 col-sm-6 col-md-6 golfdemowrap benefits_right_side_content">
                            <?php
                            if ( get_field( "benefits_right_side_content" ) ):
                                the_field( "benefits_right_side_content" );
                            endif;
                            ?>
                        </article>

                        <article class="col-xs-12 col-sm-6 col-md-6 pull-right testmonialRightText">
                            <?php
                            if ( get_field( "program_breakdown" ) ):
                                the_field( "program_breakdown" );
                            endif;
                            ?>
                        </article>
                    </div>
                </div>
            </div>
        </section>

        <section class="rest-area -additional-benefits"></section>

        <section class="highlight60days threecolgrids">
            <style scoped>
                .highlight60days h4 {
                    text-align: center;
                }
            </style>

            <div class="clearfix"></div>

            <div class="container" style="padding-top:40px !important;">
                <div class="row">
                    <article class="col-xs-12 col-sm-4 col-md-4">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/highLightsImg1.png" class="img-responsive">

                        <h5><span class="red">Mental</span> Training</h5>

                        <p>Not designed to help you just get a little better, Tathata mental training practices and principles open you up to a profound greatness; a power to move mountains, in a sense, and go far beyond any limitations you may have once had.</p>

                        <p>A true sense of strength comes from knowing what you are doing. When Tathata movement training is combined with mental training in a strategically developed path of learning, the "whole" dynamically exceeds the sum of the parts.</p>
                    </article>

                    <article class="col-xs-12 col-sm-4 col-md-4l">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/highLightsImg2.png" class="img-responsive">

                        <h5><span class="red">Martial</span> Arts Training</h5>

                        <p>Built to resemble how you would learn a martial art, the strategically developed, complete path of learning and training builds on itself and introduces you to a way to rapidly improve and enjoy lasting growth in the game of golf.</p>

                        <p>Move beyond all doubt as you validate your own truths once and for all. Through this, comes a reverence to the path you are on and where you are going. This alone helps to dramatically increase your capacity and pace of learning.</p>
                    </article>

                    <article class="col-xs-12 col-sm-4 col-md-4">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/highLightsImg3.png" class="img-responsive">

                        <h5><span class="red">Movement</span> Training</h5>

                        <p>Tathata's revolutionary movements and routines are based on the movements of the world's greatest golfers and athletes of all-time along with timeless truths from the martial art world. </p>

                        <p>All movements and fundamentals build on and completely support each other from the setup to the finish of the swing. Very detailed and intricate fundamentals and mechanics are taught in a completely new way, making learning for the student easy and faster than ever before.</p>
                    </article>
                </div>
            </div>

            <div class="clearfix" style="margin-top:25px;"></div>

            <div class="container">
                <div class="row">
                    <article class="col-xs-12 col-sm-4 col-md-4">
                        <img src="<?php echo esc_url( home_url( '/wp-content/uploads/2015/04/image-box4.png' ) ); ?>" width="100%" height="auto">

                        <h5><span class="red">Revolutionary</span> Training</h5>

                        <p>Strategically builds on itself each day over 60 days including:</p>

                        <ul>
                            <li>Complete full swing, short game and putting training</li>
                            <li>Over 140 training movements</li>
                            <li>14 different movement routines</li>
                            <li>3 Greatest Golfers &amp; Athletes Movements Videos</li>
                            <li>Over 195 Questions &amp; Video Answers</li>
                            <li>Over 45 mental training &amp; swing deeper discussions</li>
                            <li>25 Mental training exercises</li>
                        </ul>
                    </article>

                    <article class="col-xs-12 col-sm-4 col-md-4">
                        <img src="<?php echo esc_url( home_url( '/wp-content/uploads/2015/04/image-box5.png' ) ); ?>" width="100%" height="auto">

                        <h5><span class="red">Chapter</span> Support</h5>

                        <p>Convenient, Greatest Players Support. Watch them anywhere, anytime.</p>

                        <ul>
                            <li>Greatest Player Support Slideshows</li>
                            <li>Movement Support Training Videos</li>
                            <li>Enhance the performance and accuracy of your movements</li>
                            <li>Movement Assessment Videos</li>
                            <li>Downloadable assessment forms and grading details</li>
                            <li>Anatomical Review Videos: A kinesiologist perspective of how the body moves</li>
                        </ul>
                    </article>

                    <article class="col-xs-12 col-sm-4 col-md-4">
                        <img src="<?php echo esc_url( home_url( '/wp-content/uploads/2015/05/60d-new_ph-cert.jpg' ) ); ?>" width="100%" height="auto">

                        <h5><span class="red">Upon</span> Graduation</h5>

                        <p>Unlock additional material and resources after you finish the 60-Day Program.</p>

                        <ul>
                            <li>60-Day Certificate &amp; Badges</li>
                            <li>Access to 60-Day Program conveniently broken apart</li>
                            <li>Access to Tathata Graduate Schools at our world class facility</li>
                            <li>Movement Specialists personalized training</li>
                            <li>Special "Graduate-Only" areas in the Tathata Forum</li>
                        </ul>
                    </article>
                </div>
            </div>

            <div class="clearfix"></div>

            <section class="featured_testimonial">
                <div class="testimonial_bgimage"></div>

                <style scoped>
                    .featured_testimonial {
                        position: relative;
                        display: block;
                        width: 100%;
                        margin: 40px 0;
                        background: rgba(0, 0, 0, 0.7);
                        color: rgb(255, 255, 255) !important;
                    }

                    .testimonial_bgimage {
                        width: 100%;
                        height: 100%;
                        display: block;
                        background: linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url(<?php echo get_template_directory_uri(); ?>/assets/img/bkg_slider-04.jpg) center center no-repeat;
                        background-size: cover;
                        position: absolute;
                    }

                    .featured_testimonial .row {
                        padding: 100px 45px;
                        text-align: center;
                    }
                </style>

                <div class="container" style="position: relative;">
                    <div class="row">
                        <blockquote style="color:rgb(255, 255, 255);border: none 0 !important; font-size: 26px; font-style: normal;">"The work is done in the gym with exercises that are fun to do. You actually can go out on the range and the course and it's already into your swing if you've done the training."</blockquote>
                        <cite style="color:rgb(255, 255, 255);font-size: 16px;"><strong>Todd Demsey,</strong> Tour Professional, 4-Time All-American</cite>
                    </div>
                </div>
            </section>

            <section class="instant-access">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60Day.png" alt="Access the 60-Day Program online" width="100%" height="auto">
                        </div>

                        <div class="col-md-4">
                            <h2>Train Anywhere</h2>
                            <p>Enjoy our streaming product on any device wherever you may be.</p>

                            <div class="redBoxContent">
                                <div class="woocommerce">
                                    <p class="wootitle">The 60-Day Training Program</p>

                                    <p>Let's get started on your path of learning…</p>

                                    <p class="woobottom">
                                        <a href="<?php echo esc_url( home_url( '/?add_to_cart=8251' ) ); ?>" title="The 60-Day Training Program" class="greyLinkBtn">Buy Now <i class="fa fa-angle-right"></i></a>
                                        <span class="price">
                                            <span class="amount">$179.95 - Start Training Today!</span>
                                        </span>
                                    </p>
                                </div>
                            </div>

                            <p class="dvd-addon">
                                <a href="<?php echo esc_url( home_url( '/?add_to_cart=8251' ) ); ?>">Add on the 30-Disc<br>DVD set for offline viewing</a>
                            </p>
                        </div>
                    </div>
                </div>
            </section>

            <?php get_template_part( 'parts/money-back' ); ?>

            <section class="threecolgrids" style="padding-top: 40px">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 style="text-align: center; font-weight: 300; margin-bottom: 20px;">Available Upon Completion Of The 60-Day Program</h2>
                        </div>
                    </div>

                    <div class="row bodySwing-3col">
                        <article class="col-xs-12 col-sm-4 col-md-4">
                            <img src="<?php echo esc_url( home_url( '/wp-content/uploads/2015/04/benefits-box1-img.jpg' ) ); ?>" class="img-responsive">

                            <h5>60-Day <span class="red">Certificate &amp; Badges</span></h5>

                            <p>60-Day Program graduates receive a custom certificate of completion and all chapter badges upon graduation from the Tathata 60-Day Training Program.</p>
                        </article>

                        <article class="col-xs-12 col-sm-4 col-md-4">
                            <img src="<?php echo esc_url( home_url( '/wp-content/uploads/2015/08/benefits-box1-img5.jpg' ) ); ?>" class="img-responsive">

                            <h5>60-Day Program <span class="red">Video Library</span></h5>

                            <p>Enjoy all of the content from the revolutionary 60-Day Program completely broken apart for you convenience. This feature offers users a convenient and easy way to access any of the program's material for continued learning and training long after completion of the program.</p>
                        </article>

                        <article class="col-xs-12 col-sm-4 col-md-4">
                            <img src="<?php echo esc_url( home_url( '/wp-content/uploads/2015/08/benefits-box1-img4.jpg' ) ); ?>" class="img-responsive">

                            <h5>Book Your <span class="red">Grad School Experience</span></h5>

                            <p>Our one-of-a-kind continued education training experiences are available upon 60-Day Training Program graduation. 60-Day Program graduates receive complete movement follow up with Tathata master trainers, truly experience the essence of Tathata mental training and leave with a sense of being able to train and correct themselves.</p>
                        </article>
                    </div>
                </div>
            </section>
        </section>
    </div><!--  END id="gtwrapper" -->

<?php get_template_part('parts/script', 'ajga'); ?>

<?php // Facebook Pixel ?>
<script>fbq('track', 'ViewContent');</script>

<?php
get_footer();
