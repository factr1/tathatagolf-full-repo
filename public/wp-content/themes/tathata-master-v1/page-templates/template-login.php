<?php
/**
 * Template Name: Login Template
 *
 * This is the template that displays home page with multiple sections.
 */

get_header();
?>

    <section id="custom-login" class="login-page">

        <div class="login-content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-9">
                        <div class="-content">
                            <?php the_field('login_screen_content'); ?>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-3">
                        <div class="-content">
                            <img class="tt-loginLogo img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/tt-logo.png">

                            <?php
                            while ( have_posts() ) : the_post();
                                the_content();
                            endwhile;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
