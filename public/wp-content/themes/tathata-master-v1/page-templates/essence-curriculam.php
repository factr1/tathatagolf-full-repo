<?php
/**
 * Template Name: Essence Curriculam
 * This is the template that displays home page with multiple sections.
 */

get_header();
getPageHero();
?>

<?php get_template_part( 'parts/breadcrumbs' ); ?>

<section class="contentWrapper storypage">
    <article class="pageInnerContentWrap completePathLearning">
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-6 col-md-6">
                    <?php
                    if ( get_field( "curriculam_complete_path_left" ) ):
                        the_field( "curriculam_complete_path_left" );
                    endif;
                    ?>

                    <div class="clearfix"></div>
                </article>

                <article class="col-xs-12 col-sm-6 col-md-6 right">
                    <?php
                    if ( get_field( "curriculam_complete_path_right" ) ):
                        the_field( "curriculam_complete_path_right" );
                    endif;
                    ?>
                </article>
            </div>

            <div class="row cpL-secondRow">
                <article class="col-xs-12 col-sm-6 col-md-6">
                    <?php
                    if ( get_field( "tathata_curriculum_left" ) ):
                        the_field( "tathata_curriculum_left" );
                    endif;
                    ?>

                    <div class="clearfix"></div>
                </article>

                <article class="col-xs-12 col-sm-6 col-md-6 right">
                    <?php
                    if ( get_field( "tathata_curriculum_right" ) ):
                        the_field( "tathata_curriculum_right" );
                    endif;
                    ?>
                </article>
            </div>

            <div class="row threeColProgram">
                <div class="threeColProgram-in">
                    <article class="col-xs-12 col-sm-4 col-md-4 left-col">
                        <?php
                        if ( get_field( "essence_curriculam_60_day" ) ):
                            the_field( "essence_curriculam_60_day" );
                        endif;
                        ?>
                    </article>

                    <article class="col-xs-12 col-sm-4 col-md-4 center-col">
                        <?php
                        if ( get_field( "essence_curriculam_oa" ) ):
                            the_field( "essence_curriculam_oa" );
                        endif;
                        ?>
                    </article>

                    <article class="col-xs-12 col-sm-4 col-md-4 right-col">
                        <?php
                        if ( get_field( "essence_curriculam_gs" ) ):
                            the_field( "essence_curriculam_gs" );
                        endif;
                        ?>
                    </article>
                </div>
            </div>

            <div class="row additional-section">
                <h5>Additional up incoming 2015</h5>

                <h4>Movement Specialist</h4>

                <article class="col-xs-12 col-sm-6 col-md-8 left-col">
                    <?php
                    if ( get_field( "essence_curriculam_ms_left" ) ):
                        the_field( "essence_curriculam_ms_left" );
                    endif;
                    ?>
                </article>

                <article class="col-xs-12 col-sm-6 col-md-4 right-col">
                    <?php
                    if ( get_field( "essence_curriculam_ms_right" ) ):
                        the_field( "essence_curriculam_ms_right" );
                    endif;
                    ?>
                </article>
            </div>

            <div class="row storyBehindTathataGolf">
                <article class="col-xs-12 col-sm-6 col-md-6 left-col">
                    <?php
                    if ( get_field( "story_behind_tathata_golf" ) ):
                        the_field( "story_behind_tathata_golf" );
                    endif;
                    ?>
                </article>

                <article class="col-xs-12 col-sm-6 col-md-6 right-col">
                    <?php
                    if ( get_field( "tathata_golf_facilities" ) ):
                        the_field( "tathata_golf_facilities" );
                    endif;
                    ?>
                </article>
            </div>
        </div>
    </article>
</section>

<div class="clearfix"></div>

<?php
get_footer();
