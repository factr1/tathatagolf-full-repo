<?php
/**
 * Template Name: Congratulations
 * This is the template that displays home page with multiple sections.
 */

get_header();
getPageHero();
?>

<div class="container">
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <?php
            // Start the Loop.
            while ( have_posts() ) : the_post();

                // Include the page content template.
                get_template_part( 'parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) {
                    comments_template();
                }
            endwhile;
            ?>
        </div><!-- #content -->
    </div><!-- #primary -->

    <div class="clearfix"></div>
</div>

<?php
get_footer();
