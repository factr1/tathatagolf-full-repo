<?php
  /**
   * Template Name: Broken Apart Video Detail
   */

  get_header();
  getPageHero();
?>

    <div id="blackbar"></div>

    <?php get_template_part('parts/breadcrumbs'); ?>

    <section class="contentWrapper">
        <section class="contentWrapper storypage">
            <article class="pageInnerContentWrap">
                <div class="container">
                    <?php
                    while ( have_posts() ) : the_post();
                        the_content();
                    endwhile
                    ?>
                </div>
            </article>
        </section>
    </section>

<?php
  get_footer();
