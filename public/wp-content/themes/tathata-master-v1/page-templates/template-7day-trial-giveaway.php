<?php
/* Template Name: 7day giveaway */

get_header();
?>

    <style>
        .tathataTraining {
            background: url("<?php the_field('testi_background'); ?>");
        }

        .movement-specialists-bottom {
            background: url("<?php the_field('boxes_background'); ?>");
        }
    </style>

    <section class="pageInnerContentWrap">
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-6 col-md-6">
                    <div class="ms-bot-left">
                        <p>
                            <?php while ( have_posts() ) : the_post();
                                get_template_part( 'parts/content', 'page' );
                            endwhile; ?>
                        </p>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-6 col-md-6">
                    <div class="ms-bot-right">
                        <?php
                        if ( get_field( "right_side_text" ) ):
                            the_field( "right_side_text" );
                        endif;
                        ?>

                        <div class="redBoxContent" style="margin-bottom: 40px;">
                            <div class="woocommerce">
                                <p class="wootitle">60-Day Training Program | 7-Day Trial</p>

                                <p>Join the movement & start your training now...your trial promo code will be automatically applied.</p>

                                <p>
                                    <script type="text/javascript">
                                        var addthis_for_wordpress = 'wpp-4.0.7';
                                    </script>
                                    <script async="async" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54f23b0841238168" type="text/javascript"></script>
                                </p>

                                <p class="woobottom">
                                    <a class="greyLinkBtn" title="The 60-Day Training Program" href="<?php echo esc_url( home_url( '/coupon/7daytrial/' ) ); ?>">Start Trial Now</a>
                                </p>

                                <?php
                                $args = array( 'post_type' => 'product', 'posts_per_page' => 1, 'p' => '4388' );
                                $loop = new WP_Query( $args );

                                while ( $loop->have_posts() ) : $loop->the_post();
                                    global $product;
                                ?>
                                    <div class="star-rating" title="Rated <?php echo( $product->get_average_rating() ? $product->get_average_rating() : '0' ); ?> out of 5">
                                        <span style="width: <?php echo $product->get_average_rating() * 20; ?>%;">
                                            <strong itemprop="ratingValue" class="rating"><?php echo( $product->get_average_rating() ? $product->get_average_rating() : '0' ); ?></strong> out of 5
                                        </span>
                                    </div>

                                    <div class="count">(<?php echo $product->get_rating_count(); ?>)</div>
                                <?php endwhile; wp_reset_query(); ?>

                                <span class="price"><span class="amount">Free 7 day Trial</span></span>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <section class="tathataTraining pageInnerContentWrap">
        <div class="container">
            <div class="GolfschoolConsumerWrapPara-1" style="font-style:italic;">
                <?php
                if ( get_field( 'first_text_testimonial' ) ):
                    the_field( 'first_text_testimonial' );
                endif;
                ?>
            </div>

            <div class="GolfschoolConsumerWrapPara-second-text">
                <?php the_field( 'second_text_testimonial' ); ?>
            </div>
        </div>
    </section>

    <section class="tathataTraining-2 pageInnerContentWrap">
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-6 col-md-6">
                    <div class="ms-bot-left">
                        <?php
                        if ( get_field( "intro_section_3" ) ):
                            the_field( "intro_section_3" );
                        endif;
                        ?>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-6 col-md-6">
                    <div class="ms-bot-right-2">
                        <?php
                        if ( get_field( "intro_section_4" ) ):
                            the_field( "intro_section_4" );
                        endif;
                        ?>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-12 col-md-12">
                    <div class="cover-points-title">
                        <?php the_field( 'title_covers' ); ?>
                    </div>

                    <div class="cover-points-ul">
                        <?php if ( get_field( 'access_points' ) ) {
                            echo '<ul>';
                            while ( has_sub_field( 'access_points' ) ) { ?>
                                <li><?php the_sub_field( 'point' ); ?></li>
                            <?php }
                            echo '</ul>';
                        } ?>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <section class="msprofileThumbnailWrap movement-specialists-bottom">
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-6 col-md-4">
                    <div>
                        <h2 class="quickTips2"><?php the_field( 'first_box_title' ); ?></h2>

                        <img src="<?php the_field( 'first_box_image' ); ?>" alt="">

                        <div class="textContent2">
                            <?php the_field( 'first_box_text' ); ?>
                            <br>
                            <br>
                            <img src="<?php the_field( 'first_box_second_image' ); ?>" />

                            <p class="purple-button-area">
                                <a class="greyLinkBtn-1" title="The 60-Day Training Program" href="<?php echo esc_url( home_url( '/coupon/7daytrial/' ) ); ?>">GET YOUR TRIAL NOW</a>
                            </p>
                        </div>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-6 col-md-4">
                    <div>
                        <h2 class="quickTips2"><?php the_field( 'second_box_title' ); ?></h2>

                        <img src="<?php the_field( 'second_box_image' ); ?>" alt="">

                        <div class="textContent2">
                            <?php the_field( 'second_box_text' ); ?>

                            <form class="sender-second" method="post" action="<?php echo get_permalink( 7619 ); ?>">
                                <?php $postid = get_the_ID(); ?>

                                <p class="purple-button-area" style="margin-top:37px">
                                    <input style="border:none;" type="submit" class="greyLinkBtn-1" value="LET'S GET STARTED">
                                </p>
                            </form>
                        </div>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-12 col-md-4">
                    <div>
                        <h2 class="quickTips2"><?php the_field( 'third_box_title' ); ?></h2>

                        <img src="<?php the_field( 'third_box_image' ); ?>" alt="">

                        <div class="textContent2 newsletter-signup-box">
                            <?php the_field( 'third_box_text' ); ?>

                            <p style="purple-button-area">
                                <a class="greyLinkBtn-1" style="bottom: 19px;position: absolute;left: 14%;" title="The 60-Day Training Program" href="<?php echo esc_url( home_url( '/cart/?add-to-cart=4388' ) ); ?>">BUY NOW $179.95</a>
                            </p>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <?php testimonialSlider( '60-day-training-program' ); ?>

    <div class="clearfix"></div>

<?php
get_footer();
