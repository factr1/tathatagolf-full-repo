<?php
/**
 * Template Name: Movement Specialist Template
 *
 * This is the template that displays home page with multiple sections.
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

get_header();
?>

<?php // this template may be able to be deleted ?>
    <article class="captionWrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-7">
                    <?php
                    if ( get_field( "black_box_content" ) ):
                        the_field( "black_box_content" );
                    endif;
                    ?>
                </div>

                <div class="col-xs-12 col-sm-5 col-md-5">
                    <div class="redBoxContent msRedBoxContent">
                        <?php
                        if ( get_field( "red_box_content" ) ):
                            the_field( "red_box_content" );
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </article>

    <article class="chapterlinkContentWrap MovementSpecialists">
        <div class="container">
            <ul class="chapterLinks">
                <?php do_action( 'sensei_course_single_lessons_custom' ); ?>
            </ul>

            <div class="clearfix"></div>

            <ol class="breadcrumb pull-left">
                <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                <li>Movement Specialist</li>
            </ol>

            <?php echo do_shortcode( '[followUs]' ); ?>
        </div>
    </article>

    <section class="pageInnerContentWrap">
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-6 col-md-6">
                    <div class="ms-bot-left">
                        <p>
                            <?php
                            // Start the Loop.
                            while ( have_posts() ) : the_post();
                                // Include the page content template.
                                get_template_part( 'parts/content', 'page' );
                            endwhile;
                            ?>
                        </p>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-6 col-md-6">
                    <div class="ms-bot-right">
                        <?php
                        if ( get_field( "secondary_content_section" ) ):
                            the_field( "secondary_content_section" );
                        endif;
                        ?>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <section class="tathataTraining pageInnerContentWrap">
        <div class="GolfschoolConsumerWrapPara">
            <?php
            if ( get_field( 'tathata_training_wrap' ) ):
                the_field( 'tathata_training_wrap' );
            endif;
            ?>
        </div>
    </section>

    <section class="tathataTraining-2 pageInnerContentWrap">
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-6 col-md-6">
                    <div class="ms-bot-left">
                        <?php
                        if ( get_field( "intro_section4" ) ):
                            the_field( "intro_section4" );
                        endif;
                        ?>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-6 col-md-6">
                    <div class="ms-bot-right">
                        <?php
                        if ( get_field( "intro_section5" ) ):
                            the_field( "intro_section5" );
                        endif;
                        ?>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <section class="msprofileThumbnailWrap movement-specialists-bottom">
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-6 col-md-4">
                    <div>
                        <h2 class="quickTips">Know What to Expect</h2>

                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/unnamed.jpg" alt="">

                        <div class="textContent">
                            <?php
                            if ( get_field( "intro_section1" ) ):
                                the_field( "intro_section1" );
                            endif;
                            ?>
                        </div>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-6 col-md-4">
                    <div>
                        <h2 class="accProfile">Find, Research, and Learn</h2>

                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/unnamed-1.jpg" alt="">

                        <div class="textContent">
                            <?php
                            if ( get_field( "intro_section2" ) ):
                                the_field( "intro_section2" );
                            endif;
                            ?>
                        </div>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-12 col-md-4">
                    <div>
                        <h2 class="suppForum">Visit a Specialist</h2>

                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/unnamed-2.jpg" alt="">

                        <div class="textContent">
                            <?php
                            if ( get_field( "intro_section3" ) ):
                                the_field( "intro_section3" );
                            endif;
                            ?>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <?php testimonialSlider( 'movement-specialist' ); ?>

    <div class="clearfix"></div>

    <img class="beacon" border="0" src="http://r.turn.com/r/beacon?b2=P_AYcJBQXbQH0yAC_9IQpZhwxyZGxpt9A9pzJmqIATDwmwgCAYlZ5lTJGOrh3DGCbjopRYcdYaZeLRI9P-WP4Q&cid=">

<?php
get_footer();
