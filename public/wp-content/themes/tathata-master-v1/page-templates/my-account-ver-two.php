<?php
/**
 *Template Name: my account version two
 * @author      WooThemes
 * @package     Sensei/Templates
 * @version     1.0.0
 */

get_header();
?>

    <div id="main-content" class="main-content">
        <div id="primary" class="content-area">
            <div id="content" class="site-content profile-page" role="main">
                <section class="contentWrapper">
                    <?php get_template_part( 'parts/page-title' ); ?>

                    <!-- Black Bar -->
                    <div id="blackbar-menu">
                        <div class="container">
                            <div class="c_my_account">
                                <ul class="ms-nav">
                                    <li class="i-menu cy menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo esc_url( home_url( '/dashboard' ) ); ?>">AC</a></li>
                                </ul>
                            </div>

                            <div class="<?php echo tath_role_class( 'learnervisibility' ); ?>">
                                <?php wp_nav_menu( array('menu' => 'Lesson Menu') ); ?>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <?php setBreadcrumbs( 'Account' ); ?>

                    <section class="pageInnerContentWrap profilePage">
                        <div class="container">
                            <div class="row">
                                <article class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="clearfix"></div>

                                    <!-- quick links drop down start -->
                                    <div class="profile-dropdown dropdown">
                                        <button class="btn btn-default dropdown-toggle niceselect" type="button" id="select-your-profile-items" data-toggle="dropdown">Select your profile items</button>

                                        <ul class="profile-items-selection-ul dropdown-menu" role="menu" aria-labelledby="menu1">
                                            <li class="profile-items-selection <?php if ( bbp_is_single_user_profile() ) : ?>current<?php endif; ?>" role="presentation">
                                                <span class="vcard bbp-user-profile-link">
                                                    <a role="menuitem" tabindex="-1" href=" <?php global $current_user;
                                                    get_currentuserinfo();
                                                    echo "http://www.tathatagolf.com/forums/members/" . $current_user->user_login . "/"; ?> ">
                                                        Profile
                                                    </a>
                                                </span>
                                            </li>
                                            <li class="profile-items-selection <?php if ( bbp_is_single_user_topics() ) : ?>current<?php endif; ?>">
                                                <span class='bbp-user-topics-created-link'>
                                                    <a role="menuitem" tabindex="-1" href=" <?php global $current_user;
                                                    get_currentuserinfo();
                                                    echo "http://www.tathatagolf.com/forums/members/" . $current_user->user_login . "/topics/"; ?> ">
                                                        Topics Started
                                                    </a>
                                                </span>
                                            </li>
                                            <li class="profile-items-selection <?php if ( bbp_is_single_user_replies() ) : ?>current<?php endif; ?>">
                                                <span class='bbp-user-replies-created-link'>
                                                    <a role="menuitem" tabindex="-1" href=" <?php global $current_user;
                                                    get_currentuserinfo();
                                                    echo "http://www.tathatagolf.com/forums/members/" . $current_user->user_login . "/replies/"; ?> ">
                                                        Recent Replies
                                                    </a>
                                                </span>
                                            </li>
                                            <?php if ( bbp_is_user_home() || current_user_can( 'edit_users' ) ) : ?>
                                                <li class="profile-items-selection <?php if ( bbp_is_single_user_edit() ) : ?>current<?php endif; ?>">
                                                    <span class="bbp-user-edit-link">
                                                        <a role="menuitem" tabindex="-1" href=" <?php global $current_user;
                                                        get_currentuserinfo();
                                                        echo "http://www.tathatagolf.com/forums/members/" . $current_user->user_login . "/edit/"; ?> ">
                                                            Edit Profile
                                                        </a>
                                                    </span>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div><!-- quick links drop down end -->

                                    <div class="my-account-original-contents">
                                        <?php echo do_shortcode( '[woocommerce_my_account]' ); ?>
                                        <?php echo do_shortcode( '[usercourses] ' ); ?>
                                    </div>
                                </article>

                                <article class="col-xs-12 col-sm-6 col-md-6 profile_details">
                                    <?php
                                    $statuses = courses_status( $current_user->ID );
                                    foreach ( $courses as $course ) {
                                        if ( array_key_exists( $course->ID, $statuses ) ) {
                                            echo '<div class="courseInProgress"> <span style="display:none;">' . $course->post_title . '-</span><h5 class="status in-progress"> ' . $statuses[ $course->ID ]['status'] . '</h5> </div>';
                                        }
                                    }
                                    ?>
                                    <div class="clearfix"></div>

                                    <h3>Profile Details</h3>

                                    <article class="profileDetailWrap">
                                        <div class="leftPanelDetail">
                                            <div class="profileImg"><?php echo get_avatar( $current_user->ID, 230 ); ?></div>
                                            <h6>Badges Earned</h6>

                                            <?php do_shortcode( '[badgeos_achievements_list_custom]' ); ?>

                                            <?php
                                            if ( ! empty( $UserMeta['user_thumbnail'][0] ) ) {
                                                $user_thumb       = get_post_meta( $UserMeta['user_thumbnail'][0] );
                                                $profile_img_path = site_url() . '/wp-content/uploads/' . $user_thumb['_wp_attached_file'][0];
                                            ?>

                                                <style scoped>
                                                    .profilePage .profileDetailWrap .leftPanelDetail .profileImg {
                                                        background: url("<?php print $profile_img_path; ?>") no-repeat scroll 10px 10px transparent;
                                                    }
                                                </style>
                                            <?php } ?>
                                        </div>

                                        <div class="rightPanelDetail">
                                            <h1><span>User</span> Name</h1>

                                            <h5><?php print ucfirst( $current_user->data->user_nicename ); ?></h5>

                                            <div class="<?php echo tath_role_class( 'learnervisibility' ); ?>">
                                                <ul class="memberActivityList">
                                                    <?php
                                                    $order_date_combine        = wcr_check_user_bought( $current_user->user_email, $current_user->ID, 72 );
                                                    $order_date_combine_status = 0;

                                                    if ( $order_date_combine ) {
                                                        $order_date_combine_status = 1;
                                                        $order_date                = date( 'd/m/y', strtotime( $order_date_combine ) );
                                                        echo '<li class="profSixtyDayPro">60-Day Program Member<span class="startDate">Start Date: ' . $order_date . '</span></li>';
                                                        echo '<li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: ' . $order_date . '</span></li>';
                                                    }

                                                    if ( $order_date_combine_status === 0 ) {
                                                        $order_date_60 = wcr_check_user_bought( $current_user->user_email, $current_user->ID, 69 );

                                                        if ( $order_date_60 ) {
                                                            echo '<li class="profSixtyDayPro">60-Day Program Member<span class="startDate">Start Date: ' . date( 'd/m/y', strtotime( $order_date_60 ) ) . '</span></li>';
                                                        } else {
                                                            echo '<li class="profSixtyDayPro">60-Day Program Member<span class="startDate">Start Date: Manually Added</span></li>';
                                                        }

                                                        $order_date_online = wcr_check_user_bought( $current_user->user_email, $current_user->ID, 70 );

                                                        if ( $order_date_online ) {
                                                            echo '<li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: ' . date( 'd/m/y', strtotime( $order_date_online ) ) . '</span></li>';
                                                        } else {
                                                            echo '<li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: Manually Added</span></li>';
                                                        }
                                                    }
                                                    ?>
                                                </ul>
                                            </div>

                                            <?php echo do_shortcode( '[usereditlink]' ); ?>

                                            <div class="clearfix"></div>

                                            <address>
                                                <span>Address: </span>

                                                <?php
                                                if ( ! empty( $UserMeta['address'][0] ) ) {
                                                    print $UserMeta['address'][0];
                                                }
                                                ?>

                                                <span>Phone: </span>

                                                <?php
                                                if ( ! empty( $UserMeta['phone'][0] ) ) {
                                                    print $UserMeta['phone'][0];
                                                }
                                                ?>

                                                <span>eMail: </span>

                                                <?php print $current_user->data->user_email; ?>
                                            </address>
                                        </div>
                                    </article>

                                    <div class="<?php echo tath_role_class( 'learnervisibility' ); ?>">
                                        <?php
                                        $html = '';

                                        if ( is_user_logged_in() && $is_user_taking_course ) {
                                            //  $html .= '<span class="course-completion-rate">' . sprintf( __( 'Currently completed %1$s of %2$s in total', 'woothemes-sensei' ), '######', $total_lessons ) . '</span>';
                                            $html .= '<div class="meter+++++"><span style="width: @@@@@%">@@@@@%</span></div>';
                                        }

                                        if ( is_user_logged_in() && $is_user_taking_course ) {
                                            // Add dynamic data to the output
                                            $html = str_replace( '######', $lesson_count, $html );
                                            $progress_percentage = abs( round( (doubleval( $lesson_count ) * 100) / ($total_lessons_count), 0 ) );

                                            /* if ( 0 == $progress_percentage ) { $progress_percentage = 5; } */
                                            $html = str_replace( '@@@@@', $progress_percentage, $html );
                                            if ( 50 < $progress_percentage ) {
                                                $class = ' green';
                                            } elseif ( 25 <= $progress_percentage && 50 >= $progress_percentage ) {
                                                $class = ' orange';
                                            } else {
                                                $class = ' red';
                                            }

                                            $html = str_replace( '+++++', $class, $html );
                                            $html = '';
                                            // $html = '<span class="course-completion-rate">PROGRESS OF CHAPTER ' . $chapter_num . "</span>";
                                        } // End If Statement

                                        $html .= '<div class="barfiller-container"><div id="bar4" class="barfiller">
                                                    <div class = "tipWrap">
                                                        <span class = "tip"></span>
                                                    </div>
                                                    <span class="fill" data-percentage="' . $progress_percentageEXTERNAL_FRAGMENT . '"></span>
                                                    <div style="display:none;">' . $progress_percentage . '%</div></div>';
                                        echo $html;
                                        ?>
                                    </div>

                                    <div class="<?php echo tath_role_class( 'learnervisibility' ); ?>">
                                        <div class="training-actions">
                                            <a href="<?php echo get_permalink( $course->ID ); ?>" class="training-actions--button"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/60DayWhiteIcon.png" alt=""></a>
                                            <a href="<?php echo $course_page_url; ?>" class="training-actions--button">Continue Training</a>
                                        </div>
                                    </div>

                                    <!--continue training condition start -->
                                    <?php
                                    $statuses = courses_status( $current_user->ID );
                                    foreach ( $courses as $course ) {
                                        if ( array_key_exists( $course->ID, $statuses ) ) {
                                            echo '<a href="http://www.tathatagolf.com/course/60-day-program/" class="redLinkBtn c_btn_right">Continue Training</a>';
                                        } else {
                                            echo '<a href="http://www.tathatagolf.com/60-day-program/" class="redLinkBtn c_btn_right">Continue Training</a>';
                                        }
                                    }
                                    ?>
                                </article>
                            </div>
                        </div>
                    </section>

                    <div class="clearfix"></div>
                </section>
            </div><!-- #content -->
        </div><!-- #primary -->
    </div><!-- #main-content -->

<?php
  get_footer();
