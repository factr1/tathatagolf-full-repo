<?php
/*
* Template Name: New template for new topix
*/

get_header();
?>

<?php get_template_part( 'parts/page-title' ); ?>

<?php if ( ! bbp_is_single_user() ) { ?>
    <div class="headerwidgets">
        <div class="forehead-wrapper">
            <?php dynamic_sidebar( 'sidebar-forehead' ); ?>
        </div>

        <div class="clear"></div>
    </div>
<?php } ?>

    <section id="breadcrumbs">
        <div class="hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <div class="flex -flex-start">
                            <ul class="breadcrumb">
                                <?php bbp_breadcrumb(); ?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="flex -flex-end">
                            <?php echo do_shortcode( '[followUs]' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="visible-xs">
            <div class="container">
                <div class="flex">
                    <div>
                        <ul class="breadcrumb">
                            <?php bbp_breadcrumb(); ?>
                        </ul>
                    </div>

                    <div>
                        <?php echo do_shortcode( '[followUs]' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pageInnerContentWrap">
        <div class="container">
            <div class="row bbpress-forum-container bbpress">
                <article class="col-sm-9 left-forum">
                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">
                            <div class="new-forum-topic"><?php echo do_shortcode( '[bbp-topic-form]' ); ?> </div>
                        </div><!-- #content -->
                    </div><!-- #primary -->
                </article>

                <article class="col-sm-3 rightPanel right-forum">
                    <?php
                    get_sidebar();
                    get_sidebar( 'content' );
                    ?>
                </article>
            </div>
        </div><!-- #main-content -->
    </section>

<?php
get_footer();
