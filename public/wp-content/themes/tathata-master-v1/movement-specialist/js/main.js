/*	Movement Specialist JS
 *	Author: Eric Stout / Factor 1
 *  Copyright 2015 Factor 1
 */

// Load Fonts
WebFontConfig = {
	google: {families: ['Open+Sans:400,600,700,800:latin', 'Open+Sans+Condensed:300,700:latin']}
};
(function() {
	var wf = document.createElement('script');
	wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
	wf.type = 'text/javascript';
	wf.async = 'true';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(wf, s);
})();

// Testimonials Slider
jQuery('.ms-testimonials--slider').slick({
	autoplay: true,
	autoplaySpeed: 2000,
	arrows: false,
	dots: false,
	infinite: true,
	slidesToShow: 1,
	fade: true,
	cssEase: 'linear',
	draggable: false,
	responsive: [
		{
			breakpoint: 640,
			settings: {
				slidesToShow: 1
			}
		}
	]
});

// Launch Second Slick Slider for Golf Industry
jQuery('.ms-golf-industry').slick({
	autoplay: false, infinite: true, arrows: true, dots: false, draggable: false
});

// Launch Third Slick Slider for Golf Opporunity Slider
jQuery('.ms-opportunity--slider').slick({
	autoplay: false, infinite: true, arrows: true, dots: false,
});

// Icon Description Toggles
var hideDescriptions = function() {
	jQuery('.ms-description--container').css('display', 'none');
};

jQuery('li a').on('hover', function() {
	var $this = jQuery(this);
	var $class = $this.attr('class');

	hideDescriptions();
	jQuery('#' + $class).css({
		'display': 'block'
	});

});

// Materials Toggle
// var materialsOpen = false;
// jQuery('#ms-materials--compact .button').on('click', function(){
//   if (materialsOpen === false) {
//     jQuery('#ms-materials').css({
//       'display': 'block'
//     });
//     materialsOpen = true;
//   } else{
//     jQuery('#ms-materials').css('display', 'none');
//     materialsOpen = false;
//   }
// });

// Hours Trained Counter
jQuery(document).ready(function() {
	var DayDiff = function(start) {
		var now = new Date().getTime();
		start = new Date(start).getTime();

		return Math.floor((now - start) / 86400000);
	};

	var hoursTrained = (DayDiff('2015-12-29') * 209 + 24514 ); // take the initial date, how many days have passed? multiply that by 50 installs and add it to the base install

	function commaSeparateNumber(val) { //add commas to the end value
		while (/(\d+)(\d{3})/.test(val.toString())) {
			val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
		}
		return val;
	}

	function updateHours() { //throw it in the HTML
		jQuery('#hoursTrained').html(commaSeparateNumber(hoursTrained));
	}

	updateHours();
});

// Video Modal Play Controls
var video = document.getElementById('video');

jQuery(document).on('open', '.remodal', function() {
	video.play();
});

jQuery(document).on('close', '.remodal', function(e) {
	video.pause();
	video.currentTime = 0;
});

// Link the two slides together for Slick Slider syncing
jQuery('.msLinkedSlider').slick({
	autoplay: false,
	infinite: true,
	arrows: true,
	dots: false,
	asNavFor: '.msLinkedSlider',
	draggable: false,
	speed: 500,
	fade: true,
	cssEase: 'linear'
});
