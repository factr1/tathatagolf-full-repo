<?php
/* Template Name: Movement Specialist - Become */

get_header();
getPageHero();
?>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/movement-specialist/css/ms-styles.min.css">

    <div id="blackbar"></div>

    <div class="ms-blocks">

        <!-- MS Testimonials -->
        <?php // WP_Query arguments
        $args = array(
            'post_type'            => array( 'testimonial' ),
            'testimonial_category' => 'teacher-testimonials',
            'posts_per_page'       => '20',
            'orderby'              => 'rand',
        );

        // The Query
        $query = new WP_Query( $args );
        if ( $query->have_posts() ):
            ?>
            <div id="ms-testimonials">
                <h1>Students Everywhere Are Waiting For You</h1>
                <div class="ms-testimonials--row">
                    <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/MapGif_v4.gif" alt="Animated map">
                    <div class="ms-testimonials--slider">
                        <?php while ( $query->have_posts() ): $query->the_post();

                            // Social Source Settings
                            $source = get_field( 'social_source' );

                            if ( $source == 'facebook' ):
                                $social_icon  = 'fa-facebook';
                                $social_color = '#3b5998';
                            elseif ( $source == 'twitter' ):
                                $social_icon  = 'fa-twitter';
                                $social_color = '#55acee';
                            else:
                                $social_icon  = 'fa-envelope';
                                $social_color = '#CF0A2C';
                            endif;

                            ?>
                            <div class="ms-testimonials--social" style="border-left: 5px solid <?php echo $social_color; ?>">
                                <h3><?php the_title(); ?></h3>
                                <?php the_excerpt(); ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>

                <div class="ms-hours">
                    <h3>
                        <span id="hoursTrained">24,514</span> Hours of Learning &amp; Training Completed by Tathata Students Since September 1, 2015
                    </h3>
                </div>
            </div>
        <?php endif;
        wp_reset_postdata(); ?>

        <!-- MS First Row -->
        <div class="ms-blocks--row">
            <div class="ms-blocks--column ms-education ms-half">
                <a>
                    <h2><span class="red">Unprecedented</span> Education</h2>
                    <p>Be exposed to and trained in Tathata Golf mind, body &amp; swing practices and principles hailed as revolutionary and refreshing by industry leaders.</p>
                </a>
            </div>

            <div class="ms-blocks--column ms-curriculum ms-half">
                <a>
                    <h2><span class="red">Extensive</span> Curriculum</h2>
                    <p>Reap the benefits of being supported by one of the most extensive learning and training curriculums revered as truth in the game of golf, the Tathata Golf 60-Day Program.</p>
                </a>
            </div>
        </div>

        <!-- MS Second Row -->
        <div class="ms-blocks--row">
            <div class="ms-blocks--column ms-teaching ms-third">
                <a>
                    <div>
                        <h2>The Teaching <span class="red">Entrepreneur</span></h2>
                        <p>Full post-certification support for the teaching entrepreneur including the Tathata Golf business model, marketing kit and daily operations/training materials for you generate revenue and enhance your career.</p>
                    </div>
                </a>
            </div>

            <div class="ms-blocks--column ms-webpage ms-third">
                <a>
                    <div>
                        <h2><span class="red">Personalized</span> Webpage</h2>
                        <p>Upon certification, enjoy your own personalized webpage on TathataGolf.com findable by all current students and website visitors craving your support and expertise.</p>
                    </div>
                </a>
            </div>

            <div class="ms-blocks--column ms-market ms-third">
                <a>
                    <div>
                        <h2>We <span class="red">Market</span> You</h2>
                        <p>Be promoted, marketed and validated by a worldwide brand and supported by the most well-respected and recognizable individuals and organizations in the game of golf.</p>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <!-- What is TG MS Slider -->
    <section id="ms-whatis">
        <div>
            <h2>What Is A Tathata Golf Certified Movement Specialist?</h2>

            <?php the_field( 'ms_intro' ); ?>

            <br style="clear: both;">

            <h3 class="text-center">Proven turnkey business models to bring the new amenity of Tathata Golf Training into your facility.</h3>

            <ul class="ms-icon--grid">
                <li>
                    <a class="golf-instructors">
                        <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/golf-instructors.png" alt="">
                        <span>Golf<br>Instructors</span>
                    </a>
                </li>
                <li>
                    <a class="personal-trainer">
                        <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/personal-trainers.png" alt="">
                        <span>Personal<br>Trainers</span>
                    </a>
                </li>
                <li>
                    <a class="physical-education">
                        <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/physical-education.png" alt="">
                        <span>Physical Education<br>Providers</span>
                    </a>
                </li>
                <li>
                    <a class="yoga-instructors">
                        <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/yoga.png" alt="">
                        <span>Yoga<br>Instructors</span>
                    </a>
                </li>
                <li>
                    <a class="coaches">
                        <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/coaches.png" alt="">
                        <span>High School &amp;<br>Collegiate Coaches</span>
                    </a>
                </li>
                <li>
                    <a class="martial-arts">
                        <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/martial-arts.png" alt="">
                        <span>Martial Art<br>Instructors</span>
                    </a>
                </li>
            </ul>
        </div>
    </section>

    <!-- Icon Descriptions --><!-- Golf Instructor -->
    <div id="golf-instructors" class="ms-description--container">
        <div>
            <div class="ms-description--image"></div>

            <div class="ms-description--detail">
                <div>
                    <h2>Golf Instructors</h2>
                    <?php the_field( 'ms_golf_instructors' ); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Personal Trainer -->
    <div id="personal-trainer" class="ms-description--container">
        <div>
            <div class="ms-description--image"></div>

            <div class="ms-description--detail">
                <div>
                    <h2>Personal Trainers</h2>
                    <?php the_field( 'ms_personal_trainers' ); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Physical Education -->
    <div id="physical-education" class="ms-description--container">
        <div>
            <div class="ms-description--image"></div>

            <div class="ms-description--detail">
                <div>
                    <h2>Physical Education Providers</h2>
                    <?php the_field( 'ms_physical_educators' ); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- yoga Instructors -->
    <div id="yoga-instructors" class="ms-description--container">
        <div>
            <div class="ms-description--image"></div>

            <div class="ms-description--detail">
                <div>
                    <h2>Yoga Instructors</h2>
                    <?php the_field( 'ms_yoga_instructors' ); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Coaches -->
    <div id="coaches" class="ms-description--container">
        <div>
            <div class="ms-description--image"></div>

            <div class="ms-description--detail">
                <div>
                    <h2>High School &amp; Collegiate Coaches</h2>
                    <?php the_field( 'ms_coaches' ); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Martial Arts -->
    <div id="martial-arts" class="ms-description--container">
        <div>
            <div class="ms-description--image"></div>

            <div class="ms-description--detail">
                <div>
                    <h2>Martial Art Instructors</h2>
                    <?php the_field( 'ms_martial_arts' ); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Disclaimer -->
<?php if ( get_field( 'ms_disclaimer' ) ) : ?>
    <section id="ms-disclaimer" style="background: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.9)), url('<?php the_field( 'ms_disclaimer_background' ); ?>') #5a5a5a center center/cover no-repeat">
        <div>
            <p><strong>Disclaimer: </strong><?php the_field( 'ms_disclaimer' ); ?></p>
        </div>
    </section>
<?php endif; ?>

    <!-- Video Callout -->
    <section id="ms-video-callout">
        <h2>
            This is your opportunity to be part of
            <span class="red">reshaping golf</span> instruction out of care for the game and its future
        </h2>
    </section>

    <section id="ms-video" style="background: <?php the_field( 'ms_video_bc' ); ?>">
        <div class="container -padded-v">
            <div class="row">
                <div class="col-xs-12">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe id="ms-video" src="https://player.vimeo.com/video/154794864?title=0&byline=0&portrait=0&player_id=ms-video" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Offset Blocks -->
    <div class="ms-blocks--row">
        <div class="ms-blocks--column ms-half">

            <!-- Left slider -->
            <div class="ms-blocks--column" style="background: #f7f7f7">
                <h3 class="golf-industry">Challenges In The Golf Industry</h3>

                <div class="ms-golf-industry" style="background: #f7f7f7">
                    <div class="ms-golf-industry--slide">
                        <span class="ms-number">1</span>
                        <p>Over 29,000 golf teaching pros teaching a variety of 29,000 different things with conflicting tips constantly being shared in golf magazines, online publications and videos.</p>
                    </div>

                    <div class="ms-golf-industry--slide">
                        <span class="ms-number">2</span>
                        <p>No standardized curriculum of golf education/instruction that golfers and instructors can revere as truth and rely on.</p>
                    </div>

                    <div class="ms-golf-industry--slide">
                        <span class="ms-number">3</span>
                        <p>No opportunity for yoga instructors, personal trainers, and martial art instructors to teach full swing, short game, and putting golf movements.</p>
                    </div>

                    <div class="ms-golf-industry--slide">
                        <span class="ms-number">4</span>
                        <p>Golfers are slowly leaving the game because it is too difficult and time consuming to learn.</p>
                    </div>

                    <div class="ms-golf-industry--slide">
                        <span class="ms-number">5</span>
                        <p>Nowhere to validate customer satisfaction prior to spending money on a golf training session.</p>
                    </div>
                </div>

                <p class="swipe-text">Swipe left or right for more </p>
            </div>
        </div>

        <div class="ms-blocks--column ms-half">
            <div class="ms-blocks--column ms-opportunity">
                <style scoped>
                    .slick-prev, .slick-next {
                        top: 38%;
                    }
                </style>

                <h3>Tathata Golf Is The Solution</h3>
                <div class="ms-opportunity--slider">

                    <div class="ms-opportunity--slide">
                        <div class="ms-opportunity--content">
						<span class="ms-number">1</span>
                            <p>Become part of the most united voice and trusted brand in golf instruction, education and training.</p>
                        </div>
                    </div>

                    <div class="ms-opportunity--slide">
                        <div class="ms-opportunity--content">
						<span class="ms-number">2</span>
                            <p>Transform a generation of golfers leading to current golfers playing more and bringing more newcomers to the game than any other previous generation.</p>
                        </div>
                    </div>

                    <div class="ms-opportunity--slide">
                        <div class="ms-opportunity--content">
						<span class="ms-number">3</span>
                            <p>Make the game fun, easy and fast to learn for all who come to excel and experience an uncommon sense of greatness with a golf club in their hand.</p>
                        </div>
                    </div>

                    <div class="ms-opportunity--slide">
                        <div class="ms-opportunity--content">
						<span class="ms-number">4</span>
                            <p>For the first time, be supported far beyond just an education by the Tathata Golf business model, marketing kit and operational/training materials upon certification.</p>
                        </div>
                    </div>

                    <div class="ms-opportunity--slide">
                        <div class="ms-opportunity--content">
						<span class="ms-number">5</span>
                            <p>Movement Specialist Ratings to protect the golfer, movement specialists, and ensure a world-class training experience true to the essence of Tathata Golf.</p>
                        </div>
                    </div>
                </div>

                <p class="swipe-text">Swipe left or right for more </p>
            </div>
        </div>
    </div>

    <!-- Training and Cert Program -->
    <section id="ms-programs">
        <h2><?php the_field( 'ms_training_title' ); ?></h2>
        <div class="ms-programs--content">
            <h3 class="text-center"><?php the_field( 'ms_training_secondary_title' ); ?></h3>

            <!-- Toggle Containers -->
            <div class="ms-toggle--container">
                <!-- Section 1 Toggle -->
                <div class="ms-toggle ms-toggle--half">
                    <div class="ms-toggle--content">
                        <div class="ms-chapter">
                            <p><span>Chapter 1 | </span> Body, Stretching &amp; Mind 1</p>
                            <span>Hours 1 - 15</span>
                        </div>

                        <div class="ms-chapter">
                            <p><span>Chapter 2 | </span> Hands, Arms &amp; Mind 2</p>
                            <span>Hours 16 - 29</span>
                        </div>

                        <div class="ms-chapter">
                            <p><span>Chapter 3 | </span> Pressure, Impact &amp; Mind 3</p>
                            <span>Hours 30 - 45</span>
                        </div>

                        <div class="ms-chapter">
                            <p><span>Chapter 4 | </span> Speed, Strength &amp; Mind 4</p>
                            <span>Hours 46 - 56</span>
                        </div>

                        <div class="ms-chapter">
                            <p><span>Chapter 5 | </span> Short Game, Putting &amp; Mind 5</p>
                            <span>Hours 57 - 71</span>
                        </div>

                        <div class="ms-chapter">
                            <p><span>Chapter 6 | </span> Shape, Trajectory &amp; Mind 6</p>
                            <span>Hours 72 - 84</span>
                        </div>

                        <a data-remodal-target="additional"> Also included in each chapter <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
                <!-- Section 2 Toggle -->
                <div class="ms-toggle ms-toggle--half">
                    <div class="ms-toggle--content">
                        <div class="ms-chapter">
                            <p><span>Chapter 7 | </span> Enhancing Your Career</p>

                            <span>Hours 85 - 90</span>

                            <ul>
                                <li>Tathata Golf Product Education</li>
                                <li>Certified Incentive Program</li>
                                <li>Lesson Packages and Product Sales Training</li>
                                <li>Tathata Golf Branding Materials</li>
                                <li>Operations &amp; Training Materials Support</li>
                                <li>Entrepreneurial Career Support &amp; Growth</li>
                            </ul>
                        </div>

                        <h3>BONUS: Professionally Accredited Program </h3>

                        <p>Receive continued education credits upon completion of the training program from the LPGA and other professional golf, yoga, and personal training organizations.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Post Certification Materials and Support -->
    <section id="ms-materials--compact">
        <h1>Post Certification Materials and Support</h1>
        <?php the_field( 'ms_post-cert' ); ?>
    </section>

    <section id="ms-materials">
        <div>
            <div class="row">
                <div class="medium-4 text-center columns">
                    <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/BusinessModel03.png" alt="">
                </div>

                <div class="medium-7 end columns">
                    <h4>Business Model</h4>
                    <?php the_field( 'ms_post-cert_business_model' ); ?>
                </div>
            </div>

            <div class="row">
                <div class="medium-7 columns">
                    <h4>Marketing Kit</h4>
                    <?php the_field( 'ms_post-cert_marketing' ); ?>
                </div>

                <div class="medium-4 text-center columns">
                    <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/marketingkit.png">
                </div>
            </div>

            <div class="row">
                <div class="medium-4 text-center columns">
                    <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/dailyoperations.png">
                </div>

                <div class="medium-7 columns">
                    <h4>Daily Operations &amp; Training Support Materials</h4>
                    <?php the_field( 'ms_post-cert_daily_ops' ); ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Training Package -->
    <section id="ms-training">
        <div class="row">
            <h1>Training Packages Tathata Golf Certified Movement Specialists Offer</h1>
            <div class="medium-4 text-center columns">
                <h4>Chapter Follow Ups </h4>
                <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/assessment3.jpg" alt="">
                <?php the_field( 'ms_follow_ups' ); ?>
            </div>

            <div class="medium-4 text-center columns">
                <h4>Group Training Sessions </h4>
                <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/grouptraining.jpg" alt="">
                <?php the_field( 'ms_group_training' ); ?>
            </div>

            <div class="medium-4 text-center columns">
                <h4>Half &amp; Full Day Training </h4>
                <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/1on1indoor1.jpg" alt="">
                <?php the_field( 'ms_half_training' ); ?>
            </div>
        </div>
    </section>

    <!-- New Sliders -->
<?php if ( have_rows( 'ms_sliders' ) ): ?>
    <div id="msInfoSliders" class="ms-blocks--row">
        <?php while ( have_rows( 'ms_sliders' ) ) : the_row(); ?>
            <div class="ms-blocks--column ms-half">
                <div class="ms-blocks--column">
                    <h3 class="golf-industry"><?php the_sub_field( 'ms_sub_slider_title' ); ?></h3>

                    <?php if ( have_rows( 'ms_sub_slider' ) ): ?>
                        <div class="msLinkedSlider">
                            <?php while ( have_rows( 'ms_sub_slider' ) ) : the_row(); ?>
                                <div class="msLinkedSlider--slide">
                                    <div class="giSlide-container">
                                        <p class="ms-number--title"><?php the_sub_field( 'ms_slider_slide_title' ); ?></p>

                                        <div class="giSlide-content">
                                            <span class="ms-number"><?php the_sub_field( 'ms_slider_slide_id' ); ?></span>
                                            <p><?php the_sub_field( 'ms_slider_slide_content' ); ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>

                    <p class="swipe-text">Swipe left or right for more </p>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>

    <!-- Buy Now -->
    <section id="ms-buy">
        <div class="ms-row text-center">
            <h3><?php the_field( 'ms_buy_now_title' ); ?></h3>

            <?php if ( get_field( 'ms_buy_now_img' ) ) : ?>
                <img src="<?php the_field( 'ms_buy_now_img' ); ?>" alt="Buy Now">
            <?php else : ?>
                <img src="<?php echo get_template_directory_uri(); ?>/movement-specialist/images/BuyNow-03.jpg" alt="Buy Now">
            <?php endif; ?>

            <?php the_field( 'ms_buy_now' ); ?>

            <a href="<?php echo esc_url( 'http://www.tathatagolfcertified.com?add_to_cart=16448' ); ?>" class="button button-green"> Buy Now </a>
        </div>
    </section>

    <!-- Video Modal -->
    <div class="remodal text-center" data-remodal-id="video" data-remodal-options="hashTracking: false">
        <style scoped>
            .remodal-close {
                left: initial;
                right: 0;
            }
        </style>

        <button data-remodal-action="close" class="remodal-close"></button>

        <div class="flex-video widescreen vimeo"></div>
    </div>

    <!-- Additional Info Modal -->
    <div class="remodal additional-remodal" data-remodal-id="additional" data-remodal-options="hashTracking: false">
        <button data-remodal-action="close" class="remodal-close"></button>
        <ul>
            <li>Administering and Delivering Individual/Small Group Chapter Follow-Up Assesments Per Chapter</li>
            <li>Administering and Delivering Small and Large Group Training Sessions Per Chapter</li>
            <li>24 Additional Lessons of Learning Specific to Certified Movement Specialists</li>
            <li>Greatest Player and Athlete Supporting Movement Breakdowns (Chapters 1, 2, 3 &amp; 5)</li>
        </ul>
    </div>

    <img class="beacon" border="0" src="http://r.turn.com/r/beacon?b2=P_AYcJBQXbQH0yAC_9IQpZhwxyZGxpt9A9pzJmqIATDwmwgCAYlZ5lTJGOrh3DGCbjopRYcdYaZeLRI9P-WP4Q&cid=">

    <script src="<?php echo get_template_directory_uri(); ?>/movement-specialist/js/global.min.js" defer></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/remodal.min.js" defer></script>
    <script src="https://use.fontawesome.com/71eb3125bb.js" defer></script>

<?php
get_footer();
