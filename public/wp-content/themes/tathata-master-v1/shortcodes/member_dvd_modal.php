<div id="remodal-dvdpromo" class="remodal" data-remodal-id="dvdpromo" data-remodal-options="hashTracking: false">
    <button data-remodal-action="close" class="remodal-close"></button>

    <div class="memberPromo">
        <h2 style="text-align:center; margin-bottom:22px;">30-Disc DVD Set Available</h2>

        <div class="col-sm-6">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/memberdvds_modal.jpg" class="img-responsive" alt="DVD Bundle">
        </div>

        <div class="col-sm-6 promocontent">
            <p style="text-align:left;">As a purchaser of 60-Day Training Program, you are eligible to purchase the 30-Disc hardback of DVDs. These DVDs are great to have for offline training. Please note, the DVDs contain only the 60 lessons of the program, no chapter support or daily extras.</p>

            <div class="memberbuy">
                <p class="price">$89.95</p>
                <a href="<?php echo esc_url( home_url( '/?add_to_cart=42489' ) ); ?>" class="buynowbutton">Add to Cart</a>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function($) {
        var inst = $('[data-remodal-id=dvdpromo]').remodal();
        inst.open();

        $(document).on('closed', '#remodal-dvdpromo', function() {
            inst.destroy();
        });
    });
</script>
