<?php
/**
 * This template is actually used for the dashboard/profile page
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $post, $woothemes_sensei, $current_user, $cour_id;
$html = '';

$UserMeta          = get_user_meta( $current_user->data->ID );
$lessons_completed = 0;
$mid               = isset( $_GET['mid'] ) ? $_GET['mid'] : '';
// do_action('sensei_course_single_chapter', $mid);
// global $course_module, $course_module_lessons;
// $course_lessons    = $course_module_lessons;
// $ChapterID         = @trim(str_ireplace('Chapter', '', strstr($course_module->name, ':', true)));
// $total_lessons     = count($course_lessons);
$total_lessons = 1;

// Check if the user is taking the course
$is_user_taking_course = $course_ids ? WooThemes_Sensei_Utils::user_started_course( $course_ids[0], $current_user->ID ) : false;

// Get User Meta
get_currentuserinfo();

if ( 0 < $total_lessons ) {
    $lessons_completed   = 0;
    $show_lesson_numbers = false;
    $post_classes        = array( 'course', 'post' );
?>

    <section class="contentWrapper">
        <section id="title-section">
            <div class="container">
                <h1>Profile</h1>
            </div>
        </section>

        <!-- Black Bar -->
        <div id="blackbar-menu">
            <div class="container">
                <div class="c_my_account">
                    <ul class="ms-nav">
                        <li class="i-menu cy menu-item menu-item-type-custom menu-item-object-custom">
                            <a href="<?php echo esc_url( home_url( '/dashboard' ) ); ?>">AC</a>
                        </li>
                    </ul>
                </div>

                <div class="<?php echo tath_role_class( 'learnervisibility' ); ?>">
                    <?php wp_nav_menu( array('menu' => 'Lesson Menu') ); ?>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <?php get_template_part( 'parts/breadcrumbs' ); ?>

        <section class="profilePage">
            <div class="container">
                <div class="row">
                    <article class="col-xs-12 col-sm-6 <?php echo tath_role_class( 'learnervisibility' ); ?>">
                        <?php
                        foreach ( $courses as $course ) {
                            $course_modules          = $sen_modules->get_course_modules( $course->ID );
                            $cour_id                 = $course->ID;
                            $total_lessons_count     = 0;
                            $lessons_completed_count = 0;
                            ?>

                            <h1 class="day-program-head-for-private-profile"><?php echo $course->post_title; ?></h1>

                            <!-- quick links drop down -->
                            <ul class="profile_menu">
                                <li><a href="#">Select your profile items</a>
                                    <ul>
                                        <li class="profile-items-selection <?php if ( bbp_is_single_user_profile() ) : ?>current<?php endif; ?>" role="presentation">
                                            <span class="vcard bbp-user-profile-link">
                                                <a role="menuitem" tabindex="-1" href=" <?php global $current_user;
                                                get_currentuserinfo();
                                                echo "http://www.tathatagolf.com/forums/members/" . $current_user->user_login . "/"; ?> ">
                                                    Profile
                                                </a>
                                            </span>
                                        </li>
                                        <li class="profile-items-selection <?php if ( bbp_is_single_user_topics() ) : ?>current<?php endif; ?>">
                                            <span class='bbp-user-topics-created-link'>
                                                <a role="menuitem" tabindex="-1" href=" <?php global $current_user;
                                                get_currentuserinfo();
                                                echo "http://www.tathatagolf.com/forums/members/" . $current_user->user_login . "/topics/"; ?> ">
                                                    Topics Started
                                                </a>
                                            </span>
                                        </li>
                                        <li class="profile-items-selection <?php if ( bbp_is_single_user_replies() ) : ?>current<?php endif; ?>">
                                            <span class='bbp-user-replies-created-link'>
                                                <a role="menuitem" tabindex="-1" href=" <?php global $current_user;
                                                get_currentuserinfo();
                                                echo "http://www.tathatagolf.com/forums/members/" . $current_user->user_login . "/replies/"; ?> ">
                                                    Recent Replies
                                                </a>
                                            </span>
                                        </li>
                                        <?php if ( bbp_is_user_home() || current_user_can( 'edit_users' ) ) : ?>
                                            <li class="profile-items-selection <?php if ( bbp_is_single_user_edit() ) : ?>current<?php endif; ?>">
                                                <span class="bbp-user-edit-link">
                                                    <a role="menuitem" tabindex="-1" href=" <?php global $current_user;
                                                    get_currentuserinfo();
                                                    echo "http://www.tathatagolf.com/forums/members/" . $current_user->user_login . "/edit/"; ?> ">
                                                        UpdatevAccount Info
                                                    </a>
                                                </span>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            </ul><!-- quick links drop down end -->

                            <ul class="accordion_example smk_accordion">
                                <?php
                                $chap_id       = $start_lesson_count = 1;
                                $total_lessons = 0;
                                $key           = 1;
                                $next_lesson   = null;

                                foreach ( $course_modules as $course_module ) {
                                    if ( $course_module->parent == 0 ) {
                                        do_action( 'sensei_course_single_chapter', $course_module->term_id );
                                        global $course_module, $course_module_lessons;
                                        $course_lessons = $course_module_lessons;
                                        //$ChapterID = @trim(str_ireplace('Chapter', '', strstr($course_module->name, ':', true)));

                                        $start_lesson_count = $total_lessons + $start_lesson_count;
                                        $total_lessons      = count( $course_lessons );
                                        $total_lessons_count += $total_lessons;
                                        ?>

                                        <!-- Section 1 -->
                                        <li class="acc_active accordion_in">
                                            <div class="welcomeToHeading acc_head">
                                                <div class="acc_icon_expand"></div>

                                                <ul>
                                                    <li>Chapter<span><?php printf( "%02s", $chap_id ); ?></span></li>
                                                    <li>Days<span><?php echo ( $total_lessons != 0 ) ? $start_lesson_count . '-' . $total_lessons_count : 0; ?></span></li>
                                                </ul>
                                            </div>

                                            <div class="acc_content">
                                                <p>Click on a day to begin your training!</p>

                                                <ul class="trainingProgressLevel">
                                                    <?php
                                                    $lesson_count = 1;

                                                    foreach ( $course_lessons as $lesson_item ) {
                                                        $single_lesson_complete = false;
                                                        $user_lesson_end        = '';

                                                        if ( is_user_logged_in() ) {
                                                            // Check if Lesson is complete
                                                            $user_lesson_status = WooThemes_Sensei_Utils::user_lesson_status( $lesson_item->ID, $current_user->ID );
                                                            $user_lesson_end    = WooThemes_Sensei_Utils::user_completed_lesson( $user_lesson_status );

                                                            if ( '' != $user_lesson_end ) {
                                                                //Check for Passed or Completed Setting
                                                                $course_completion = $woothemes_sensei->settings->settings['course_completion'];

                                                                if ( 'passed' == $course_completion ) {
                                                                    $lessons_completed ++;
                                                                    $single_lesson_complete = true;
                                                                    $post_classes[]         = 'lesson-completed';
                                                                }
                                                            }
                                                        }

                                                        if ( ! $single_lesson_complete && is_null( $next_lesson ) ) {
                                                            $next_lesson = $lesson_item->ID;
                                                        }

                                                        // Get Lesson data
                                                        $complexity_array  = $woothemes_sensei->frontend->lesson->lesson_complexities();
                                                        $lesson_length     = get_post_meta( $lesson_item->ID, '_lesson_length', true );
                                                        $lesson_complexity = get_post_meta( $lesson_item->ID, '_lesson_complexity', true );

                                                        if ( '' != $lesson_complexity ) {
                                                            $lesson_complexity = $complexity_array[ $lesson_complexity ];
                                                        }

                                                        $user_info     = get_userdata( absint( $lesson_item->post_author ) );
                                                        $is_preview    = WooThemes_Sensei_Utils::is_preview_lesson( $lesson_item->ID );
                                                        $preview_label = '';

                                                        if ( $is_preview && ! $is_user_taking_course ) {
                                                            $preview_label  = $woothemes_sensei->frontend->sensei_lesson_preview_title_text( $post->ID );
                                                            $preview_label  = '<span class="preview-heading">' . $preview_label . '</span>';
                                                            $post_classes[] = 'lesson-preview';
                                                        }
                                                        ?>

                                                        <?php if ( '' != $user_lesson_end && $single_lesson_complete ) { ?>
                                                            <li class="completed">
                                                                <span class="completed-level-complete completedLevel">
                                                                    <?php $comp_lesson ++; ?><?php print apply_filters( 'sensei_complete_text', __( 'Complete', 'woothemes-sensei' ) ) . '!'; ?>
                                                                </span>
                                                        <?php } else { ?>
                                                            <li>
                                                                <span class="test2 completed-level-inProgress completedLevel">
                                                                    <?php print apply_filters( 'sensei_in_progress_text', __( 'In Progress', 'woothemes-sensei' ) ); ?>
                                                                </span>
                                                        <?php } ?>
                                                                <div class="dayCount">
                                                                    <a href="<?php print esc_url( get_permalink( $lesson_item->ID ) ); ?>">Day <span><?php printf( "%02s", $key ++ ); ?></span></a>
                                                                </div>

                                                                <div class="trainingTimeDetail">
                                                                    <span>Total Time (<?php echo $lesson_length; ?> Minutes)</span>

                                                                    <span style="display:none;">
                                                                        <?php
                                                                        if ( '' != $lesson_complexity ) {
                                                                            print apply_filters( 'sensei_complexity_text', __( 'Complexity: ', 'woothemes-sensei' ) ) . $lesson_complexity;
                                                                        }
                                                                      ?>
                                                                    </span>
                                                                </div>
                                                            </li>
                                                        <?php
                                                        $lesson_count ++;
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    $chap_id ++;
                                }
                                ?>
                            </ul>
                        <?php } ?>
                    </article>

                    <article class="col-xs-12 col-sm-6 profile_details">
                        <?php
                        $statuses = courses_status( $current_user->ID );
                        foreach ( $courses as $course ) {
                            if ( array_key_exists( $course->ID, $statuses ) ) {
                                echo '<div class="courseInProgress"><span style="display:none;">' . $course->post_title . '-</span><h5 class="status in-progress"> ' . $statuses[ $course->ID ]['status'] . '</h5></div>';
                            }
                        }
                        ?>

                        <h3>Profile Details</h3>

                        <article class="profileDetailWrap">
                            <div class="leftPanelDetail">
                                <div class="profileImg"><?php echo get_avatar( $current_user->ID, 230 ); ?></div>

                                <a class="new-btn large" href="<?php echo esc_url( home_url( '/tathata-golf-testimonials' ) ); ?>">Submit a testimonial</a>

                                <h6>Badges Earned</h6>

                                <?php do_shortcode( '[badgeos_achievements_list_custom]' ); ?>

                                <?php
                                if ( ! empty( $UserMeta['user_thumbnail'][0] ) ) {
                                    $user_thumb       = get_post_meta( $UserMeta['user_thumbnail'][0] );
                                    $profile_img_path = site_url() . '/wp-content/uploads/' . $user_thumb['_wp_attached_file'][0];
                                ?>
                                    <style>
                                        .profilePage .profileDetailWrap .leftPanelDetail .profileImg {
                                            background: url("<?php print $profile_img_path; ?>") no-repeat scroll 10px 10px transparent;
                                        }
                                    </style>
                                <?php } ?>
                            </div>

                            <div class="rightPanelDetail">
                                <h1><span><?php print ucfirst( $current_user->data->user_nicename ); ?></span></h1>

                                <ul class="memberActivityList <?php echo tath_role_class( 'learnervisibility' ); ?>">
                                    <?php
                                    $order_date_combine        = wcr_check_user_bought( $current_user->user_email, $current_user->ID, 72 );
                                    $order_date_combine_status = 0;

                                    if ( $order_date_combine ) {
                                        $order_date_combine_status = 1;
                                        $order_date                = date( 'd/m/y', strtotime( $order_date_combine ) );
                                        echo '<li class="profSixtyDayPro">60-Day Program Member<span class="startDate">Start Date: ' . $order_date . '</span></li>';
                                        echo '<li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: ' . $order_date . '</span></li>';
                                    }

                                    if ( $order_date_combine_status === 0 ) {
                                        $order_date_60 = wcr_check_user_bought( $current_user->user_email, $current_user->ID, 69 );

                                        if ( $order_date_60 ) {
                                            echo '<li class="profSixtyDayPro">60-Day Program Member<span class="startDate">Start Date: ' . date( 'd/m/y', strtotime( $order_date_60 ) ) . '</span></li>';
                                        } else {
                                            echo '<li class="profSixtyDayPro">60-Day Program Member<span class="startDate">Start Date: Manually Added</span></li>';
                                        }

                                        $order_date_online = wcr_check_user_bought( $current_user->user_email, $current_user->ID, 70 );

                                        if ( $order_date_online ) {
                                            echo '<li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: ' . date( 'd/m/y', strtotime( $order_date_online ) ) . '</span></li>';
                                        } else {
                                            echo '<li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: Manually Added</span></li>';
                                        }
                                    }
                                    ?>
                                </ul>

                                <?php echo do_shortcode( '[usereditlink]' ); ?>

                                <address>
                                    <span>Address</span>
                                    <?php
                                    if ( ! empty( $UserMeta['address'][0] ) ) {
                                        print $UserMeta['address'][0];
                                    }
                                    ?>

                                    <span>Phone </span>
                                    <?php
                                    if ( ! empty( $UserMeta['phone'][0] ) ) {
                                        print $UserMeta['phone'][0];
                                    }
                                    ?>

                                    <span>eMail </span>
                                    <?php print $current_user->data->user_email; ?>
                                </address>
                            </div>
                        </article>

                        <?php
                        $certificates = get_posts( [
                            'posts_per_page' => - 1,
                            'post_type'      => 'certificate',
                            'post_status'    => 'publish',
                            'meta_query'     => [
                                [
                                    'key'   => 'learner_id',
                                    'value' => get_current_user_id(),
                                ],
                                [
                                    'key'   => 'course_id',
                                    'value' => $course->ID,
                                ],
                            ]
                        ] );
                        ?>

                        <div>
                            <h6>My Certificates</h6>
                            <?php foreach ( $certificates as $certificate ) { ?>
                                <div>
                                    <a href="<?php echo esc_attr( get_permalink( $certificate->ID ) ); ?>" target="_blank">
                                        <i class="fa fa-file-o"></i> <?php echo esc_html( $course->post_title ); ?> (<?php echo $certificate->post_date; ?>)
                                    </a>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="<?php echo tath_role_class( 'learnervisibility' ); ?>">
                            <?php
                              $html = '';

                              if ( is_user_logged_in() && $is_user_taking_course ) {
                                $html .= '<span class="course-completion-rate">' . sprintf( __( 'Currently completed %1$s of %2$s in total', 'woothemes-sensei' ), '######', $total_lessons ) . '</span>';
                                $html .= '<div class="meter+++++"><span style="width: @@@@@%">@@@@@%</span></div>';

                                // Add dynamic data to the output
                                $html = str_replace( '######', $comp_lesson, $html );

                                $progress_percentage = abs( round( (doubleval( $comp_lesson ) * 100) / ($total_lessons_count), 0 ) );

                                if ( 0 == $progress_percentage ) {
                                  $progress_percentage = 0;
                                }

                                $html = str_replace( '@@@@@', $progress_percentage, $html );

                                if ( 50 < $progress_percentage ) {
                                  $class = ' green';
                                } elseif ( 25 <= $progress_percentage && 50 >= $progress_percentage ) {
                                  $class = ' orange';
                                } else {
                                  $class = ' red';
                                }

                                $html = str_replace( '+++++', $class, $html );
                                $html = '';
                              }

                              $html .= '<div class="barfiller-container">
                                            <div id="bar4" class="barfiller">
                                                <div class="tipWrap">
                                                    <span class="tip"></span>
                                                </div>
                                                <span class="fill" data-percentage="' . $progress_percentageEXTERNAL_FRAGMENT . '"></span>
                                                <div style="display:none;">' . $progress_percentage .'%</div>
                                            </div>';
                              echo $html;
                            ?>
                        </div>

                        <div class="<?php echo tath_role_class( 'learnervisibility' ); ?>">
                            <div class="training-actions">
                                <a href="<?php echo get_permalink( $course->ID ); ?>" class="training-actions--button"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/60DayWhiteIcon.png" alt=""></a>
                                <a href="<?php echo esc_url( get_permalink( $next_lesson ) ); ?>" class="training-actions--button">Continue Training</a>
                            </div>
                        </div>

                        <?php echo do_shortcode( '[woocommerce_my_account]' ); ?>
                    </article>
                </div>
            </div>
        </section>
    </section>

    <?php
  }
