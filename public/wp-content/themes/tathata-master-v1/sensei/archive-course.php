<?php
/**
 * Template Name: 60 Day promo staged
 */

// Grab the page IDs where the videos live
$ids = [ 1917 ];

get_header();
?>

    <section id="page-hero" class="hero" style="background: linear-gradient(rgba(0, 0, 0, .3), rgba(0, 0, 0, .4)), url('<?php echo esc_url( home_url( '/wp-content/uploads/2015/03/60Day-home-edition_mini.jpg' ) ); ?>') center center/cover no-repeat #990000">
        <div class="container">
            <div class="flex">
                <div class="hero-image">
                    <img class="img-responsive" src="<?php echo esc_url( home_url( '/wp-content/uploads/2015/03/Tathata-60dayTrianglex300-e1466191479694.png' ) ); ?>">
                </div>

                <div class="hero-content">
                    <h1 class="page-title">INTRODUCING THE TRAINING PROGRAM THAT IS CHANGING GOLF INSTRUCTION <span>FOREVER</span></h1>
                </div>
            </div>
        </div>
    </section>

    <div id="blackbar"></div>

    <div id="gtwrapper">
        <!-- As Seen On -->
        <section class="as-seen-on" style="border: none">
            <div class="container">
                <div class="row">
                    <p>As seen on <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gtimages/nbc-golf.png" width="100" height="25"> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ESPN.png" width="100" height="25"></p>
                </div>
            </div>
        </section>

        <!-- Ticker -->
        <style>
            p#tickerHours {
                font-weight: 700
            }
        </style>

        <?php get_template_part( 'parts/testimonial-ticker' ); ?>

        <div class="video-selection">
            <?php // Vimeo API bridge ?>
            <script src="https://f.vimeocdn.com/js/froogaloop2.min.js" defer></script>

            <div class="container">
                <div class="row">
                    <?php if ( have_rows( '60day_intro_videos', setEnvironmentID( $ids ) ) ): while ( have_rows( '60day_intro_videos', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                        <div class="col-md-4">
                            <div class="-screenshot hidden-xs hidden-sm" style="background: url('<?php the_sub_field( '60day_intro_video_screenshot' ); ?>') center center/cover no-repeat">
                                <a href="video" data-remodal-target="<?php the_sub_field( '60day_intro_video_title' ); ?>" class="ms-play"><i class="fa fa-play"></i></a>
                            </div>

                            <div class="flex-video vimeo widescreen visible-xs visible-sm">
                                <iframe src="https://player.vimeo.com/video/<?php the_sub_field( '60day_intro_video_video' ); ?>?color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>

                            <p class="video-title"><?php the_sub_field( '60day_intro_video_title' ); ?></p>

                            <p>Total Time (<?php the_sub_field( '60day_intro_video_duration' ); ?>)</p>

                            <div class="remodal hidden-xs hidden-sm" data-remodal-id="<?php the_sub_field( '60day_intro_video_title' ); ?>" data-remodal-options="hashTracking: false">
                                <style scoped>
                                    .remodal-close {
                                        right: 0;
                                        left: initial;
                                    }
                                </style>

                                <button data-remodal-action="close" class="remodal-close"></button>

                                <div class="flex-video vimeo widescreen video-top-video">
                                    <iframe id="<?php the_sub_field( '60day_intro_video_video' ); ?>" src="https://player.vimeo.com/video/<?php the_sub_field( '60day_intro_video_video' ); ?>?api=1&amp;player_id=<?php the_sub_field( '60day_intro_video_video' ); ?>&amp;color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                </div>
                            </div>

                            <?php // Reset the Vimeo video on modal close ?>
                            <script>
                                jQuery(function($) {
                                    var iframe = $('#<?php the_sub_field( '60day_intro_video_video' ); ?>')[0];
                                    var player = $f(iframe);

                                    $(document).on('closed', '.remodal', function() {
                                        player.api('unload');
                                    });
                                });
                            </script>
                        </div>
                    <?php endwhile; endif; ?>
                </div>
            </div>

            <!-- Featured Videos Intro -->
            <section class="promo_intro" style="padding: 10px 0">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <p style="font-weight: 700; padding: 5px 0; font-size: 15px">For more free videos, previews, &amp; routines enter your email:</p>
                        </div>

                        <div class="col-xs-12 col-sm-6">
                            <form class="hide-for-touch">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="email" name="initial_email" id="initial_email" value="" placeholder="Email Address" width="100%;">
                                    </div>

                                    <div class="col-md-6">
                                        <input type="submit" value="Access My Free Videos" data-remodal-target="emailform" href="#" onclick="getEmail()">
                                    </div>
                                </div>
                            </form>

                            <form class="show-for-touch">
                                <input type="submit" value="Enter Email and Access Free Videos" data-remodal-target="emailform" href="#">
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <!-- Call to Action -->
        <section class="product-buy">
            <div class="container">
                <div class="row">
                    <h1 class="col-md-12 text-center">The Revolutionary 60-Day<br>In-Home Golf Training Program</h1>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60Day.png" alt="Online instant Access on all devices">
                    </div>

                    <div class="col-md-4">
                        <div class="redBoxContent">
                            <div class="woocommerce">
                                <p class="wootitle">The 60-Day Training Program</p>
                                <p>Immediate and lifetime streaming access to the 60-Day Program.</p>

                                <p class="woobottom">
                                    <a href="<?php echo esc_url( home_url( '/?add_to_cart=8251' ) ); ?>" title="The 60-Day Training Program" class="greyLinkBtn">Buy Now <i class="fa fa-angle-right"></i></a>
                                    <span class="price">
                                        <span class="amount">$179.95 - Start Training Today!</span>
                                    </span>
                                </p>
                            </div>
                        </div>

                        <p class="dvd-addon">
                            <a href="<?php echo esc_url( home_url( '/?add_to_cart=8579' ) ); ?>">
                                Add on the 30-Disc<br>
                                DVD set for offline viewing
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="featured_headline">
            <style>
                .featured_headline {
                    position: relative;
                    display: block;
                    width: 100%;
                    margin: 0 0 30px;
                    background: rgb(102, 0, 0);
                    color: #fff;
                }

                /* TODO: is this actually used? */
                .headline_bgimage {
                    width: 100%;
                    height: 100%;
                    display: block;
                    background: url('http://www.tathatagolf.com/wp-content/uploads/2015/10/bkg_slider-04.jpg') center center no-repeat;
                    background-size: cover;
                    position: absolute;
                    z-index: -1;
                }

                .featured_headline .row {
                    padding: 20px 25px;
                    text-align: center;
                }

                .featured_headline h1 {
                    color: #fff;
                    font-size: 32px !important;
                    text-align: center;
                }
            </style>

            <div class="container">
                <div class="row ">
                    <h1>Revolutionizing the way golf Is Taught and Played</h1>
                </div>
            </div>
        </section>

        <section class="image-grid">
            <div class="container">
                <div class="row">
                    <p class="visible-xs" style="text-align:center; font-weight: bold">(Tap On Each Photo To See More)</p>

                    <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-3">
                        <?php if ( have_rows( '60day_image_grid', setEnvironmentID( $ids ) ) ): while ( have_rows( '60day_image_grid', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                            <li>
                                <div class="image-grid-container" style="background: url('<?php the_sub_field( '60day_ig_image' ); ?>') top center no-repeat;">
                                    <div class="box-overlay">
                                        <p class="text-center"><?php the_sub_field( '60day_ig_content' ); ?></p>
                                    </div>

                                    <p class="text-center grid-title"><?php the_sub_field( '60day_ig_title' ); ?></p>
                                </div>
                            </li>
                        <?php endwhile; endif; ?>
                    </ul>
                </div>
            </div>
        </section>

        <section class="inside-the-program">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-md-centered text-center">
                        <h1>What's Inside the Program?</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <p><strong>Chapter Breakdown:</strong></p>

                        <ul>
                            <li><i class="fa fa-check-circle"></i> Chapter 1 | Days 1-10 | Body, Stretching &amp; Mind 1</li>
                            <li><i class="fa fa-check-circle"></i> Chapter 2 | Days 11-20 | Hands, Arms &amp; Mind 2</li>
                            <li><i class="fa fa-check-circle"></i> Chapter 3 | Days 21-30 | Pressure, Impact &amp; Mind 3</li>
                            <li><i class="fa fa-check-circle"></i> Chapter 4 | Days 31-40 | Speed, Strength &amp; Mind 4</li>
                            <li><i class="fa fa-check-circle"></i> Chapter 5 | Days 41-50 | Short Game, Putting &amp; Mind 5</li>
                            <li><i class="fa fa-check-circle"></i> Chapter 6 | Days 51-60 | Shape, Trajectory &amp; Mind 6</li>
                        </ul>
                    </div>

                    <div class="col-md-6">
                        <p><strong>Course Takeaways:</strong></p>

                        <ul>
                            <li><i class="fa fa-plus-circle"></i> 140 training movements</li>
                            <li><i class="fa fa-plus-circle"></i> 14 different movement routines</li>
                            <li><i class="fa fa-plus-circle"></i> 3 greatest golfer and athletes movement videos</li>
                            <li><i class="fa fa-plus-circle"></i> 195 test questions and video answers</li>
                            <li><i class="fa fa-plus-circle"></i> 45 deeper discussions (mental and movement)</li>
                            <li><i class="fa fa-plus-circle"></i> 13 mental training lay down exercises</li>
                            <li><i class="fa fa-plus-circle"></i> 12 sitting/standing mental training exercises</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <div class="container twovideos">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h1 class="title" style="text-align: center; text-transform: uppercase; font-size: 2.5em; margin-bottom: .25em">A Message From <span class="red">Gary McCord</span> &amp; <span class="red">Brandel Chamblee</span></h1>

                    <div class="flex-video vimeo widescreen">
                        <iframe src="https://player.vimeo.com/video/134999203?color=ffffff&title=0&byline=0&portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

        <section class="instant-access">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60dayAccess.jpg" alt="Get online access to mobile devices for the Tathata 60-Day program" width="1000" height="600">
                    </div>

                    <div class="col-md-4">
                        <h2>Train Anywhere</h2>
                        <p>Enjoy our streaming product on any device wherever you may be.</p>

                        <div class="redBoxContent">
                            <div class="woocommerce">
                                <p class="wootitle">The 60-Day Training Program</p>
                                <p>Immediate and lifetime streaming access to the 60-Day Program.</p>

                                <p class="woobottom">
                                    <a href="<?php echo esc_url( home_url( '/?add_to_cart=8251' ) ); ?>" title="The 60-Day Training Program" class="greyLinkBtn">Buy Now <i class="fa fa-angle-right"></i></a>
                                    <span class="price">
                                        <span class="amount">$179.95 - Start Training Today!</span>
                                    </span>
                                </p>
                            </div>
                        </div>

                        <p class="dvd-addon">
                            <a href="<?php echo esc_url( home_url( '/?add_to_cart=8579' ) ); ?>">
                                Add on the 30-Disc<br>
                                DVD set for offline viewing
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <?php get_template_part( 'parts/money-back' ); ?>

        <!-- Email CTA -->
        <section class="email-signup">
            <div class="email-signup_bgimage1"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Free Video Giveaway</h3>
                        <p>Enter your e-mail for immediate access and learn how Tathata Golf will help your game.</p>
                    </div>

                    <div class="col-md-6">
                        <form class="hide-for-touch">
                            <input type="email" name="initial_email" id="initial_email" value="" placeholder="Email Address" width="100%;">
                            <br>
                            <input type="submit" value="Access My Free Videos" data-remodal-target="emailform" href="#" onclick="getEmail()">
                        </form>

                        <form class="show-for-touch">
                            <input type="submit" value="Enter Email and Access Free Videos" data-remodal-target="emailform" href="#">
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <!-- Sliders -->
        <?php if ( have_rows( 'ms_sliders', setEnvironmentID( $ids ) ) ): ?>
            <section id="ms-linked-sliders">
                <div class="holder"><h1>The Change Golf Has Been Waiting For</h1></div>

                <div id="msInfoSliders" class="ms-blocks--row" style="background: #f7f7f7">
                    <?php while ( have_rows( 'ms_sliders', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                        <div class="ms-blocks--column ms-half">
                            <div class="ms-blocks--column">
                                <h3 class="golf-industry"><?php the_sub_field( 'ms_sub_slider_title', setEnvironmentID( $ids ) ); ?></h3>

                                <?php if ( have_rows( 'ms_sub_slider', setEnvironmentID( $ids ) ) ): ?>
                                    <div class="msLinkedSlider">
                                        <?php while ( have_rows( 'ms_sub_slider', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                                            <div class="msLinkedSlider--slide">
                                                <div class="giSlide-container">
                                                    <p class="ms-number--title"><?php the_sub_field( 'ms_slider_slide_title', setEnvironmentID( $ids ) ); ?></p>

                                                    <div class="giSlide-content">
                                                        <span class="ms-number"><?php the_sub_field( 'ms_slider_slide_id', setEnvironmentID( $ids ) ); ?></span>
                                                        <p><?php the_sub_field( 'ms_slider_slide_content', setEnvironmentID( $ids ) ); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>

                                <p class="swipe-text">Swipe left or right for more </p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </section>
        <?php endif; ?>

        <?php
        get_template_part( 'parts/additional-support' );
        ?>

        <article class="darkbar fun-easy-convienent">
            <div class="container">
                <div class="row">
                    <div class="small-centered col-md-6 col-md-uncentered ">
                        <h1>Fun, Easy and Convenient</h1>
                        <h3><?php the_field( '60day_fec_description', setEnvironmentID( $ids ) ); ?></h3>

                        <ul>
                            <?php if ( have_rows( '60day_fec_features', setEnvironmentID( $ids ) ) ): while ( have_rows( '60day_fec_features', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                                <li><?php the_sub_field( '60day_fec_feature' ); ?></li>
                            <?php endwhile; endif; ?>
                        </ul>

                        <a href="<?php echo esc_url( home_url( '/additional-benefits/' ) ); ?>" class="greyLinkBtn additional_benefitsBTN">See additional benefits</a>
                    </div>
                </div>
            </div>
        </article>

        <section class="TathataMovementTraining">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center">Tathata Movement Training</h2>

                        <h4 class="text-center"><?php the_field( '60day_mt_description', setEnvironmentID( $ids ) ); ?></h4>

                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/gtimages/60d-con-mSet-full.png">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            <?php if ( have_rows( '60day_mt_features', setEnvironmentID( $ids ) ) ): while ( have_rows( '60day_mt_features', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                                <li><?php the_sub_field( '60day_mt_feature' ); ?></li>
                            <?php endwhile; endif; ?>
                        </ul>
                    </div>

                    <div class="col-md-6">
                        <ul>
                            <?php if ( have_rows( '60day_mt_features2', setEnvironmentID( $ids ) ) ): while ( have_rows( '60day_mt_features2', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                                <li><?php the_sub_field( '60day_mt_feature2' ); ?></li>
                            <?php endwhile; endif; ?>
                        </ul>
                    </div>
                </div>

                <hr>

                <h2 style="text-align: center; font-size: 25px; line-height: 1.5">Based on a combination of movements from the <span class="red">greatest golfers and athletes</span> of all-time, and <span class="red">timeless martial-art</span> movement and striking principles.</h2>
            </div>
        </section>

        <section class="cta email-signup">
            <div class="email-signup_bgimage2"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h2 style="color:#fff; font-size:32px;">Experience golf's <span>most transformational and empowering</span> training opportunity ever introduced to the game.</h2>
                    </div>

                    <div class="col-md-4">
                        <div class="redBoxContent">
                            <div class="woocommerce">
                                <p class="wootitle">The 60-Day Training Program</p>
                                <p>Immediate and lifetime streaming access to the 60-Day Program.</p>

                                <p class="woobottom">
                                    <a href="<?php echo esc_url( home_url( '/?add_to_cart=8251' ) ); ?>" title="The 60-Day Training Program" class="greyLinkBtn">Buy Now <i class="fa fa-angle-right"></i></a>
                                    <span class="price">
                                        <span class="amount">$179.95 - Start Training Today!</span>
                                    </span>
                                </p>
                            </div>
                        </div>

                        <p class="dvd-addon">
                            <a href="<?php echo esc_url( home_url( '/?add_to_cart=8579' ) ); ?>">
                                Add on the 30-Disc<br>
                                DVD set for offline viewing
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="mental-training">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>Tathata Mental Training</h1>
                        <h3><?php the_field( '60day_mental_description', setEnvironmentID( $ids ) ); ?></h3>

                        <ul>
                            <?php if ( have_rows( '60day_mental_features', setEnvironmentID( $ids ) ) ): while ( have_rows( '60day_mental_features', setEnvironmentID( $ids ) ) ) : the_row(); ?>
                                <li><?php the_sub_field( '60day_mental_feature' ); ?></li>
                            <?php endwhile; endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="testimonials toast">
            <div class="container">
                <!-- Pro Testimonials -->
                <div class="row">
                    <div class="col-md-12 col-md-centered text-center">
                        <h1>What Professionals Are Saying</h1>
                    </div>
                </div>

                <div class="row single">
                    <div class="col-xs-4 col-md-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/DebVangellow.jpg">
                    </div>

                    <div class="col-xs-8 col-md-10">
                        <blockquote>"A teacher/student win-win for sure!"</blockquote>
                        <cite>Deb Vangellow, LPGA President, 2012 National Teacher of the Year</cite>
                    </div>
                </div>

                <div class="row single">
                    <div class="col-xs-4 col-md-2 col-md-push-10">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/SueWieger.jpg">
                    </div>

                    <div class="col-xs-8 col-md-10 col-md-pull-2">
                        <blockquote>"I've incorporated it into newer students, brand new players as well as tour players. They are absorbing it, they are loving it, they're getting better and the rapid pace they are getting better is incredible."</blockquote>
                        <cite>Sue Wieger, LPGA, M. Ed., Peak Performance Coach and Golf Professional</cite>
                    </div>
                </div>

                <div class="row single">
                    <div class="col-xs-4 col-md-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/Ed-Gowan.jpg">
                    </div>

                    <div class="col-xs-8 col-md-10">
                        <blockquote>"Tathata is a new and revolutionary concept for learning that helps the average golfers as well as the very accomplished one understand how to swing the club effectively."</blockquote>
                        <cite>Ed Gowan, Executive Director of the Arizona Golf Association</cite>
                    </div>
                </div>

                <div class="row single">
                    <div class="col-xs-4 col-md-2 col-md-push-10">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/t-cir-JasonLoth.png">
                    </div>

                    <div class="col-xs-8 col-md-10 col-md-pull-2">
                        <blockquote>"Tathata Golf doesn't just allow someone to play better golf, it allows them to stay injury free. It will also allow them to become healthier with everything built into the program."</blockquote>
                        <cite>Dr. Jason Loth, Certified Chiropractic Sports Physician, Certified Strength and Conditioning Specialist</cite>
                    </div>
                </div>

                <div class="row single">
                    <div class="col-xs-4 col-md-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/Todd-Demsey.jpg">
                    </div>

                    <div class="col-xs-8 col-md-10">
                        <blockquote>"With Bryan and Tathata, it just becomes a part of you, golf becomes fun again. You do the training and you're ready to play."</blockquote>
                        <cite>Todd Demsey, Tour Professional, 4-Time All-American</cite>
                    </div>
                </div>

                <div class="row single">
                    <div class="col-xs-4 col-md-2 col-md-push-10">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/KarenDavies2.jpg">
                    </div>

                    <div class="col-xs-8 col-md-10 col-md-pull-2">
                        <blockquote>"Tathata has given me a new way to look at how I play and teach the game and excited me to do more than I have ever done. My game has changed. I have more power, more presence and more enthusiasm. I get up in the morning and I want to go do it. That hasn't happened for more than a decade."</blockquote>
                        <cite>Karen Davies, LPGA, 2010 LPGA T&amp;GC National Champion</cite>
                    </div>
                </div>

                <!-- Student Testimonials -->
                <div class="row">
                    <div class="col-md-12 col-md-centered text-center">
                        <h1>What Students Are Saying</h1>
                    </div>
                </div>

                <div class="row single">
                    <div class="col-xs-4 col-md-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/PaxtonJevnick.png">
                    </div>

                    <div class="col-xs-8 col-md-10">
                        <blockquote>"It's not a bunch of swing tips, its not a bunch of band-aids, its built from the ground up, the right way, and it's going to last for me, its not going to disappear tomorrow, it's in me."</blockquote>
                        <cite>Paxton Jevnick, HDCP 12, Tathata Student</cite>
                    </div>
                </div>

                <div class="row single">
                    <div class="col-xs-4 col-md-2 col-md-push-10">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/DianeMott.png">
                    </div>

                    <div class="col-xs-8 col-md-10 col-md-pull-2">
                        <blockquote>"Tathata Golf has definitely caused me to enjoy golf more and look forward to playing, look forward to shots that otherwise I might have feared."</blockquote>
                        <cite>Diane Mott, HDCP 12, Tathata Student</cite>
                    </div>
                </div>

                <div class="row single">
                    <div class="col-xs-4 col-md-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/Rob-Mangini.jpg">
                    </div>

                    <div class="col-xs-8 col-md-10">
                        <blockquote>"I've been fortunate to have worked with some of the worlds top-rated instructors. Tathata Golf is definitely on an entirely different learning level."</blockquote>
                        <cite>Rob Mangini, HDCP +1, Tathata Student</cite>
                    </div>
                </div>

                <div class="row single">
                    <div class="col-xs-4 col-md-2 col-md-push-10">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/60day/Alan-Jackson.jpg">
                    </div>

                    <div class="col-xs-8 col-md-10 col-md-pull-2">
                        <blockquote>"I've never tried anything that compares to Tathata training. I make better contact, I'm hitting it longer, I just have more enthusiasm for the game."</blockquote>
                        <cite>Alan Jackson, HDCP 15, Tathata Student</cite>
                    </div>
                </div>
            </div>
        </section>

        <article class="CompleteTraining">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="red" style="padding-top: 40px;">A Complete Learning and <br>Training Curriculum</h1>

                        <p>The world's most thorough and complete mind, body and swing training curriculum ever created for golfers of all ages, body types and ability levels. </p>

                        <p>Tathata students quickly realize they are capable of much greater learning and retention while being guided through a strategically built and structured path of learning than what is currently seen in golf instruction. For the first time ever, students are discovering not only how they can improve in their games, but also come to understand all the things they already do well in their swing and dozens of complimentary movements to add to their natural efficiencies.</p>

                        <p>With the most transformational and empowering learning and training experience ever offered in the game of golf, you are just 2 months away from playing golf at a level far beyond anything you have ever thought possible and never looking back. </p>
                    </div>

                    <div class="col-md-6">
                        <img src="<?php echo esc_url( home_url( '/wp-content/uploads/2015/04/Tathata-Curriculum.png' ) ); ?>" style="padding:30px; width:70%; margin:0 auto;">

                        <h1 class="red" style="padding:0 20px 10px;">Improve Every Aspect Of Your Game</h1>

                        <div class="row">
                            <div class="col-md-6">
                                <ul>
                                    <li>Full Swing</li>
                                    <li>Pitching</li>
                                    <li>Chipping</li>
                                    <li>Flop/lob shots</li>
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <ul>
                                    <li>Bunker shots</li>
                                    <li>Putting</li>
                                    <li>All shapes</li>
                                    <li>All trajectories</li>
                                </ul>
                            </div>

                            <div class="clearfix"></div>

                            <a href="<?php echo esc_url( home_url( '/additional-benefits' ) ); ?>" class="greyLinkBtn additional_benefitsBTN" style="min-width:35%; text-align: center; margin:10px auto; padding: 10px 20px">See additional benefits</i></a>
                        </div>
                    </div>
                </div>
            </div>
        </article>

    </div><!-- END gtwrapper -->

    <div class="remodal" data-remodal-id="emailform" data-remodal-options="hashTracking: false">
        <style scoped>
            .remodal-close {
                right: 0;
                left: initial;
            }
        </style>

        <button data-remodal-action="close" class="remodal-close"></button>

        <h1>Free Video Giveaway</h1>

        <p>Enter your e-mail for immediate access and <br>learn how Tathata Golf will help your game.</p>

        <?php gravity_form( 4, false, false, false, '', false ); ?>

        <script>
            var userEmail;
            var getEmail = function() {
                userEmail = document.getElementById('initial_email').value;
                jQuery('#input_4_2').val(userEmail);
            }
        </script>
    </div>

    <script>
        jQuery(function($) {
            $('#page-hero').append('<span class="official-ajga">The Preferred Golf Training Program of the <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/partners/ajga.svg"></span>');
        });
    </script>

<?php // SharpSpring Form tracking ?>
    <script type="text/javascript">
        var __ss_noform = __ss_noform || [];
        __ss_noform.push([
            'baseURI', 'https://app-3QDIROJGW8.marketingautomation.services/webforms/receivePostback/MzYwtzC2sDS3AAA/'
        ]);
        __ss_noform.push(['endpoint', '460fedef-9c50-40b9-8f8a-77ec0cd0b573']);
    </script>
    <script type="text/javascript" src="https://koi-3QDIROJGW8.marketingautomation.services/client/noform.js?ver=1.24"></script>

<?php // Beacon ?>
<img class="beacon" border="0" src="http://r.turn.com/r/beacon?b2=P_AYcJBQXbQH0yAC_9IQpZhwxyZGxpt9A9pzJmqIATDwmwgCAYlZ5lTJGOrh3DGCbjopRYcdYaZeLRI9P-WP4Q&cid=">

<?php // Facebook Pixel ?>
<script>fbq('track', 'ViewContent');</script>

<?php
get_footer();
