<?php
/**
 * The Template for displaying all single courses.
 * Override this template by copying it to yourtheme/sensei/single-course.php
 * @author      WooThemes
 * @package     Sensei/Templates
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $woothemes_sensei, $post, $current_user, $first_module_content, $chapter_name_glob, $chapters_global, $first_module_id;

get_header();
?>

    <section id="page-hero" class="hero -program-overview">
        <div class="container container-reset">
            <div class="flex">
                <div class="hero-content">
                    <h1 class="page-title">Welcome to the <strong>Tathata Golf</strong> family!</h1>

                    <h2>This is <span>your</span> place for <span>creating new and incredible</span> outcomes on and off the course.</h2>

                    <p>Your authentic greatness is within you right now. From this moment forward, if you choose, you will begin to simply separate from it less and less.</p>
                </div>
            </div>
        </div>
    </section>

    <div class="chapterlinkContentWrap">
        <div class="container container-reset">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="chapterLinks">
                        <?php do_action( 'sensei_course_single_lessons_custom' ); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="captionWrap">
        <div class="container container-reset">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <?php
                    if ( get_field( 'course_chapter_top_left' ) ) {
                        echo the_field( 'course_chapter_top_left' );
                    }
                    ?>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <div class="redBoxContent pull-right">
                        <?php
                        if ( get_field( 'course_chapter_top_right' ) ) {
                            echo the_field( 'course_chapter_top_right' );
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section class="pageInnerContentWrap courseChapter marginBottom">
        <div class="container container-reset">
            <div class="row">
                <article class="col-xs-12 col-sm-6">
                    <div class="welcomeToHeading">
                        <h1>What to Expect</h1>
                    </div>
                    <p>
                        <?php
                        if ( get_field( 'mem_page_content_left' ) ) {
                            echo the_field( 'mem_page_content_left' );
                        }
                        ?>
                    </p>
                </article>

                <article class="col-xs-12 col-sm-6">
                    <?php
                    if ( get_field( 'mem_page_content_right' ) ) {
                        echo the_field( 'mem_page_content_right' );
                    }
                    ?>

                    <ul class="chaptersList">
                        <?php
                        // print_r($chapter_name_glob);
                        // foreach ($chapters_global as $key => $val) {
                        //   $vals    = explode('href', $val);
                        //   $chapter = explode('/', explode('=', $vals[1])[1])[4];
                        //   $terms   = get_term_by("slug", $chapter, "module");
                        //   $sbhd    = get_field('subhead', "module_" . $terms->term_id);
                        ?>
                        <!-- <script>console.log(<?php // echo json_encode($sbhd); ?>)</script> -->
                        <?php
                        //   $val = str_replace("Introduction to", "", $val);
                        //   $val = str_replace("class='chaptName'>", "class='chaptName'> ", $val);
                        //   $val = str_replace("Training", "Training", $val);
                        //   $val = str_replace("</span> -", "</span> - " . $sbhd, $val);
                        //   echo $val;
                        // }
                        ?>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-1/?course_id=224' ) ); ?>">Chapter 1 | Days 01-10 | Body, Stretching &amp; Mind 1</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-2/?course_id=224' ) ); ?>">Chapter 2 | Days 11-20 | Hands, Arms &amp; Mind 2</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-3/?course_id=224' ) ); ?>">Chapter 3 | Days 21-30 | Pressure, Impact &amp; Mind 3</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-4/?course_id=224' ) ); ?>">Chapter 4 | Days 31-40 | Speed, Strength &amp; Mind 4</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-5/?course_id=224' ) ); ?>">Chapter 5 | Days 41-50 | Short Game, Putting &amp; Mind 5</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-6/?course_id=224' ) ); ?>">Chapter 6 | Days 51-60 | Shape, Trajectory &amp; Mind 6</a>
                        </li>
                    </ul>
                </article>
            </div>
        </div>
    </section>

    <section class="profileThumbnailWrap courseChaptersThumbnail">
        <div class="container container-reset">
            <div class="row row-reset">
                <article class="col-xs-12 col-md-4">
                    <div class="spd-f-item">
                        <h2 class="quickTips">Quick Tips &amp; Reminders</h2>
                        <?php
                        if ( get_field( 'quick_tips_and__remainders' ) ) {
                            echo the_field( 'quick_tips_and__remainders' );
                        }
                        ?>
                    </div>
                </article>

                <article class="col-xs-12 col-md-4">
                    <div class="spd-f-item">
                        <h2 class="accProfile">Account / Profile</h2>
                        <?php
                        if ( get_field( 'account_or_profile' ) ) {
                            echo the_field( 'account_or_profile' );
                        }
                        ?>
                    </div>
                </article>

                <article class="col-xs-12 col-md-4">
                    <div class="spd-f-item">
                        <h2 class="suppForum">Support &amp; Forum</h2>
                        <?php
                        if ( get_field( 'support_and_forum' ) ) {
                            echo the_field( 'support_and_forum' );
                        }
                        ?>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <script>
        jQuery(function($) {
            $('.spd-f-item').matchHeight();
            $('#page-hero').append('<span class="official-ajga">The Preferred Golf Training Program of the <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/partners/ajga.svg"></span>');
        });
    </script>

<?php
  get_footer();
