<!-- Chapter Tabs -->
<section id="senseiChapterTabs" class="tabSectionWrap">
    <div class="container">
        <div class="row">
            <div id="parentHorizontalTab">
                <!-- Chapter Tab Titles -->
                <ul class="resp-tabs-list hor_1">
                    <li id="chapter-overview">Chapter Overview</li>
                    <li id="chapter-lessons" <?php echo( ( is_archive() ) ? ' class="default"' : '' ); ?>>
                        <?php echo $module->name; ?> | Days
                        <span><?php echo ( ( ( $chapter_num - 1 ) * 10 ) + 1 ) . ' - ' . ( ( ( $chapter_num - 1 ) * 10 ) + 10 ); ?></span> Lessons
                    </li>
                    <li id="chapter-support" <?php echo( ( isset( $post->post_type ) && $post->post_type == 'lesson' && ! is_archive() ) ? ' class="default"' : '' ); ?>>Chapter Support</li>
                    <?php if ( get_field( 'lesson_media' ) && ! is_archive() ) : ?>
                        <li id="daily-extras" class="lesson-video-tab">Daily Extras</li>
                    <?php endif; ?>
                </ul>

                <div class="resp-tabs-container hor_1">
                    <!-- Chapter Overview Tab Content -->
                    <div class="chapterOverViewTab welcomeToHeading">
                        <div class="chap-overview-head">
                            <div class="overview-title">
                                <p><?php echo $chapter; ?> <span><?php echo $chapter_num; ?></span></p>
                                <p>Days
                                    <span><?php echo ( ( ( $chapter_num - 1 ) * 10 ) + 1 ) . '-' . ( ( ( $chapter_num - 1 ) * 10 ) + 10 ); ?></span>
                                </p>
                            </div>
                        </div>

                        <div class="chap-overview">
                            <div class="overview-content">
                                <h4>Overview</h4>

                                <div class="row">
                                    <?php echo get_field( 'chapter_overview_tab', "module_" . $module->term_id ); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Video Lessons Tab Content -->
                    <div class="dayLessonsTab">
                        <div class="lessonsContainer">
                            <?php
                            $day = ( ( ( $chapter_num - 1 ) * 10 ) + 1 );

                            foreach ( $lessons as $value ) {
                                $day_temp = $day;
                                $cur_day  = ( strlen( $day ) > 1 ) ? $day : "0" . (string) $day_temp;
                                ?>
                                <div class="columnContent">
                                    <h4>
                                        <a href="<?php echo get_permalink( $value->ID ); ?>">
                                            Day <span><?php echo $cur_day; ?></span>
                                        </a>
                                    </h4>

                                    <a href="<?php echo get_permalink( $value->ID ); ?>" class="play_button_hover">
                                        <?php
                                        if ( 0 < intval( $value->ID ) ) {
                                            $video_thumbnail = get_video_thumbnail( $value->ID );
                                            $thumbnail       = empty( $video_thumbnail ) ? get_template_directory_uri() . "/img/no-videos.jpeg" : $video_thumbnail;
                                            $show            = empty( $video_thumbnail ) ? "hide" : "show";
                                            ?>

                                            <div class="videoContainer" style="background-image: url('<?php echo $thumbnail; ?>');">
                                                <?php if ( $show == "show" ) { ?>
                                                    <i class="fa fa-play"></i>
                                                <?php } ?>
                                            </div>

                                            <?php
                                        }
                                        $video_field_arr = get_post_custom_values( 'video_field', $value->ID );

                                        if ( ! empty( $video_field_arr ) ) {
                                            echo $video_field_arr[0];
                                        } else {
                                        ?>

                                            <ul class="post-det"></ul>

                                            <?php
                                        }
                                        ?>
                                    </a>
                                </div>
                                <?php
                                $day ++;
                            }
                            ?>
                        </div>
                    </div>

                    <!-- Chapter Support Tab Content -->
                    <div class="onlineAcademyTab">
                        <div class="col-xs-12 col-sm-7 col-md-7 c_left ch3">
                            <h3><?php echo $module->name; ?> Support</h3>

                            <div class="onlineAcademy-videos">
                                <?php
                                $args        = array(
                                    'post_type' => 'chapter-support',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'chapter',
                                            'field'    => 'slug',
                                            'terms'    => $module->name
                                        )
                                    )
                                );
                                $supportVids = new WP_Query( $args );

                                if ( $supportVids->have_posts() ) : while ( $supportVids->have_posts() ) : $supportVids->the_post();
                                    ?>
                                    <a class="chapter-support-video" href="<?php the_permalink(); ?>">
                                        <div class="chapter-support-thumbnail" style="background: url('<?php echo getFeaturedImage(); ?>') center center/cover no-repeat">
                                            <i class="fa fa-play" aria-hidden="true"></i>
                                        </div>

                                        <h3 class="csv-title" style="margin: 10px 0 0; text-transform: none; font-weight: 400; line-height: 1.2; font-size: 14px !important"><?php the_title(); ?></h3>
                                    </a>
                                <?php endwhile; endif;
                                wp_reset_postdata(); ?>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-5 col-md-5 c_right ch3">
                            <h3>Chapter Support Discussions &amp; Topics</h3>

                            <?php
                            $ch_for = get_field( 'chapter_forum', "module_" . $module->term_id );

                            // get forum ID
                            $nforum_id = str_replace( "[bbp-single-forum id=", "", $ch_for );
                            $nforum_id = str_replace( "]", "", $nforum_id );

                            // get forum topics
                            $m_ntopics = get_posts( array(
                                'post_type'      => 'topic',
                                // Narrow query down to bbPress topics
                                'post_parent'    => $nforum_id,
                                // Forum ID
                                'meta_key'       => '_bbp_last_active_time',
                                // Make sure topic has some last activity time
                                'orderby'        => 'title',
                                'order'          => 'ASC',
                                'posts_per_page' => 10,
                            ) );

                            if ( ! empty( $m_ntopics ) ) {
                                ?>
                                <ul id="bbp-forum-4936" class="bbp-topics" style="margin-left: 0; margin-bottom:40px;">

                                    <?php
                                    $ntopics = $m_ntopics;

                                    // display topics
                                    foreach ( $ntopics as $ntopic ) { ?>
                                        <div class="single-topic-content" style="padding-bottom: 0; margin-left: 0;">
                                            <ul id="bbp-topic-<?php echo $ntopic->ID; ?>" class="post-<?php echo $ntopic->ID; ?> topic type-topic status-publish hentry" style="margin-left: 0;">
                                                <div class="single-forum-topic-holder row" style="padding: 5px 0;">
                                                    <li class="bbp-topic-title" style="margin-left: 0;">
                                                        <a class="bbp-topic-permalink" href="<?php echo get_permalink( $ntopic->ID ); ?>"><?php echo $ntopic->post_title; ?></a>
                                                    </li>
                                                </div>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                </ul>
                                <?php
                            } // end if $m_topics
                            ?>
                        </div>
                    </div>

                    <?php if ( get_field( 'lesson_media' ) ) : ?>
                        <div class="lessonMediaTab">
                            <div class="lessonMedia-container">
                                <?php if ( have_rows( 'lesson_media' ) ) : while ( have_rows( 'lesson_media' ) ) : the_row(); ?>
                                    <div class="lessonMedia-video">
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/<?php the_sub_field( 'de_video' ); ?>?title=0&byline=0&portrait=0" width="500" height="210" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                        </div>

                                        <?php if ( get_sub_field( 'de_title' ) ) : ?>
                                            <h3><?php the_sub_field( 'de_title' ); ?></h3>
                                        <?php endif; ?>
                                    </div>
                                <?php endwhile; endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
