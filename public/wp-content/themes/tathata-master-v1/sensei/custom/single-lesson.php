<?php
/**
 * single-lesson.php template actions
 */

remove_action( 'sensei_pagination', array( 'Sensei_Lesson', 'output_comments' ), 90 );

remove_all_actions( 'sensei_course_single_lessons' );
remove_all_actions( 'sensei_lesson_single_meta' );

remove_action( 'sensei_single_lesson_content_inside_before', array( 'Sensei_Lesson', 'the_title' ), 15 );
remove_action( 'sensei_single_lesson_content_inside_after', array( 'Sensei_Lesson', 'footer_quiz_call_to_action' ) );

add_filter( 'sensei_breadcrumb_output', function() {
    return '';
} );

/**
 *   Hook inside the single lesson above the content
 *
 * @priority 1
 */
add_action( 'sensei_single_lesson_content_inside_before', function( $post_id ) {

    $post = get_post( $post_id );

    $course_id = get_post_meta( $post_id, '_lesson_course', true );
    $module    = ( $modules = wp_get_post_terms( $post_id, 'module' ) ) ? $modules[0] : null;
    $title     = $module ? $module->name : '';

?>
<section id="title-section">
    <div class="container">
        <h1><?php echo $title; ?></h1>
    </div>
</section>

<!-- Black Bar -->
<div id="blackbar-menu">
    <div class="container">
        <div class="c_my_account">
            <ul class="ms-nav">
                <li class="i-menu cy menu-item menu-item-type-custom menu-item-object-custom">
                    <a href="<?php echo esc_url( home_url( '/dashboard' ) ); ?>">AC</a>
                </li>
            </ul>
        </div>

        <div class="<?php echo tath_role_class( 'learnervisibility' ); ?>">
            <?php wp_nav_menu( array( 'menu' => 'Lesson Menu' ) ); ?>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<section id="breadcrumbs">
    <div class="hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <div class="flex -flex-start">
                        <ul class="breadcrumb">
                            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                            <li><a href="<?php echo get_permalink( $course_id ); ?>"><?php echo get_the_title( $course_id ); ?></a></li>
                            <li><a href="<?php echo get_term_link( $module->term_id, 'module' ) . '?course_id=' . $course_id; ?>"><?php echo $module->name; ?></a></li>
                            <li><?php echo esc_html( $post->post_title ); ?></li>
                        </ul>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="flex -flex-end">
                        <?php echo do_shortcode( '[followUs]' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="visible-xs">
        <div class="container">
            <div class="flex">
                <div>
                    <ul class="breadcrumb">
                        <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                        <li><a href="<?php echo get_permalink( $course_id ); ?>"><?php echo get_the_title( $course_id ); ?></a></li>
                        <li><a href="<?php echo get_term_link( $module->term_id, 'module' ) . '?course_id=' . $cour_id; ?>"><?php echo $module->name; ?></a></li>
                        <li><?php echo esc_html( $post->post_title ); ?></li>
                    </ul>
                </div>

                <div>
                    <?php echo do_shortcode( '[followUs]' ); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <?php
    }, 1 );

    /**
     *   Hook the single lesson content
     *
     * @priority default 10
     */
    add_action( 'sensei_single_lesson_content_inside', function( $post_id ) {

        // User has not completed previous lession, bail on content
        if ( ! custom_sensei_has_user_completed_prerequisite_lesson( $post_id, get_current_user_id() ) ) {
            return;
        }

        $course_id = get_post_meta( $post_id, '_lesson_course', true );
        $module    = ( $modules = wp_get_post_terms( $post_id, 'module' ) ) ? $modules[0] : null;

        $post               = get_post( $post_id );
        $lesson_video_embed = get_post_meta( $post_id, '_lesson_video_embed', true );

        $pdfs = ( $meta = get_post_meta( $post_id, '_attached_media', true ) ) ? $meta : [];
        $pdfs = array_filter( $pdfs, function( $media ) {
            return preg_match( "#\.pdf$#ismu", $media );
        } );

        $undervids = get_field( "undervids", $post_id );

        $badges = ( function_exists( 'badgeos_get_user_achievements' ) ) ? badgeos_get_user_achievements( [
            'user_id'          => get_current_user_id(),
            'site_id'          => get_current_blog_id(),
            'achievement_id'   => false,
            'achievement_type' => false,
            'since'            => 0,
        ] ) : [];

        $content = apply_filters( 'the_content', $post->post_content );

        $chapterpdfurl = get_field( 'studyguidepdf', 'module_' . $module->term_id );

        $quiz_id = Sensei()->lesson->lesson_quizzes( $post_id );

        $prev_url = get_term_link( $module->term_id, 'module' ) . "?course_id=" . $course_id;

        $lessons = ( class_exists( 'Sensei_Custom' ) ) ? Sensei_Custom::getLessons( $course_id, $module->term_id ) : [];

        $lesson_key = array_search( $post_id, array_map( function( $post ) {
            return $post->ID;
        }, $lessons ) );

        $chapter_name     = $module->name;
        $chap_name_arr    = explode( ":", $chapter_name );
        $chapter_name_arr = explode( " ", $chap_name_arr[0] );
        $chapter          = $chapter_name_arr[0];
        $chapter_num      = ( strlen( $chapter_name_arr[1] ) < 2 ) ? "0" . $chapter_name_arr[1] : $chapter_name_arr[1];

        $day      = $lesson_key + 1;
        $day      = ( strlen( $day ) > 1 ) ? (string) $day : "0" . (string) $day;
        $day      = ( ( $chapter_num - 1 ) * 10 ) + $day;
        $day_next = $day + 1;

        $next_url = ( isset( $lessons[ $lesson_key + 1 ] ) ) ? get_permalink( $lessons[ $lesson_key + 1 ] ) : "javascript:void(0)";

        $lesson_prerequisite = absint( get_post_meta( $post_id, '_lesson_prerequisite', true ) );

        ?>
        <div class="row">
            <?php if ( sensei_can_user_view_lesson() ) { ?>
                <article class="col-xs-12 col-sm-7">
                    <div id="video-container" class="embed-responsive embed-responsive-16by9">
                        <?php
                        if ( strlen( $lesson_video_embed ) < 1 ) {
                            echo "There is no video found for this lesson";
                        } else {
                            echo apply_filters( 'the_content', "[embed height=\"350\"]" . $lesson_video_embed . "[/embed]" );
                        }
                        ?>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-5">
                    <div class="lessonDay">
                        <h2>DAY <span><?php echo $day; ?></span></h2>
                    </div>

                    <div class="clearfix"></div>

                    <article id="video-details" class="trainingDetailTextWrap">
                        <?php echo $content; ?>
                    </article>

                    <div id="forum-chap" class="forum-chap">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/suppForumBg.png">
                        <a href="<?php echo site_url() . "/forums/forum/tathata-golf/"; ?>" class="c_f">FAQ</a>
                        <a href="<?php echo site_url() . "/forums/forum/tathata-golf/60-day-member-support/"; ?>">Forum</a>
                    </div>

                    <div style="margin-bottom: 10px;" class="clearfix"></div>

                    <?php do_action( 'sensei_lesson_quiz_meta_custom', $post_id, get_current_user_id() ); ?>
                </article>

                <?php
            } else {
                if ( $lesson_prerequisite > 0 ) {
                    echo sprintf( __( '<div class="alert">You must first complete %1$s before viewing this Lesson</div>', 'woothemes-sensei' ), '<a href="' . esc_url( get_permalink( $lesson_prerequisite ) ) . '" title="' . esc_attr( sprintf( __( 'You must first complete: %1$s', 'woothemes-sensei' ), get_the_title( $lesson_prerequisite ) ) ) . '">' . get_the_title( $lesson_prerequisite ) . '</a>' );
                } else {
                    echo '<div class="alert">You do not have access to this resource.</div>';
                }
            }
            ?>
        </div>

        <div style="margin-bottom: 30px"></div>

        <div class="row">
            <div class="col-xs-12 col-sm-7">
                <?php if ( $undervids ) : ?>
                    <div class="under-vid-conts ">
                        <?php the_field( "undervids" ); ?>
                    </div>
                <?php endif; ?>

                <!-- Lesson Media -->
                <div id="lesson-badges" class="lesson-badges">
                    <?php if ( $pdfs ) { ?>
                        <h6>Lesson Media</h6>

                        <div class="clearfix"></div>

                        <ul class="studyGuideList">
                            <?php foreach ( $pdfs as $k => $pdf ) { ?>
                                <li>
                                    <a href="<?php echo $pdf; ?>"> Day <?php echo $day; ?> <?php echo get_the_title( $post_id ); ?> Media <?php echo $k + 1; ?>
                                        <span>Download</span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>

                    <ul class="otherProfileImgs">
                        <?php
                        $count = 1;
                        foreach ( $badges as $value ) {
                            if ( $value->post_type == "badges" && $i <= 6 ) {
                                $attachment_id = get_post_thumbnail_id( $value->ID );
                                ?>
                                <li><?php echo wp_get_attachment_image( $attachment_id, array( 100, 100 ), false ); ?></li>
                                <?php
                                $i ++;
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12 col-sm-5">
                <div id="training-actions" class="training-actions">
                    <a href="javascript:void(0);" class="training-actions--button" onclick="history.go(-1);">Back</a>

                    <a id="lesson-quiz" href="<?php echo get_permalink( $quiz_id ); ?>" class="training-actions--button">Lesson Test</a>

                    <a href="<?php echo $prev_url; ?>" class="training-actions--link">Back to Chapter</a>

                    <?php if ( $day == '10' || $day == '20' || $day == '30' || $day == '40' || $day == '50' ) : ?>
                        <a href="<?php echo home_url(); ?>/lesson/day-<?php echo $day_next; ?>" class="training-actions--link">Next Day</a>
                    <?php endif; ?>

                    <?php if ( strpos( $next_url, 'javascript' ) === false ) : ?>
                        <a href="<?php echo $next_url; ?>" class="training-actions--link">Next Day</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php // Add a introductory tour for day 1 ?>
    <?php if ( $day == '1' ) : ?>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/tour/hopscotch.js" async defer></script>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/tour/day-1-tour.js" async defer></script>
        <?php
    endif;

        // Add a Promo Pop Up on days 6, 12, & 32
        if ( $day == '6' || $day == '12' || $day == '32' ) {
            get_template_part( 'parts/30-disc-promo' );
        }

        // Promote the alternate course, for now on day 2 just for example purposes
        if ( $day == '5' || $day == '11' || $day == '22' || $day == '33' || $day == '44' || $day == '55' ) {
            get_template_part( 'parts/course-promo' );
        }

    } );

    /**
     *   Hook inside the single lesson template after the content
     *
     * @priority 98
     */
    add_action( 'sensei_single_lesson_content_inside_after', function( $post_id ) {

        ?>
    </div>
</section>

<?php
// User has not completed previous lession, bail on content
if ( ! custom_sensei_has_user_completed_prerequisite_lesson( $post_id, get_current_user_id() ) ) {
    return;
}

$course_id = get_post_meta( $post_id, '_lesson_course', true );
$module    = ( $modules = wp_get_post_terms( $post_id, 'module' ) ) ? $modules[0] : null;
$post      = get_post( $post_id );

$lessons = ( class_exists( 'Sensei_Custom' ) ) ? Sensei_Custom::getLessons( $course_id, $module->term_id ) : [];

$lesson_key = array_search( $post_id, array_map( function( $post ) {
    return $post->ID;
}, $lessons ) );

$day      = $lesson_key + 1;
$day      = ( strlen( $day ) > 1 ) ? (string) $day : "0" . (string) $day;
$day      = ( ( $chapter_num - 1 ) * 10 ) + $day;
$day_next = $day + 1;
}, 98 );

/**
 *   Hook inside the single lesson template after the content
 *
 * @priority 99
 */
add_action( 'sensei_single_lesson_content_inside_after', function( $post_id ) {

    $course_id = get_post_meta( $post_id, '_lesson_course', true );
    $module    = ( $modules = wp_get_post_terms( $post_id, 'module' ) ) ? $modules[0] : null;
    $post      = get_post( $post_id );

    $chapter_name     = $module->name;
    $chap_name_arr    = explode( ":", $chapter_name );
    $chapter_name_arr = explode( " ", $chap_name_arr[0] );
    $chapter          = $chapter_name_arr[0];
    $chapter_num      = ( strlen( $chapter_name_arr[1] ) < 2 ) ? "0" . $chapter_name_arr[1] : $chapter_name_arr[1];

    $lessons = ( class_exists( 'Sensei_Custom' ) ) ? Sensei_Custom::getLessons( $course_id, $module->term_id ) : [];

    include_once __DIR__ . '/chapter-tabs.php';
}, 99 );
