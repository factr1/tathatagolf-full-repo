<?php
remove_action( 'sensei_pagination', array( Sensei()->modules, 'module_navigation_links' ), 11 );

add_action( 'sensei_archive_before_lesson_loop', function() {
    $module = get_queried_object();

    $course_page_id  = intval( $woothemes_sensei->settings->settings['course_page'] );
    $course_page_url = ( 0 < $course_page_id ? get_permalink( $course_page_id ) : get_post_type_archive_link( 'course' ) );

    $course_id = intval( $_GET['course_id'] );
    ?>

    <style type="text/css">
        .sensei-message {
            clear: both;
            padding: 1em 1.618em;
            border: none !important;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            -moz-background-clip: padding;
            -webkit-background-clip: padding-box;
            background-clip: padding-box;
            background: rgb(255, 217, 200);
            width: 75%;
            margin: 35px auto;
        }

        .sensei-message p:before {
            content: '\f071';
            color: rgb(237, 108, 108);
            font-family: 'FontAwesomeSensei';
            display: inline-block;
            margin-right: .618em;
            font-weight: normal;
            line-height: 1em;
            width: 1em;
            font-size: 1.387em;
            position: relative;
            top: .1em;
        }
    </style>

    <section id="title-section">
        <div class="container">
            <h1><?php echo esc_html( $module->name ); ?> Introduction</h1>
        </div>
    </section>

    <!-- Black Bar -->
    <div id="blackbar-menu">
        <div class="container">
            <div class="c_my_account">
                <ul class="ms-nav">
                    <li class="i-menu cy menu-item menu-item-type-custom menu-item-object-custom">
                        <a href="<?php echo esc_url( home_url( '/dashboard' ) ); ?>">AC</a>
                    </li>
                </ul>
            </div>

            <div class="<?php echo tath_role_class( 'learnervisibility' ); ?>">
                <?php wp_nav_menu( array('menu' => 'Lesson Menu') ); ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <section id="breadcrumbs">
        <div class="hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <div class="flex -flex-start">
                            <ul class="breadcrumb">
                                <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                                <li><a href="<?php echo get_permalink( $course_id ); ?>"><?php echo get_the_title( $course_id ); ?></a></li>
                                <li><?php echo $module->name; ?></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="flex -flex-end">
                            <?php echo do_shortcode( '[followUs]' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="visible-xs">
            <div class="container">
                <div class="flex">
                    <div>
                        <ul class="breadcrumb">
                            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                            <li><a href="<?php echo get_permalink( $course_id ); ?>"><?php echo get_the_title( $course_id ); ?></a></li>
                            <li><?php echo $terms->name; ?></li>
                        </ul>
                    </div>

                    <div>
                        <?php echo do_shortcode( '[followUs]' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
} );

add_action( 'sensei_archive_lesson_loop', function() {
    if ( ! is_user_logged_in() ) {
        return;
    }

    $module  = get_queried_object();
    $user_id = get_current_user_id();

    $chapter_name     = $module->name;
    $chap_name_arr    = explode( ":", $chapter_name );
    $chapter_name_arr = explode( " ", $chap_name_arr[0] );
    $chapter          = $chapter_name_arr[0];
    $chapter_num      = ( strlen( $chapter_name_arr[1] ) < 2 ) ? "0" . $chapter_name_arr[1] : $chapter_name_arr[1];

    $module_progress = false;

    $course_id       = intval( $_GET['course_id'] );
    $progress        = get_user_meta( $user_id, '_module_progress_' . $course_id . '_' . $module->term_id, true );
    $module_progress = ( $progress ) ? (float) $progress : 0;

    if ( $module_progress && $module_progress > 0 ) {
        $status = __( 'Completed', 'sensei_modules' );
        $class  = 'completed';

        if ( $module_progress < 100 ) {
            $status = __( 'Course in progress', 'sensei_modules' );
            $class  = 'in-progress';
        }
    }

    $lessons           = ( class_exists( 'Sensei_Custom' ) ) ? Sensei_Custom::getLessons( $course_id, $module->term_id ) : [];
    $total_lessons     = count( $lessons );
    $lessons_completed = 0;

    foreach ( $lessons as $lesson ) {
        if ( WooThemes_Sensei_Utils::user_completed_lesson( $lesson->ID, $user_id ) ) {
            ++ $lessons_completed;
        }
    }

    $progress_percentage = abs( round( ( doubleval( $lessons_completed ) * 100 ) / ( $total_lessons ), 0 ) );
    ?>

    <section>
        <div class="container">
            <div class="row">
                <article class="col-xs-12 col-sm-6">
                    <div class="welcomeToHeading">
                        <h3 class="welcome">Welcome to</h3>

                        <ul class="c_hd">
                            <li><?php echo $chapter; ?><span><?php echo $chapter_num; ?></span></li>
                            <li>Days<span><?php echo ( ( ( $chapter_num - 1 ) * 10 ) + 1 ) . '-' . ( ( ( $chapter_num - 1 ) * 10 ) + 10 ); ?></span></li>
                        </ul>

                        <h3 class="sub_head">
                            <?php echo get_field( 'subhead', "module_" . $module->term_id ); ?>
                        </h3>
                    </div>

                    <div class="chap_intro">
                        <p><?php echo $module->description; ?></p>
                    </div>

                    <p class="chapterWritten writtenBy text-right"><?php echo the_field( 'authorname', 'module_' . $module->term_id ); ?></p>
                </article>

                <article class="col-xs-12 col-sm-6">
                    <div class="chaptersPageRightContent">
                        <div class="courseInProgress">
                            <?php if ( $status ) : ?>
                                <h5 class="status <?php echo esc_attr( $class ); ?>"><?php echo esc_html( $status ); ?></h5>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="left-desc">
                        <?php echo get_field( 'faq', "module_" . $module->term_id ); ?>
                    </div>
                </article>
            </div>

            <div class="row">
                <article class="col-xs-12 col-sm-6">
                    <div class="progressBarWrap">
                        <span class="course-completion-rate">PROGRESS OF CHAPTER <?php echo $chapter_num; ?></span>

                        <div id='bar4' class='barfiller'>
                            <div class='tipWrap'>
                                <span class='tip'></span>
                            </div>

                            <span class='fill' data-percentage='<?php echo esc_attr( $progress_percentage ); ?>'></span>
                        </div>

                        <div style='display:inline;'><?php echo esc_attr( $progress_percentage ); ?>%</div>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-6">
                    <div class="training-actions">
                        <a href="<?php echo esc_attr( get_permalink( $course_id ) ); ?>" class="training-actions--button"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/60DayWhiteIcon.png" alt=""></a>
                        <a href="<?php echo esc_attr( get_permalink( get_next_lesson( $course_id ) ) ); ?>" class="training-actions--button">Continue Training</a>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <?php
    include_once __DIR__ . '/chapter-tabs.php';
} );

add_action( 'sensei_archive_after_lesson_loop', function() {
    if ( ! is_user_logged_in() ) {
        ?>
        <div class="container">
            <section class="entry fix">
                <div class="sensei-message alert">
                    <p>This is for 60-Day members only....Please
                        <a href="<?php echo esc_url( home_url( '/login/' ) ); ?>">login here</a>, or
                        <a href="<?php echo esc_url( home_url( '/60-day-program/' ) ); ?>">purchase here</a>
                    </p>
                </div>
            </section>
        </div>

        <?php
    }
} );
