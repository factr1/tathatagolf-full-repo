'use strict';
jQuery(function($) {
    /* --------------------------------------------------------------
		## Owl Carousel
	-------------------------------------------------------------- */
    $('#owl-demo').owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        navigationText: [
            '<i class="icon-chevron-left icon-control"></i>',
            '<i class="icon-chevron-right icon-control"></i>'
        ]
    });

    $('#owl-demo2').owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        navigationText: [
            '<i class="icon-chevron-left icon-control"></i>',
            '<i class="icon-chevron-right icon-control"></i>'
        ]
    });

    /* --------------------------------------------------------------
		## Easy Responsive Tabs
	-------------------------------------------------------------- */
    // Horizontal Tabs in Courses
    var tabs$ = $('#parentHorizontalTab').find('.resp-tabs-list');

    if (tabs$.length && ! window.location.hash) {
        tabs$.children('li').each(function(index) {
            if ($(this).is('.default')) {
                location.href = location.href + '#parentHorizontalTab' + (index + 1);
            }
        });
    }

    $('#parentHorizontalTab').easyResponsiveTabs({
        type: 'default',
        width: 'auto',
        fit: true,
        tabidentify: 'hor_1',
        activate: function(event) {
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);

            $name.text($tab.text());
            $info.show();
        }
    });

    /* --------------------------------------------------------------
		## Single Course(s)
	-------------------------------------------------------------- */
    // Course Progress bars
    $('#bar4').barfiller({
        barColor: '#900'
    });

    $('.accordion_example').accordion();

    // Course pagination
    $('.pagination-quiz').hide();
    $('.pagi-10').show();
    $('.page-num').click(function() {
        var pageView = $(this).data('value') * 10;

        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        $('.pagination-quiz').hide();
        $('.pagi-' + pageView).show();
    });

    /* --------------------------------------------------------------
		## matchHeight
	-------------------------------------------------------------- */
    $('#ms-testimonials').find('.content').matchHeight();
    $('.gearPart, .mb-content, .ms-description--container, .msComparison-item, .card.-as-nav-link').matchHeight();
    $('.chapter-support-video-item').find('.title').matchHeight();

    // temp until have a better solution
    $('.-mh').matchHeight();

    /* --------------------------------------------------------------
		## Mobile Menu
	-------------------------------------------------------------- */
    // Add the needed ul classes to the new header navigation
    $('.header-bottom .menu').addClass('nav navbar-nav');

    $('#mobile-nav .menu').each(function() {
        $(this).parent().attr({
            id: 'mobile-menu',
            class: 'slinky-menu'
        });
    });

    // Prevent Default on Submenu items
    $('#mobile-nav .menu .menu-item-has-children > a').on('click', function(e) {
        e.preventDefault();
    });

    // Move the parent item into its own child sub-menu
    $('#mobile-nav .menu .menu-item-has-children > a').each(function() {
        var $this = $(this);
        var menuLink = $this.attr('href');
        var menuText = $this.html();
        var newParentItem = '<li class="new-parent-item"><a href="' + menuLink + '">' + menuText + '</a></li>';

        if ('#' !== menuLink) {
            $this.next().prepend(newParentItem);
        }
    });

    // Slinky Mobile Menu
    $('#mobile-menu').slinky({
        label: 'Back',
        speed: 200,
        resize: true
    });

    /* --------------------------------------------------------------
		## Testimonials
	-------------------------------------------------------------- */
    // Testimonial Ticker
    $('.tt-ticker').css('display', 'block');

    // Testimonial ticker hours counter
    // The math for this entire function is not accurate, but is calculated as just an estimate
    // The hoursPerDay is the number of hours trained per day (estimated)
    // The magicNumber is a random number to use to get whatever target number Tathata wants
    // today is the current time
    var hoursPerDay = 849;
    var magicNumber = 129396;
    var today = new Date().getTime();
    // Find the number of days between the start date and today
    var dayDiff = function(start) {
        start = new Date(start).getTime();

        // Convert ms to days
        return Math.floor((today - start) / 86400000);
    };
    // Multiply how many days it has been by the magic number
    var hoursTrained = ((dayDiff('2015-12-29') * hoursPerDay) - magicNumber);

    function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d+)(\d{3})/, '$1,$2');
        }

        return val;
    }

    function updateHours() {
        $('#tickerHours, #hoursTrained').html(commaSeparateNumber(hoursTrained));
    }

    updateHours();

    /* --------------------------------------------------------------
		## Become a Movement Specialist Page
	-------------------------------------------------------------- */
    // Icon Description Toggles - Redesigned MS page
    var hideDescriptions = function() {
        $('.ms-description--container').css('display', 'none');
    };

    $('#new-ms-training').find('a').on('hover', function() {
        var $this = $(this);
        var $class = $this.attr('class');

        hideDescriptions();
        $('#' + $class).css({
            'display': 'block'
        });
    });

    /* --------------------------------------------------------------
		## Slick Sliders
	-------------------------------------------------------------- */
    // Homepage Slider
    $('#slick-hero').slick({
        autoplay: true, arrows: true, dots: true, infinite: true, responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false
                }
            }
        ]
    });

    // Testimonial Ticker
    $('#testimonial-ticker').slick({
        adaptiveHeight: false,
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        mobileFirst: true,
        respondTo: 'min',
        swipeToSlide: true,
        cssEase: 'linear'
    });

    // Testimonial Slider - Desktop
    $('#testimonial-slickslider--desktop').find('.testimonial-slides').slick({
        adaptiveHeight: true,
        autoplay: false,
        autoplaySpeed: 5000,
        arrows: true,
        dots: true,
        infinite: true,
        speed: 500,
        fade: false,
        mobileFirst: true,
        respondTo: 'min',
        swipeToSlide: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        variableWidth: false,
        cssEase: 'linear'
    });

    // Testimonial Slider - Mobile
    $('#testimonial-slickslider--mobile').find('.testimonial-slides').slick({
        adaptiveHeight: true,
        arrows: false,
        dots: true,
        autoplay: false,
        infinite: true,
        speed: 300,
        mobileFirst: true,
        respondTo: 'min',
        swipeToSlide: true,
        cssEase: 'linear'
    });

    // Testimonial Slider - Homepage & Become a Movement Specialist
    $('#globe-ticker, .ms-testimonials--slider').slick({
        autoplay: true,
        autoplaySpeed: 4000,
        arrows: false,
        dots: false,
        infinite: true,
        slidesToShow: 1,
        fade: true,
        cssEase: 'linear',
        draggable: false,
        responsive: [
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    // Testimonial Slider - Homepage
    $('#globe-ticker2').slick({
        autoplay: false,
        autoplaySpeed: 4000,
        arrows: false,
        dots: false,
        infinite: true,
        slidesToShow: 1,
        fade: true,
        cssEase: 'linear',
        draggable: false,
        responsive: [
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    setTimeout(function() {
        $('#globe-ticker2').slick('slickPlay');
    }, 2000);

    // Additional Support Slider
    $('.card-slider').slick({
        autoplay: true,
        autoplaySpeed: 3500,
        arrows: false,
        dots: false,
        infinite: true,
        fade: true
    });

    // Hero slider
    $('.hero-slider').slick({
        dots: true,
        arrows: false,
        infinite: true,
        adaptiveHeight: true,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 3000,
        lazyLoad: 'ondemand'
    });

    // Homepage solutions slider
    $('#solution-slider').slick({
        autoplay: true,
        autoplaySpeed: 7000,
        adaptiveHeight: true
    });

    // 60 Day page linked slider
    $('.msLinkedSlider').slick({
        autoplay: false,
        infinite: true,
        arrows: true,
        dots: false,
        asNavFor: '.msLinkedSlider',
        draggable: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });

    /* --------------------------------------------------------------
		## Misc.
	-------------------------------------------------------------- */
    // Utilize the default Bootstrap responsive embeds
    $('.embed-responsive iframe').addClass('embed-responsive-item');

    // Move Elements Down in the DOM
    $.fn.moveDown = function() {
        var after = $(this).next();

        $(this).insertAfter(after);
    };

    // Hide certain WooCommerce product categories from appearing in the Product categories widget
    var wcExcludeCategories = ['1-Day Jumpstart', '2 1/2 Day Live Experience', 'Golf School', 'Grad School', 'DVD\'s'];

    $.each(wcExcludeCategories, function(index, value) {
        $('#menu2').find('li > a:contains(' + value + ')').parent('li').css('display', 'none');
    });
});
