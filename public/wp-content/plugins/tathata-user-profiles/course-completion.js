;(function($) {
    $(document).ready(function() {

        var form$ = $("form[name='add_learner']");
        var select$ = form$.find("[name='add_user_id']");
        var checkbox$ = form$.find("input[type='checkbox'][name='add_complete_course']");
        var lessons$ = form$.find('div.lesson-data[data-lessons]');

        if(!form$.length || !select$.length || !checkbox$.length || !lessons$.length) {
            return;
        }

        var completeNextLesson = function(user_id,course_id,lessons) {

            var lesson = lessons.shift();
            var postURL = lessons$.data('ajax-url');
            var token = lessons$.data('token');
            var nonce = lessons$.data('nonce');

            // Missing ajax paramters, bail
            if(!postURL || !token || !nonce) {
                return;
            }

            // No lesson to complete, show completed message
            if(!lesson) {
                return;
            }

            // Prepare post data
            var postData = {
                action: 'admin_complete_user_lesson',
                user_id: user_id,
                course_id: course_id,
                lesson_id: lesson.id,
                last_lesson: ((!lessons.length)?1:0)
            };
            postData[token] = nonce;

            // Prepare user notices
            var submit$ = form$.find("input[type='submit']");
            var msg$ = $(document.createElement('p')).html('Updating lesson "'+lesson.name+'" ...');

            submit$.toggle(false);
            lessons$.append(msg$);

            // Complete the course
            $.post(postURL,postData,function(data) {

                msg$.append(data.message);

                // Failed, stop here
                if(!data.result) {
                    submit$.toggle(true);
                    return;
                }

                // No more lessons, show complete
                if(!lessons.length) {
                    lessons$.append(
                        $(document.createElement('p')).html('Course marked complete! Reloading the page...')
                    );
                    window.setTimeout(function() {
                        window.location.reload();
                    },1000);
                    return;
                }

                // Start the next lesson
                completeNextLesson(user_id,course_id,lessons);
            },'json');

        };

        form$.submit(function(e) {

            // Not full course, proceed as normal
            if(!checkbox$.prop('checked')) {
                return true;
            }

            e.preventDefault();

            var user_id = select$.val();

            // No user selected, bail
            if(!user_id) {
                return false;
            }

            var course_id = form$.find("input[name='add_course_id']").val();
            var lessons = lessons$.data('lessons');

            // No user selected, bail
            if(!course_id || !lessons) {
                return true;
            }

            completeNextLesson(user_id,course_id,lessons);

            return false;
        });

    });
})(jQuery);