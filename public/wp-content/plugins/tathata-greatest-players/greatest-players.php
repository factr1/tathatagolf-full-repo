<?php

   /*
   Plugin Name: Tathata Greatest Players
   Plugin URI: http://tathata.com
   Description: Custom user controls and profiles for the Online Academy Greatest Players
   Version: 1.0
   Author: Chris Jenkins
   Author URI: http://lucidindustries.com
   License: GPL3
   */

// Register Custom Post Type
function player_video() {

	$labels = array(
		'name'                => _x( 'Videos', 'Post Type General Name', 'tathata' ),
		'singular_name'       => _x( 'Video', 'Post Type Singular Name', 'tathata' ),
		'menu_name'           => __( 'Player Videos', 'tathata' ),
		'parent_item_colon'   => __( 'Parent Video:', 'tathata' ),
		'all_items'           => __( 'All Player Videos', 'tathata' ),
		'view_item'           => __( 'View Video', 'tathata' ),
		'add_new_item'        => __( 'Add New Video', 'tathata' ),
		'add_new'             => __( 'Add New', 'tathata' ),
		'edit_item'           => __( 'Edit Video', 'tathata' ),
		'update_item'         => __( 'Update Video', 'tathata' ),
		'search_items'        => __( 'Search Videos', 'tathata' ),
		'not_found'           => __( 'Not found', 'tathata' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'tathata' ),
	);
	$args = array(
		'label'               => __( 'player-video', 'tathata' ),
		'description'         => __( 'Greatest Player Videos', 'tathata' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
		'taxonomies'          => array( 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-format-video',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'player-video', $args );

}

// Hook into the 'init' action
add_action( 'init', 'player_video', 0 );

// Register Custom Taxonomy
function player_video_taxonomy() {

	$labels = array(
		'name'                       => _x( 'GPV Players', 'Taxonomy General Name', 'tathata' ),
		'singular_name'              => _x( 'GPV Player', 'Taxonomy Singular Name', 'tathata' ),
		'menu_name'                  => __( 'Player', 'tathata' ),
		'all_items'                  => __( 'All Players', 'tathata' ),
		'parent_item'                => __( 'Parent Player', 'tathata' ),
		'parent_item_colon'          => __( 'Parent Player:', 'tathata' ),
		'new_item_name'              => __( 'New Player', 'tathata' ),
		'add_new_item'               => __( 'Add New Player', 'tathata' ),
		'edit_item'                  => __( 'Edit Player', 'tathata' ),
		'update_item'                => __( 'Update Player', 'tathata' ),
		'separate_items_with_commas' => __( 'Separate Players with commas', 'tathata' ),
		'search_items'               => __( 'Search Player', 'tathata' ),
		'add_or_remove_items'        => __( 'Add or remove Player', 'tathata' ),
		'choose_from_most_used'      => __( 'Choose from the most used Players', 'tathata' ),
		'not_found'                  => __( 'Not Found', 'tathata' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'greatest-player-videos', array( 'player-video' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'player_video_taxonomy', 0 );

// Add sport as a taxonomy

function player_sport_taxonomy() {

	$labels = array(
		'name'                       => _x( 'GPV Sports', 'Taxonomy General Name', 'tathata' ),
		'singular_name'              => _x( 'GPV Sport', 'Taxonomy Singular Name', 'tathata' ),
		'menu_name'                  => __( 'Sport', 'tathata' ),
		'all_items'                  => __( 'All Sports', 'tathata' ),
		'parent_item'                => __( 'Parent Sport', 'tathata' ),
		'parent_item_colon'          => __( 'Parent Sport:', 'tathata' ),
		'new_item_name'              => __( 'New Sport', 'tathata' ),
		'add_new_item'               => __( 'Add New Sport', 'tathata' ),
		'edit_item'                  => __( 'Edit Sport', 'tathata' ),
		'update_item'                => __( 'Update Sport', 'tathata' ),
		'separate_items_with_commas' => __( 'Separate Sports with commas', 'tathata' ),
		'search_items'               => __( 'Search Sports', 'tathata' ),
		'add_or_remove_items'        => __( 'Add or remove Sports', 'tathata' ),
		'choose_from_most_used'      => __( 'Choose from the most used Sports', 'tathata' ),
		'not_found'                  => __( 'Not Found', 'tathata' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'greatest-player-sports', array( 'player-video' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'player_sport_taxonomy', 0 );

// Add focus as a taxonomy

function player_focus_taxonomy() {

	$labels = array(
		'name'                       => _x( 'GPV Focus', 'Taxonomy General Name', 'tathata' ),
		'singular_name'              => _x( 'GPV Focus', 'Taxonomy Singular Name', 'tathata' ),
		'menu_name'                  => __( 'Focus', 'tathata' ),
		'all_items'                  => __( 'All Focuses', 'tathata' ),
		'parent_item'                => __( 'Parent Focus', 'tathata' ),
		'parent_item_colon'          => __( 'Parent Focus:', 'tathata' ),
		'new_item_name'              => __( 'New Focus', 'tathata' ),
		'add_new_item'               => __( 'Add New Focus', 'tathata' ),
		'edit_item'                  => __( 'Edit Focus', 'tathata' ),
		'update_item'                => __( 'Update Focus', 'tathata' ),
		'separate_items_with_commas' => __( 'Separate Focuses with commas', 'tathata' ),
		'search_items'               => __( 'Search Focuses', 'tathata' ),
		'add_or_remove_items'        => __( 'Add or remove Focus', 'tathata' ),
		'choose_from_most_used'      => __( 'Choose from the most used Focuses', 'tathata' ),
		'not_found'                  => __( 'Not Found', 'tathata' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'greatest-player-focus', array( 'player-video' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'player_focus_taxonomy', 0 );

?>