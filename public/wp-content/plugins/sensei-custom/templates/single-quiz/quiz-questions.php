<?php
/**
 * The Template for displaying all Quiz Questions.
 *
 * Override this template by copying it to yourtheme/sensei/single-quiz/quiz-questions.php
 *
 * @author 		WooThemes
 * @package 	Sensei/Templates
 * @version     1.0.0
 */
if (!defined('ABSPATH'))
    exit;

global $post, $woothemes_sensei, $current_user, $pagi_num, $cour_id;

// Get User Meta
get_currentuserinfo();

if (isset($_REQUEST['quiz_complete'])) {
    ?>
    <script type="text/javascript"> window.location = '<?php echo site_url() . "/lesson-quiz/" . $post->ID ?>';</script>
    <?php
}
// Handle Quiz Completion
do_action('sensei_complete_quiz');
// Get Frontend data
$user_quizzes = $woothemes_sensei->frontend->data->user_quizzes;
$user_quiz_grade = $woothemes_sensei->frontend->data->user_quiz_grade;
$quiz_lesson = $woothemes_sensei->frontend->data->quiz_lesson;
$quiz_grade_type = $woothemes_sensei->frontend->data->quiz_grade_type;
$user_lesson_end = $woothemes_sensei->frontend->data->user_lesson_end;
$user_lesson_complete = $woothemes_sensei->frontend->data->user_lesson_complete;
$lesson_quiz_questions = $woothemes_sensei->frontend->lesson->lesson_quiz_questions($post->ID);
$terms = get_the_terms($quiz_lesson, 'module');
$termid = ($terms[0]->parent == 0) ? $terms[0]->term_id : $terms[0]->parent;
$cour_id = 224;
do_action('sensei_course_single_chapter', $termid);
global $course_lessons_arr;
$curr_key = array_search($quiz_lesson, $course_lessons_arr);
$day = $curr_key + 1;
$day = (strlen($day) > 1) ? (string) $day : "0" . (string) $day;

// Check if the user has started the course
$lesson_course_id = absint(get_post_meta($quiz_lesson, '_lesson_course', true));
$has_user_start_the_course = sensei_has_user_started_course($lesson_course_id, $current_user->ID);

// Get the meta info
$quiz_passmark = absint(get_post_meta($post->ID, '_quiz_passmark', true));
$quiz_passmark_float = (float) $quiz_passmark;
?>
<div class="questionPageHeading">
    <div class="lesson-meta-custom">
        <section class="questionPageWrap">
            <div class="container">
                <div class="row">
                    <div class="welcomeToHeading">
                        <ul>
                            <li>Day<span><?php echo $day; ?></span></li>
                            <li>PART<span>01 TEST</span></li>
                        </ul>
                        <div class="clearfix"></div>
                        <h3 class="quizTitle"><?php echo $post->post_title; ?></h3>
                    </div>


                    <div class="clearfix"></div>   
                    <form method="POST" action="<?php //echo esc_url(get_permalink());                                 ?><?php echo site_url() . "/lesson-quiz/" . $post->ID ?>" enctype="multipart/form-data">
                        <div class="questionsWrap"> 
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <?php
//                            echo "<pre>";
//                            print_r($lesson_quiz_questions);
//                            echo "</pre>";
                                // Display user's quiz status
                                $status = WooThemes_Sensei_Utils::sensei_user_quiz_status_message($quiz_lesson, $current_user->ID);
//  echo '<div class="sensei-message ' . $status['box_class'] . '">' . $status['message'] . '</div>';
                                // Lesson Quiz Meta
                                $n_value = isset($_GET['n']) ? $_GET['n'] : 1;
                                //echo "<pre>";print_r($lesson_quiz_questions);
                                //	$last_key_value = count($lesson_quiz_questions1);
                                if ($n_value == 1) {
                                    $page = 1;
                                    $lesson_quiz_questions1 = array_slice($lesson_quiz_questions, 0, 10);
                                } else {
                                    $page = ($n_value / 10) + 1;
                                    $lesson_quiz_questions1 = array_slice($lesson_quiz_questions, $n_value, 10, true);
                                }
                                //echo $page;
                                $total = count($lesson_quiz_questions);
                                $limit = 10;
                                $totalPages = ceil($total / $limit);
                                $page = max($page, 1); //get 1 page when $_GET['page'] <= 0
                                $page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages */
                                $offset = ($page - 1) * $limit;
                                /* if( $offset < 0 ) $offset = 0;
                                  $lesson_quiz_questions2 = array_slice( $lesson_quiz_questions1, $offset, $limit ); */
                                //echo "<pre>";print_r($lesson_quiz_questions1);

                                if (0 < count($lesson_quiz_questions)) {
                                    $question_count = 1;
                                    ?>
                                    <?php
                                    $odd = array();
                                    $even = array();
//                                    $even[0] = $lesson_quiz_questions[0];
                                    $kk = 1;
                                    foreach ($lesson_quiz_questions as $k => $v) {
//                                        echo "<pre>";
//                                        print_r($v);
//                                        echo "</pre>";
//                                        if ($k >= 6) {
//                                            break;
//                                        }
                                        if ($kk % 2 == 0) {
                                            $even[$kk] = $v;
                                        } else {
                                            $odd[$kk] = $v;
                                        }
                                        $kk++;
                                    }
//                                    unset($odd[1]);
//                                    echo "<pre>";
//                                    print_r(array_keys($even));
//                                    print_r($even);
////                                    print_r(array_keys($odd));
//                                    echo "</pre>";
//                                    exit();
//                                    echo "<pre>";
//                                    print_r($lesson_quiz_questions);
//                                    echo "</pre>";
//                                    echo "<pre>";
//                                    print_r($even);
//                                    echo "</pre>";
//                                    echo "<pre>";
//                                    print_r($odd);
//                                    echo "</pre>";
//                                    exit();
                                    ?>
                                    <ol id="sensei-quiz-list-custom">
                                        <?php
                                        foreach ($odd as $k => $question_item) {
                                            $pagi_num = (int) $k;
                                            if ($pagi_num % 10 != 0) {
                                                $pagi_num += 10 - ($pagi_num % 10);
                                            } else {
                                                $pagi_num;
                                            }
                                            // Setup current Frontend Question
                                            $woothemes_sensei->frontend->data->question_item = $question_item;
                                            $woothemes_sensei->frontend->data->question_count = $k;
                                            // Question Type
                                            $question_type = 'multiple-choice';
                                            $question_types_array = wp_get_post_terms($question_item->ID, 'question-type', array('fields' => 'names'));
                                            if (isset($question_types_array[0]) && '' != $question_types_array[0]) {
                                                $question_type = $question_types_array[0];
                                            } // End If Statement

                                            echo '<input type="hidden" name="questions_asked[]" value="' . $question_item->ID . '" />';

                                            do_action('sensei_quiz_question_type_custom', $question_type);

                                            $question_count++;
                                        } // End For Loop 
                                        ?>

                                    </ol>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-6">    
                                    <ol id="sensei-quiz-list-custom even">
                                        <?php
                                        foreach ($even as $k => $question_item) {
                                            $pagi_num = (int) $k;
                                            if ($pagi_num % 10 != 0) {
                                                $pagi_num += 10 - ($pagi_num % 10);
                                            } else {
                                                $pagi_num;
                                            }
                                            // Setup current Frontend Question
                                            $woothemes_sensei->frontend->data->question_item = $question_item;
                                            $woothemes_sensei->frontend->data->question_count = $k;
                                            // Question Type
                                            $question_type = 'multiple-choice';
                                            $question_types_array = wp_get_post_terms($question_item->ID, 'question-type', array('fields' => 'names'));
                                            if (isset($question_types_array[0]) && '' != $question_types_array[0]) {
                                                $question_type = $question_types_array[0];
                                            } // End If Statement

                                            echo '<input type="hidden" name="questions_asked[]" value="' . $question_item->ID . '" />';

                                            do_action('sensei_quiz_question_type_custom', $question_type);

                                            $question_count++;
                                        } // End For Loop 
                                        ?>

                                    </ol>
                                    <?php
                                    global $wp;
                                    $current_url = add_query_arg($wp->query_string, '', home_url($wp->request));
                                    $new_url = explode('?', $current_url);
                                    $curr_page_url = $new_url[0];

                                    //echo $link;
                                    $pagerContainer = '<div class="pagiQuestion">';
                                    if ($totalPages != 0) {
                                        for ($i = 1; $i <= $totalPages; $i++) {
                                            $activeClass = ($i == 1) ? "active" : "";
                                            $pagerContainer .= '<a href="javascript:void(0);" data-value ="' . $i . '" style="color: #c00" class="page-num ' . $activeClass . '"> ' . $i . ' </a>';
                                        }
                                    }
                                    $pagerContainer .= '</div>';
                                } else {
                                    ?>
                                    <div class="sensei-message alert"><?php _e('There are no questions for this Quiz yet. Check back soon.', 'woothemes-sensei'); ?></div>
                                <?php } // End If Statement   ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="quiz-controls">
                            <?php
//                                if ($page == $totalPages) {
                            do_action('sensei_quiz_action_buttons');
//                                }
                            ?>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    <?php
                    if ($total > $limit) {

                        echo $pagerContainer;
                    }
                    ?>
                </div>
            </div>
        </section>
<!--        <script type="text/javascript"> window.location = 'http://www.google.com'; </script>-->
        <?php
        // do_action('sensei_quiz_back_link', $quiz_lesson); ?>