<?php

if(!class_exists('Sensei_Core_Modules')) {
    return;
}

class Sensei_Custom extends Sensei_Core_Modules {

    private $dir;
    private $file;
    private $assets_dir;
    private $assets_url;
    private $order_page_slug;
    public $taxonomy;

    public function __construct($file)
    {
        $this->file = $file;
        $this->dir = dirname($this->file);
        $this->assets_dir = trailingslashit($this->dir) . 'assets';
        //$this->assets_url      = esc_url(trailingslashit(plugins_url('/assets/', $this->file)));
        $this->taxonomy = 'module';
        $this->order_page_slug = 'module-order';

        add_action('sensei_course_single_lessons_custom', array($this, 'single_course_modules_custom'));
        add_action('sensei_course_single_chapter', array($this, 'sensei_course_single_chapter'));
        add_action('sensei_course_single_course_overview_lessons', array($this, 'single_course_overview_modules'));
        add_filter('the_posts',[$this,'filter_lesson_posts'],10,2);
    }

    public function single_course_modules_custom()
    {
        global $post, $current_user, $woothemes_sensei, $first_module_content, $wp_query, $chapter_name_glob, $chapters_global, $first_module_id;

        $course_id = $post->ID;

        $html = '';

        if (has_term('', $this->taxonomy, $course_id)) {

            do_action('sensei_modules_page_before');

            // Get user data
            get_currentuserinfo();

            // Check if user is taking this course
            $is_user_taking_course = WooThemes_Sensei_Utils::sensei_check_for_activity(array('post_id' => intval($course_id),
                        'user_id' => intval($current_user->ID), 'type' => 'sensei_course_start'));

            // Get all modules
            $modules = $this->get_course_modules(intval($course_id));
            $lessons_completed = 0;

            //            Start building HTML
            $html .= '<section class="course-lessons">';
            //            Display course progress for users who are taking the course
            if (is_user_logged_in() && $is_user_taking_course) {
                $course_lessons = $woothemes_sensei->frontend->course->course_lessons(intval($post->ID));
                $total_lessons = count($course_lessons);
            }

            $first = 1;
            $chapters_global = array();
            $num_inc = 1;
            foreach ($modules as $key => $module) {
                $chap_num = $num_inc;
                $module_url = esc_url(add_query_arg('course_id', $course_id, get_term_link($module, $this->taxonomy)));

                if ($module->parent == 0) {
                    $chapter_name = explode(":", $module->name);
                    if (isset($_GET['chap_id'])) {
                        $active = ($module->term_id == $_GET['chap_id']) ? "active" : "";
                    } else {
                        $active = ($first == 1) ? "active" : "";
                    }
                    $chapters_global[] = "<li class=''><a href='" . $module_url . "'>Chapter <span class='chaptCount'>0" . (string) $chap_num . "</span> - <span class='chaptName'>" . $chapter_name[1] . "</span></a></li>";
                    $num_inc = $num_inc + 1;

                    //esc_url($module_url)
                    $html .= "<li class=''><a href='" . $module_url . "'>" . $chapter_name[0] . "</a></li>";
                }
                //                echo $first . "<br/>" . $module->term_id . "<br/>";
                if (isset($_GET['chap_id']) && $first == 1) {
                    if ($module->term_id == $_GET['chap_id']) {
                        $first_module_content = $module->description . "  <a href='" . site_url() . "/chapters/$module->term_id/?course_id=$course_id'>Continue...</a>";
                        $first_module_id = $module->term_id;
                    }
                    $first = 1;
                } elseif (!isset($_GET['chap_id']) && $first == 1) {
                    $first_module_content = $module->description . "  <a href='" . site_url() . "/chapters/$module->term_id/?course_id=$course_id'>Continue...</a>";
                    $first = 0;
                    $first_module_id = $module->term_id;
                }
            }

            // Replace place holders in course progress widget
            if (is_user_logged_in() && $is_user_taking_course) {

                // Add dynamic data to the output
                $html = str_replace('######', $lessons_completed, $html);
                $progress_percentage = abs(round(( doubleval($lessons_completed) * 100 ) / ( $total_lessons ), 0));

                $html = str_replace('@@@@@', $progress_percentage, $html);
                if (50 < $progress_percentage) {
                    $class = ' green';
                } elseif (25 <= $progress_percentage && 50 >= $progress_percentage) {
                    $class = ' orange';
                } else {
                    $class = ' red';
                }

                $html = str_replace('+++++', $class, $html);
            }
        }

        // Display output
        echo $html;

        if (has_term('', $this->taxonomy, $course_id)) {
            do_action('sensei_modules_page_after');
        }
    }

    public static function getLessons($course_id,$module_id)
    {
        global $woothemes_sensei;

        $args = array(
            'post_type' => 'lesson',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => '_lesson_course',
                    'value' => intval($course_id),
                    'compare' => '='
                )
            ),
            'tax_query' => array(
                array(
                    'taxonomy' => 'module',
                    'field' => 'id',
                    'terms' => intval($module_id)
                )
            ),
            'orderby' => 'menu_order',
            'order' => 'ASC',
            // 'suppress_filters' => 0
        );
        if (version_compare($woothemes_sensei->version, '1.6.0', '>=')) {
            $args['meta_key'] = '_order_module_' . intval($module_id);
            $args['orderby'] = 'meta_value_num date';
        }

        $lessons = get_posts($args);

        static::setLessonDays($lessons);

        usort($lessons,[get_called_class(),'compareLessons']);

        return $lessons;
    }

    public function sensei_course_single_chapter($id)
    {
        global $course_module, $course_module_lessons, $course_lessons_arr, $cour_id, $woothemes_sensei, $args;
        $course_id = !empty($cour_id) ? $cour_id : $_GET['course_id'];

        $term = get_term($id, $this->taxonomy);

        $args = array(
            'post_type' => 'lesson',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => '_lesson_course',
                    'value' => intval($course_id),
                    'compare' => '='
                )
            ),
            'tax_query' => array(
                array(
                    'taxonomy' => $this->taxonomy,
                    'field' => 'id',
                    'terms' => intval($id)
                )
            ),
            'orderby' => 'menu_order',
            'order' => 'ASC',
            // 'suppress_filters' => 0
        );
        if (version_compare($woothemes_sensei->version, '1.6.0', '>=')) {
            $args['meta_key'] = '_order_module_' . intval($id);
            $args['orderby'] = 'meta_value_num date';
        }

        $lessons = get_posts($args);

        $this->setLessonDays($lessons);

        usort($lessons,[get_called_class(),'compareLessons']);

        $course_module = $term;
        $course_module_lessons = $lessons;

        $course_lessons_arr = array_map(function($lesson) {
            return (int) $lesson->ID;
        },$lessons);

        $course_module->day_start = $this->getDayStart($lessons);
        $course_module->day_end = $this->getDayEnd($lessons);
        $course_module->lessons = $lessons;

        $first_lesson = ($course_module->lessons) ? $course_module->lessons[0] : null;
        $last_lesson = ($course_module->lessons>1) ? end($course_module->lessons) : null;

        $course_module->lessons_covered = (($first_lesson)?$first_lesson->day:'').(($last_lesson)?'-'.$last_lesson->day:'');
    }

    public function compareLessons($a,$b)
    {
        $a = floatval($a->day);
        $b = floatval($b->day);
        if($a===$b) { return 0; }
        return ($a<$b) ? -1 : 1;
    }

    public function setLessonDays(array $lessons)
    {
        foreach($lessons as $lesson) {
            $lesson->day = 0;
            if(!preg_match("#^Day ([0-9]+)(\.[0-9]+)?#ismu",$lesson->post_title,$matches)) {
                continue;
            }
            $lesson->day = $matches[1].$matches[2];
        }
    }

    public function getDayStart(array $lessons)
    {
        $start = 0;
        foreach($lessons as $lesson) {
            $day = intval($lesson->day);
            $start = (!$start || $day<$start) ? $day : $start;
        }
        return $start;
    }

    public function getDayEnd(array $lessons)
    {
        $last = 0;
        foreach($lessons as $lesson) {
            $day = intval($lesson->day);
            $last = (!$last || $day>$last) ? $day : $last;
        }
        return $last;
    }

    public function filter_lesson_posts($posts,$query)
    {
        if($query->get('order') !== 'ASC') {
            return $posts;
        }

        if(!is_tax($this->taxonomy) && ($query->get('post_type') !== 'lesson' || $query->get('orderby') !== 'title')) {
            return $posts;
        }

        if(is_tax($this->taxonomy) && !$query->is_main_query()) {
            return $posts;
        }

        $this->setLessonDays($posts);

        usort($posts,[get_called_class(),'compareLessons']);

        return $posts;
    }

}