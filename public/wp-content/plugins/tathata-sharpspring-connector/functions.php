<?php if(!defined('TGSSAPI_VERSION')) { return; }

use \TathataGolf\SharpSpring\SharpSpring;

if(!function_exists('sharpspring_add_product'))
{
    function sharpspring_add_product($email,$product)
    {
        return SharpSpring::add_product($email,$product);
    }
}

if(!function_exists('sharpspring_sync_email'))
{
    function sharpspring_sync_email($old_email,$new_email)
    {
        return SharpSpring::sync_email($old_email,$new_email);
    }
}