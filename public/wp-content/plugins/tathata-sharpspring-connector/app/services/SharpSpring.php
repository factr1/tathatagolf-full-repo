<?php namespace TathataGolf\SharpSpring;

class SharpSpring {

    use StaticCalls;

    const ENDPOINT = 'https://api.sharpspring.com/pubapi/v1/';

    protected static $instance;

    protected $params;
    protected $error;

    /**
    *   Create a new instance
    *
    *   @return void
    */
    public function __construct(array $params = [])
    {
        $this->params = array_merge([
            'accountID' => ((defined("SHARPSPRING_ACCOUNT_ID"))?SHARPSPRING_ACCOUNT_ID:''),
            'secretKey' => ((defined("SHARPSPRING_SECRET_KEY"))?SHARPSPRING_SECRET_KEY:''),
        ],$params);
    }

    /**
    *   Fetch a given URL via cURL post with json encoded data
    *
    *   @param string $url
    *   @param array $data
    *
    *   @return string|false
    */
    protected function fetchUrl($url,array $data)
    {
        $data = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HEADER,false);
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($ch,CURLOPT_MAXREDIRS,10);
        curl_setopt($ch,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_1);
        curl_setopt($ch,CURLOPT_REFERER,"http://".$_SERVER['HTTP_HOST']."/");
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,true);
        curl_setopt($ch,CURLOPT_CAINFO,ssl_path()."/Mozilla_Bundle.pem");
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,10);
        curl_setopt($ch,CURLOPT_TIMEOUT,60);
        curl_setopt($ch, CURLOPT_HTTPHEADER,[
            'Content-Type: application/json',
            'Content-Length: '.strlen($data)
        ]);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /**
    *   Returns the json decoded response from the url endpoint
    *
    *   @param array $data
    *
    *   @return object|false
    */
    protected function fetchResponse(array $data)
    {
        if(!($response=$this->fetchUrl($this->url(),$data))) {
            return false;
        }

        if(!($data=json_decode($response))) {
            return false;
        }

        // Check for expected data format
        if(!is_object($data)||!property_exists($data,'result')||!property_exists($data,'error')) {
            return false;
        }

        // Store any error
        $this->error = $data->error;

        // Return result
        return $data->result;
    }

    /**
    *   Returns the last error from the last api response
    *
    *   @return object
    */
    public function _error()
    {
        return $this->error;
    }

    /**
    *   Returns base endpoint query url
    *
    *   @return string
    */
    protected function url()
    {
        return static::ENDPOINT.'?'.http_build_query([
            'accountID' => $this->params['accountID'],
            'secretKey' => $this->params['secretKey'],
        ]);
    }

    /**
    *   Returns a data object for a request from the method and
    *   parameters
    *
    *   @param string $method
    *   @param object $params
    *
    *   @return array
    */
    protected function request($method,array $params)
    {
        return [
            'method' => $method,
            'params' => $params,
            'id' => session_id()
        ];
    }

    /**
    *   Return a list of fields from the api
    *
    *   @param array $params
    *
    *   @return array|false
    */
    public function _get_fields(array $params = [])
    {
        $response = $this->fetchResponse($this->request('getFields',array_merge([
            'where' => '',
        ],$params)));

        if(!$response||!property_exists($response,'field')) {
            $this->error = 'Invalid Response Received';
            return false;
        }

        return $response->field;
    }

    /**
    *   Return a list of customer fields from the api
    *
    *   @param array $where
    *
    *   @return array|false
    */
    public function _get_custom_fields(array $where = [])
    {
        return $this->_get_fields([
            'where' => array_merge([
                'isCustom' => 1,
            ],$where)
        ]);
    }

    /**
    *   Returns a custom field by the label
    *
    *   @param string $label
    *
    *   @return object|false
    */
    public function _get_custom_field($label)
    {
        $fields = $this->_get_custom_fields([
            'label' => $label,
        ]);

        if(!$fields) {
            $this->error = 'No Field Found';
            return false;
        }

        return $fields[0];
    }

    /**
    *   Return a list of leads from the api
    *
    *   @param array $params
    *
    *   @return array|false
    */
    public function _get_leads(array $params = [])
    {
        $response = $this->fetchResponse($this->request('getLeads',array_merge([
            'where' => '',
        ],$params)));

        if(!$response||!property_exists($response,'lead')) {
            $this->error = 'Invalid Response Received';
            return false;
        }

        return $response->lead;
    }

    /**
    *   Returns a lead by email address
    *
    *   @param string $email
    *
    *   @return object|false
    */
    public function _get_lead($email)
    {
        $leads = $this->_get_leads([
            'where' => [
                'emailAddress' => $email,
            ]
        ]);

        if(!$leads) {
            $this->error = 'No Leads Found';
            return false;
        }

        return $leads[0];
    }

    /**
    *   Updates an array of lead objects and returns a list of
    *   updates indicating success and any errors
    *
    *   @param array $leads
    *
    *   @return array|false
    */
    public function _update_leads(array $leads)
    {
        $response = $this->fetchResponse($this->request('updateLeads',[
            'objects' => $leads,
        ]));

        if(!$response||!property_exists($response,'updates')) {
            $this->error = 'Invalid Response Received';
            return false;
        }

        return $response->updates;
    }

    /**
    *   Adds a product to a leads "Customer Products". Returns
    *   true or false based on success.
    *
    *   @param string $email
    *   @param string $product
    *
    *   @return boolean
    */
    public function _add_product($email,$product)
    {
        $lead = $this->_get_lead($email);
        $field = $this->_get_custom_field('Customer Products');

        if(!$lead) {
            $this->error = 'No Lead Found';
            return false;
        }

        if(!$field) {
            $this->error = 'No "Customer Products" Field Found';
            return false;
        }

        $k = $field->systemName;

        $products = (property_exists($lead,$k)) ? explode(',',$lead->$k) : [];
        $products[] = $product;

        $data = [
            (object) [
                'id' => $lead->id,
                $k => implode(',',array_unique($products))
            ]
        ];

        $updates = $this->_update_leads($data);

        if(!$updates || !is_array($updates) || !property_exists($updates[0],'success') || !$updates[0]->success) {
            return false;
        }

        return true;
    }

    /**
    *   Updates a leads email address in the Sharpspring
    *   system.
    *
    *   @param string $old_email
    *   @param string $new_email
    *
    *   @return boolean
    */
    public function _sync_email($old_email,$new_email)
    {
        $lead = $this->_get_lead($old_email);

        if(!$lead) {
            $this->error = 'No Lead Found';
            return false;
        }

        $data = [
            (object) [
                'id' => $lead->id,
                'emailAddress' => $new_email,
            ]
        ];

        $updates = $this->_update_leads($data);

        if(!$updates || !is_array($updates) || !property_exists($updates[0],'success') || !$updates[0]->success) {
            return false;
        }

        return true;
    }

    /**
    *   Handle dynamic static calls using same instance
    *
    *   @param string $method
    *   @param array $args
    *
    *   @return void
    */
    public static function __callStatic($method,$args = [])
    {
        $method = "_".$method;
        $cls = get_called_class();
        if(method_exists($cls,$method)) {
            if(!static::$instance) {
                static::$instance = new $cls();
            }
            $c = static::$instance;
            return ($args) ? call_user_func_array([&$c,$method],$args) : $c->$method();
        } else {
            throw new \Exception("Unable to access ".$method." for class \"".get_called_class()."\" in static context");
        }
    }

}