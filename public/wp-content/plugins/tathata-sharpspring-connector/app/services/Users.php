<?php namespace TathataGolf\SharpSpring;

class Users {

    /**
    *   Promotes a user to a new group on course end.
    *
    *   @used-by add_action('sensei_user_course_end',[static::class,'course_end'])
    *
    *   @param integer $user_id
    *   @param integer $course_id
    *
    *   @return void
    */
    public static function course_end($user_id,$course_id) {

        // Load the course id and name
        $api_course_id = (defined('SHARPSPRING_COURSE_ID')) ? intval(SHARPSPRING_COURSE_ID) : 0;
        $api_course_name = (defined('SHARPSPRING_COURSE_PRODUCT_NAME')) ? strval(SHARPSPRING_COURSE_PRODUCT_NAME) : '';

        if(!$api_course_id || !$api_course_name) {
            return;
        }

        // Not the certified course, then bail
        if(intval($course_id)!==$api_course_id) {
            return;
        }

        $user = get_user_by('ID',$user_id);

        // User not found? shouldn't be triggered but bail
        if(!$user) {
            return;
        }

        $result = SharpSpring::add_product($user->user_email,$api_course_name);

        // Log any failures
        if(!$result) {
            @error_log(
                'Failed to add product "'.$api_course_name.'" to SharpSpring for lead "'.$user->user_email.'".'.
                ' SharpSpring Error: '.print_r(SharpSpring::error(),true)
            );
        }

    }

    /**
    *   Syncs a users email to Sharpspring on profile update
    *
    *   @used-by add_action('profile_update',[static::class,'profile_update'])
    *
    *   @param integer $user_id
    *   @param array $old_user_data
    *
    *   @return void
    */
    public static function profile_update($user_id, $old_user_data) {

        // Load the new user profile data
        $user = get_user_by('ID',intval($user_id));

        // User not found? shouldn't be triggered but bail
        if(!$user) {
            return;
        }

        // No change in email, then bail here
        if($user->user_email === $old_user_data->user_email) {
            return;
        }

        // Update the email
        $result = SharpSpring::sync_email($old_user_data->user_email,$user->user_email);

        // Log any failures
        if(!$result) {
            @error_log(
                'Failed to sync email address on profile update for lead "'.$user->user_email.'" with old email "'.$old_user_data->user_email.'".'.
                ' SharpSpring Error: '.print_r(SharpSpring::error(),true)
            );
        }

    }

}