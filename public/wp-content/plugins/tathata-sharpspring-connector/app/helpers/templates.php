<?php namespace TathataGolf\SharpSpring;

if(!defined('TGSSAPI_VERSION')) { return; }

function view($view,array $data = [])
{
    return PluginController::view($view,$data);
}