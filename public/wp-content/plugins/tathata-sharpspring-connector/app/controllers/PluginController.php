<?php namespace TathataGolf\SharpSpring;

class PluginController extends BaseController {

    use StaticCalls;

    protected static $instance;

    /**
    *   Create a new plugin instance and initialize controllers
    *
    *   @return string
    */
    public function __construct()
    {
        if(static::$instance) {
            return;
        }

        if(!session_id()) {
            session_start();
        }

        // Add user to grads after course completion
        add_action('sensei_user_course_end',[Users::class,'course_end'],10,2);

        // Sync profile email changes to SharpSpring
        add_action('profile_update',[Users::class,'profile_update'],10,2);

        static::$instance = $this;
    }

}