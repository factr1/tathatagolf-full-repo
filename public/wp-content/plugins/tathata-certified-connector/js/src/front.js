;(function($) {

    /**
    *   Initializes and draws google map for first .map div found
    *   with data coordinates
    *
    */
    var gmapsBackgroundInit = function() {

        var map$ = $('div.map[data-lat][data-lng]').eq(0);

        if(!map$.length) {
            return;
        }

        var lat = map$.data('lat');
        var lng = map$.data('lng');

        if(!lat||!lng) {
            return;
        }

        var latlng = new google.maps.LatLng(lat,lng);
        var myLatLng = new google.maps.LatLng(lat,lng);

        var stylez = [
            {
                stylers: [
                    {
                        saturation: 0,
                        lightness: 0
                    }
                ]
            }
        ];

        var mapOptions = {
            zoom: 15,
            center: latlng,
            scrollwheel: false,
            mapTypeControlOptions: {
                mapTypeIds: []
            }
        };

        var styledMapOptions = {
            name: "I + M"
        }

        var map = new google.maps.Map(map$.get(0), mapOptions);

        var jayzMapType = new google.maps.StyledMapType(stylez, styledMapOptions);

        map.mapTypes.set('I + M', jayzMapType);
        map.setMapTypeId('I + M');

        var image = '';

        var beachMarker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            animation: google.maps.Animation.DROP,
            icon: image
        });

    };

    $(document).ready(function() {

        $('section.certified-search').foundation();

        gmapsBackgroundInit();

        // Search Button Event
        $(document).on('click','div.search-form form a.button.search',function(e) {
            e.preventDefault();

            var q = $(this).closest('form').find('.input-group-field').val();

            if(!q) {
                return;
            }

            window.location.href = '/find-a-specialist/search?q='+encodeURIComponent(q);
        });

        // Search Button Submit on Enter Key
        $(document).on('keydown','div.search-form form input[type="text"]',function(e) {
            if(event.which==13) {
                e.preventDefault();
                $(this).closest('form').find('a.button.search').click();
            }
        });

        // Instructor Card Click Event
        $(document).on('click','div.row.instructor-card[data-href]',function(e) {
            e.preventDefault();
            var loc=$(this).data('href');
            if(!loc) {
                return;
            }
            window.location.href = loc;
        });

        // Animated load of custom profile header image
        $('.search-hero[data-image-url]').each(function() {
            var url = $(this).data('image-url');
            if(!url) {
                return;
            }

            var custom$ = $(this).clone().css({
                width: '100%',
                position: 'absolute',
                backgroundImage: 'url('+$(this).data('image-url')+')'
            });

            $(this).before(custom$);

            $(this).animate({
                opacity: 0
            },{
                duration: 800,
                complete: function() {

                    $(this).remove();

                    custom$.css({
                        width: 'auto',
                        position: 'static'
                    });

                }
            });
        });

        // Pagination Control Event
        $(document).on('click','.paginate-control[data-page]',function(e) {
            e.preventDefault();

            var page = $(this).data('page');

            if(!page) {
                return;
            }

            var url = window.location.pathname;
            var query = window.location.search;

            query = query.replace(/(&|\?)page=[^&]+/gi,'');

            window.location.href = url+query+'&page='+page;
        });

        // Instructor Review Button Click Event
        $(document).on('click','.button.instructor-review',function(e) {
            e.preventDefault();

            var modal$ = $(this).closest('.profile').find('.reveal[data-form]').eq(0);

            modal$.children().not(':first-child').remove();

            if(!modal$.length) {
                return;
            }

            var iframe$ = $(document.createElement('iframe'))
                .addClass('gfiframe')
                .addClass('instructor-review');

            iframe$.css({
                width: '100%',
                height: '550px',
                maxHeight: '95%',
                padding: '0px',
                margin: 'auto'
            });

            iframe$
                .attr('frameborder','0')
                .attr('allowTransparency','true')
                .attr('seamless','seamless');

            console.log('Form: '+modal$.data('form'));

            iframe$.prop('src',modal$.data('form'));

            modal$.append(iframe$);

            modal$.foundation('open');
        });

        // On modal open
        $(document).on('open.zf.reveal','.reveal[data-form]',function() {

            $(this).css({
                top: '10px'
            });

            window.scrollTo(0,0);

        });

    });

})(jQuery);