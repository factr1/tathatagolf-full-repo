# Tathata Golf Certified API Connector

The purpose of this plugin is to connect the the certified instructor API on the tathatagolfcertified.com site to 
query and display instructors based on search terms. It adds a new /find-a-specialist endpiont to the site providing
a search box for locating instructors.

The plugin also exposes a public functions to retrieve all instructors and those that have completed certification.

* tg_get_instructors()
* tg_get_certified_instructors()

## API Authorization

To connect to the API you'll need to define the following constants in the WP config file:

```php

define('TGC_API_EMAIL','email_address_of_user@certified.site');
define('TGC_API_KEY','api_key_defined_on_certified_users_profile');

```

## Overriding Template Views

All default templates for the plugin are stored in the app/views folder. Any of these templates can be overridden 
by the theme by copying them to a folder in the theme named "tathata-certified-connector".