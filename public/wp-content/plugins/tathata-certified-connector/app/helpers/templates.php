<?php namespace TathataGolfCertifiedConnector;

if(!defined('TGCCNCTR_VERSION')) { return; }

function view($view,array $data = [])
{
    return PluginController::view($view,$data);
}

function ajax_token()
{
    return AjaxController::TOKEN_NAME;
}

function nonce()
{
    return AjaxController::nonce();
}

function strip_description($s)
{
    $tags = [
        '<br>', '<div>', '<a>', '<p>', '<h1>', '<h2>', '<h3>', '<h4>', '<h5>', '<h6>', '<b>', '<i>', '<u>',
        '<strong>', '<em>', '<ul>', '<ol>', '<li>', '<sup>', '<sub>',
    ];

    return strip_tags($s,implode('',$tags));
}