<?php namespace TathataGolfCertifiedConnector;

class Profiles {

    use StaticCalls;

    /**
    *   Create a new instance
    *
    */
    public function __construct()
    {
        $this->api = API::getInstance();
    }

    /**
    *   Return json data for all profiles
    *
    *   @return string
    */
    public function _getProfiles()
    {
        self::__protect($this,__FUNCTION__);

        return ($profiles=$this->api->get('profiles')) ? $profiles : [];
    }

    /**
    *   Return json data for all completed profiles
    *
    *   @return string
    */
    public function _getCompletedProfiles($page = 1)
    {
        self::__protect($this,__FUNCTION__);

        return ($profiles=$this->api->get('profiles/completed'.(($page)?'?page='.intval($page):''))) ? $profiles : [];
    }

    /**
    *   Return json data for all featured profiles
    *
    *   @return string
    */
    public function _getFeaturedProfiles()
    {
        self::__protect($this,__FUNCTION__);

        return ($profiles=$this->api->get('profiles/featured')) ? $profiles : [];
    }

    /**
    *   Return json data for single profile by user id
    *
    *   @param integer $id
    *
    *   @return string
    */
    public function _getProfileById($id)
    {
        self::__protect($this,__FUNCTION__);

        return ($profile=$this->api->get('profiles/'.intval($id))) ? $profile : null;
    }

    /**
    *   Return json data for single profile by user slug
    *
    *   @param string $slug
    *
    *   @return string
    */
    public function _getProfileBySlug($slug)
    {
        self::__protect($this,__FUNCTION__);

        return ($profile=$this->api->get('profiles/slug/'.urlencode($slug))) ? $profile : null;
    }

    /**
    *   Return json data for all completed profiles by search query
    *
    *   @param string $q
    *   @param integer $page
    *
    *   @return string
    */
    public function _getProfilesByQuery($q,$page = 1)
    {
        self::__protect($this,__FUNCTION__);

        return ($profile=$this->api->get('profiles/search?q='.urlencode($q).'&page='.intval($page))) ? $profile : [];
    }

    /**
    *   Returns the url route for a profile
    *
    *   @param object $profile
    *
    *   @return string
    */
    public static function url(\stdClass $profile)
    {
        $e = RouteController::ENDPOINT;
        if(!property_exists($profile,'profile_slug')||!$profile->profile_slug) {
            return $e.'/'.$profile->ID;
        }
        return $e.'/profiles/'.urlencode($profile->profile_slug);
    }

}