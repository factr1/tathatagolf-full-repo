<?php namespace TathataGolfCertifiedConnector;

class API {

    const ENDPOINT = 'http://tathatagolfcertified.com/tathata-profiles-api';
    //const ENDPOINT = 'http://www.tathatagolfcertified.dev/tathata-profiles-api';

    protected static $instance;

    protected $params;

    /**
    *   Create a new instance
    *
    */
    public function __construct(array $params = [])
    {
        $this->params = array_merge([
            'apiEmail' => '',
            'apiKey' => ''
        ],$params);
    }

    /**
    *   Returns an authorization header from the email and key
    *
    *   @return string
    */
    protected function getAuthorizationHeader()
    {
        return 'ApiAuth: '.base64_encode('TGCAPI '.$this->params['apiEmail'].':'.$this->params['apiKey']);
    }

    /**
    *   Fetch a Given URL via cURL
    *
    *   @param string $url
    */
    protected function fetchUrl($url)
    {
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_HTTPGET,true);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HEADER,false);
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($ch,CURLOPT_MAXREDIRS,10);
        curl_setopt($ch,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_1);
        curl_setopt($ch,CURLOPT_REFERER,"http://".$_SERVER['HTTP_HOST']."/");
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,true);
        curl_setopt($ch,CURLOPT_CAINFO,ssl_path()."/Mozilla_Bundle.pem");
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,10);
        curl_setopt($ch,CURLOPT_TIMEOUT,60);
        curl_setopt($ch,CURLOPT_HTTPHEADER,[$this->getAuthorizationHeader()]);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /**
    *   Returns the json decoded response from a url endpoint
    *
    *   @param string $url
    */
    protected function fetchResponse($url)
    {
        if(!($response=$this->fetchUrl($url))) {
            return false;
        }

        if(!($data=json_decode($response))) {
            return false;
        }

        return $data;
    }

    /**
    *   Returns base endpoint query url
    *
    *   @param string $route
    */
    protected function query($route)
    {
        if(!preg_match('#^/#ismu',$route)) {
            $route = '/'.$route;
        }

        return static::ENDPOINT.$route;
    }

    /**
    *   Returns response for a given route
    *
    *   @param string $route
    */
    public function get($route)
    {
        return $this->fetchResponse($this->query($route));
    }

    /**
    *   Returns new API instance loading any defined email/key
    *
    *   @param array $params
    */
    public static function getInstance(array $params = [])
    {
        if(is_null(static::$instance)) {
            static::$instance = new static(array_merge([
                'apiEmail' => ((defined("TGC_API_EMAIL"))?TGC_API_EMAIL:''),
                'apiKey' => ((defined("TGC_API_KEY"))?TGC_API_KEY:'')
            ],$params));
        }
        return static::$instance;
    }

}