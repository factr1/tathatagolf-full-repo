<?php namespace TathataGolfCertifiedConnector;
if(!defined("TGCCNCTR_PLUGIN_FILE")) { exit; } ?>

<div class="small-14 small-centered columns search-form">
    <h1><?php echo $heading; ?></h1>
    <form>
        <div class="input-group">
            <input type="text" class="input-group-field" placeholder="Search by City & State, Zip or Movement Specialist Name" />
            <div class="input-group-button">
                <a href="#" class="button search">Search</a>
            </div>
        </div>
    </form>
    <p class="search-form-info">All movement specialists within 100 miles are shown</p>
    <div class="search-links">
        <a href="http://www.tathatagolf.com/movement-specialists/" class="tg-btn"><i class="fa fa-question-circle" aria-hidden="true"></i> What is a Movement Specialist</a>
        <a href="http://www.tathatagolf.com/become-a-certified-movement-specialist/" class="tg-btn"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Become a Movement Specialist</a>
    </div>
</div>
