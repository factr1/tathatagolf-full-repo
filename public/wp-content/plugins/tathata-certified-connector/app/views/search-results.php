<?php namespace TathataGolfCertifiedConnector;
if(!defined("TGCCNCTR_PLUGIN_FILE")) { exit; } ?>

<!--<div class="search-hero"></div>-->
<div class="row get-started">
  <?php echo view('search-form',[
    'heading' => 'Search Results for "'.esc_html($query).'"',
  ]); ?>
</div>
<div class="row search-results">
    <div class="expanded row">
        <div class="small-18 small-centered columns">
            <div class="expanded row small-up-1 large-up-2">
                <?php if($results) { foreach($results as $profile) { ?>
                    <div class="column ms-result">
                        <?php echo view('instructor-card',['user'=>$profile]); ?>
                    </div>
                <?php } } else { ?>
                    <h1 class="text-center">No specialists found. Try searching again.</h1>
                <?php } ?>
            </div>
            <?php if($pages>1) { ?>
            <br />
            <br />
            <div class="expanded row">
                <div class="small-12 columns text-left">
                    <?php if($page>1) { ?>
                    <a href="#" class="button paginate-control" data-page="<?php echo intval($page-1); ?>"><i class="fa fa-angle-double-left"></i> Previous Results</a>
                    <?php } ?>
                </div>
                <div class="small-12 columns text-right">
                    <?php if($page<$pages) { ?>
                    <a href="#" class="button paginate-control" data-page="<?php echo intval($page+1); ?>">More Results <i class="fa fa-angle-double-right"></i></a>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
