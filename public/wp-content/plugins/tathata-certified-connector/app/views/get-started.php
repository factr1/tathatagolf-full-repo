<?php namespace TathataGolfCertifiedConnector;
if(!defined("TGCCNCTR_PLUGIN_FILE")) { exit; } ?>

<div class="row get-started">
    <?php echo view('search-form',[
        'heading' => 'Find a Certified Movement Specialist',
    ]); ?>
</div>
<?php if($featured) { ?>
<div class="row search-results">
    <div class="expanded row">
        <div class="small-18 small-centered columns">
            <h1 class="featured-title">Featured Movement Specialists</h1>
        </div>
    </div>
    <div class="expanded row">
        <div class="small-18 small-centered columns">
            <div class="expanded row small-up-1 large-up-2">
                <?php foreach($featured as $profile) { ?>
                <div class="column ms-result">
                    <?php echo view('instructor-card',['user'=>$profile]); ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>
