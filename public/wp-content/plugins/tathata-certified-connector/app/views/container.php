<?php namespace TathataGolfCertifiedConnector;
if(!defined("TGCCNCTR_PLUGIN_FILE")) { exit; } ?>

<?php get_header(); ?>

<?php echo view('assets'); ?>

<section class="certified-search"
data-token="<?php echo esc_attr(ajax_token()); ?>"
data-nonce="<?php echo esc_attr(nonce()); ?>"
data-ajax-url="<?php echo esc_attr(ajax_url()); ?>">
    <?php if(isset($view)) { ?>
        <?php echo view($view,(isset($data)?$data:[])); ?>
    <?php } ?>
</section>

<?php

get_footer();