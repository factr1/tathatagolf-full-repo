<?php namespace TathataGolfCertifiedConnector;
if(!defined("TGCCNCTR_PLUGIN_FILE")) { exit; } ?>

<?php /* ?>
<p><a data-open="exampleModal1">Click me for a modal</a></p>

<div class="reveal" id="exampleModal1" data-reveal>
  <h1>Awesome. I Have It.</h1>
  <p class="lead">Your couch. It is mine.</p>
  <p>I'm a cool paragraph that lives inside of an even cooler modal. Wins!</p>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<script type="text/javascript">
(function($) {

    $(document).ready(function() {

        var modal$ = $('#exampleModal1');

        console.log('Modal Found: '+modal$.length);

        modal$.foundation('open');

    });

})(jQuery);
</script>

<?php
*/
?>



<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<div class="search-hero" data-image-url="<?php echo esc_attr($user->profile_header_image); ?>"></div>
<div class="column row profile">
    <div class="row">
        <div class="large-6 columns text-center">
            <img src="<?php echo esc_attr($user->avatar); ?>" alt="Meet Your Local Tathata Golf Certified Movement Specialist" />
        </div>
        <div class="large-18 columns">
            <h1>
                <?php echo esc_html($user->first_name.' '.$user->last_name); ?>
                <span>
                    <?php for($x=1;$x<=5;$x++) { ?>
                        <i class="fa fa-star<?php echo ((intval($user->student_rating)<$x)?'-o':''); ?>"></i>
                    <?php } ?>
                </span>
                <?php if($user->course_completed) { ?>
                <small>
                    <?php echo 'Certified Movement Specialist since '.date('F Y',intval($user->course_completed)); ?>
                </small>
                <?php } ?>
            </h1>
            <?php if($user->locations) { foreach($user->locations as $location) { ?>
            <div class="expanded row medium-up-2">
                <div class="column">
                    <?php echo esc_html($location->name); ?>
                </div>
                <div class="column">
                    <a href="mailto:<?php echo esc_html($user->email); ?>"><?php echo esc_html($user->email); ?></a>
                </div>
                <div class="column">
                    <?php echo esc_html($location->address?$location->address->address:''); ?>
                </div>
                <div class="column">
                    <?php echo esc_html($location->phone); ?>
                </div>
                <div class="column">
                    <?php echo intval($location->months_of_year); ?> months of the year
                </div>
                <div class="column">
                    Offers:
                    <?php if($user->live_training) { ?>
                    <span>Chapter Follow-Ups</span>
                    <?php } ?>
                    <?php if($user->group_training_offerings) { ?>
                    <span>Group Training</span>
                    <?php } ?>
                </div>
            </div>
            <?php /* ?>
            <div class="expanded row">
                <?php echo strip_description($location->description); ?>
            </div>
            <?php */ ?>
            <?php }} ?>
            <div class="expanded row social">
                <?php if($user->email) { ?>
                <a href="mailto:<?php echo esc_attr($user->email); ?>"><i class="fa fa-envelope"></i></a>
                <?php } ?>
                <?php if($user->website) { ?>
                <a href="<?php echo esc_attr($user->website); ?>"><i class="fa fa-globe"></i></a>
                <?php } ?>
                <?php if($user->facebook) { ?>
                <a href="<?php echo esc_attr($user->facebook); ?>"><i class="fa fa-facebook"></i></a>
                <?php } ?>
                <?php if($user->twitter) { ?>
                <a href="https://twitter.com/<?php echo esc_attr($user->twitter); ?>"><i class="fa fa-twitter"></i></a>
                <?php } ?>
                <?php if($user->instagram) { ?>
                <a href="<?php echo esc_attr($user->instagram); ?>"><i class="fa fa-instagram"></i></a>
                <?php } ?>
                <?php if($user->googleplus) { ?>
                <a href="<?php echo esc_attr($user->googleplus); ?>"><i class="fa fa-google-plus"></i></a>
                <?php } ?>
                <?php if($user->youtube) { ?>
                <a href="<?php echo esc_attr($user->youtube); ?>"><i class="fa fa-youtube"></i></a>
                <?php } ?>
            </div>
            <div class="expanded row bio">
                <?php echo strip_description($user->bio); ?>
            </div>
            <div class="expanded row text-center">
                <?php if(is_user_logged_in()) { ?>
                <a href="#" class="button instructor-review">Review this instructor</a>
                <?php } else { ?>
                <a href="<?php echo wp_login_url(); ?>" class="button">Please login to leave a review</a>
                <?php } ?>
            </div>
            <div class="reveal" data-reveal data-form="http://staging.tathatagolfcertified.com/gfembed/?f=6&instructor_id=<?php echo $user->ID; ?>">
                <a class="close-button" data-close aria-label="Close modal">
                    <span aria-hidden="true">&times;</span>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="expanded row">
    <?php if($user->locations) { foreach($user->locations as $location) { ?>
        <div class="map" data-lat="<?php echo esc_attr($location->address->lat); ?>" data-lng="<?php echo esc_attr($location->address->lng); ?>"></div>
    <?php }} ?>
</div>

