<?php namespace TathataGolfCertifiedConnector;
if(!defined("TGCCNCTR_PLUGIN_FILE")) { exit; } ?>

<?php $location = ($user->locations) ? $user->locations[0] : null; ?>

<div class="row instructor-card" data-href="<?php echo esc_attr(profile_url($user)); ?>">
    <div class="small-6 columns">
        <img src="<?php echo esc_attr($user->avatar); ?>" alt="Find a Tathata Golf Certified Movement Specialist Near You" />
    </div>
    <div class="small-18 columns">
        <span>
            <?php for($x=1;$x<=5;$x++) { ?>
                <i class="fa fa-star<?php echo ((intval($user->student_rating)<$x)?'-o':''); ?>"></i>
            <?php } ?>
        </span>
        <h4><?php echo esc_html($user->first_name.' '.$user->last_name); ?></h4>
        <?php if($location) { ?>
        <address>
            <?php echo esc_html($location->name); ?><br />
            <?php echo esc_html($location->address?$location->address->address:''); ?>
        </address>
        <?php } ?>
        <?php if($user->live_training) { ?>
        <span>Chapter Follow-Ups</span>
        <?php } ?>
        <?php if($user->group_training_offerings) { ?>
        <span>Group Training</span>
        <?php } ?>
    </div>
</div>