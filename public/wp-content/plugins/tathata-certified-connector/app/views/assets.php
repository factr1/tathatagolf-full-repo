<?php namespace TathataGolfCertifiedConnector;
if(!defined("TGCCNCTR_PLUGIN_FILE")) { exit; } ?>

<link rel="stylesheet" href="<?php echo plugin_url('css/front.min.css'); ?>" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic|Open+Sans+Condensed:300,300italic,700" rel="stylesheet" />
<script type="text/javascript" src="<?php echo plugin_url('js/front.min.js'); ?>"></script>