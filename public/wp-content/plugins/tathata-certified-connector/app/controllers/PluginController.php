<?php namespace TathataGolfCertifiedConnector;

class PluginController extends BaseController {

    use StaticCalls;

    protected static $instance;

    /**
    *   Create a new plugin instance and initialize controllers
    *
    *   @return string
    */
    public function _init()
    {
        if(static::$instance) {
            return;
        }

        RouteController::init();
        AjaxController::init();

        static::$instance = $this;
    }

}