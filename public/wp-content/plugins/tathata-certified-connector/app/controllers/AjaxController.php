<?php namespace TathataGolfCertifiedConnector;

class AjaxController extends BaseController {

    const TOKEN_NAME = 'tgccnctr_plugin_ajax_token';
    const NONCE_TOKEN = '[Pi0TU^gJd|`|vx%e]0,W@Qb-Q+-N+H|DUSO6,,[HjV8q98yd|Fy?[8yWC.AXk`~';
    const MAX_FILE_UPLOAD_SIZE = 16777216;

    use StaticCalls;

    protected static $nonce = null;

    public static function init()
    {
        //add_action("wp_ajax_tgccnctr_plugin_fetch_job",function() { AjaxController::fetch_job(); });
        //add_action("wp_ajax_nopriv_tgccnctr_plugin_fetch_job",function() { AjaxController::fetch_job(); });
    }

    public function __construct()
    {
        parent::__construct();
        check_ajax_referer(static::NONCE_TOKEN,static::TOKEN_NAME);
    }

    public static function nonce()
    {
        if(is_null(self::$nonce)) {
            self::$nonce = \wp_create_nonce(self::NONCE_TOKEN);
        }
        return self::$nonce;
    }

}