<?php namespace TathataGolfCertifiedConnector;

class ProfileController extends BaseController {

    use StaticCalls;

    /**
    *   Returns search form view
    *
    *   @return string
    */
    public function _getSearchForm()
    {
        self::__protect($this,__FUNCTION__);

        return view('container',[
            'view' => 'get-started',
            'data' => [
                'featured' => Profiles::getFeaturedProfiles(),
            ],
        ]);
    }

    /**
    *   Returns search results from a given query
    *
    *   @param string $q
    *
    *   @return string
    */
    public function _getSearchResults($q = '',$page = 1)
    {
        self::__protect($this,__FUNCTION__);

        if(!is_string($q)||!preg_match('#^[0-9]+$#ismu',(string)$page)) {
            return;
        }

        $q = urldecode($q);

        $results = ($q) ? Profiles::getProfilesByQuery($q,$page) : Profiles::getCompletedProfiles($page);

        return view('container',[
            'view' => 'search-results',
            'data' => [
                'query' => $q,
                'results' => $results->items,
                'count' => $results->count,
                'pages' => $results->pages,
                'page' => $results->page
            ]
        ]);
    }

    /**
    *   Returns profile view by user id
    *
    *   @param integer $id
    *
    *   @return string
    */
    public function _getProfileById($id)
    {
        self::__protect($this,__FUNCTION__);

        $user = Profiles::getProfileById($id);

        if(!$user) {
            return;
        }

        return view('container',[
            'view' => 'profile',
            'data' => [
                'user' => $user
            ]
        ]);
    }

    /**
    *   Returns profile view by user slug
    *
    *   @param string $slug
    *
    *   @return string
    */
    public function _getProfileBySlug($slug)
    {
        self::__protect($this,__FUNCTION__);

        $user = Profiles::getProfileBySlug($slug);

        if(!$user) {
            return;
        }

        return view('container',[
            'view' => 'profile',
            'data' => [
                'user' => $user
            ]
        ]);
    }

}