<?php namespace TathataGolfCertifiedConnector;
/*
Plugin Name: Tathata Golf Certified API Connector
Plugin URI: http://www.tathatagolfcertified.com/
Description: API Connector for Certified Instructor Profiles
Version: 0.0.1
Author: Factor1, James R. Latham
Author URI: http://www.factor1studios.com
*/

if(!function_exists('add_action')) {
    exit;
}

require_once('constants.php');
require_once('vendor/autoload.php');

spl_autoload_register([__NAMESPACE__.'\BaseController','__autoload']);

add_action('init',function() {
    PluginController::init();
});

require_once(TGCCNCTR_PLUGIN_DIR.'/functions.php');