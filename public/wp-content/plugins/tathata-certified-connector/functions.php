<?php

use TathataGolfCertifiedConnector\ProfileController;

if(!function_exists("tg_get_instructors"))
{
    function tg_get_instructors()
    {
        return Profiles::getProfiles();
    }
}

if(!function_exists("tg_get_certified_instructors"))
{
    function tg_get_certified_instructors()
    {
        return Profiles::getCompletedProfiles();
    }
}