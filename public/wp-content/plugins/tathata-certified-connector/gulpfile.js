;

/*
|--------------------------------------------------------------------------
|  App Task Options
|--------------------------------------------------------------------------
|
|
*/

var app = {
    production: false,
    defaultTasks: [
        'copy',
        'scss',
        'scripts'
    ]
};

/*
|--------------------------------------------------------------------------
|  Dependencies
|--------------------------------------------------------------------------
|
|
*/

var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var minifyCss = require('gulp-cssnano');
var rename = require('gulp-rename');
var minifyJs = require('gulp-uglify');
var sass = require('gulp-sass');
var notify = require('gulp-notify');

/*
|--------------------------------------------------------------------------
|  Vendor Asset Paths
|--------------------------------------------------------------------------
|
|
*/

var paths = {
    PluginSCSS: 'css/scss',
    PluginJS: 'js/src',
    PublicFonts: 'fonts',
    PublicCSS: 'css',
    PublicJS: 'js',
    Foundation: 'bower_components/foundation-sites',
    FontAwesome: 'bower_components/fontawesome',
    jQuery: 'bower_components/jquery',
};

/*
|--------------------------------------------------------------------------
|  App Tasks
|--------------------------------------------------------------------------
|
|
*/

gulp.task('copy',function() {

    gulp
        .src(paths.FontAwesome+'/fonts/**')
        .pipe(gulp.dest(paths.PublicFonts))
        ;

});

gulp.task('scss',function() {

    var task = gulp
        .src([
            paths.PluginSCSS+'/front.scss'
        ])
        .pipe(sass({
            outputStyle: app.production ? 'compressed' : 'nested',
            includePaths: [
                paths.Foundation+'/scss/',
                paths.FontAwesome+'/scss/',
                paths.PluginSCSS,
            ]
        }))
        .pipe(autoprefixer());

    if(app.production) {
        task.pipe(minifyCss())
    }

    return task.pipe(rename({
        extname: '.min.css'
    }))
    .pipe(gulp.dest(paths.PublicCSS));

});

gulp.task('scripts',function() {
    var task = gulp
        .src([
            /*paths.jQuery+'/dist/jquery.js',*/
            paths.Foundation+'/dist/foundation.js',
            paths.PluginJS+'/front.js'
        ])
        .pipe(concat('front.js'));

    if(app.production) {
        task.pipe(minifyJs().on('error',function(err) {
            console.log('Minify Error: '+err);
        }));
    }

    return task.pipe(rename({
        extname: '.min.js'
    }))
    .pipe(gulp.dest(paths.PublicJS));

});

gulp.task('watch',function() {
    gulp.watch(paths.PublicCSS+'/scss/**',['scss']);
    gulp.watch(paths.PublicJS+'/src/*.js',['scripts']);
});

gulp.task('default',app.defaultTasks);