<?php namespace TathataGolfCertifiedConnector;

if(!defined('TGCCNCTR_VERSION')) {
    define('TGCCNCTR_VERSION','0.0.2');
}
if(!defined('TGCCNCTR_PLUGIN_FILE')) {
    define("TGCCNCTR_PLUGIN_FILE",str_replace("\\","/",realpath(__FILE__)));
}
if(!defined('TGCCNCTR_PLUGIN_DIR')) {
    define("TGCCNCTR_PLUGIN_DIR",str_replace("\\","/",realpath(dirname(TGCCNCTR_PLUGIN_FILE))));
}
if(!defined('TGCCNCTR_PLUGIN_PREFIX')) {
    define('TGCCNCTR_PLUGIN_PREFIX','TGCCNCTR_');
}
if(!defined('TGC_API_EMAIL')) {
    define('TGC_API_EMAIL','');
}
if(!defined('TGC_API_KEY')) {
    define('TGC_API_KEY','');
}
