/*
 * NOTE: all actions are prefixed by plugin shortnam_action_name
 */

jQuery(function($){
	
	setTimeout(function() {
		$( "ul.login-menus li:nth-child(4)" ).find("a").attr("href", "javascript:load_post_form()");
	}, 2000);
	
	
	// Alert messages
	var successMsg;
	var failedMsg;
	var waitMsg;

	if (get_option('_success_message')) {
		successMsg = get_option('_success_message');
	} else{
		successMsg = 'Post is saved successfully...';
	};
	if (get_option('_error_message')) {
		errorMsg = get_option('_error_message');
	} else{
		errorMsg = 'Please provide missing details above...';
	};
	if (get_option('_wait_message')) {
		waitMsg = get_option('_wait_message');
	} else{
		waitMsg = 'Please wait while saving...';
	};

	//posting form
	$("#nm-post-front-form").submit(function(e){
		e.preventDefault();

		$("#saving-response").html(waitMsg);
		
		var content;
		
		if (get_option('_rich_editor') == 'yes') {

			var editor = tinyMCE.get('post_description');
			if (editor) {
			    // Ok, the active tab is Visual
			    content = editor.getContent();
			} else {
			    // The active tab is HTML, so just query the textarea
			    content = $('#post_description').val();
			}
		}
		
		
		//console.log( content ); return false;
			
		if( is_valid() ){
			
			var post_data = $(this).serialize();
			post_data = post_data + '&action=nm_postfront_save_post&postcontents='+content;
			//post_data = post_data + '&action=nm_postfront_save_post';
			
			console.log(post_data);

			$.post(nm_postfront_vars.ajaxurl, post_data, function(resp){
				
				console.log(resp);
				
				$("#saving-response").html(successMsg).css({'color':'green'});
			});
			
			
		}else{
			$("#saving-response").html(errorMsg).css({'color':'red'});
		}
		
		
	});

	// Taxonomy Dropdown Scripts

	$('.tax-styles li:has(ul)').addClass('expand').find('ul').hide();
	$('.tax-styles li.expand>label').after('<span>[ + ]</span>');

	
	$('.tax-styles').on('click', 'li.collapse span', function (e) {
		$(this).text('[ + ]').parent().addClass('expand').removeClass('collapse').find('>ul').slideUp();
		e.stopImmediatePropagation();
	});

	$('.tax-styles').on('click', 'li.expand span', function (e) {
		$(this).text('[ - ]').parent().addClass('collapse').removeClass('expand').find('>ul').slideDown();
		e.stopImmediatePropagation();
	});

	$('.tax-styles').on('click', 'li.collapse li:not(.collapse)', function (e) {
		e.stopImmediatePropagation();
	}); 	

});




function load_post_form(){
	
	var uri_string = encodeURI('action=nm_postfront_load_post_form&&width=960&height=500');
	
	var url = nm_postfront_vars.ajaxurl + '?' + uri_string;
	tb_show('Create New Post', url);
}



function is_valid(){
	
	var valid_flag = true;
	if(jQuery("#post_title").val() == ''){
		jQuery("#post_title").css({'border':'1px solid red'});
		valid_flag = false
	}
	
	/*if(jQuery("#post_description").val() == ''){
		jQuery("#post_description").css({'border':'1px solid red'});
		valid_flag = false
	}*/
	
	return valid_flag;
}

function get_option(key){
	
	/*
	 * TODO: change plugin shortname
	 */
	var keyprefix = 'nm_postfront';
	
	key = keyprefix + key;
	
	var req_option = '';
	
	jQuery.each(nm_postfront_vars.settings, function(k, option){
		
		//console.log(k);
		
		if (k == key)
			req_option = option;		
	});
	
	//console.log(req_option);
	return req_option;
	
}