# Tathata Golf Factor1 Dev Repo #
This is a repo of TathataGolf.com and was pulled from the live production server on 7/7/15. All changes to the theme will be committed here. The purpose of the TathataGolf site is to promote the tools and resources of a digital golf training system. Tathata has many tools and systems for teaching a healthy lifestyle revolving around Golf, Meditation, Martial Arts, and Clean eating.

The TG site is primarily focused on a 60 day digital learning model, with 45 minute to 1 hour videos, with a mandatory quiz between each lesson. As users progress at their own pace, they move up the prerequisite levels of the course, and earn badges along the way. Once a user completes the 60day course, they graduate. Users can participate in the course via DVD, but still must take the lessons quizzes to officially graduate. Graduated users can take an optional 2.5 day on site training class called the 2.5 day live Grad School Experience. These are currently held 1 - 2 times a month.

Visit [the wiki](https://bitbucket.org/factr1/tathatagolf-full-repo/wiki/Home) for more detailed information on more specific parts of the site.

## Who do I talk to?

[Matt Adams](mailto: matt@factor1studios.com)
[Alexander Persky](mailto: alexander@factor1studios.com)
[Eric Stout](mailto: eric@factor1studios.com)

### To setup your local environment:

Create a "wp-config.settings" file in the project root like this and change the hostnames
to your local environment host names.

##### Example wp-config.settings file

```
<?php

$config = [
  'environments' => [
    'local'=>[
      'error_reporting' =>E_ALL,
      'http_hosts' => [
        'www.tathata:8888',
        'tathata:8888'
      ],
      'public_paths'=>[
        '',
      ]
    ]
  ]
];
```

##### Example wp-config.local file

```
<?php

/* MySQL settings */
define( 'DB_NAME',     'tg_local' );
define( 'DB_USER',     'root' );
define( 'DB_PASSWORD', 'root' );
define( 'DB_HOST',     'localhost' );
define( 'DB_CHARSET',  'utf8' );


/* MySQL database table prefix. */
$table_prefix = 'tg_';


define('AUTH_KEY',         'H.cJP{O$={H@K>;f+mySX)Zq ff*E*/abGp|~Fde~F+9RXuwcI/-Wg+5REM2h2tL');
define('SECURE_AUTH_KEY',  'JJz(*<8P)q1Uyo3Se=toSB;/QEtB0I{@;bh%]f;jn#-FbdeGM8iErY^.=t8F@{#u');
define('LOGGED_IN_KEY',    'hk-!6.R9(u!A(p6@;Vzewjn/x99~6:)-dL7w7,[fi~!f[wc[](t};0oIP>^)=@(|');
define('NONCE_KEY',        '=3A>0%oB1nfh (c4elJx6>WQCe%&_0!OK @HAc!?DLLq-u|&-#[7l?<B-86aIdVn');
define('AUTH_SALT',        '+M_xNcr!t>M_mhV6arvxBM=iuM`#9Q7*W4t|=k8ei]t!uZ?>Wo:~Jn6V0TC]&QKe');
define('SECURE_AUTH_SALT', '<-O87UVElhwcZWb,YC7p!8)g_dZv<NAj.,<r@JPOAHr6?z7=#xKD+@@=_Bh7,|TN');
define('LOGGED_IN_SALT',   'B]f&3A6yBi-ueN 0[BR:sVhD:~!aIA]FqKd^Ll@UD-Aa^D$Y+4X-3$K`<PSip|tv');
define('NONCE_SALT',       'qmhCQRS3Q=q<DIXhIO8&-ZSbU=?4vNT00/4hM7Ob[exyCh(-Qce%=(;R|dnP|V-e');


/* WordPress Localized Language. */
define( 'WPLANG', '' );


/* Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') ) {
  define('ABSPATH', dirname(__FILE__) . '/');
}

/* Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
```

## Repo Organization

Server Repo URL: ssh://tathata2@tathatagolf.com/home/tathata2/repos/TathataGolf.git

* `master` branch: Production, on www.tathatagolf.com

* `develop` branch: Staged for testing only, on staging.tathatagolf.org

### Tathata Git Workflow

A good working model to follow: http://nvie.com/posts/a-successful-git-branching-model/.

The model described in the link above from Nvie.com eventually gave way to Git Flow. Git Flow is not a deviation of the workflow, but a centralized "standard" for accomplishing the model. You can read about it [here on Bitbucket](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) or [here on Tower](https://www.git-tower.com/learn/git/ebook/en/mac/advanced-topics/git-flow). Pick one of the 2 models, learn how to use it, and stick with it. Git FLow can be done via the command line or within Git GUIs, Tower for example makes using Git Flow extremely easy.

#### New Features

Many branches have already been completed using the Git Flow model, and in order to aid in collaboration efforts here is an example flow of a new feature (assuming a recent release, so that both `master` and `develop` are the same):

1. client requests a new feature "hero"
2. a new `feature/hero` branch is created from the `develop` branch. All development for this feature is completed strictly on this branch
3. when the feature is ready for client review, the `feature/hero` branch is "finished". This automatically merges `feature/hero` into `develop`, and deletes `feature/hero`
4. `develop` is pushed to both `origin/develop` and `staging/develop`
5. you should now manually delete `origin/feature/hero` from the remote, as there is no longer any use for it
6. as the current codebase on the `develop` branch is a potential release, now is a good time to prepare to take the code live via a release branch
7. a new `release/x.x.x` is created from `develop`
8. the client reviews the feature and requests changes. **NOTE:** even if there are no changes from the client, a release branch is still a good idea for any final QA testing
9. the changes are completed on the `release/x.x.x` branch, and pushed to `staging/develop` until the client approves to take live. **NOTE:** you may prefer to merge the changes back into `develop`, and then to push to `staging/develop` but from my experience this is just an extra step. Do what feels right to you - in the end the result will be the same
10. when the client approves to take live, the `release/x.x.x` branch is "finished". This automatically merges `release/x.x.x` into `develop` and `master`
11. `develop` is again pushed to `origin/develop` and `staging/develop`
12. `master` is pushed to `origin/master` and `staging/master`
13. you should now manually delete `origin/release/x.x.x` from the remote, as there is no longer any use for it

If you are reluctant to delete branches from the remote server because you want to preserve a history of the work that was done, simply `tag` the release and push them. The tag can then be accessed within the repo and the history reviewed.

#### Hotfixes

Hotfixes are exactly what they sound like. Let's say a bug went unnoticed and was pushed to `master`, or there are already bugs in production that you are about to work on. Create a `hotfix` for these *quick* patches instead of creating a new `feature` branch or working directly on `master` or `develop` (which given this model you should not ever be doing).

An example workflow for a hotfix:

1. a button is red, when it should actually be green
2. create a new `hotfix/button` off of the `master` branch
3. fix the button
4. "finish" the `hotfix/button`. This automatically merges `hotfix/button` into `master`, back merges into `develop`, and deletes `hotfix/button`. **NOTE:** during the back merge, you may encounter a merge conflict. This will most commonly be due to a difference in compiled CSS. If this is the case, you will automatically be switched to the branch with the conflict for you to resolve the conflict(s). *Do not simply choose one file version over the other. Actually resolve the issue.* A simple recompile of the `.scss` will usually do this. If not, please take the time to properly merge the files into one
5. `master` is pushed to `origin/master` and `staging/master`
6. `develop` is pushed to `origin/develop` and `staging/develop`
7. delete `origin/hotfix/button` from the remote

Remember to `tag` the hotfixes just like you would a release in order to keep it easy to reference the code changes within the fix.

## Server Organization

TathataGolf.com is actually the name of the dedicated LiquidWeb server. This created a bit of an issue, so we have the main site hosted as tathatagolf.org, with the .com as an alias. Because of this, staging.tathata *MUST* be under the .org to not conflict with the .com of the host file. It's kind of a wonky set up that we are currently leaving alone due to the production nature of the site. I'm sure someday we can handle flipping this all around, but worry it's not worth the risk.

The Master and Develop repositories are indeed mapped to the correct system paths for each site. So a push to the develop repo will publish changes to the staging site, and master to production.

## Critical Plugins

These plugins are critical to the operation of the site, and should not disabled or deleted as they are not labeled as a `mu-plugin`:

- WooCommerce (Shopping tools with 20+ add on plugins)
- WooCommerce - Sensei (Digital course management)
- BBpress for user forums
- Theme My Login with custom theme files for logging in and login panel styles
- Testimonial Widget for Home, MS and 60day page testimonial sliders
- Revslider is on a few internal pages here and there, but it is being slowly removed per MattAdams wanting it gone for security reasons
- Advanced CustomFields pro
- WCK - Custom Fields and Custom Post Types Creator   (not sure exactly what post types use this currently)
- Affiliate WP - Manages the Affiliate tools for reporting and tracking sales.

### Updating Plugins

We need to try to test all WP core and plugin updates one by one on the staging server, With the help of TG staff. WooCommerce and Sense plugins are fragile the way the code leverages these currently. Often an update to a plugin means modifying and updating the theme.

### Adding and Deleting Plugins

When adding and deleting plugins, there is an extra step you must take. There is a custom plugin found in `mu-plugins`, the Tathata Ajax Plugin Disabler. This plugin works while WordPress is undergoing updates. This plugin contains a list of installed plugins, and it disables all of the plugins on that list, and then re-activates them one by one after WordPress core has updated. WordPress then continues its standard process of taking the site out of maintenance mode.

The list of plugins is **hardcoded**. This means that when you add or remove a new plugin that will play an important role on the site, we need to manually add that plugin to the list found in the `$unload` array within `ajax-plugins-disabler.php:28`.

## Website Theme

The theme is a fork of TwentyFourteen. The theme is continuously, and carefully being organized in a manner that aligns with a modern workflow. The base of the CSS is the latest version of [Bootstrap 3](http://getbootstrap.com/getting-started/), however custom CSS has been added to override many styles from the framework, it particular the grid. Be aware of this, as sometimes using default Bootstrap components will not work as expected. Do some digging, and you will find that custom CSS is overriding the default behavior. Work is currently in progress to rectify this.

### How the Menu System Works
Using a custom plugin (tathata user profiles) the system is looking to the user role for the right navigation tools. In `tathata-user-profiles.php:380` there are several variables referenced in the theme, these currently include: `$tathusermenu`, `$oavisibility`, `$learnervisibility`, `$cmsvisibility`, `$advisibility`, `$gradvisibility`. These visibility classes are used to show and hide the entire main menu system (`header.php:88`).

We also leverage the Groups plugin to identify if a user is part of a group or not. 7 day trial expired users for example are shown an expired page on all logged in pages. As seen on header.php line 107

## Working with Tathatagolfcertified.com
This website was forked to create Tathatagolfcertified.com. There has not been any direct link between the two sites, and any improvements to the main site needed to be manually copied and pasted into the Certified site. There is now a `certified-merger` branch that bridges this gap. Any changes made on this site that also need to be ported over to the Certified site should be pushed to this branch, which in turn pushes to the Certified repository. Then you just have to switch to the Certified repo and pull the `certified-merger` branch down from the origin.

**NOTE:** Not all files are exactly the same! In fact, there are a lot of files within the theme that have absolutely no purpose being on this site (work is in progress to remove these). *MOST* of the Sass/CSS, images, template parts, core template files, etc. are the same but take **special care** when editing WooCommerce and Sensei files. The logic varies slightly, so be sure to test what you are doing. Once completed, merge `certified-merger` into `master` or `develop` as needed.
